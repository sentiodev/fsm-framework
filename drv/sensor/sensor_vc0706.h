/**
 * @file sensor_vc0706.h
 *
 * Driver for the VC0706 Jpeg camera sensor
 *
 * This is a library for the Adafruit TTL JPEG Camera (VC0706 chipset)
 *
 * The MIUN version of the driver-libratry is based on the ADA fruit version thus we keep
 * the following note:
 * **************************************************************************************
 * This is a library for the Adafruit TTL JPEG Camera (VC0706 chipset)
 *
 * Pick one up today in the adafruit shop!
 * ------> http://www.adafruit.com/products/397
 *
 * These displays use Serial to communicate, 2 pins are required to interface
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.
 * BSD license, all text above must be included in any redistribution
 * **************************************************************************************
 */

#ifndef SENSOR_VC0706_H_
#define SENSOR_VC0706_H_


// Platform selection based on application_config.h
#if SENTIO_CAM_ENABLE
#include "sentio_cam_io.h"
#include "sentio_cam.h"
#include "mcu_efm32.h"

#define CAMERA_UART_RX     SENSOR_UART_RX
#define CAMERA_UART_TX     SENSOR_UART_TX
#define CAMERA_UART        SENSOR_UART
#define CAMERA_UART_CLOCK  SENSOR_UART_CLOCK
#define CAMERA_UART_LOC    SENSOR_UART_LOC
#define CAMERA_UART_RX_ISR SENSOR_UART_RX_ISR

#define CAMERA_PWR         SENSOR_PWR
#elif SENTIO_EM_ENABLE
#include "sentio_em_io.h"
#include "sentio_em.h"

#include "em_gpio.h"
#include "em_usart.h"
#include "em_cmu.h"

#define CAMERA_UART_RX     SENSOR_UART_RX
#define CAMERA_UART_TX     SENSOR_UART_TX
#define CAMERA_UART        SENSOR_UART
#define CAMERA_UART_CLOCK  SENSOR_UART_CLOCK
#define CAMERA_UART_LOC    SENSOR_UART_LOC
#define CAMERA_UART_RX_ISR SENSOR_UART_RX_ISR

#define CAMERA_PWR         SENSOR_PIN_1
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef SENSOR_VC0706_ENABLE
#error Missing define in application_config.h: SENSOR_VC0706_ENABLE
#endif

#define VC0706_RESET                0x26  ///< Strobe Command: System reset
#define VC0706_GEN_VERSION          0x11  ///< Strobe Command: 
#define VC0706_READ_FBUF            0x32  ///< Strobe Command: Read picture buffer
#define VC0706_GET_FBUF_LEN         0x34  ///< 
#define VC0706_FBUF_CTRL            0x36  ///< 
#define VC0706_DOWNSIZE_CTRL        0x54  ///< 
#define VC0706_DOWNSIZE_STATUS      0x55  ///< 
#define VC0706_READ_DATA            0x30  ///< Strobe Command: Read data form cameras memory
#define VC0706_WRITE_DATA           0x31  ///< Strobe Command: Write data to cameras memory 
#define VC0706_COMM_MOTION_CTRL     0x37  ///< 
#define VC0706_COMM_MOTION_STATUS   0x38  ///< 
#define VC0706_COMM_MOTION_DETECTED 0x39  ///< 
#define VC0706_MOTION_CTRL          0x42  ///< 
#define VC0706_MOTION_STATUS        0x43  ///< Strobe Command: Get Motion Control Status
#define VC0706_TVOUT_CTRL           0x44  ///< Strobe Command: Control Composite Output
#define VC0706_OSD_ADD_CHAR         0x45  ///< Strobe Command: Put Character on On-Screen Display

#define VC0706_STOPCURRENTFRAME     0x0   ///< Strobe Command: take current frame in readout buffer
#define VC0706_STOPNEXTFRAME        0x1   ///< Strobe Command: take next frame in readout buffer
#define VC0706_RESUMEFRAME          0x3   ///< 
#define VC0706_STEPFRAME            0x2   ///< Strobe Command: resume video

#define VC0706_640x480              0x00  ///< Set Frame size to 640x480
#define VC0706_320x240              0x11  ///< Set Frame size to 320x240 
#define VC0706_160x120              0x22  ///< Set Frame size to 160x120

#define VC0706_MOTIONCONTROL        0x0
#define VC0706_UARTMOTION           0x01
#define VC0706_ACTIVATEMOTION       0x01

#define VC0706_SET_ZOOM             0x52 /// < Set Zoom Factor
#define VC0706_GET_ZOOM             0x53 /// < Get Zoom Factor

#define CAMERABUFFSIZ               14000
#define CAMERADELAY                 10

/** Define a pointer-to-function that can be used as Interrupt handler */
typedef void ( *ptISR_Handler )( uint32_t );

/**
 * Driver class for the VC0706 camera module
 *
 * The class contains the interface to interact with the VC0706 camera module.
 */
class sensor_vc0706
{
public:
	sensor_vc0706();
	~sensor_vc0706() {}

	ptISR_Handler getIsrFunctionPointer();
	uint8_t getIsrNumber();

	uint8_t available();
	void setUserBuffer( uint8_t* userBuff );

	/**
	 * Initializes the interface to the micro-controller.
	 *
	 * The function initializes the I2C communication to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Enable the VC0706 camera.
	 *
	 * The function initializes UART pins to the camera module, which are disabled when the module is in shut-down
	 * and moreover enables the auxiliary LDO which powers the camera.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void enable();

	/**
	 * Disable the VC0706 camera.
	 *
	 * The function disables UART pins to the camera module
	 * and moreover disables the auxiliary LDO which powers the camera.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void disable();

	/**
	 * System Reset of the camera module.
	 *
	 * This function sends a strobe command reset
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	bool reset();

	/**
	 * Trigger take a image.
	 *
	 * This function sends a strobe command to the camera module which will keep the current image frame in the readout buffer
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	bool takePicture();

	/**
	 * Read the image from the camera
	 *
	 * This function will obtain the picture currently stored in the camera's buffer, by initiating a serial transmission
	 * via the UART interface.
	 *
	 * @param[in]  uint32_t length of the image frame that shall be read from the camera
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	uint8_t *readPicture( uint32_t n );

	/**
	 * Start pulling image frames in to the buffer periodically.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	bool resumeVideo();

	/**
	 * Get the length of the frame currently in the camera buffer.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint32_t length of the JPEG frame currently in the readout buffer
	 */
	uint32_t frameLength();

	/**
	 * Enable the composite output of the camera.
	 *
	 * This function sends a strobe command to the camera module to enable the analog output
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	bool TVon();

	/**
	 * Disable the composite output of the camera.
	 *
	 * This function sends a strobe command to the camera module to enable the analog output
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	bool TVoff();

	/**
	 * Read the ASCII Version-string of the camera module
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     char command successful/failed
	 */
	char *getVersion();

	/**
	 * Set the cameras zoom factor
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t command successful/failed
	 */
	bool setFrameSize( uint8_t size );

	/**
	 * Read the currently set Downsize value programmed in the camera module.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t command successful/failed
	 */
	uint8_t getDownsize();

	/**
	 * Read the currently set Downsize value programmed in the camera module.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t command successful/failed
	 */
	bool setDownsize( uint8_t );


	bool cameraFrameBuffCtrl( uint8_t command );

	/**
	 * Get the image compression rate
	 *
	 * This function reads the compression rate used reduce the frame size of the JPEG image.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t Image compression rate
	 */
	uint8_t getCompression();

	/**
	 * Configure the image compression rate
	 *
	 * This function sets the compression rate used reduce the frame size of the JPEG image.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool command successful/failed
	 */
	bool setCompression( uint8_t c );

private:
	uint8_t                  serialNum;
	static uint8_t*          camerabuff;
	static volatile uint32_t bufferLen;
	static uint32_t          frameptr;
	static volatile bool     interruptOccured;
	volatile uint32_t        counter;

	static void uartInterruptHandler( uint32_t temp );

	bool    runCommand( uint8_t cmd, uint8_t args[], uint8_t argn, uint8_t resp, bool flushflag = true );
	void    sendCommand( uint8_t cmd, uint8_t args[], uint8_t argn );
	uint8_t readResponse( uint32_t numbytes, uint8_t timeout );
	bool    verifyResponse( uint8_t command );
};
#endif // SENSOR_VC0706_H_

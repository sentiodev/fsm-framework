/**
 * @file sensor_ppd42nj.h
 *
 * Driver: sensor_ppd42nj
 *
 * Driver for the Shinyei PPD42NJ Particle Sensor
 */

#ifndef SENSOR_PPD42NJ
#define SENSOR_PPD42NJ

#include <stdint.h>
#include "em_gpio.h"
#include "em_cmu.h"


// Platform selection
#ifdef SENTIO_EM_ENABLE
#include "sentio_em_io.h"

#define LED_ENABLE_PIN  SENSOR_PIN_2
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef SENSOR_PPD42NJ_ENABLE
#error Missing define in application_config.h: SENSOR_PPD42NJ_ENABLE
#endif


/**
 * Driver class for the Shinyei PPD42NJ Particle Sensor
 *
 * The class contains the interface to interact with the Shinyei PPD42NJ Particle Sensor.
 */
class sensor_ppd42nj
{
public:
	sensor_ppd42nj();
	~sensor_ppd42nj() {}

	/**
	 * Gets the particle concentration.
	 *
	 * The function takes a single particle measurement.
	 *
	 * @param[in]  none
	 * @param[out] float: the particle concentration
	 * @return     uint16_t
	 */
	uint16_t getMeasurement( float &particles );

private:

};

#endif /* SENSOR_PPD42NJ */

/**
 * Driver: sensor_max4400x
 *
 * Driver for the LTC2990 ADC
 */

#include "sensor_max4400x.h"

sensor_max4400x::sensor_max4400x()
{
	i2cInit.master  = true;
	i2cInit.enable  = true;
	i2cInit.refFreq = MCU_CLOCK;
	i2cInit.freq    = _I2C_CTRL_CLHR_STANDARD;

	i2cTransfer.buf[0].data = i2c_txBuffer;
	i2cTransfer.buf[1].data = i2c_rxBuffer;
	i2cTransfer.addr        = 0x00;

	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	GPIO_PinModeSet( MAX4400X_I2C_SCL, gpioModeWiredAndPullUpFilter, 1 );
	GPIO_PinModeSet( MAX4400X_I2C_SDA, gpioModeWiredAndPullUpFilter, 1 );

	init();
}


void sensor_max4400x::init()
{
	CMU_ClockEnable( cmuClock_I2C0, true );

	MAX4400X_I2C->ROUTE = I2C_ROUTE_SDAPEN |
						  I2C_ROUTE_SCLPEN |
						  MAX4400X_I2C_LOC;

	I2C_Init( MAX4400X_I2C, &i2cInit );

	getInterruptStatus();
}


void sensor_max4400x::setConfig( const MAX4400X_CONFIG config )
{
	i2c_txBuffer[0]        = 0x03;
	i2c_txBuffer[1]        = config.main.regVal;
	i2c_txBuffer[2]        = config.ambient.regVal;

	i2cTransfer.buf[0].len = 0x03;
	i2cTransfer.buf[1].len = 0x00;
	i2cTransfer.addr       = config.chipAddress;

	i2cTransfer.flags      = I2C_FLAG_WRITE_WRITE;

	GPIO_PinModeSet( gpioPortA, 0, gpioModePushPull, 1 );

	I2C_TransferInit( MAX4400X_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( MAX4400X_I2C ) == i2cTransferInProgress );

	GPIO_PinModeSet( gpioPortA, 0, gpioModePushPull, 0 );

	i2c_txBuffer[0]         = msbUpperThrAddr;
	i2c_txBuffer[1]         = config.irqTreshold.regVal[1];
	i2c_txBuffer[2]         = config.irqTreshold.regVal[0];
	i2c_txBuffer[3]         = config.irqTreshold.regVal[3];
	i2c_txBuffer[4]         = config.irqTreshold.regVal[2];
	i2c_txBuffer[5]         = config.irqTreshold.regVal[4];

	i2cTransfer.buf[0].len  = 0x06;
	i2cTransfer.buf[1].len  = 0x00;
	i2cTransfer.addr        = config.chipAddress;

	i2cTransfer.flags       = I2C_FLAG_WRITE_WRITE;

	I2C_TransferInit( MAX4400X_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( MAX4400X_I2C ) == i2cTransferInProgress );


	i2c_txBuffer[0]         = trimGainClearAddr;
	i2c_txBuffer[1]         = config.userTrimValue.regVal[0];
	i2c_txBuffer[2]         = config.userTrimValue.regVal[1];
	i2c_txBuffer[3]         = config.userTrimValue.regVal[2];
	i2c_txBuffer[4]         = config.userTrimValue.regVal[3];
	i2c_txBuffer[5]         = config.userTrimValue.regVal[4];

	i2cTransfer.buf[0].len  = 0x06;
	i2cTransfer.buf[1].len  = 0x00;
	i2cTransfer.addr        = config.chipAddress;

	i2cTransfer.flags       = I2C_FLAG_WRITE_WRITE;

	I2C_TransferInit( MAX4400X_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( MAX4400X_I2C ) == i2cTransferInProgress );

	switch ( config.ambient.reg.integTime )
	{
	case t100ms:
		dataBitmask = 0x3FFF;
		readDataReg = 2;
		dataRegOffset = 0;
		break;
	case t25ms:
		dataBitmask = 0x0FFF;
		readDataReg = 2;
		dataRegOffset = 0;
		break;
	case t6p25ms:
		dataBitmask = 0x03FF;
		readDataReg = 2;
		dataRegOffset = 0;
		break;
	case t1p5625ms:
		dataBitmask = 0x00FF;
		readDataReg = 1;
		dataRegOffset = 1;
		break;
	case t400ms:
		dataBitmask = 0x3FFF;
		readDataReg = 2;
		dataRegOffset = 0;
		break;
	}
}


uint8_t sensor_max4400x::getInterruptStatus()
{
	i2c_txBuffer[0]         = irqStatusAddr;

	i2cTransfer.buf[0].len  = 0x01;
	i2cTransfer.buf[1].len  = 0x01;
	i2cTransfer.addr       = max44008low;

	i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

	I2C_TransferInit( MAX4400X_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( MAX4400X_I2C ) == i2cTransferInProgress );

	return i2c_rxBuffer[0];
}


void sensor_max4400x::getMeasurement( uint16_t &counts, MAX4400X_CHANNEL_SELECTION selectChannel )
{
	i2c_txBuffer[0]         = selectChannel + dataRegOffset;

	i2cTransfer.buf[0].len  = 0x01;
	i2cTransfer.buf[1].len  = readDataReg;

	i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

	I2C_TransferInit( MAX4400X_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( MAX4400X_I2C ) == i2cTransferInProgress );

	counts = *( ( uint16_t* ) i2c_rxBuffer ) & dataBitmask;
}

/**
 * @file sensor_sht15.h
 *
 * Driver: sensor_sht15
 *
 * Driver for the SHT15 temperature and humidity sensor
 */

#ifndef SENSOR_SHT15
#define SENSOR_SHT15

#include <stdint.h>
// Platform selection
#if SENTIO_EM_ENABLE
	#if SMARTLIGHT_ENABLE
		#include "smartlight_board_io.h"

		#define SHT_DATA_PIN SMARTLIGHT_SHT_DataPin
		#define SHT_SCK_PIN  SMARTLIGHT_SHT_SCK_Pin
	#elif DUSTMETER_ENABLE
		#include "sentio_em_io.h"
		#define SHT_DATA_PIN SENSOR_PIN_3
		#define SHT_SCK_PIN  SENSOR_PIN_1
	#else
		#include "sentio_em_io.h"
		#define SHT_DATA_PIN SENSOR_PIN_2
		#define SHT_SCK_PIN  SENSOR_PIN_3
	#endif
#else
	#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef SENSOR_SHT15_ENABLE
#error Missing define in application_config.h: SENSOR_SHT15_ENABLE
#endif


#define noACK 0
#define ACK   1
//adr  command  r/w
#define STATUS_REG_W 0x06   //000   0011    0
#define STATUS_REG_R 0x07   //000   0011    1
#define MEASURE_TEMP 0x03   //000   0001    1
#define MEASURE_HUMI 0x05   //000   0010    1
#define RESET        0x1e   //000   1111    0


/**
 * Driver class for the SHT15 temperature and humidity sensor
 *
 * The class contains the interface to interact with the SHT15 temperature and humidity sensor.
 */
class sensor_sht15
{
public:
	sensor_sht15();
	~sensor_sht15() {}

	/**
	 * Gets the temperature and humidity measurements.
	 *
	 * The function triggers an SHT15 measurement and gets the values for temperature (deg C) and relative humidity (%).
	 *
	 * @param[in]  none
	 * @param[out] humidity: the humidity value in percent
	 * @param[out] temperature: the temperature value in degree C
	 * @return     success (true/false)
	 */
	bool getMeasurement( float &humidity, float &temperature );

	/**
	 * Gets the dewpoint estimation.
	 *
	 * The function takes a temperature and humidity measurement and calculates the dewpoint.
	 *
	 * @param[in]  none
	 * @param[out] dewpoint: the dewpoint estimation
	 * @return     success (true/false)
	 */
	bool getDewpoint( float &dewpoint );

private:
	union INPUT_VAR
	{
		uint8_t in[2];
		uint16_t out;
	};

	enum {TEMP, HUMI};

	void delayDriver();
	uint8_t s_write_byte( uint8_t value );
	uint8_t s_read_byte( uint8_t ack );

	void s_transstart();
	void s_connectionreset();
	uint8_t s_softreset();

	uint8_t s_read_statusreg( uint8_t *p_value, uint8_t *p_checksum );
	uint8_t s_write_statusreg( uint8_t *p_value );

	uint8_t s_measure( uint8_t *p_value, uint8_t *p_checksum, uint8_t mode );
};

#endif /* SENSOR_SHT15 */

/**
 * @file sensor_gp2y1010.h
 *
 * Driver: sensor_gp2y1010
 *
 * Driver for the Sharp GP2Y1010AU0F Dust Sensor
 */

#ifndef SENSOR_GP2Y1010
#define SENSOR_GP2Y1010

#include "adc_ad7xxx.h"
#include "em_letimer.h"

// Platform selection
#ifdef SENTIO_EM_ENABLE
#include "sentio_em_io.h"

#define PULSE_TIMER          LETIMER0
#define PULSE_TIMER_LOCATION LETIMER_ROUTE_LOCATION_LOC1
#define LED_ENABLE_PIN       SENSOR_PIN_2
#define PULSE_TIMER_PIN0     SENSOR_PIN_11
#define PULSE_TIMER_PIN1     SENSOR_PIN_13
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef SENSOR_GP2Y1010_ENABLE
#error Missing define in application_config.h: SENSOR_GP2Y1010_ENABLE
#endif


// #define SAMPLETIME    2
// #define PULSEWIDTH    5
#define DUTYCYCLE     302

#define MAXIMAL_NUMBER_OF_SAMPLES 100

/**
 * Driver class for the Sharp GP2Y1010AU0F Dust Sensor
 *
 * The class contains the interface to interact with the Sharp GP2Y1010AU0F Dust Sensor.
 */
class sensor_gp2y1010
{
public:
	sensor_gp2y1010();
	~sensor_gp2y1010() {}

	void init();

	void getMeasurement( uint16_t* dataBuffer );

private:
	static adc_ad7xxx adc;
	static volatile uint16_t* bufferWrite;
	static volatile uint32_t  sampleCount;
	static volatile bool      newMeasurementReady;

	friend void LETIMER0_IRQHandler();
};

#endif /* SENSOR_GP2Y1010 */

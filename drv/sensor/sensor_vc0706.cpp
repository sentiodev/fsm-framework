/**
 * Driver: sensor_vc0706
 *
 * Driver for the VC0706 Jpeg camera sensor
 *
 * The MIUN version of the driver-libratry is based on the ADA fruit version thus we keep
 * the following note:
 * **************************************************************************************
 * This is a library for the Adafruit TTL JPEG Camera (VC0706 chipset)
 *
 * Pick one up today in the adafruit shop!
 * ------> http://www.adafruit.com/products/397
 *
 * These displays use Serial to communicate, 2 pins are required to interface
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.
 * BSD license, all text above must be included in any redistribution
 * **************************************************************************************
 */

#include "sensor_vc0706.h"

volatile uint32_t sensor_vc0706::bufferLen;
uint32_t          sensor_vc0706::frameptr;
uint8_t*          sensor_vc0706::camerabuff;
volatile bool     sensor_vc0706::interruptOccured;

sensor_vc0706::sensor_vc0706()
{
	init(); // Set everything to common state, then...
}


void sensor_vc0706::setUserBuffer( uint8_t* userbuff )
{
	camerabuff = userbuff;
}


uint8_t sensor_vc0706::getIsrNumber()
{
	return CAMERA_UART_RX_ISR;
}


void sensor_vc0706::init()
{
	// Setup the Parameters used to configure the selected UART/USART module
	USART_InitAsync_TypeDef  initUart =
	{
		usartEnable,      // Enable the UART after the Configuration is finalized
		MCU_CLOCK,        // The HF-Clock Frequency is used to calculate the Baudrate-Clock-Divider Setting
		38400,            // Resulting Baudrate at which the UART module runs
		usartOVS16,       // RX-Pin Over-sampling-Rate
		usartDatabits8,   // Number of Data-Bits
		usartNoParity,    // Parity-Bit setup
		usartStopbits1    // Number of Stop-Bits
	};

	// Enable the IO Pins of the MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	//Enable the required periphery clock
	CMU_ClockEnable( CAMERA_UART_CLOCK, true );

	CAMERA_UART->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;

	USART_InitAsync( CAMERA_UART, &initUart );

	// The Routing Register of the selected UART/USART Register is configured. The following register-access
	// enables the UART modules RX/TX shift-register and furthermore selects the one of the possible Locations of the modules IO-Pins
	//
	// NOTE!!!
	// Beside setting a modules Routing Register the functionality of the GPIO-Pins IO-Port-driver has to be configured separately

	CAMERA_UART->ROUTE = USART_ROUTE_RXPEN | USART_ROUTE_TXPEN | CAMERA_UART_LOC;

	// Configure the IO-Port driver of the EFM32-GPIO Pins
	GPIO_PinModeSet( CAMERA_UART_RX, gpioModeInputPullFilter, 1 );
	GPIO_PinModeSet( CAMERA_UART_TX, gpioModePushPull, 1 );

	GPIO_PinModeSet( CAMERA_PWR, gpioModePushPull, 0 );

	USART_IntEnable( CAMERA_UART, USART_IEN_RXDATAV );
}

void sensor_vc0706::enable()
{
	GPIO_PinModeSet( CAMERA_PWR, gpioModePushPull, 1 );

	GPIO_PinModeSet( CAMERA_UART_RX, gpioModeInputPullFilter, 1 );
	GPIO_PinModeSet( CAMERA_UART_TX, gpioModePushPull, 1 );
}

void sensor_vc0706::disable()
{
	GPIO_PinModeSet( CAMERA_PWR, gpioModePushPull, 0 );

	GPIO_PinModeSet( CAMERA_UART_RX, gpioModeDisabled, 0 );
	GPIO_PinModeSet( CAMERA_UART_TX, gpioModeDisabled, 0 );
}

bool sensor_vc0706::setFrameSize( uint8_t size )
{
	uint8_t args[] = {0x01, size };

	return runCommand( VC0706_SET_ZOOM, args, sizeof( args ), 5 );

}

bool sensor_vc0706::reset()
{
	uint8_t args[] = {0x0};

	return runCommand( VC0706_RESET, args, 1, 5 );
}

/****************** downsize image control */

uint8_t sensor_vc0706::getDownsize( void )
{
	uint8_t args[] = {0x0};
	if ( ! runCommand( VC0706_DOWNSIZE_STATUS, args, 1, 6 ) )
		return -1;

	return camerabuff[5];
}

bool sensor_vc0706::setDownsize( uint8_t newsize )
{
	uint8_t args[] = {0x01, newsize};

	return runCommand( VC0706_DOWNSIZE_CTRL, args, 2, 5 );
}

/***************** other high level commands */

char * sensor_vc0706::getVersion( void )
{
	uint8_t args[] = {0x01};

	sendCommand( VC0706_GEN_VERSION, args, 1 );
	// get reply
	if ( !readResponse( CAMERABUFFSIZ, 200 ) )
		return 0;
	camerabuff[bufferLen] = 0; // end it!
	return ( char * )camerabuff; // return it!
}

/****************** high level photo commands */

bool sensor_vc0706::setCompression( uint8_t compression )
{
	uint8_t args[] = {0x5, 0x1, 0x1, 0x12, 0x04, compression};
	return runCommand( VC0706_WRITE_DATA, args, sizeof( args ), 5 );
}

uint8_t sensor_vc0706::getCompression( void )
{
	uint8_t args[] = {0x4, 0x1, 0x1, 0x12, 0x04};
	runCommand( VC0706_READ_DATA, args, sizeof( args ), 6 );

	return camerabuff[5];
}

bool sensor_vc0706::takePicture()
{
	frameptr = 0;

	return cameraFrameBuffCtrl( VC0706_STOPCURRENTFRAME );
}

bool sensor_vc0706::resumeVideo()
{
	return cameraFrameBuffCtrl( VC0706_STEPFRAME );
}

bool sensor_vc0706::TVon()
{
	uint8_t args[] = {0x1, 0x1};
	return runCommand( VC0706_TVOUT_CTRL, args, sizeof( args ), 5 );
}
bool sensor_vc0706::TVoff()
{
	uint8_t args[] = {0x1, 0x0};
	return runCommand( VC0706_TVOUT_CTRL, args, sizeof( args ), 5 );
}

bool sensor_vc0706::cameraFrameBuffCtrl( uint8_t command )
{
	uint8_t args[] = {0x1, command};
	return runCommand( VC0706_FBUF_CTRL, args, sizeof( args ), 5 );
}

uint32_t sensor_vc0706::frameLength( void )
{
	uint8_t args[] = {0x01, 0x00};
	if ( !runCommand( VC0706_GET_FBUF_LEN, args, sizeof( args ), 9 ) )
		return 0;

	uint32_t len;
	len = camerabuff[5];
	len <<= 8;
	len |= camerabuff[6];
	len <<= 8;
	len |= camerabuff[7];
	len <<= 8;
	len |= camerabuff[8];

	return len;
}

uint8_t sensor_vc0706::available( void )
{
	return bufferLen;
}

uint8_t * sensor_vc0706::readPicture( uint32_t n )
{
	uint8_t args[13];
	args[0]  = 0x0C;
	args[1]  = 0x0;
	args[2]  = 0x0A;
	args[3]  = 0;
	args[4]  = 0;
	args[5]  = frameptr >> 8;
	args[6]  = frameptr & 0xFF;
	args[7]  = n >> 24;
	args[8]  = n >> 16;
	args[9]  = n >> 8;
	args[10] = n;
	args[11] = CAMERADELAY >> 8;
	args[12] = CAMERADELAY & 0xFF;

	if ( ! runCommand( VC0706_READ_FBUF, args, sizeof( args ), 5, false ) )
		return 0;

	//  read into the buffer PACKETLEN!
	if ( readResponse( n + 5, CAMERADELAY ) == 0 )
		return 0;

	frameptr += n;

	return camerabuff;
}

/**************** low level commands */

bool sensor_vc0706::runCommand( uint8_t cmd, uint8_t *args, uint8_t argn, uint8_t resplen, bool flushflag )
{
	// flush out anything in the buffer?
	if ( flushflag )
	{
		readResponse( 100, 10 );
	}

	sendCommand( cmd, args, argn );

	if ( readResponse( resplen, 200 ) != resplen )
		return false;
	if ( ! verifyResponse( cmd ) )
		return false;
	return true;
}

void sensor_vc0706::sendCommand( uint8_t cmd, uint8_t args[] = 0, uint8_t argn = 0 )
{
	USART_Tx( CAMERA_UART, 0x56 );
	USART_Tx( CAMERA_UART, serialNum );
	USART_Tx( CAMERA_UART, cmd );

	for ( uint8_t i = 0; i < argn; i++ )
	{
		USART_Tx( CAMERA_UART, args[i] );
	}
}

void sensor_vc0706::uartInterruptHandler( uint32_t temp )
{

	camerabuff[bufferLen] = ( uint8_t )temp;
	bufferLen++;
}

ptISR_Handler sensor_vc0706::getIsrFunctionPointer()
{
	return &uartInterruptHandler;
}

uint8_t sensor_vc0706::readResponse( uint32_t numbytes, uint8_t )
{
	bufferLen = 0;
	counter = 0;

	while ( ( bufferLen < numbytes ) && ( counter < 800 * numbytes ) )
	{
		counter++;
	}

	return bufferLen;
}

bool sensor_vc0706::verifyResponse( uint8_t command )
{
	if ( ( camerabuff[0] != 0x76 )     ||
			( camerabuff[1] != serialNum ) ||
			( camerabuff[2] != command )   ||
			( camerabuff[3] != 0x0 ) )
		return false;
	return true;
}

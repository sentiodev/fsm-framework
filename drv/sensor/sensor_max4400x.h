/**
 * @file sensor_max4400x.h
 *
 * Driver: sensor_max4400x
 *
 * Driver for the LightSensor MAXIM 44006/44008
 */

#ifndef SENSOR_MAX4400X_H_
#define SENSOR_MAX4400X_H_

#include<stdint.h>

#include "em_gpio.h"
#include "em_cmu.h"
#include "em_i2c.h"

// Platform selection
#ifdef SENTIO_EM_ENABLE
#include "sentio_em_io.h"
#define MAX4400X_I2C_SDA  POWER_I2C_SDA  ///< I2C DATA pin assignment
#define MAX4400X_I2C_SCL  POWER_I2C_SCL  ///< I2C CLK pin assignment
#define MAX4400X_I2C      POWER_I2C      ///< I2C module
#define MAX4400X_I2C_LOC  POWER_I2C_LOC  ///< I2C location
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef SENSOR_MAX4400X_ENABLE
#error Missing define in application_config.h: SENSOR_MAX4400X_ENABLE
#endif


enum MAX4400X_IRQ
{
	selectClearCh = 0x00,
	selectGreenCh = 0x01,
	selectIrCh    = 0x02,
	selectTempCh  = 0x03
};

enum MAX4400X_MODE
{
	useClear      = 0x00,
	useClearIr    = 0x01,
	useClearRgbIr = 0x02
};

enum MAX4400X_RESOLUTION
{
	short2long0p5   = 0x00,
	res8nW   = 0x01,
	res32nW  = 0x02,
	res512nW = 0x03
};


enum MAX4400X_INTEGRATION
{
	t100ms    = 0x00,
	t25ms     = 0x01,
	t6p25ms   = 0x02,
	t1p5625ms = 0x03,
	t400ms    = 0x04
};


enum MAX4400X_IRQ_PERSISTENCE
{
	holdForOneCycle     = 0x00,
	holdforFourCycle    = 0x01,
	holdForEightCycle   = 0x02,
	holdForsixteenCycle = 0x03
};


enum MAX4400X_REG_ADDR
{
	irqStatusAddr   = 0x00,
	mainConfigAddr  = 0x01,
	ambConfigAddr   = 0x02,

	msbDataClearAddr = 0x04,
	lsbDataClearAddr = 0x05,
	msbDataRedAddr   = 0x06,
	lsbDataRedAddr   = 0x07,
	msbDataGreenAddr = 0x08,
	lsbDataGreenAddr = 0x09,
	msbDataBlueAddr  = 0x0A,
	lsbDataBlueAddr  = 0x0B,
	msbDataIrAddr    = 0x0C,
	lsbDataIrAddr    = 0x0D,
	msbDataCompAddr  = 0x0E,
	lsbDataCompAddr  = 0x0F,
	msbDataTempAddr  = 0x12,
	lsbDataTempAddr  = 0x13,

	msbUpperThrAddr  = 0x14,
	lsbUpperThrAddr  = 0x15,
	msbLowerThrAddr  = 0x16,
	lsbLowerThrAddr  = 0x17,
	persistTimerAddr = 0x18,

	trimGainClearAddr = 0x1D,
	trimGainRedAddr   = 0x1E,
	trimGainGreenAddr = 0x1F,
	trimGainBlueAddr  = 0x20,
	trimGainIrAddr    = 0x21
};


enum MAX4400X_CHANNEL_SELECTION
{
	clearCh     = msbDataClearAddr,
	clearCh8bit = lsbDataClearAddr,
	redCh       = msbDataRedAddr,
	redCh8bit   = lsbDataRedAddr,
	greenCh     = msbDataGreenAddr,
	greenCh8bit = lsbDataGreenAddr,
	blueCh      = msbDataBlueAddr,
	blueCh8bit  = lsbDataBlueAddr,
	irCh        = msbDataIrAddr,
	irCh8bit    = lsbDataIrAddr,
	compCh      = msbDataCompAddr,
	compCh8bit  = lsbDataCompAddr,
	tempCh      = msbDataTempAddr,
	tempCh8bit  = lsbDataTempAddr
};

enum MAX4400X_CHIP_ADDR
{
	max44006high = 0x88,
	max44008high = 0x80,
	max44006low  = 0x8A,
	max44008low  = 0x82
};


struct MAX4400X_CONFIG
{
	union
	{
		struct
		{
			bool irqEnable                 : 1;
			bool freeA                     : 1;
			MAX4400X_IRQ irqChannel        : 2;
			MAX4400X_MODE operatingMode    : 2;
			uint8_t freeB                  : 2;
		} reg;
		uint8_t regVal;
	} main;

	union
	{
		struct
		{
			MAX4400X_RESOLUTION resolution : 2;
			MAX4400X_INTEGRATION integTime : 3;
			bool tempEnable                : 1;
			bool compEmable                : 1;
			bool trim                      : 1;
		} reg;
		uint8_t regVal;
	} ambient;

	union
	{
		struct
		{
			uint16_t upper;
			uint16_t lower;
			MAX4400X_IRQ_PERSISTENCE persistance;
		} reg;
		uint8_t regVal[5];
	} irqTreshold;

	union
	{
		struct
		{
			uint8_t gainClearCh : 6;
			uint8_t gainRedCh   : 6;
			uint8_t gainGreen   : 6;
			uint8_t gainBlue    : 6;
			uint8_t gainIRCh    : 6;
		} reg;
		uint8_t regVal[5];
	} userTrimValue;

	MAX4400X_CHIP_ADDR chipAddress;
};



/**
 * Driver class for the LightSensor MAXIM 44006/44008
 *
 * The class contains the interface to interact with the LightSensor MAXIM 44006/44008 IC.
 */
class sensor_max4400x
{
public:
	sensor_max4400x();
	~sensor_max4400x() {}
	void init();

	void setConfig( const MAX4400X_CONFIG config );
	uint8_t getInterruptStatus();
	void getMeasurement( uint16_t &counts, MAX4400X_CHANNEL_SELECTION selectChannel );

private:
	I2C_Init_TypeDef        i2cInit;
	I2C_TransferSeq_TypeDef i2cTransfer;

	uint8_t i2c_txBuffer[2];
	uint8_t i2c_rxBuffer[2];

	uint16_t dataBitmask;
	uint8_t readDataReg;
	uint8_t dataRegOffset;
};

#endif /* SENSOR_MAX4400X_H_ */

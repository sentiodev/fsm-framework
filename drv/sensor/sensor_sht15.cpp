/**
 * Driver: sensor_sht15
 *
 * Driver for the SHT15 temperature and humidity sensor
 */

#include "sensor_sht15.h"

#include <math.h>
#include "em_cmu.h"
#include "em_gpio.h"

sensor_sht15::sensor_sht15()
{
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	GPIO_PinModeSet( SHT_DATA_PIN, gpioModeWiredAndPullUpFilter, 0 );
	GPIO_PinModeSet( SHT_SCK_PIN, gpioModePushPull, 0 );

	// Read the SHT once to enable the low-power sleep mode afterwards.
	float h, t;
	getMeasurement( h, t );
}


void sensor_sht15::delayDriver()
{
	volatile uint8_t test = 0;
	test++;
	test++;
	test++;
	test++;
	test++;
	test++;
	test++;
}


uint8_t sensor_sht15::s_write_byte( uint8_t value )
{
	uint8_t i, error = 0;

	for ( i = 0x80; i > 0; i >>= 1 )                //shift bit for masking
	{
		if ( i & value )
			GPIO_PinOutSet( SHT_DATA_PIN );  //masking value with i , write to SENSI-BUS
		else
			GPIO_PinOutClear( SHT_DATA_PIN );

		delayDriver();                  //observe setup time
		GPIO_PinOutSet( SHT_SCK_PIN );  //clk for SENSI-BUS
		delayDriver();
		delayDriver();                  //pulse-width approx. 5 us
		GPIO_PinOutClear( SHT_SCK_PIN );
		delayDriver();                  //observe hold time
	}

	GPIO_PinOutSet( SHT_DATA_PIN );          //release DATA-line

	delayDriver();                          //observe setup time
	GPIO_PinOutSet( SHT_SCK_PIN );          //clk #9 for ack
	error = GPIO_PinInGet( SHT_DATA_PIN );   //check ack (DATA will be pulled down by SHT11)
	GPIO_PinOutClear( SHT_SCK_PIN );

	return error;                           //error=1 in case of no acknowledge
}


uint8_t sensor_sht15::s_read_byte( uint8_t ack )
{
	uint8_t i, val = 0;

	GPIO_PinOutSet( SHT_DATA_PIN );              //release DATA-line

	for ( i = 0x80; i > 0; i /= 2 )             //shift bit for masking
	{
		GPIO_PinOutSet( SHT_SCK_PIN );      //clk for SENSI-BUS
		delayDriver();
		if ( GPIO_PinInGet( SHT_DATA_PIN ) ) //read bit
			val = ( val | i );

		GPIO_PinOutClear( SHT_SCK_PIN );
		delayDriver();
	}

	if ( ack )                                  //in case of "ack==1" pull down DATA-Line
		GPIO_PinOutClear( SHT_DATA_PIN );

	delayDriver();                   //observe setup time
	GPIO_PinOutSet( SHT_SCK_PIN );   //clk #9 for ack
	delayDriver();
	delayDriver();                   //pulse-width approx. 5 us
	GPIO_PinOutClear( SHT_SCK_PIN );
	delayDriver();                   //observe hold time
	GPIO_PinOutSet( SHT_DATA_PIN );   //release DATA-line

	return val;
}


void sensor_sht15::s_transstart()
{
	// Initial state
	GPIO_PinOutSet( SHT_DATA_PIN );
	GPIO_PinOutClear( SHT_SCK_PIN );
	delayDriver();

	// Generate the transmission start pattern
	GPIO_PinOutSet( SHT_SCK_PIN );
	delayDriver();
	GPIO_PinOutClear( SHT_DATA_PIN );
	delayDriver();
	GPIO_PinOutClear( SHT_SCK_PIN );
	delayDriver();
	delayDriver();
	delayDriver();
	delayDriver();
	GPIO_PinOutSet( SHT_SCK_PIN );
	delayDriver();
	GPIO_PinOutSet( SHT_DATA_PIN );
	delayDriver();
	GPIO_PinOutClear( SHT_SCK_PIN );
}


void sensor_sht15::s_connectionreset()
{
	//Initial state
	GPIO_PinOutSet( SHT_DATA_PIN );

	GPIO_PinOutClear( SHT_SCK_PIN );

	for ( volatile uint8_t i = 0; i < 18; i++ )
	{
		GPIO_PinOutToggle( SHT_SCK_PIN );
		delayDriver();
	}

	delayDriver();

	s_transstart();
}


uint8_t sensor_sht15::s_softreset()
{
	uint8_t error = 0;

	s_connectionreset();              //reset communication

	error += s_write_byte( RESET );   //send RESET-command to sensor

	return error;                     //error=1 in case of no response form the sensor
}


uint8_t sensor_sht15::s_read_statusreg( uint8_t *p_value, uint8_t *p_checksum )
{
	uint8_t error = 0;

	s_transstart();                       //transmission start
	error = s_write_byte( STATUS_REG_R ); //send command to sensor

	*p_value = s_read_byte( ACK );        //read status register (8-bit)
	*p_checksum = s_read_byte( noACK );   //read checksum (8-bit)

	return error;                         //error=1 in case of no response form the sensor
}


uint8_t sensor_sht15::s_write_statusreg( uint8_t *p_value )
{
	uint8_t error = 0;

	s_transstart();                       //transmission start

	error += s_write_byte( STATUS_REG_W );//send command to sensor

	error += s_write_byte( *p_value );    //send value of status register

	return error;                         //error>=1 in case of no response form the sensor
}


uint8_t sensor_sht15::s_measure( uint8_t *p_value, uint8_t *p_checksum, uint8_t mode )
{
	uint8_t error = 0;
	volatile uint32_t i;

	s_transstart(); //transmission start

	switch ( mode )
	{
		//send command to sensor
	case TEMP:
		error += s_write_byte( MEASURE_TEMP );
		for ( i = 0; i < 800000; i++ )            //wait until sensor has finished the measurement
		{
			// or timeout is reached
			if ( !( GPIO_PinInGet( SHT_DATA_PIN ) ) )
				break;
		}
		break;

	case HUMI:
		error += s_write_byte( MEASURE_HUMI );
		for ( i = 0; i < 200000; i++ )            //wait until sensor has finished the measurement
		{
			// or timeout is reached
			if ( !( GPIO_PinInGet( SHT_DATA_PIN ) ) )
				break;
		}
		break;

	default:
		break;
	}

	if ( GPIO_PinInGet( SHT_DATA_PIN ) )
		error += 1;

	*( p_value + 1 ) = s_read_byte( ACK ); //read the first byte (MSB)
	*( p_value )    = s_read_byte( ACK ); //read the second byte (LSB)
	*p_checksum   = s_read_byte( noACK ); //read checksum

	return error;
}


bool sensor_sht15::getMeasurement( float &humidity, float &temperature )
{
	INPUT_VAR humid, temp;
	float bufferHumid, bufferTemp;
	uint8_t checksum;

	s_connectionreset();

	if ( s_measure( humid.in, &checksum, HUMI ) ) //measure humidity
	{
		s_connectionreset();        //in case of an error: connection reset
		return false;
	}

	if ( s_measure( temp.in, &checksum, TEMP ) ) //measure temperature
	{
		s_connectionreset();        //in case of an error: connection reset
		return false;
	}

	bufferHumid = ( float ) humid.out;
	bufferTemp  = ( float ) temp.out;


	const float C1 = -2.0468;           // for 12 Bit RH
	const float C2 = +0.0367;           // for 12 Bit RH
	const float C3 = -0.0000015955;     // for 12 Bit RH
	const float T1 = +0.01;             // for 12 Bit RH
	const float T2 = +0.00008;          // for 12 Bit RH

	float rh_lin;                       // rh_lin:  Humidity linear
	float rh_true;                      // rh_true: Temperature compensated humidity
	float t_C;                          // t_C   :  Temperature [°C]

	t_C = bufferTemp * 0.01 - 39.6;              //calc. temperature[°C]from 14 bit temp.ticks
	rh_lin = C3 * bufferHumid * bufferHumid + C2 * bufferHumid + C1; //calc. humidity from ticks to [%RH]
	rh_true = ( t_C - 25 ) * ( T1 + T2 * bufferHumid ) + rh_lin; //calc. temperature compensated humidity [%RH]
	if ( rh_true > 100 )rh_true = 100;  //cut if the value is outside of
	if ( rh_true < 0.1 )rh_true = 0.1;  //the physical possible range

	temperature  = t_C; //return temperature [°C]
	humidity = rh_true; //return humidity[%RH]

	return true;
}


bool sensor_sht15::getDewpoint( float &dewpoint )
{
	float h, t;
	if ( !getMeasurement( h, t ) )
		return false;

	float k = ( log10( h ) - 2 ) / 0.4343 + ( 17.62 * t ) / ( 243.12 + t );
	dewpoint = 243.12 * k / ( 17.62 - k );

	return true;
}

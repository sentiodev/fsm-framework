/**
 * Driver: sensor_gp2y1010
 *
 * Driver for the Sharp GP2Y1010AU0F Dust Sensor
 */

#include "sensor_ppd42nj.h"

sensor_ppd42nj::sensor_ppd42nj()
{
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	GPIO_PinModeSet( LED_ENABLE_PIN, gpioModePushPull, 0 );

}

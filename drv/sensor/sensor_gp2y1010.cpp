/**
 * Driver: sensor_gp2y1010
 *
 * Driver for the Sharp GP2Y1010AU0F Dust Sensor
 */

#include "sensor_gp2y1010.h"
#include <tgmath.h>

adc_ad7xxx sensor_gp2y1010::adc;
volatile uint16_t* sensor_gp2y1010::bufferWrite;
volatile bool      sensor_gp2y1010::newMeasurementReady = false;
volatile uint32_t  sensor_gp2y1010::sampleCount = 0;

sensor_gp2y1010::sensor_gp2y1010()
{
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );
	CMU_ClockEnable( cmuClock_LETIMER0, true );

	GPIO_PinModeSet( LED_ENABLE_PIN, gpioModePushPull, 0 );
	GPIO_PinModeSet( PULSE_TIMER_PIN0, gpioModePushPull, 0 );
	GPIO_PinModeSet( PULSE_TIMER_PIN1, gpioModePushPull, 0 );

	init();
}


void sensor_gp2y1010::init()
{
	AD7XXX_CONFIG config;
	config.refVoltage = 4.096;
	config.daisyChain = 1;
	config.precision  = prec16bit;

	adc.setConfig( config );

	LETIMER_Init_TypeDef initLeTimer;
	initLeTimer.enable         = false;
	initLeTimer.debugRun       = true;
	initLeTimer.rtcComp0Enable = false;
	initLeTimer.rtcComp1Enable = false;
	initLeTimer.comp0Top       = true;
	initLeTimer.bufTop         = true;
	initLeTimer.out0Pol        = 0;
	initLeTimer.out1Pol        = 0;
	initLeTimer.ufoa0          = letimerUFOANone;
	initLeTimer.ufoa1          = letimerUFOANone;
	initLeTimer.repMode        = letimerRepeatFree;

	LETIMER_Init( PULSE_TIMER, &initLeTimer );

	LETIMER_CompareSet( PULSE_TIMER, 0, DUTYCYCLE );

	LETIMER0->IFC = _LETIMER_IFC_MASK;
	LETIMER_IntEnable( PULSE_TIMER, 0x02 );
	NVIC_EnableIRQ( LETIMER0_IRQn );

}


void sensor_gp2y1010::getMeasurement( uint16_t* dataBuffer )
{
	bufferWrite         = dataBuffer;
	newMeasurementReady = false;

	LETIMER_Enable( LETIMER0, true );

	while ( !newMeasurementReady );

	LETIMER_Enable( LETIMER0, false );
}


void LETIMER0_IRQHandler()  __attribute__( ( used, externally_visible ) );

void LETIMER0_IRQHandler()
{
	uint16_t sensorSampling = 0;
	sensor_gp2y1010::bufferWrite[sensor_gp2y1010::sampleCount] = 0;

	GPIO_PinOutSet( PULSE_TIMER_PIN0 );

	for ( volatile uint32_t i = 0; i < 400; i++ );

	for ( volatile uint32_t i = 0; i < 100; i++ )
	{
		sensor_gp2y1010::adc.getValue( sensorSampling );

		if ( i == 16 )
			GPIO_PinOutClear( PULSE_TIMER_PIN0 );

		if ( sensorSampling > sensor_gp2y1010::bufferWrite[sensor_gp2y1010::sampleCount] )
			sensor_gp2y1010::bufferWrite[sensor_gp2y1010::sampleCount] =  sensorSampling;

		for ( volatile uint32_t j = 0; j < 15; j++ );
	}

	if ( sensor_gp2y1010::sampleCount < MAXIMAL_NUMBER_OF_SAMPLES )
	{
		sensor_gp2y1010::sampleCount++;
	}
	else
	{
		sensor_gp2y1010::sampleCount = 0;
		
		sensor_gp2y1010::newMeasurementReady = true;
	}

	LETIMER0->IFC = _LETIMER_IFC_MASK;
}
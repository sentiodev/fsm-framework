/*
 * time.h
 */

#ifndef TIME_H_
#define TIME_H_

#include <stdint.h>

class time
{
public:
	time( uint8_t hour, uint8_t minute, uint8_t second, uint16_t ms = 0 );
	time( uint32_t seconds, uint16_t ms = 0 );
	time() {}
	~time() {}

	inline uint32_t getInSeconds() const {
		return varSecFull;
	}
	inline uint16_t getMilliSec()  const {
		return varMs;
	}
	inline uint8_t  getSecond()    const {
		return varSecond;
	}
	inline uint8_t  getMinute()    const {
		return varMinute;
	}
	inline uint8_t  getHour()      const {
		return varHour;
	}

	inline void setSecond( uint8_t input )   {
		varSecond  = input;
		varSecFull = ( ( ( varHour * 60 ) + varMinute ) * 60 ) + varSecond;
	}
	inline void setMinute( uint8_t input )   {
		varMinute  = input;
		varSecFull = ( ( ( varHour * 60 ) + varMinute ) * 60 ) + varSecond;
	}
	inline void setHour( uint8_t input )     {
		varHour    = input;
		varSecFull = ( ( ( varHour * 60 ) + varMinute ) * 60 ) + varSecond;
	}

	time    operator+ ( const time &summand );
	time&   operator+= ( const time &summand );

	time     operator- ( const time &subtrahend );
	uint32_t operator/ ( const time &divisor );
	time&    operator/ ( const uint32_t divisor );
	time     operator* ( uint32_t multiplicant );
	time&    operator= ( const time &source );

	bool     operator> ( const time input );
	bool     operator< ( const time input );
	bool     operator<=( const time input );
	bool     operator>=( const time input );
	bool     operator==( const time input );
	bool     operator!=( const time input );

protected:
	void convertTime( const uint32_t seconds, const uint16_t ms );

	uint16_t varMs;
	uint8_t  varSecond;
	uint8_t  varMinute;
	uint8_t  varHour;

	uint32_t varSecFull;
};
#endif /* TIME_H_ */

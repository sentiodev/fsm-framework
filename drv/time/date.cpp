/*
 * date.cpp
 */

#include "date.h"

uint16_t date::sumDays[13]       = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
uint16_t date::sumDaysL[13]       = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366};

void date::convertDate( uint16_t year, MONTH month, uint8_t dayOfMonth, uint8_t hour, uint8_t minute, uint8_t second, uint16_t ms )
{

	bool leap = checkLeapYear( year );
	uint16_t* getSumDay = ( leap ? sumDaysL : sumDays );
	uint32_t tempLengthOfYearInSec = ( leap ? 366 : 365 );

	varSecFull = ( ( getSumDay[( uint8_t ) month - 1 ] + dayOfMonth ) * 86400 ) + ( ( ( ( hour * 60 ) + minute ) * 60 ) + second + ( ms / 1000 ) );

	uint32_t tempDays = ( varSecFull / 86400 );

	uint32_t yearOverflow = 0;

	if ( tempDays / ( leap ? 367 : 366 ) )
	{
		varYear  = year + 1;
		tempDays -= ( leap ? 367 : 366 );

		yearOverflow++;
	}
	else
	{
		varYear = year;
	}

	uint32_t probeMonth = 0;
	uint32_t i = 0;
	do
	{
		probeMonth++;
		i++;
	} while ( tempDays > getSumDay[ i ] );

	varMonth      = ( MONTH ) probeMonth;
	varDayOfMonth = tempDays - getSumDay[ probeMonth - 1 ] + yearOverflow;

	convertTime( varSecFull % 86400, ms );
}


date::date( uint16_t year, MONTH month, uint8_t dayOfMonth, uint8_t hour, uint8_t minute, uint8_t second, uint16_t ms )
{
	convertDate( year, month, dayOfMonth, hour, minute, second, ms );
}


date::date( char* input, char* format, uint8_t lengthOfString )
{
	convertDateString( input, format, lengthOfString );
}


date date::operator+ ( const time &summand )
{
	date ret = *this;

	return ret += summand;
}


date& date::operator+= ( const time &summand )
{
	convertDate( varYear, varMonth, varDayOfMonth, varHour + summand.getHour(), varMinute + summand.getMinute(), varSecond + summand.getSecond(), varMs + summand.getMilliSec() );

	return *this;
}


date date::operator- ( const time &subtrahend )
{

//  convertDate(varYear,varMonth,varDayOfMonth,varHour+summand.getHour(),varMinute+summand.getMinute(),varSecond+summand.getSecond(), varMs+ summand.getMilliSec());

	return *this;
}

date& date::operator= ( const date &source )
{
	varSecond     = source.varSecond;
	varMs         = source.varMs;
	varMinute     = source.varMinute;
	varHour       = source.varHour;
	varDayOfMonth = source.varDayOfMonth;
	varMonth      = source.varMonth;
	varYear       = source.varYear;

	varSecFull    = source.varSecFull;

	return *this;
}

bool date::operator< ( const date input )
{
	if ( ( varYear < input.varYear ) || ( ( varYear == input.varYear ) && ( varSecFull < input.varSecFull ) ) || ( ( varYear == input.varYear ) && ( varSecFull == input.varSecFull ) && ( varMs < input.varMs ) ) )
		return true;
	else
		return false;
}

bool date::operator> ( const date input )
{
	if ( ( varYear > input.varYear ) || ( ( varYear == input.varYear ) && ( varSecFull > input.varSecFull ) ) || ( ( varYear == input.varYear ) && ( varSecFull == input.varSecFull ) && ( varMs > input.varMs ) ) )
		return true;
	else
		return false;
}

bool date::operator<= ( const date input )
{
	if ( ( varYear < input.varYear ) || ( ( varYear == input.varYear ) && ( varSecFull < input.varSecFull ) ) || ( ( varYear == input.varYear ) && ( varSecFull == input.varSecFull ) && ( varMs <= input.varMs ) ) )
		return true;
	else
		return false;
}

bool date::operator>= ( const date input )
{
	if ( ( varYear > input.varYear ) || ( ( varYear == input.varYear ) && ( varSecFull > input.varSecFull ) ) || ( ( varYear == input.varYear ) && ( varSecFull == input.varSecFull ) && ( varMs >= input.varMs ) ) )
		return true;
	else
		return false;
}

bool date::operator== ( const date input )
{
	if ( ( varYear == input.varYear ) && ( varSecFull == input.varSecFull ) && ( varMs == input.varMs ) )
		return true;
	else
		return false;
}

bool date::operator!= ( const date input )
{
	if ( ( varYear != input.varYear ) || ( varSecFull != input.varSecFull ) || ( varMs != input.varMs ) )
		return true;
	else
		return false;
}


void date::convertNum( const char input , uint16_t& digit, uint16_t& digitCount, uint16_t& output )
{
	if ( ( input >= '0' ) && ( input <= '9' ) )
	{
		output += ( input - 0x30 ) * digit;
		digit *= 10;
		digitCount++;
	}
}

uint8_t date::convertDay( const char* dayString )
{
	switch ( dayString[2] )
	{
	case 'm':
		if ( dayString[1] == 'o' && dayString[0] == 'n' )
			return mon;
		break;
	case 't':
		if ( dayString[1] == 'u' && dayString[0] == 'e' )
			return tue;
		else if ( dayString[1] == 'h' && dayString[0] == 'u' )
			return thu;
		break;
	case 'w':
		if ( dayString[1] == 'e' && dayString[0] == 'n' )
			return wen;
	case 'f':
		if ( dayString[1] == 'r' && dayString[0] == 'i' )
			return fri;
		break;
	case 's':
		if ( dayString[1] == 'a' && dayString[0] == 't' )
			return sat;
		else if ( dayString[1] == 'u' && dayString[0] == 'n' )
			return sun;
	default:
		return 0;
	}

	return 0;
}

uint8_t date::convertMonth( const char* monthString )
{
	switch ( monthString[2] )
	{
	case 'j':
		if ( monthString[1] == 'a' && monthString[0] == 'n' )
			return january;
		else if ( monthString[1] == 'u' && monthString[0] == 'n' )
			return june;
		else if ( monthString[1] == 'u' && monthString[0] == 'l' )
			return july;
		break;
	case 'f':
		if ( monthString[1] == 'e' && monthString[0] == 'b' )
			return february;
		break;
	case 'm':
		if ( monthString[1] == 'a' && monthString[0] == 'r' )
			return march;
		else if ( monthString[1] == 'a' && monthString[0] == 'y' )
			return may;
		break;
	case 'a':
		if ( monthString[1] == 'p' && monthString[0] == 'r' )
			return april;
		else if ( monthString[1] == 'u' && monthString[0] == 'g' )
			return august;
		break;
	case 's':
		if ( monthString[1] == 'e' && monthString[0] == 'e' )
			return september;
		break;
	case 'o':
		if ( monthString[1] == 'c' && monthString[0] == 't' )
			return october;
		break;
	case 'n':
		if ( monthString[1] == 'o' && monthString[0] == 'v' )
			return november;
		break;
	case 'd':
		if ( monthString[1] == 'e' && monthString[0] == 'z' )
			return december;
		break;
	default:
		return 0;
	}

	return 0;
}

void date::convertDateString( char* input, char* format, uint8_t lengthOfString )
{
	uint16_t year =  0, month =  0, dayOfMonth =  0, day =  0, hour =  0, minute =  0, second =  0,  milliSec  = 0;
	uint16_t yearDigitCount = 0, monthDigitCount =  0, dayOfMonthDigitCount =  0, dayDigitCount =  0, hourDigitCount =  0, minuteDigitCount =  0, secondDigitCount =  0, milliSecDigitCount = 0;
	uint16_t yearDigit = 1, monthDigit =  1, dayOfMonthDigit =  1, dayDigit =  1, hourDigit =  1, minuteDigit =  1, secondDigit =  1, milliSecDigit = 1;

	char dayString[3];
	char monthString[3];

	for ( uint8_t i = lengthOfString; i < 254; i-- )
	{
		if ( input[i] >= 'A' && input[i] <= 'Z' )
			input[i] -= 0x20;

		switch ( format[i] )
		{
		case 'Y':
			convertNum( input[i], yearDigit, yearDigitCount, year );
			break;
		case 'M':
			if ( ( input[i] >= 'a' ) && ( input[i] <= 'z' ) )
			{
				monthString[monthDigitCount] = input[i];
				monthDigitCount++;
			}
			else
				convertNum( input[i], monthDigit, monthDigitCount, month );
			break;
		case 'D':
			convertNum( input[i], dayOfMonthDigit, dayOfMonthDigitCount, dayOfMonth );
			break;
		case 'd':
			if ( ( input[i] >= 'a' ) && ( input[i] <= 'z' ) )
			{
				dayString[dayDigitCount] = input[i];
				dayDigitCount++;
			}
			else
				convertNum( input[i], dayDigit, dayDigitCount, dayOfMonth );
			break;
		case 'h':
			convertNum( input[i], hourDigit, hourDigitCount, hour );
			break;
		case 'm':
			convertNum( input[i], minuteDigit, minuteDigitCount, minute );
			break;
		case 's':
			convertNum( input[i], secondDigit, secondDigitCount, second );
			break;
		case 'l':
			convertNum( input[i], milliSecDigit, milliSecDigitCount, milliSec );
			break;
		default:
			;
		}
	}

	varYear = year;
	varMonth = ( MONTH )month;
	varDayOfMonth = dayOfMonth;

	varHour   = hour;
	varMinute = minute;
	varSecond = second;

	varMs = 0;

	bool leap = checkLeapYear( year );
	uint16_t* getSumDay = ( leap ? sumDaysL : sumDays );
	varSecFull = ( ( getSumDay[( uint8_t ) month - 1 ] + dayOfMonth ) * 86400 ) + ( ( ( ( hour * 60 ) + minute ) * 60 ) + second );


	if ( dayDigitCount == 3 )
		day = convertDay( dayString );


	if ( monthDigitCount == 3 )
		month = convertMonth( monthString );

	if ( year < 100 )
		varYear += 2000;
}


bool date::checkLeapYear( uint16_t year )
{
	if ( !( year % 4 ) )
		return true;
	else if ( !( year % 100 ) )
		return false;
	else if ( !( year % 400 ) )
		return true;
	else
		return false;
}
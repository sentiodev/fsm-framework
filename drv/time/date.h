/*
 * date.h
 */

#ifndef DATE_H_
#define DATE_H_

#include "time.h"

enum MONTH
{
	january = 1,
	february = 2,
	march = 3,
	april = 4,
	may = 5,
	june = 6,
	july = 7,
	august = 8,
	september = 9,
	october = 10,
	november = 11,
	december = 12
};


enum WEEKDAY
{
	xx  = 0,
	mon = 1,
	tue = 2,
	wen = 3,
	thu = 4,
	fri = 5,
	sat = 6,
	sun = 7
};




class date : public time
{
	static uint16_t sumDays[13];
	static uint16_t sumDaysL[13];

	void    convertNum( const char input , uint16_t& digit, uint16_t& digitCount, uint16_t& output );
	uint8_t convertDay( const char* dayString );
	uint8_t convertMonth( const char* monthString );
	void    convertDateString( char* input, char* format, uint8_t lengthOfString );

	uint8_t varDayOfMonth;
	MONTH   varMonth;
	uint16_t varYear;

public:
	date() {}
	date( uint16_t year, MONTH month, uint8_t dayOfMonth, uint8_t hour, uint8_t minute, uint8_t second, uint16_t ms = 0 );
	date( char* input, char* format, uint8_t lengthOfString );
	~date() {}

	inline uint8_t getDate()  const {
		return varDayOfMonth;
	}
	inline MONTH   getMonth() const {
		return varMonth;
	}
	inline uint16_t getYear()  const {
		return varYear;
	}

	inline void setDate( uint8_t input )  {
		varDayOfMonth = input;
	}
	inline void setMonth( MONTH input )   {
		varMonth = input;
	}
	inline void setYear( uint16_t input ) {
		varYear = input;
	}

	date&  operator+= ( const time &summand );
	date   operator+ ( const time &summand );
	date   operator- ( const time &subtrahend );
	date&  operator= ( const date &source );

	bool   operator> ( const date input );
	bool   operator< ( const date input );
	bool   operator<= ( const date input );
	bool   operator>= ( const date input );
	bool   operator== ( const date input );
	bool   operator!= ( const date input );
private:
	bool checkLeapYear( uint16_t year );
	void convertDate( uint16_t year, MONTH month, uint8_t dayOfMonth, uint8_t hour, uint8_t minute, uint8_t second, uint16_t ms );
};
#endif /* DATE_H_ */

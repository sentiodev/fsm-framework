/*
 * time.cpp
 */

#include "time.h"
void time::convertTime( const uint32_t seconds, const uint16_t ms )
{
	uint32_t temp = seconds + ( ms / 1000 );

	temp      %= 86400;
	varSecFull = temp;
	varHour   = temp / 3600;
	temp      = temp % 3600;
	varMinute = temp / 60;
	varSecond = temp % 60;
	varMs     = ms % 1000;
}


time::time( const uint8_t hour, const uint8_t minute, const uint8_t second, const uint16_t ms )
{
	convertTime( ( ( ( ( hour * 60 ) + minute ) * 60 ) + second ), ms );
}


time::time( const uint32_t seconds, const uint16_t ms )
{
	convertTime( seconds, ms );
}


time time::operator+ ( const time &summand )
{
	time ret( varSecFull + summand.varSecFull, varMs + summand.varMs );

	return ret;
}


time& time::operator+= ( const time &summand )
{
	varSecFull += summand.varSecFull;
	varMs      += summand.varMs;

	convertTime( varSecFull, varMs );

	return *this;
}


time time::operator- ( const time &subtrahend )
{
	if ( varSecFull >= subtrahend.varSecFull )
	{
		if ( varMs >= subtrahend.varMs )
		{
			time ret( varSecFull - subtrahend.varSecFull, varMs - subtrahend.varMs );
			return ret;
		}
		else
		{
			if ( ( varSecFull >= 1 ) && ( varMs < subtrahend.varMs ) )
			{
				time ret( varSecFull - subtrahend.varSecFull - 1, ( varMs + 1000 ) - subtrahend.varMs );
				return ret;
			}
			else
			{
				time ret( 0, 0 );
				return ret;
			}
		}
	}
	else
	{
		time ret( 0, 0 );
		return ret;
	}
}


uint32_t time::operator/ ( const time &divisor )
{
	if ( divisor.varSecFull > varSecFull )
	{
		return 0;
	}
	else
	{
		return ( ( float ) varSecFull + ( float )( varMs  / 1000 ) ) / ( ( float ) divisor.varSecFull + ( float )( divisor.varMs / 1000 ) );
	}
}

time& time::operator/ ( uint32_t divisor )
{
	uint32_t temp = ( varHour  * 60 + varMinute ) * 60 + varSecond;

	if ( divisor > temp )
	{
		varHour    = 0;
		varMinute  = 0;
		varSecond  = 0;
	}

	else
	{
		temp /= divisor;

		temp      %= 86400;
		varHour   = temp / 3600;
		temp      = temp % 3600;
		varMinute = temp / 60;
		varSecond = temp % 60;
	}

	return *this;
}

time time::operator* ( uint32_t multiplicant )
{
	time ret( ( varSecFull * multiplicant ), ( varMs * multiplicant ) );

	return ret;
}

time& time::operator= ( const time &source )
{
	varSecond = source.varSecond;
	varMinute = source.varMinute;
	varHour   = source.varHour;

	varMs     = source.varMs;

	varSecFull = source.varSecFull;

	return *this;
}

bool time::operator< ( const time input )
{
	if ( ( varSecFull < input.varSecFull ) || ( ( ( varSecFull == input.varSecFull ) && ( varMs < input.varMs ) ) ) )
		return true;
	else
		return false;
}

bool time::operator> ( const time input )
{
	if ( ( varSecFull > input.varSecFull ) || ( ( ( varSecFull == input.varSecFull ) && ( varMs > input.varMs ) ) ) )
		return true;
	else
		return false;
}

bool time::operator<= ( const time input )
{
	if ( ( varSecFull < input.varSecFull ) || ( ( varSecFull == input.varSecFull ) && ( varMs <= input.varMs ) ) )
		return true;
	else
		return false;
}

bool time::operator>= ( const time input )
{
	if ( ( varSecFull > input.varSecFull ) || ( ( varSecFull == input.varSecFull ) && ( varMs >= input.varMs ) ) )
		return true;
	else
		return false;
}

bool time::operator== ( const time input )
{
	if ( ( varSecFull == input.varSecFull ) && ( varMs == input.varMs ) )
		return true;
	else
		return false;
}

bool time::operator!= ( const time input )
{
	if ( ( varSecFull != input.varSecFull ) || ( varMs != input.varMs ) )
		return true;
	else
		return false;
}




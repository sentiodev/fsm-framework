/**
 * \file time_delay.h
 *
 * Time: delay
 *
 * Driver to generate a certain delay in the range of micro/milli seconds
 */
#ifndef TIME_DELAY_H_
#define TIME_DELAY_H_

#include <stdint.h>

// Compensate for waiting states when the CPU of the micro-controller runs on high clock frequencies
#if(MCU_CLOCK >= 14000000)
#define FLASH_WAIT 2
#else
#define FLASH_WAIT 1
#endif


#define CYCLES_PER_ASM_LOOP 3

/**
 * Driver class to generate a precise time delay
 *
 * The class contains functions to generate a certain delay in the range of micro/milli seconds.
 */
class time_delay
{
private:

public:
	time_delay() {}
	~time_delay() {}

	/**
	 * Set a delay in the range of micro-seconds.
	 *
	 * The function uses an loop-counter to generate an software delay.
	 * This delay functions does not evoke an low power sleep of the micro-controller!
	 *
	 * @param[in]  uint32_t delay time in us
	 * @param[out] none
	 * @return     void
	 */
	void micro( uint32_t delay ) __attribute__( ( noinline ) )
	{
		// Inline assembler loop that takes 3 clock-cycles to complete once
		// The counter-top value is calculated once, transfered to the ASM code via the %-operator (%0)
		__asm( "loop:"              //Jump label, begin of the loop
			   "subs %0,#1;"        //Decrease the counter by one (%0 is the first register in the list, #1 is a constant value)
			   "bne loop"::"r"( ( MCU_CLOCK / ( FLASH_WAIT  * 3000000 ) ) * delay ): );  //Branch if negative or null, jump back to "loop:"
	}

	/**
	 * Set a delay in the range of milli-seconds.
	 *
	 * The function uses an loop-counter to generate an software delay.
	 * This delay functions does not evoke an low power sleep of the micro-controller!
	 *
	 * @param[in]  uint32_t delay time in ms
	 * @param[out] none
	 * @return     void
	 */
	void milli( uint32_t delay ) __attribute__( ( noinline ) )
	{
		// Inline assembler loop that takes 3 clock-cycles to complete once
		// The counter-top value is calculated once, transfered to the ASM code via the %-operator (%0)
		__asm( "loop:"              //Jump label, begin of the loop
			   "subs %0,#1;"        //Decrease the counter by one (%0 is the first register in the list, #1 is a constant value)
			   "bne loop"::"r"( ( MCU_CLOCK / ( FLASH_WAIT  * 3000 ) ) * delay ): );   //Branch if negative or null, jump back to "loop:"
	}
};


#endif /* TIME_DELAY_H_ */

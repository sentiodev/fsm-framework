/*
 * mcu_efm32.cpp
 *
 *  Created on: May, 2012
 *      Author: Matthias
 */

#include "mcu_efm32.h"

mcu_efm32_gpio mcu_efm32::gpio;
// mcu_efm32_rtc  mcu_efm32::rtc;
mcu_efm32_adc  mcu_efm32::adc;
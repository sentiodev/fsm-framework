/**
 * Driver: mcu_efm32_adc
 *
 * Driver for the internal ADC of the EFM32 MCU
 */

#ifndef ADC_EFM_H_
#define ADC_EFM_H_

#include "em_adc.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "em_chip.h"


#define _INT_ADC_CALIBRATION_GAIN   adcSingleInpCh2
#define _INT_ADC_CALIBRATION_OFFSET adcSingleInpDiff0
#define _INT_ADC_REFERENCE          adcRef2V5

struct ADC_CONFIG {
	ADC_Init_TypeDef        basic;
	ADC_InitSingle_TypeDef  single;
	ADC_InitScan_TypeDef    scan;
};


/**
 * Driver class for the EFM32 internal ADC
 *
 * The class contains all member functions to interact with the internal ADC of the EFM32.
 */
class mcu_efm32_adc
{
private:
	uint32_t calirationValue;
	float    vRefSingle;
	float    vRefScan;
	uint16_t resolutionSingle;
	uint16_t resolutionScan;
	uint32_t maskSingle;
	uint32_t maskScan;

public:
	mcu_efm32_adc();
	~mcu_efm32_adc() {}

	/**
	 * Initializes the internal ADC clock.
	 *
	 * The function initializes clock source for the internal ADC of the EFM32. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void init();

	/**
	 * Sets the configuration.
	 *
	 * The function sets the configuration of the internal ADC. The ADC_CONFIG structure can be used to define a configuration.
	 *
	 * @param[in]  adcConfig: the configuration to be set
	 * @param[out] none
	 * @return     none
	 */
	void setConfig( const ADC_CONFIG &adcConfig );

	/**
	 * Gets multiple values.
	 *
	 * detail
	 *
	 * @param[in]  numOfChannels:
	 * @param[out] data:
	 * @return     none
	 */
	void getMultipleValues( uint16_t *data, uint8_t numOfChannels );

	/**
	 * brief
	 *
	 * detail
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void getValue( uint16_t &data, ADC_InitSingle_TypeDef* single = 0 );

	/**
	 * brief
	 *
	 * detail
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void getMultipleValues( float *data, uint8_t numOfChannels );

	/**
	 * brief
	 *
	 * detail
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void getValue( float &data, ADC_InitSingle_TypeDef* single = 0 );

	/**
	 * brief
	 *
	 * detail
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void calibrateReference();

};

#endif /* ADC_EFM32_H_ */

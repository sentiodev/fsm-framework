/*
 * mcu_efm32.h
 */

#ifndef MCU_EFM32_ADC_H_
#define MCU_EFM32_ADC_H_

#include <stdint.h>

#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "em_leuart.h"

#include "mcu_efm32_gpio.h"
#include "mcu_efm32_rtc.h"
#include "mcu_efm32_adc.h"

class mcu_efm32
{
private:

public:
	static mcu_efm32_gpio gpio;
//  static mcu_efm32_rtc  rtc;
	static mcu_efm32_adc  adc;

	mcu_efm32() {}
	~mcu_efm32() {}
};

#endif

/**
 * Driver: mcu_efm32_rtc
 *
 * Driver for the internal RTC of the EFM32 MCU.
 */

#include "mcu_efm32_rtc.h"

mcu_efm32_rtc::mcu_efm32_rtc()
{
	/* Enable clock to low energy modules */
	CMU_ClockEnable( cmuClock_CORELE, true );
	CMU_ClockEnable( cmuClock_RTC, true );

	/* Set RTC pre-scaler */
	CMU_ClockDivSet( cmuClock_RTC, cmuClkDiv_8192 );


	RTC->IFC = _RTC_IFC_MASK;

	RTC_Init_TypeDef config;
	config.comp0Top = false;
	config.debugRun = true;
	config.enable   = false;

	RTC_Init( &config );

	RTC_IntEnable( 0x02 );
}


void mcu_efm32_rtc::setBaseTime( const time )
{
	RTC_CounterReset();
}


void mcu_efm32_rtc::setAlarm( time alarmTime, COMP_REG compare )
{
	RTC_CompareSet( ( uint32_t ) compare, ( ( alarmTime.getInSeconds() * 4 ) + ( alarmTime.getMilliSec() / 125 ) ) - 1 );

	RTC_Enable( true );
}


void mcu_efm32_rtc::disableAlarm( const COMP_REG compare )
{
	RTC_Enable( false );
}
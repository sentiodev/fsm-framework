/**
 * Driver: mcu_efm32_adc
 *
 * Driver for the internal ADC of the EFM32 MCU
 */

#include "mcu_efm32_adc.h"

mcu_efm32_adc::mcu_efm32_adc()
{
	init();
}


void mcu_efm32_adc::init()
{
	CMU_ClockEnable( cmuClock_GPIO, true );
	CMU_ClockEnable( cmuClock_ADC0, true );

	ADC_Reset( ADC0 );
}


void mcu_efm32_adc::setConfig( const ADC_CONFIG &adcConfig )
{

	ADC_Init( ADC0,  &adcConfig.basic );
	ADC_InitScan( ADC0, &adcConfig.scan );
	ADC_InitSingle( ADC0, &adcConfig.single );

	switch ( adcConfig.single.reference )
	{
	case adcRef1V25:
		vRefSingle = 1.25;
		break;/** Internal 1.25V reference. */
	case adcRef2V5:
		vRefSingle = 2.5;
		break;  /** Internal 2.5V reference. */
	case adcRefVDD:
		vRefSingle = 3;
		break; /** Buffered VDD. */
	case adcRef5VDIFF:
		vRefSingle = 5;
		break; /** Internal differential 5V reference. */
	case adcRefExtSingle:
		vRefSingle = 3;
		break;/** Single ended ext. ref. from pin 6. */
	case adcRef2xExtDiff:
		vRefSingle = 3;
		break;/** Differential ext. ref. from pin 6 and 7. */
	case adcRef2xVDD:
		vRefSingle = 6;
		break;/** Unbuffered 2xVDD. */
	default:
		break;
	}

	switch ( adcConfig.scan.reference )
	{
	case adcRef1V25:
		vRefScan = 1.25;
		break;/** Internal 1.25V reference. */
	case adcRef2V5:
		vRefScan = 2.5;
		break;  /** Internal 2.5V reference. */
	case adcRefVDD:
		vRefScan = 3;
		break; /** Buffered VDD. */
	case adcRef5VDIFF:
		vRefScan = 5;
		break; /** Internal differential 5V reference. */
	case adcRefExtSingle:
		vRefScan = 3;
		break;/** Single ended ext. ref. from pin 6. */
	case adcRef2xExtDiff:
		vRefScan = 3;
		break;/** Differential ext. ref. from pin 6 and 7. */
	case adcRef2xVDD:
		vRefScan = 6;
		break;/** Unbuffered 2xVDD. */
	default:
		break;
	}

	switch ( adcConfig.single.resolution )
	{
	case adcRes12Bit:
		resolutionSingle = 4095;
		maskSingle = 0x00000FFF;
		break;
	case adcRes8Bit:
		resolutionSingle = 255;
		maskSingle = 0x000000FF;
		break;
	case adcRes6Bit:
		resolutionSingle = 63;
		maskSingle = 0x0000003F;
		break;
	case adcResOVS:
		resolutionSingle = 65520;
		maskSingle = 0x0000FFFF;
		break;
	default:
		break;
	}

	switch ( adcConfig.scan.resolution )
	{
	case adcRes12Bit:
		resolutionScan = 4095;
		maskScan = 0x00000FFF;
		break;
	case adcRes8Bit:
		resolutionScan = 255;
		maskScan = 0x000000FF;
		break;
	case adcRes6Bit:
		resolutionScan = 63;
		maskScan = 0x0000003F;
		break;
	case adcResOVS:
		resolutionScan = 65520;
		maskScan = 0x0000FFFF;
		break;
	default:
		break;
	}
}


void mcu_efm32_adc::getMultipleValues( uint16_t *data, uint8_t numOfChannels )
{
	ADC_Start( ADC0, adcStartScan );

	for ( uint32_t i = 0; i < numOfChannels; i++ )
	{
		while ( !( ADC0->STATUS & ADC_STATUS_SCANDV ) );
		data[i] = ( uint16_t )( ( ADC0->SCANDATA ) & maskScan );
	}
}


void mcu_efm32_adc::getValue( uint16_t &data, ADC_InitSingle_TypeDef *single )
{
	if ( single )
		ADC_InitSingle( ADC0, single );

	ADC_Start( ADC0, adcStartSingle );

	while ( ADC0->STATUS & ADC_STATUS_SINGLEACT );

	data = ( uint16_t )( ADC0->SINGLEDATA & maskSingle );
}


void mcu_efm32_adc::getMultipleValues( float *data, uint8_t numOfChannels )
{
	ADC_Start( ADC0, adcStartScan );

	for ( uint32_t i = 0; i < numOfChannels; i++ )
	{
		while ( !( ADC0->STATUS & ADC_STATUS_SCANDV ) );
		data[i] = ( float )( ( ADC0->SCANDATA ) & maskScan ) * vRefScan / resolutionScan;
	}
}


void mcu_efm32_adc::getValue( float &data, ADC_InitSingle_TypeDef *single )
{
	if ( single )
		ADC_InitSingle( ADC0, single );

	ADC_Start( ADC0, adcStartSingle );

	while ( ADC0->STATUS & ADC_STATUS_SINGLEACT );

	data = ( float )( ( ADC0->SINGLEDATA ) & maskSingle ) * vRefSingle / resolutionSingle;
}


void mcu_efm32_adc::calibrateReference()
{
	int32_t  sample;
	uint32_t cal;

	/* Binary search variables */
	uint8_t high;
	uint8_t mid;
	uint8_t low;

	/* Reset ADC to be sure we have default settings and wait for ongoing */
	/* conversions to be complete. */
	ADC_Reset( ADC0 );

	ADC_Init_TypeDef       l_init     = ADC_INIT_DEFAULT;
	ADC_InitSingle_TypeDef singleInit = ADC_INITSINGLE_DEFAULT;

	/* Init common settings for both single conversion and scan mode */
	l_init.timebase = ADC_TimebaseCalc( 0 );
	/* Might as well finish conversion as quickly as possibly since polling */
	/* for completion. */
	/* Set ADC clock to 7 MHz, use default HFPERCLK */
	l_init.prescale = ADC_PrescaleCalc( 7000000, 0 );

	/* Set an oversampling rate for more accuracy */
	l_init.ovsRateSel = adcOvsRateSel4096;
	/* Leave other settings at default values */
	ADC_Init( ADC0, &l_init );

	/* Init for single conversion use, measure diff 0 with selected reference. */
	singleInit.reference = adcRef2V5;
	singleInit.input     = adcSingleInpCh2;
	singleInit.acqTime   = adcAcqTime16;
	singleInit.diff      = true;
	/* Enable oversampling rate */
	singleInit.resolution = adcResOVS;

	ADC_InitSingle( ADC0, &singleInit );



	/* ADC is now set up for offset calibration */
	/* Offset calibration register is a 7 bit signed 2's complement value. */
	/* Use unsigned indexes for binary search, and convert when calibration */
	/* register is written to. */
	high = 128;
	low  = 0;

	/* Do binary search for offset calibration*/
	while ( low < high )
	{
		/* Calculate midpoint */
		mid = low + ( high - low ) / 2;

		/* Midpoint is converted to 2's complement and written to both scan and */
		/* single calibration registers */
		cal      = ADC0->CAL & ~( _ADC_CAL_SINGLEOFFSET_MASK | _ADC_CAL_SCANOFFSET_MASK );
		cal     |= ( mid - 63 ) << _ADC_CAL_SINGLEOFFSET_SHIFT;
		cal     |= ( mid - 63 ) << _ADC_CAL_SCANOFFSET_SHIFT;
		ADC0->CAL = cal;

		/* Do a conversion */
		ADC_Start( ADC0, adcStartSingle );

		/* Wait while conversion is active */
		while ( ADC0->STATUS & ADC_STATUS_SINGLEACT ) ;

		/* Get ADC result */
		sample = ADC_DataSingleGet( ADC0 );

		/* Check result and decide in which part of to repeat search */
		/* Calibration register has negative effect on result */
		if ( sample < 0 )
		{
			/* Repeat search in bottom half. */
			high = mid;
		}
		else if ( sample > 0 )
		{
			/* Repeat search in top half. */
			low = mid + 1;
		}
		else
		{
			/* Found it, exit while loop */
			break;
		}
	}


	/* Now do gain calibration, only input and diff settings needs to be changed */
	ADC0->SINGLECTRL &= ~( _ADC_SINGLECTRL_INPUTSEL_MASK | _ADC_SINGLECTRL_DIFF_MASK );
	ADC0->SINGLECTRL |= ( adcSingleInpCh2 << _ADC_SINGLECTRL_INPUTSEL_SHIFT );
	ADC0->SINGLECTRL |= ( false << _ADC_SINGLECTRL_DIFF_SHIFT );

	/* ADC is now set up for gain calibration */
	/* Gain calibration register is a 7 bit unsigned value. */

	high = 128;
	low  = 0;

	/* Do binary search for gain calibration */
	while ( low < high )
	{
		/* Calculate midpoint and write to calibration register */
		mid = low + ( high - low ) / 2;

		/* Midpoint is converted to 2's complement */
		cal      = ADC0->CAL & ~( _ADC_CAL_SINGLEGAIN_MASK | _ADC_CAL_SCANGAIN_MASK );
		cal     |= mid << _ADC_CAL_SINGLEGAIN_SHIFT;
		cal     |= mid << _ADC_CAL_SCANGAIN_SHIFT;
		ADC0->CAL = cal;

		/* Do a conversion */
		ADC_Start( ADC0, adcStartSingle );

		/* Wait while conversion is active */
		while ( ADC0->STATUS & ADC_STATUS_SINGLEACT ) ;

		/* Get ADC result */
		sample = ADC_DataSingleGet( ADC0 );

		/* Check result and decide in which part to repeat search */
		/* Compare with a value atleast one LSB's less than top to avoid overshooting */
		/* Since oversampling is used, the result is 16 bits, but a couple of lsb's */
		/* applies to the 12 bit result value, if 0xffe is the top value in 12 bit, this */
		/* is in turn 0xffe0 in the 16 bit result. */
		/* Calibration register has positive effect on result */
		if ( sample > 0xffd0 )
		{
			/* Repeat search in bottom half. */
			high = mid;
		}
		else if ( sample < 0xffd0 )
		{
			/* Repeat search in top half. */
			low = mid + 1;
		}
		else
		{
			/* Found it, exit while loop */
			break;
		}
	}
}

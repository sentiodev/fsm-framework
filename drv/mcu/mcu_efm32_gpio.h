/**
 * Driver: mcu_efm32_gpio
 *
 * Driver for the internal RTC of the EFM32 MCU.
 */

#ifndef MCU_EFM32_GPIO_H_
#define MCU_EFM32_GPIO_H_

#include "em_gpio.h"

#define portA gpioPortA
#define portB gpioPortB
#define portC gpioPortC
#define portD gpioPortD
#define portE gpioPortE
#define portF gpioPortF


class mcu_efm32_gpio
{
private:

public:
	mcu_efm32_gpio()  {}
	~mcu_efm32_gpio() {}

	/**
	 * brief.
	 *
	 * detail.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void setMode( GPIO_Port_TypeDef , uint32_t ) {}

	/**
	 * brief.
	 *
	 * detail.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     none
	 */
	void setInterrupt( GPIO_Port_TypeDef , uint32_t , bool , bool ) {}

	/**
	 * Sets a GPIO pin.
	 *
	 * The function sets a GPIO pin of the EFM32 MCU high.
	 *
	 * @param[in]  port: the port of the pin
	*             pin: the pin number
	 * @param[out] none
	 * @return     none
	 */
	inline void setPin( GPIO_Port_TypeDef port, uint32_t pin ) {
		GPIO->P[port].DOUTSET = 1 << pin;
	}

	/**
	 * Clears a GPIO pin.
	 *
	 * The function clears a GPIO pin of the EFM32 MCU (sets it low).
	 *
	 * @param[in]  port: the port of the pin
	*             pin: the pin number
	 * @param[out] none
	 * @return     none
	 */
	inline void clrPin( GPIO_Port_TypeDef port, uint32_t pin ) {
		GPIO->P[port].DOUTCLR = 1 << pin;
	}

	/**
	 * Toggles a GPIO pin.
	 *
	 * The function toggles a GPIO pin of the EFM32 MCU.
	 *
	 * @param[in]  port: the port of the pin
	*             pin: the pin number
	 * @param[out] none
	 * @return     none
	 */
	inline void tglPin( GPIO_Port_TypeDef port, uint32_t pin ) {
		GPIO->P[port].DOUTTGL = 1 << pin;
	}

	/**
	 * Gets the state of a GPIO pin.
	 *
	 * The function gets and returns the state of a GPIO pin of the EFM32 MCU.
	 *
	 * @param[in]  port: the port of the pin
	*             pin: the pin number
	 * @param[out] none
	 * @return     true/false based on the state of the pin
	 */
	inline bool getPin( GPIO_Port_TypeDef port, uint32_t pin ) {
		return ( GPIO->P[port].DIN >> pin ) & _GPIO_P_DIN_DIN_MASK;
	}
};

#endif

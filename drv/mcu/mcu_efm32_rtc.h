/**
 * Driver: mcu_efm32_rtc
 *
 * Driver for the internal RTC of the EFM32 MCU.
 */

#ifndef MCU_EFM32_RTC_H_
#define MCU_EFM32_RTC_H_

#include "time.h"
#include "em_rtc.h"
#include "em_cmu.h"

enum COMP_REG {
	comp0 = 0,
	comp1 = 1
};

//extern "C" void RTC_IRQHandler() __attribute__( ( used, externally_visible ) );


class mcu_efm32_rtc
{
private:
	friend void RTC_IRQHandler();

public:
	mcu_efm32_rtc();
	~mcu_efm32_rtc() {}

	void setBaseTime( const time systemTime );
	void setAlarm( const time alarmTime, const COMP_REG compare );
	void disableAlarm( const COMP_REG compare );

};

#endif

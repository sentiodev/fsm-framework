/**
 * Driver: mcu_efm32_gpio
 *
 * Driver for GPIO operations of the EFM32 MCU.
 */

#include "mcu_efm32_gpio.h"

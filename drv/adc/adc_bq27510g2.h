/*
 * adc_bq27510g2.h
 *
 *  Created on: 05.06.2013
 *      Author: Florian Gebben
 */

#ifndef ADC_BQ27510G2_H_
#define ADC_BQ27510G2_H_

#include "time_delay.h"

#include "em_gpio.h"
#include "em_cmu.h"
#include "em_i2c.h"


/****************************************************************************************************************************************//**
 * @brief
 *  Definition of Standard Commands
 *
 *******************************************************************************************************************************************/
typedef enum {
	control                       = 0x00,  // units: N/A,
	atRate                        = 0x02,  // units: mA       type: signed int
	atRateTimeToEmpty             = 0x04,  // units: Minutes  type: unsigned int
	gasGaugeTemperature           = 0x06,  // units: 0.1K     type: unsigned int
	voltage                       = 0x08,  // units: mV       type: unsigned int
	flags                         = 0x0a,  // units: N/A
	nominalAvailableCapacity      = 0x0c,  // units: mAh      type: unsigned int
	fullAvailableCapacity         = 0x0e,  // units: mAh      type: unsigned int
	remainingCapacity             = 0x10,  // units: mAh      type: unsigned int
	fullChargeCapacity            = 0x12,  // units: mAh      type: unsigned int
	averageCurrent                = 0x14,  // units: mA       type: signed int
	timeToEmpty                   = 0x16,  // units: Minutes  type: unsigned int
	timeToFull                    = 0x18,  // units: Minutes  type: unsigned int
	standbyCurrent                = 0x1a,  // units: mA       type: signed int
	standbyCurrentToEmpty         = 0x1c,  // units: Minutes  type: unsigned int
	maxLoadCurrent                = 0x1e,  // units: mA       type: signed int
	maxLoadTimeToEmpty            = 0x20,  // units: Minutes  type: unsigned int
	availableEnergy               = 0x22,  // units: mWhr     type: unsigned int
	averagePower                  = 0x24,  // units: mW       type: signed int
	timeToEmptyAtConstantPower    = 0x26,  // units: Minutes  type: unsigned int
	cycleCount                    = 0x2a,  // units: Counts   type: unsigned int
	stateOfCharge                 = 0x2c   // units: %        type: unsigned int
} stdCmds;


/****************************************************************************************************************************************//**
 * @brief
 *  Definition of Control() Subcommands
 *
 *******************************************************************************************************************************************/
typedef enum {
	controlStatus   = 0x0000,
	deviceType      = 0x0001,
	fwVersion       = 0x0002,
	hwVersion       = 0x0003,
	dfChecksum      = 0x0004,
	resetData       = 0x0005,
	prevMacWrite    = 0x0007,
	chemId          = 0x0008,
	boardOffset     = 0x0009,
	setHibernate    = 0x0011,
	clearHibernate  = 0x0012,
	setSleepPlus    = 0x0013,
	clearSleepPlus  = 0x0014,
	sealed          = 0x0020,
	ItEnable        = 0x0021,
	IfChecksum      = 0x0022,
	calMode         = 0x0040,
	reset           = 0x0041
} controlSubCmds;



/****************************************************************************************************************************************//**
 * @brief
 *  Declaration of the BQ27510-G2 driver-class.
 *
 *******************************************************************************************************************************************/
class adc_bq27510g2
{
private:
	I2C_Init_TypeDef i2cInit;
	I2C_TransferSeq_TypeDef i2cTransfer;

	uint8_t i2c_txBuffer[2];
	uint8_t i2c_rxBuffer[2];

	time_delay myDelay;

	uint8_t setQuick( uint8_t &data );
	uint8_t getQuick( uint8_t &data );

public:
	adc_bq27510g2() {}
	~adc_bq27510g2() {}

	void initializeInterfaceBq27510g2();

	uint8_t setAtRate( int16_t data );
	uint8_t getStd( uint16_t &data, stdCmds command );
	uint8_t setStd( int16_t &data, stdCmds command );
	uint8_t controlSubCommand( controlSubCmds command );

	float kToC( uint16_t K );

};

#endif /* ADC_BQ27510G2_H_ */

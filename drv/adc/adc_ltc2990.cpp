/**
 * Driver: adc_ltc2990
 *
 * Driver for the LTC2990 ADC
 */

#include "adc_ltc2990.h"

#include "em_gpio.h"
#include "em_cmu.h"


adc_ltc2990::adc_ltc2990()
{

	i2cInit.master  = true;
	i2cInit.enable  = true;
	i2cInit.refFreq = MCU_CLOCK;
	i2cInit.freq    = 400000;

	i2cInit.refFreq         = _I2C_CTRL_CLHR_STANDARD;
	i2cTransfer.buf[0].data = i2c_txBuffer;
	i2cTransfer.buf[1].data = i2c_rxBuffer;
	i2cTransfer.addr        = chip0;

	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	GPIO_PinModeSet( LTC2990_I2C_SCL, gpioModeWiredAndPullUpFilter, 1 );
	GPIO_PinModeSet( LTC2990_I2C_SDA, gpioModeWiredAndPullUpFilter, 1 );

	init();
}


void adc_ltc2990::init()
{
	CMU_ClockEnable( cmuClock_I2C0, true );

	LTC2990_I2C->ROUTE = I2C_ROUTE_SDAPEN |
						 I2C_ROUTE_SCLPEN |
						 LTC2990_I2C_LOC;

	I2C_Init( LTC2990_I2C, &i2cInit );
}


void adc_ltc2990::trigger()
{
	i2c_txBuffer[0]        = TRIGGER_ADDR;
	i2c_txBuffer[1]        = 0x00;

	i2cTransfer.buf[0].len = 0x02;
	i2cTransfer.buf[1].len = 0x00;

	i2cTransfer.flags      = I2C_FLAG_WRITE_WRITE;

	I2C_TransferInit( LTC2990_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( LTC2990_I2C ) == i2cTransferInProgress );
}


void adc_ltc2990::setConfig( const LTC2990_CONFIG ltcConfig )
{
	uint8_t modeMSB = 0x18;
	_shuntV12 = ltcConfig.shuntV12;
	_shuntV34 = ltcConfig.shuntV34;

	i2c_txBuffer[0]         = CONTROL_ADDR;
	i2c_txBuffer[1]         = ( ltcConfig.temperature | ltcConfig.aquire | modeMSB | ltcConfig.mode );

	i2cTransfer.buf[0].len  = 0x02;
	i2cTransfer.buf[1].len  = 0x00;
	i2cTransfer.addr        = ltcConfig.address;

	i2cTransfer.flags       = I2C_FLAG_WRITE_WRITE;

	I2C_TransferInit( LTC2990_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( LTC2990_I2C ) == i2cTransferInProgress );
}


void adc_ltc2990::getConfig( LTC2990_CONFIG &ltcConfig )
{
	ltcConfig.shuntV12 = _shuntV12;
	ltcConfig.shuntV34 = _shuntV34;

	i2c_txBuffer[0]         = CONTROL_ADDR;

	i2cTransfer.buf[0].len  = 0x01;
	i2cTransfer.buf[1].len  = 0x01;

	i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

	I2C_TransferInit( LTC2990_I2C, &i2cTransfer );

	// Transmit the data package via I2C and get the chip response-data
	while ( I2C_Transfer( LTC2990_I2C ) == i2cTransferInProgress );

	ltcConfig.temperature = ( LTC_TEMP )( i2c_rxBuffer[0] & 0x80 );
	ltcConfig.aquire      = ( LTC_AQUIRE )( i2c_rxBuffer[0] & 0x40 );
	ltcConfig.mode        = ( LTC_MODE )( i2c_rxBuffer[0] & 0x07 );
}


bool adc_ltc2990::getStatus( uint8_t &status )
{
	i2c_txBuffer[0]         = STATUS_ADDR;

	i2cTransfer.buf[0].len  = 0x01;
	i2cTransfer.buf[1].len  = 0x01;

	i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

	I2C_TransferInit( LTC2990_I2C, &i2cTransfer );

	bool pollContinue = true;

	// Transmit the data package via I2C and get the chip response-data
	while ( pollContinue )
	{
		switch ( I2C_Transfer( LTC2990_I2C ) )
		{
		case i2cTransferInProgress:
			break;

		case i2cTransferDone:
			pollContinue = false;
			status = i2c_rxBuffer[0];
			return true;
			break; //Just because you are paranoid does not mean that they are not after you

		default:
			return false;
		}
	}

	return false;
}


bool adc_ltc2990::getReading( uint8_t selection )
{
	uint8_t statusLTC = 0;
	uint8_t loopCount = 20;

	while ( getStatus( statusLTC ) )
	{
		if ( statusLTC & 0x01 )
		{
			i2c_txBuffer[0]         = selection;

			i2cTransfer.buf[0].len  = 0x01;
			i2cTransfer.buf[1].len  = 0x02;

			i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

			I2C_TransferInit( LTC2990_I2C, &i2cTransfer );

			bool pollContinue = true;

			while ( pollContinue )
			{
				switch ( I2C_Transfer( LTC2990_I2C ) )
				{
				case i2cTransferInProgress:
					break;

				case i2cTransferDone:
					pollContinue = false;
					break;

				default:
					return false;
				}
			}

			return true;
			break;
		}
		else
		{
			if ( loopCount == 20 )
				trigger();

			if ( loopCount )
			{
				for ( volatile uint32_t i = 0; i < ( 250 * loopCount ); i++ );
				loopCount--;
			}
			else
				return false;
		}
	}

	return false;
}


bool adc_ltc2990::getTemperature( uint16_t &temperature )
{
	if ( getReading( TEMP_ADDR ) )
	{
		temperature = ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x1FFF;

		return true;
	}
	else
		return false;
}


bool adc_ltc2990::getSupplyVoltage( uint16_t &supply )
{
	if ( getReading( VCC_ADDR ) )
	{
		supply = ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x1FFF;

		return true;
	}
	else
		return false;
}


bool adc_ltc2990::getVoltage( uint16_t &voltage, REG_VOLTAGE selection )
{
	if ( getReading( ( uint8_t )selection ) )
	{
		voltage = ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x1FFF;

		return true;
	}
	else
		return false;
}


bool adc_ltc2990::getCurrent( uint16_t &current, REG_CURRENT selection )
{
	if ( getReading( ( uint8_t )selection ) )
	{
		current = ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x1FFF;

		return true;
	}
	else
		return false;
}


bool adc_ltc2990::getTemperature( float &temperature )
{
	if ( getReading( TEMP_ADDR ) )
	{
		temperature = ( float )( ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x1FFF ) * 0.0625;

		return true;
	}
	else
		return false;
}


bool adc_ltc2990::getSupplyVoltage( float &supply )
{
	if ( getReading( VCC_ADDR ) )
	{
		supply = ( ( float )( ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x7FFF ) * 305.18e-6 ) + 2.5;

		return true;
	}
	else
		return false;
}


bool adc_ltc2990::getVoltage( float &voltage, REG_VOLTAGE selection )
{
	if ( getReading( ( uint8_t )selection ) )
	{
		if ( i2c_rxBuffer[0] & 0x40 )
			voltage = ( ( float )( ( ~( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x3FFF ) + 1 ) * ( -305.18e-6 ) );
		else
			voltage = ( ( float )( ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x3FFF ) * 305.18e-6 );

		if ( voltage < 0.0 )
			voltage = 0.0;

		return true;
	}
	else
		return false;
}



bool adc_ltc2990::getCurrent( float &current, REG_CURRENT selection )
{
	float shunt = 0;

	if ( getReading( ( uint8_t )selection ) )
	{
		switch ( selection )
		{
		case currentV12:
			shunt = _shuntV12;
			break;

		case currentV34:
			shunt = _shuntV34;
			break;

		default:
			break;
		}

		if ( i2c_rxBuffer[0] & 0x40 )
			current = ( ( float )( ( ( ~( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) ) & 0x3FFF ) + 1 ) * ( -19.42e-6 ) ) / shunt;
		else
			current = ( ( float )( ( ( i2c_rxBuffer[0] << 8 ) | i2c_rxBuffer[1] ) & 0x3FFF ) * ( 19.42e-6 ) ) / shunt;

		return true;
	}
	else
		return false;
}

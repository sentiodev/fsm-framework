/**
 * @file adc_ad7xxx.h
 *
 * Driver: adc_ad7xxx
 *
 * Driver for the Analog Devices AD7xxx Analog-to-digital converters
 */

#ifndef ADC_AD7XXX_H_
#define ADC_AD7XXX_H_

#include "em_gpio.h"
#include "em_usart.h"
// #include "em_cmu.h"
#include "time_delay.h"

// Platform selection
#if SENTIO_EM_ENABLE
// 	#if OIL_IN_WATER_BOARD_ENABLE
// 		#include "oil_in_water_board_io.h"
// 
// 		#define _AD7XXX_SDO_PIN         ADC_SDO_PIN
// 		#define _AD7XXX_CLK_PIN         ADC_CLK_PIN
// 		#define _AD7XXX_CNV_PIN         ADC_CNV_PIN
// 
// 		#define _AD7XXX_USART           ADC_USART
// 		#define _AD7XXX_USART_CLOCK     ADC_USART_CLOCK
// 		#define _AD7XXX_LOCATION        ADC_LOCATION
// 
// 		#define _AD7XXX_CLOCK_MODE      usartClockMode0
// 
// 		#define _AD7XXX_BAUDRATE        16000000
// 		#define _AD7XXX_DATABITS        usartDatabits16
// 		#define _AD7XXX_ENABLE          usartEnable
// 		#define _AD7XXX_REFFREQ         MCU_CLOCK
// 		#define _AD7XXX_MASTER          true
// 		#define _AD7XXX_MSBF            true
// 
// 	#else
		#include "sentio_em.h"

		#define _AD7XXX_SDO_PIN         SENSOR_USART_RX
		#define _AD7XXX_CLK_PIN         SENSOR_USART_CLK
		#define _AD7XXX_CNV_PIN         SENSOR_USART_CS

		#define _AD7XXX_USART           SENSOR_USART
		#define _AD7XXX_USART_CLOCK     SENSOR_USART_CLOCK
		#define _AD7XXX_LOCATION        SENSOR_USART_LOC

		#define _AD7XXX_CLOCK_MODE      usartClockMode0

		#define _AD7XXX_BAUDRATE        16000000
		#define _AD7XXX_DATABITS        usartDatabits16
		#define _AD7XXX_ENABLE          usartEnable
		#define _AD7XXX_REFFREQ         MCU_CLOCK
		#define _AD7XXX_MASTER          true
		#define _AD7XXX_MSBF            true
// 	#endif
#else
	#error No Sensor Platform defined
#endif

/** ADC Precision */
enum ADC_PRECISION
{
	prec8bit  = 0x00FF,
	prec12bit = 0x0FFF,
	prec14bit = 0x3FFF,
	prec16bit = 0xFFFF
};


typedef struct
{
	float   refVoltage;
	uint8_t daisyChain;
	ADC_PRECISION precision;
} AD7XXX_CONFIG;


/**
 * Driver class for the AD7XXX analog-to-digital converter
 *
 * The class contains the interface to interract with the AD7XXX radio transceiver.
 */
class adc_ad7xxx
{
public:
	adc_ad7xxx();
	~adc_ad7xxx() {}

	/**
	 * Initializes the interface to the microcontroller.
	 *
	 * The function initializes the SPI communication to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Sets the configuration for the AD7XXX converter
	 *
	 * The function sets the configuration of the ADC driver. The ADC chip itself does not have any configuration register, thus the SPI
	 * interface is not used!
	 *
	 * @param[in]  const AD7XXX_CONFIG: Structure of configuration parameters for the ADC driver
	 * @param[out] none
	 * @return     void
	 */
	void setConfig( const AD7XXX_CONFIG &config );

	/**
	 * Read raw-data form the AD7XXX converter
	 *
	 * The function triggers an adc conversion and afterwards reads the measurement value as raw-binary value.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint16_t Bitmask that was applied on the raw-data
	 */
	uint16_t getValue( float &result );

	/**
	 * Read data form the AD7XXX converter and convert to float value.
	 *
	 * The function triggers an adc conversion and afterwards reads the measurement value. The raw-value is converted using the configured reference
	 * voltage and ADC precision.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint16_t Bitmask that was applied on the raw-data
	 */
	uint16_t getValue( uint16_t &result );

private:
	AD7XXX_CONFIG           adcConfig;

	time_delay              delay;

	inline void trigger();
};

#endif  /*ADC_AD7XXX_H_ */

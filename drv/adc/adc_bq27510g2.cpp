/*
 * adc_bq27510g2.cpp
 *
 *  Created on: 05.06.2013
 *      Author: Florian Gebben
 */

#include "adc_bq27510g2.h"


void adc_bq27510g2::initializeInterfaceBq27510g2()
{
	  i2cInit.master  = true;
	  i2cInit.enable  = true;
	  i2cInit.refFreq = 0;
	  i2cInit.freq    = 400000;
	  i2cInit.refFreq = _I2C_CTRL_CLHR_STANDARD;

	  CMU_ClockEnable(cmuClock_I2C0, true);

	  GPIO_PinModeSet(gpioPortD, 7, gpioModeWiredAndDrivePullUpFilter, 1);
	  GPIO_PinModeSet(gpioPortD, 6, gpioModeWiredAndDrivePullUpFilter, 1);

	  /* Enable pins at location 1 */
	  I2C0->ROUTE = I2C_ROUTE_SDAPEN |
					I2C_ROUTE_SCLPEN |
					I2C_ROUTE_LOCATION_LOC1;

	  I2C_Init(I2C0, &i2cInit);

	  i2cTransfer.buf[0].data = i2c_txBuffer;
	  i2cTransfer.buf[1].data = i2c_rxBuffer;
}


uint8_t adc_bq27510g2::setAtRate( int16_t data )
{
	I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;

	uint8_t tmp;
	// Set I2C address of bq27510-g2 (read/write bit is set in I2C_TransferInit)
	i2cTransfer.addr = 0xAA;

	i2c_txBuffer[0]       = 0x02;
	i2c_txBuffer[1]       = data;

	i2cTransfer.buf[0].len  = 0x02;  // length of i2c_txBuffer
	i2cTransfer.buf[1].len  = 0x00;  // length of i2c_rxBuffer

	i2cTransfer.flags       = I2C_FLAG_WRITE_WRITE;

	I2C_TransferInit( I2C0, &i2cTransfer );
	while (ret == i2cTransferInProgress)
	{
		ret = I2C_Transfer(I2C0);
	}

	tmp = (uint8_t) (data >> 8) ;

	if (ret == i2cTransferDone)
		return setQuick( tmp );
	else if (ret == i2cTransferNack)
		return -1;
	else if (ret == i2cTransferBusErr)
		return -2;
	else if (ret == i2cTransferArbLost)
		return -3;
	else if (ret == i2cTransferUsageFault)
		return -4;
	else if (ret == i2cTransferSwFault)
		return -5;
	else return -6;
}


uint8_t adc_bq27510g2::getStd( uint16_t &data, stdCmds command )
{
	I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;

	// Set I2C address of bq27510-g2 (0xAA)
	i2cTransfer.addr = 0xAA;

	i2c_txBuffer[0]       = command;

	i2cTransfer.buf[0].len  = 0x01;  // length of i2c_txBuffer
	i2cTransfer.buf[1].len  = 0x02;  // length of i2c_rxBuffer

	i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

	// I2C Transfer
	I2C_TransferInit( I2C0, &i2cTransfer );
	while (ret == i2cTransferInProgress)
	{
		ret = I2C_Transfer(I2C0);
	}

	data = ( ( i2c_rxBuffer[1] <<8 ) | i2c_rxBuffer[0] );

	if (ret == i2cTransferDone)
		return 0;
	else if (ret == i2cTransferNack)
		return -1;
	else if (ret == i2cTransferBusErr)
		return -2;
	else if (ret == i2cTransferArbLost)
		return -3;
	else if (ret == i2cTransferUsageFault)
		return -4;
	else if (ret == i2cTransferSwFault)
		return -5;
	else return -6;
}
/*

uint8_t adc_bq27510g2::read_std( int16_t &data, stdCmds command )
{
	I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;

	// Set I2C address of bq27510-g2 (0xAA)
	i2cTransfer.addr = 0xAA;

	i2c_txBuffer[0]       = command;

	i2cTransfer.buf[0].len  = 0x01;  // length of i2c_txBuffer
	i2cTransfer.buf[1].len  = 0x02;  // length of i2c_rxBuffer

	i2cTransfer.flags       = I2C_FLAG_WRITE_READ;

	// I2C Transfer
	I2C_TransferInit( I2C0, &i2cTransfer );
	while (ret == i2cTransferInProgress)
	{
		ret = I2C_Transfer(I2C0);
	}

	data = ( ( i2c_rxBuffer[1] <<8 ) | i2c_rxBuffer[0] );

	if (ret == i2cTransferDone)
		return 0;
	else if (ret == i2cTransferNack)
		return -1;
	else if (ret == i2cTransferBusErr)
		return -2;
	else if (ret == i2cTransferArbLost)
		return -3;
	else if (ret == i2cTransferUsageFault)
		return -4;
	else if (ret == i2cTransferSwFault)
		return -5;
	else return -6;
}*/


uint8_t adc_bq27510g2::setQuick( uint8_t &data )
{
	I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;
	// Set I2C address of bq27510-g2 (0xAA)
	i2cTransfer.addr = 0xAA;

	i2cTransfer.buf[0].len  = 0x00;  // length of i2c_txBuffer
	i2cTransfer.buf[1].len  = 0x01;  // length of i2c_rxBuffer

	i2cTransfer.flags       = I2C_FLAG_READ;

	I2C_TransferInit( I2C0, &i2cTransfer );

	/* Sending data */
	while (ret == i2cTransferInProgress)
	{
	ret = I2C_Transfer(I2C0);
	}

	data = (i2c_rxBuffer[0]  );

	if (ret == i2cTransferDone)
		return 0;
	else if (ret == i2cTransferNack)
		return -1;
	else if (ret == i2cTransferBusErr)
		return -2;
	else if (ret == i2cTransferArbLost)
		return -3;
	else if (ret == i2cTransferUsageFault)
		return -4;
	else if (ret == i2cTransferSwFault)
		return -5;
	else return -6;
}


uint8_t adc_bq27510g2::getQuick( uint8_t &data )
{
	I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;

	// Set I2C address of bq27510-g2 (0xAA)
	i2cTransfer.addr = 0xAA;

	i2c_txBuffer[0]         = data;

	i2cTransfer.buf[0].len  = 0x01;  // length of i2c_txBuffer
	i2cTransfer.buf[1].len  = 0x00;  // length of i2c_rxBuffer

	i2cTransfer.flags       = I2C_FLAG_WRITE;

	I2C_TransferInit( I2C0, &i2cTransfer );
	while (ret == i2cTransferInProgress)
	{
		ret = I2C_Transfer(I2C0);
	}

	if (ret == i2cTransferDone)
		return 0;
	else if (ret == i2cTransferNack)
		return -1;
	else if (ret == i2cTransferBusErr)
		return -2;
	else if (ret == i2cTransferArbLost)
		return -3;
	else if (ret == i2cTransferUsageFault)
		return -4;
	else if (ret == i2cTransferSwFault)
		return -5;
	else return -6;
}


uint8_t adc_bq27510g2::controlSubCommand( controlSubCmds command )
{
	I2C_TransferReturn_TypeDef ret = i2cTransferInProgress;

	// Set I2C address of bq27510-g2 (0xAA)
	i2cTransfer.addr = 0xAA;

	i2c_txBuffer[0]       = 0x00;
	i2c_txBuffer[1]       = command;
	i2c_txBuffer[2]       = 0x00;

	i2cTransfer.buf[0].len  = 0x03;  // length of i2c_txBuffer
	i2cTransfer.buf[1].len  = 0x00;  // length of i2c_rxBuffer

	i2cTransfer.flags       = I2C_FLAG_WRITE_WRITE;

	// I2C Transfer
	I2C_TransferInit( I2C0, &i2cTransfer );
	while (ret == i2cTransferInProgress)
	{
		ret = I2C_Transfer(I2C0);
	}

	//small delay
	myDelay.micro(200);

	if (ret == i2cTransferDone)
		return 0;
	else if (ret == i2cTransferNack)
		return -1;
	else if (ret == i2cTransferBusErr)
		return -2;
	else if (ret == i2cTransferArbLost)
		return -3;
	else if (ret == i2cTransferUsageFault)
		return -4;
	else if (ret == i2cTransferSwFault)
		return -5;
	else return -6;
}


/*
 * Kelvin to Celcius conversion
 * K in 0.1K
 */
float adc_bq27510g2::kToC(uint16_t K)
{
	return ( K/10.0 - 273.15 ) ;
}



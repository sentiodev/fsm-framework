/**
 * @file adc_ltc2990.h
 *
 * Driver: adc_ltc2990
 *
 * Driver for the LTC2990 ADC
 */

#ifndef ADC_LTC2990_H_
#define ADC_LTC2990_H_

#include<stdint.h>
#include "em_i2c.h"

// Platform selection
#if SENTIO_EM_ENABLE
#if CONF_EH_ENABLE || SOLARHARVESTER_ENABLE
#include "sentio_em_io.h"
#define LTC2990_I2C_SDA  POWER_I2C_SDA  ///< I2C DATA pin assignment
#define LTC2990_I2C_SCL  POWER_I2C_SCL  ///< I2C CLK pin assignment
#define LTC2990_I2C      POWER_I2C      ///< I2C module
#define LTC2990_I2C_LOC  POWER_I2C_LOC  ///< I2C location
#elif SMARTLIGHT_ENABLE
#include "smartlight_board_io.h"
#define LTC2990_I2C_SDA  SENSOR_I2C_SDA  ///< I2C DATA pin assignment
#define LTC2990_I2C_SCL  SENSOR_I2C_SCL  ///< I2C CLK pin assignment
#define LTC2990_I2C      SENSOR_I2C      ///< I2C module
#define LTC2990_I2C_LOC  SENSOR_I2C_LOC  ///< I2C location
#else
#error No Extension board enabled
#endif
#else
#error No Sensor Platform enabled
#endif

#define STATUS_ADDR   0x00  ///< Status register address
#define CONTROL_ADDR  0x01  ///< Control register address
#define TRIGGER_ADDR  0x02  ///< Trigger register address
#define TEMP_ADDR     0x04  ///< Temperature register address
#define VCC_ADDR      0x0E  ///< Supply voltage register address

/** Voltage register addresses */
enum REG_VOLTAGE
{
	voltageV1   = 0x06,
	voltageV2   = 0x08,
	voltageV3   = 0x0A,
	voltageV4   = 0x0C
};

/** Current register addresses */
enum REG_CURRENT
{
	currentV12   = 0x06,
	currentV34   = 0x08
};

/** Temperature formats */
enum LTC_TEMP
{
	kelvin  = 0x80,
	celsius = 0x00
};

/** Acquisition modes */
enum LTC_AQUIRE
{
	repeat  = 0x00,
	single  = 0x40
};

/** Operation modes */
enum LTC_MODE
{
	V1V2TR2    = 0x00,
	V1_V2TR2   = 0x01,
	V1_V2V3V4  = 0x02,
	TR1V3V4    = 0x03,
	TR1V3_V4   = 0x04,
	TR1TR2     = 0x05,
	V1_V2V3_V4 = 0x06,
	V1V2V3V4   = 0x07
};

/** LTC2990 I2C addresses */
enum LTC_ADDR
{
	chip0  = 0x98,
	chip1  = 0x9A,
	chip2  = 0x9C,
	chip3  = 0x9E,
	global = 0xEE
};


union LTC_STATUS
{
	struct _LTC_STATUS
	{
		uint8_t zero      : 1;
		uint8_t VCC_Valid : 1;
		uint8_t V4_Vaild  : 1;
		uint8_t V3_Valid  : 1;
		uint8_t V2_Valid  : 1;
		uint8_t V1_Valid  : 1;
		uint8_t TempValid : 1;
		uint8_t Busy      : 1;
	};
	uint8_t array;
};


typedef struct
{
	LTC_TEMP   temperature;
	LTC_MODE   mode;
	LTC_AQUIRE aquire;
	float      shuntV12;
	float      shuntV34;
	LTC_ADDR   address;
} LTC2990_CONFIG;


/**
 * Driver class for the LTC2990 ADC
 *
 * The class contains the interface to interact with the LTC2990 IC.
 */
class adc_ltc2990
{
public:
	adc_ltc2990();
	~adc_ltc2990() {}

	/**
	 * Initializes the interface to the micro-controller.
	 *
	 * The function initializes the I2C communication to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Triggers a reading.
	 *
	 * The function triggers a reading of the LTC2990 according the to the current configuration.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void trigger();

	/**
	 * Sets the configuration.
	 *
	 * The function sets the configuration of the LTC2990. The LTC2990_CONFIG structure (see adc_ltc2990.h) can be used to define a configuration.
	 *
	 * @param[in]  ltcConfig: the configuration to be set
	 * @param[out] none
	 * @return     void
	 */
	void setConfig( const LTC2990_CONFIG ltcConfig );

	/**
	 * Gets the current configuration.
	 *
	 * The function gets the currently set configuration of the LTC2990. The current configuration is saved in a LTC2990_CONFIG structure (see adc_ltc2990.h).
	 *
	 * @param[in]  none
	 * @param[out] ltcConfig: the current configuration
	 * @return     void
	 */
	void getConfig( LTC2990_CONFIG &ltcConfig );

	/**
	 * Gets the current status of the LTC2990.
	 *
	 * The function gets the current status of the LTC2990.
	 *
	 * @param[in]  none
	 * @param[out] status: the status response
	 * @return     function success (true/false)
	 */
	bool getStatus( uint8_t &status );

	/**
	 * Gets the internal chip temperature.
	 *
	 * The function gets the internal temperature of the LTC2990.
	 *
	 * @param[in]  none
	 * @param[out] temperature: the internal temperature in binary format
	 * @return     function success (true/false)
	 */
	bool getTemperature( uint16_t &temperature );

	/**
	 * Gets the supply voltage.
	 *
	 * The function gets the supply voltage of the LTC2990.
	 *
	 * @param[in]  none
	 * @param[out] supply: the supply voltage in binary format
	 * @return     function success (true/false)
	 */
	bool getSupplyVoltage( uint16_t &supply );

	/**
	 * Gets a voltage value.
	 *
	 * The function gets a voltage value from one of the ports of the LTC2990. Calling this function triggers a reading automatically prior to the read out of the value.
	 *
	 * @param[in]  selection: the voltage port to read from
	 * @param[out] voltage: the voltage of the selected port in binary format
	 * @return     function success (true/false)
	 */
	bool getVoltage( uint16_t &voltage, REG_VOLTAGE selection );

	/**
	 * Gets a current value.
	 *
	 * The function gets a current value from one of the differential ports of the LTC2990. Calling this function triggers a reading automatically prior to the read out of the value.
	 *
	 * @param[in]  selection: the current port to read from
	 * @param[out] current: the current of the selected port in binary format
	 * @return     function success (true/false)
	 */
	bool getCurrent( uint16_t &current, REG_CURRENT selection );

	/**
	 * Gets the internal chip temperature.
	 *
	 * The function gets the internal temperature of the LTC2990.
	 *
	 * @param[in]  none
	 * @param[out] temperature: the internal temperature in celsius
	 * @return     function success (true/false)
	 */
	bool getTemperature( float &temperature );

	/**
	 * Gets the supply voltage.
	 *
	 * The function gets the supply voltage of the LTC2990.
	 *
	 * @param[in]  none
	 * @param[out] supply: the supply voltage in volt
	 * @return     function success (true/false)
	 */
	bool getSupplyVoltage( float &supply );

	/**
	 * Gets a voltage value.
	 *
	 * The function gets a voltage value from one of the ports of the LTC2990. Calling this function triggers a reading automatically prior to the read out of the value.
	 *
	 * @param[in]  selection: the voltage port to read from
	 * @param[out] voltage: the voltage of the selected port in volt
	 * @return     function success (true/false)
	 */
	bool getVoltage( float &voltage, REG_VOLTAGE selection );

	/**
	 * Gets a current value.
	 *
	 * The function gets a current value from one of the differential ports of the LTC2990. Calling this function triggers a reading automatically prior to the read out of the value.
	 *
	 * @param[in]  selection: the current port to read from
	 * @param[out] current: the current of the selected port in ampere
	 * @return     function success (true/false)
	 */
	bool getCurrent( float &current, REG_CURRENT selection );
private:
	I2C_Init_TypeDef i2cInit;
	I2C_TransferSeq_TypeDef i2cTransfer;

	uint8_t I2C_ADDRESS_LTC2990;

	uint8_t i2c_txBuffer[2];
	uint8_t i2c_rxBuffer[2];

	float _shuntV12;
	float _shuntV34;

	bool getReading( uint8_t selection );
};

#endif /* ADC_LTC2990_H_ */

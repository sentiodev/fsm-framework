/**
 * Driver: adc_ad7xxx
 *
 * Driver for the Analog Devices AD7xxx Analog-to-digital converters
 */

#include "adc_ad7xxx.h"

adc_ad7xxx::adc_ad7xxx()
{
	CMU_ClockEnable( cmuClock_GPIO, true );
	CMU_ClockEnable( _AD7XXX_USART_CLOCK, true );

	GPIO_PinModeSet( _AD7XXX_SDO_PIN, gpioModeInputPull, 0 );
	GPIO_PinModeSet( _AD7XXX_CLK_PIN, gpioModePushPull, 0 );
	GPIO_PinModeSet( _AD7XXX_CNV_PIN, gpioModePushPull, 0 );

	adcConfig.refVoltage = 2.5;
	adcConfig.daisyChain = 1;
	adcConfig.precision  = prec14bit;
	
	init();
}


void adc_ad7xxx::init()
{
	USART_InitSync_TypeDef  interfaceInit;
	interfaceInit.baudrate  = _AD7XXX_BAUDRATE;
	interfaceInit.databits  = _AD7XXX_DATABITS;
	interfaceInit.enable    = _AD7XXX_ENABLE;
	interfaceInit.refFreq   = _AD7XXX_REFFREQ;
	interfaceInit.master    = _AD7XXX_MASTER;
	interfaceInit.msbf      = _AD7XXX_MSBF;
	interfaceInit.clockMode = _AD7XXX_CLOCK_MODE;

	USART_InitSync( _AD7XXX_USART, &interfaceInit );

	_AD7XXX_USART->ROUTE = USART_ROUTE_RXPEN |
						   USART_ROUTE_CLKPEN | // Enable the SPI-Clock Pin
						   _AD7XXX_LOCATION;
}


void adc_ad7xxx::setConfig( const AD7XXX_CONFIG &config )
{
	adcConfig.refVoltage = config.refVoltage;
	adcConfig.daisyChain = config.daisyChain;
	adcConfig.precision  = config.precision;
}


uint16_t adc_ad7xxx::getValue( float &result )
{
	if ( ( ( _AD7XXX_USART->ROUTE ) & _USART_ROUTE_LOCATION_MASK ) != _AD7XXX_LOCATION )
		init();

	trigger();

	USART_TxDouble( _AD7XXX_USART, 0x0000 );

	result = ( USART_RxDouble( _AD7XXX_USART ) & adcConfig.precision ) * adcConfig.refVoltage / ( adcConfig.precision - 1 );

	return adcConfig.precision;
}


uint16_t adc_ad7xxx::getValue( uint16_t &result )
{
	if ( ( ( _AD7XXX_USART->ROUTE ) & _USART_ROUTE_LOCATION_MASK ) != _AD7XXX_LOCATION )
		init();

	trigger();

	USART_TxDouble( _AD7XXX_USART, 0x0000 );
	result = ( USART_RxDouble( _AD7XXX_USART ) & adcConfig.precision );

	return adcConfig.precision;
}


void adc_ad7xxx::trigger()
{
	GPIO_PinOutSet( _AD7XXX_CNV_PIN );
	for ( volatile uint16_t i = 0; i < 1; i++ );
	GPIO_PinOutClear( _AD7XXX_CNV_PIN );
}
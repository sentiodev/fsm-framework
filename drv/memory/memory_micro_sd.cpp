/**
 * Driver: memory_micro_sd
 *
 * Driver for the microSD card interface
 */

#include "memory_micro_sd.h"


FATFS memory_micro_sd::Fatfs; // File system specific
UINT memory_micro_sd::br, memory_micro_sd::bw; // File read/write count


memory_micro_sd::memory_micro_sd()
{
	init();
}


void memory_micro_sd::init()
{
	MICROSD_init( MCU_CLOCK );
	mount( 0 );
}


bool memory_micro_sd::mount( uint8_t logicalDrive )
{
	if ( f_mount( logicalDrive, &Fatfs ) != FR_OK )
	{
		return false;
	}
	else
	{
		return true;
	}
}


FRESULT memory_micro_sd::writeSD( FIL file, const char* fname, const char* data, const uint16_t numBytes, const bool newLine )
{
	FRESULT res;

	res = f_open( &file, fname,  FA_WRITE );
	if ( res != FR_OK )
	{
		/*  If file does not exist create it*/
		res = f_open( &file, fname, FA_CREATE_ALWAYS | FA_WRITE );
		if ( res != FR_OK )
		{
			return res;
		}
	}

	res = f_lseek( &file, file.fsize );

	if ( res != FR_OK )
	{
		return res;
	}

	f_write( &file, data, numBytes, &bw );

	if ( newLine )
		f_write( &file, "\r\n", 2, &bw );

	f_close( &file );

	return res;
}


FRESULT memory_micro_sd::readSD( FIL file, const char* fname, char* data, const uint16_t numBytes )
{
	FRESULT res;

	res = f_open( &file, fname, FA_READ );
	if ( res != FR_OK )
		return res;

	res = f_lseek( &file, 0 );

	f_read( &file, data, numBytes, &br );

	f_close( &file );

	return res;
}


FRESULT memory_micro_sd::readLineSD( FIL file, const char* fname, char* data, uint16_t* current, const uint16_t maxBytes )
{
	FRESULT res;
	uint16_t i = 0;

	char temp;

	res = f_open( &file, fname, FA_READ );
	if ( res != FR_OK )
		return res;

	res = f_lseek( &file, *current );

	do
	{
		f_read( &file, &temp, 1, &br );
		i++;
	}
	while ( temp != '\r' && temp != '\n' && i <= maxBytes );

	res = f_lseek( &file, *current );

	f_read( &file, data, i - 1, &br );

	*current += i + 1;

	f_close( &file );

	return res;
}


void memory_micro_sd::setLowPowerMode()
{
	power_off();
}




/**
 * @file memory_micro_sd.h
 *
 * Driver: memory_micro_sd
 *
 * Driver for the microSD card interface
 */

#ifndef MEMORY_MICRO_SD_H_
#define MEMORY_MICRO_SD_H_

#include "ff.h"
#include "microsd.h"
#include "diskio.h"

/**
 * Driver class for the microSD card
 *
 * The class contains the interface to interact with the microSD card.
 */
class memory_micro_sd
{
private:
	static FATFS Fatfs; // File system specific
	static UINT br, bw; // File read/write count

public:
	memory_micro_sd();
	~memory_micro_sd() {}

	/**
	 * Initializes the interface to the micro-controller.
	 *
	 * The function initializes the SPI communication to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Mounts the microSD card file system.
	 *
	 * The function mounts the file system of the microSD card.
	 *
	 * @param[in]  logicalDrive:  uint8_t: number of the partition on the flash card
	 * @param[out] none
	 * @return     function success (true/false)
	 */
	bool mount( uint8_t logicalDrive );

	/**
	 * Writes in a file on the microSD card.
	 *
	 * This function appends an data string to the specified file on the SD card if not existed the file will be created.
	 *
	 * @param[in]  fsrc: the file location
	 * @param[in]  fname: the file name
	 * @param[in]  data: the data to be written
	 * @param[in]  numBytes: the number of bytes to be written
	 * @param[in]  newLine: append a newline (true/false)
	 * @param[out] none
	 * @return     FRESULT:  FR_OK (successful read access), otherwise fail to access the card
	 */
	FRESULT writeSD( FIL fsrc, const char* fname, const char* data, const uint16_t numBytes, const bool newLine );

	/**
	 * Reads from a file on the microSD card.
	 *
	 * This function reads the number of bytes specified in (numBytes parameter) from the SD card
	 *
	 * @param[in]  fsrc: the file location
	 * @param[in]  fname: the file name
	 * @param[in]  numBytes: the number of bytes to be read
	 * @param[out] data: the read data
	 * @return     FRESULT:  FR_OK (successful read access), otherwise fail to access the card
	 */
	FRESULT readSD( FIL fsrc, const char* fname, char* data, const uint16_t numBytes );

	/**
	 * Reads a line from a file on the microSD card.
	 *
	 * This function reads an entire line from the SD card. In order to prevent an buffer
	 * overflow it will return when the maxNumber of bytes is read without reaching a line end.
	 *
	 * @param[in]  fsrc: the file location
	 * @param[in]  fname: the file name
	 * @param[in]  current: current cursor position in the file
	 * @param[in]  maxBytes: the maximum number of bytes to be read
	 * @param[out] data: the read data
	 * @return     FRESULT: FR_OK (successful read access), otherwise fail to access the card
	 */
	FRESULT readLineSD( FIL fsrc, const char* fname, char* data, uint16_t* current, const uint16_t maxBytes );

	/**
	 * Enable low power mode.
	 *
	 * The function disconnects the SD card from its power supply in order to conserve energy.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setLowPowerMode();
};

#endif /* MEMORY_MICRO_SD_H_ */

/**
 * Driver: rtc_ds3234
 *
 * Driver for the external RTC DS3234
 */

#include "rtc_ds3234.h"

uint8_t rtc_ds3234::statusReg;
uint8_t rtc_ds3234::controlReg;

rtc_ds3234::rtc_ds3234()
{
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	// Configure the GPIO-Pins which are used to power the RTC (See schematic of the SentioEM3 version 32 or higher: VDD_RTC and VBAT)
	GPIO_PinModeSet( RTC_VBAT_PIN, gpioModePushPull, 1 );
	GPIO_PinModeSet( RTC_VDD_PIN,  gpioModePushPull, 1 );

	// Configure the GPIO Pins used for SPI communication
	// SPI->MOSI
	GPIO_PinModeSet( RTC_TX_PIN, gpioModePushPull, 1 );

	// SPI->MISO
	GPIO_PinModeSet( RTC_RX_PIN, gpioModeInputPull, 1 );

	// SPI->CLK
	GPIO_PinModeSet( RTC_CLK_PIN, gpioModePushPull, 1 );

	// SPI->CS
	GPIO_PinModeSet( RTC_CS_PIN, gpioModePushPull, 1 );

	init();
}


void rtc_ds3234::init()
{
	CMU_ClockEnable( RTC_USART_CLOCK, true );

	// Initialize the Parameters used to configure the USART in SPI-Master mode
	USART_InitSync_TypeDef  interfaceInit;
	interfaceInit.baudrate  = 4000000;
	interfaceInit.clockMode = usartClockMode1;
	interfaceInit.databits  = usartDatabits16;
	interfaceInit.enable    = usartEnable;
	interfaceInit.refFreq   = MCU_CLOCK;
	interfaceInit.master    = true;
	interfaceInit.msbf      = true;

	// Initialize the USART-interface with the settings, which were specified in the Constructor of the RTC_DS3234-Module
	USART_InitSync( RTC_USART, &interfaceInit );

	// Set the Routing Register (Location and used Pins are specified)of the USART used for communication to the DS3234
	RTC_USART->ROUTE = USART_ROUTE_RXPEN |   // Enable the MISO-Pin
					   USART_ROUTE_TXPEN |   // Enable the MOSI-Pin
					   USART_ROUTE_CLKPEN |  // Enable the SPI-Clock Pin
					   USART_ROUTE_CSPEN |   // Enable the SPI-Modules Chip-Select pin
					   RTC_USART_LOC;

	// The Chip-Select is controlled by the USART module automatically, no Software Interaction is required
	RTC_USART->CTRL |= USART_CTRL_AUTOCS;

	//Make sure the Oscillator is stable
	for ( volatile uint32_t i = 0; i < STABLE_WAIT; i++ );
}


void rtc_ds3234::setConfig( RTC_CONFIG rtcConfig )
{
	statusReg = 0x00;

	statusReg |= rtcConfig.keep32KhzRunning;
	statusReg <<= 2;

	statusReg |= ( uint8_t ) rtcConfig.convRate;
	statusReg <<= 1;

	statusReg |= rtcConfig.enable32Khz;
	statusReg <<= 3;

	setSystemRegister( STATUS_REG_ADDR, statusReg );


	uint8_t configReg = 0x00;
	configReg |= ( uint8_t ) rtcConfig.squareWaveFrequency;
	configReg <<= 1;

	configReg |= ( rtcConfig.enableAlarm1 || rtcConfig.enableAlarm2 );
	configReg <<= 1;

	configReg |= rtcConfig.enableAlarm2;
	configReg <<= 1;

	configReg |= rtcConfig.enableAlarm1;

	setSystemRegister( CONTROL_REG_ADDR, configReg );


	if ( rtcConfig.enableAlarm1 || rtcConfig.enableAlarm2 )
	{
		// Configure the Pin PA7 as Input
		GPIO_PinModeSet( RTC_INT_PIN, gpioModeInput, 1 );

		// Configure the Source of an External Interrupt 7 to be PortA and trigger the interrupt on the falling-edge
		GPIO_IntConfig( RTC_INT_PIN, false, true, true );
	}

	// Configure the Pin PB 8 as Input, this pin can be used to drive the EFM32s internal LFXO path
	// (Set the LFXO as clock-source and set the "CMU_CTRL_LFXOMODE_DIGEXTCLK" Bit in the CMU's CTRL-Register)
	// More Information can be found In the EFM32 Reference Manual in the section CMU -> Register Description
	if ( rtcConfig.enable32Khz )
	{
#if LF_CLOCK
		GPIO_PinModeSet( RTC_32KHZ_PIN, gpioModeInput, 1 );
#endif
	}

	// Perform Write-Access to the DS3234-Configuration Register
	setSystemRegister( BAT_BACKED_ADDR, !rtcConfig.enableTempConv );
}


void rtc_ds3234::setLowPowerMode()
{
	// When the VDD_RTC is disconnected the DS3234 reduces the current consumption.
	// Remember that BatteryBacked Operation needs to be enabled.
	GPIO_PinOutClear( RTC_VDD_PIN );
}


uint8_t rtc_ds3234::resetAlarmStatus()
{
	uint8_t intermediate = getSystemRegister( STATUS_REG_ADDR ) & 0x03;
	// The Interrupt Flags are reseted and the DS3234's SWQ/Interrupt-Pin is pulled high again.
	setSystemRegister( STATUS_REG_ADDR, ( statusReg & ~intermediate ) );

	return intermediate;
}


uint8_t rtc_ds3234::getAlarmStatus()
{
	return ( getSystemRegister( STATUS_REG_ADDR ) & 0x03 );
}


void rtc_ds3234::stopAlarm( const ALARM_NUMBER alarm )
{
	time step( 1 );
	setAlarmTime( now() - step, alarm );
}


bool rtc_ds3234::forceManualTempConv()
{
	if ( !( getSystemRegister( STATUS_REG_ADDR ) & 0x03 ) )
	{
		setSystemRegister( CONTROL_REG_ADDR, ( controlReg | 0x20 ) );
		return true;
	}
	else
		return false;
}


void rtc_ds3234::setCrystalAgingOffset( uint8_t agingConfig )
{
	setSystemRegister( AGING_COEF_ADDR, agingConfig );
}


uint8_t rtc_ds3234::getCrystalAgingOffset()
{
	return getSystemRegister( AGING_COEF_ADDR );
}


bool rtc_ds3234::timeValid()
{
	bool temp = ( ( getSystemRegister( STATUS_REG_ADDR ) >> 7 ) & 0x01 );

	setSystemRegister( STATUS_REG_ADDR, ( statusReg & 0x7F ) );

	return temp;
}


void rtc_ds3234::setBaseTime( date systemTime )
{
	// The MS-Bit of the Register 0x05 is always high, ( it contains the information, if we are in the 20 or 21th century )
	// As long as our time-machine is not ordered :-) it is quite useless to make this bit dynamic.
	setSystemRegister( BASETIME_SEC | 0x80, calculateTimeReg( ( systemTime.getSecond() & 0x7F ) ) );
	setSystemRegister( BASETIME_MIN | 0x80, calculateTimeReg( ( systemTime.getMinute() & 0x7F ) ) );
	setSystemRegister( BASETIME_HOUR | 0x80, calculateTimeReg( ( systemTime.getHour() & 0x7F ) ) );

	setSystemRegister( BASETIME_DAY | 0x80, calculateTimeReg( 0x01 ) );
	setSystemRegister( BASETIME_DATE | 0x80, calculateTimeReg( systemTime.getDate() ) );
	setSystemRegister( BASETIME_MONTH | 0x80, calculateTimeReg( systemTime.getMonth() ) | 0x80 );
	setSystemRegister( BASETIME_YEAR | 0x80, ( calculateTimeReg( ( uint8_t ) systemTime.getYear() - 2000 ) ) );

	baseTime = systemTime;
}


void rtc_ds3234::getBaseTime( date& systemTime )
{
	date intermediate( \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_YEAR ) ) ) + 2000,
					   ( MONTH ) calculateInverseTimeReg( ( getSystemRegister( BASETIME_MONTH ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_DATE ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_HOUR ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_MIN ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_SEC ) & 0x7F ) ), \
					   baseTime.getMilliSec() );

	systemTime = intermediate;
}


void rtc_ds3234::getBaseTime( uint8_t& year, uint8_t& month, uint8_t& day, uint8_t& hour, uint8_t& minute, uint8_t& second )
{
	year = getSystemRegister( BASETIME_YEAR );
	month = getSystemRegister( BASETIME_MONTH ) & 0x7F;
	day = getSystemRegister( BASETIME_DATE );
	hour = getSystemRegister( BASETIME_HOUR );
	minute = getSystemRegister( BASETIME_MIN );
	second = getSystemRegister( BASETIME_SEC );
}


date rtc_ds3234::now()
{
	date intermediate( \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_YEAR ) ) ) + 2000,
					   ( MONTH ) calculateInverseTimeReg( ( getSystemRegister( BASETIME_MONTH ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_DATE ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_HOUR ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_MIN ) & 0x7F ) ), \
					   calculateInverseTimeReg( ( getSystemRegister( BASETIME_SEC ) & 0x7F ) ) );

	return intermediate;
}


void rtc_ds3234::setAlarmPeriod( time alarmPeriod, ALARM_NUMBER alarm )
{
	getBaseTime( baseTime );
	baseTime += alarmPeriod;

	switch ( alarm )
	{
	case alarm1:
		setSystemRegister( 0x87, calculateTimeReg( baseTime.getSecond() ) );
		setSystemRegister( 0x88, calculateTimeReg( baseTime.getMinute() ) );
		setSystemRegister( 0x89, ( calculateTimeReg( baseTime.getHour() ) ) );
		setSystemRegister( 0x8A, ( calculateTimeReg( baseTime.getDate() ) ) );
		break;
	case alarm2:
		setSystemRegister( 0x8B, calculateTimeReg( baseTime.getMinute() ) );
		setSystemRegister( 0x8C, ( calculateTimeReg( baseTime.getHour() ) ) );
		setSystemRegister( 0x8D, ( calculateTimeReg( baseTime.getDate() ) ) );
		break;
	default:
		break;
	}
}


void rtc_ds3234::setAlarmTime( date alarmTime, ALARM_NUMBER alarm )
{
	switch ( alarm )
	{
	case alarm1:
		setSystemRegister( 0x87, calculateTimeReg( alarmTime.getSecond() ) );
		setSystemRegister( 0x88, calculateTimeReg( alarmTime.getMinute() ) );
		setSystemRegister( 0x89, ( calculateTimeReg( alarmTime.getHour() | 0x80 ) ) );
		setSystemRegister( 0x8A, ( calculateTimeReg( alarmTime.getDate() | 0x80 ) ) );

		alarmTime1 = alarmTime;
		break;
	case alarm2:
		setSystemRegister( 0x8B, calculateTimeReg( alarmTime.getMinute() ) | 0x80 );
		setSystemRegister( 0x8C, ( calculateTimeReg( alarmTime.getHour() ) | 0x80 ) );
		setSystemRegister( 0x8D, ( calculateTimeReg( alarmTime.getDate() ) | 0x80 ) );

		alarmTime2 = alarmTime;
		break;
	default:
		break;
	}

}


void rtc_ds3234::getAlarmTime( date &alarmTime, ALARM_NUMBER alarm )
{
	if ( alarm == alarm1 )
	{
		date intermediate( \
						   calculateInverseTimeReg( ( getSystemRegister( BASETIME_YEAR ) ) ) + 2000,
						   ( MONTH ) calculateInverseTimeReg( ( getSystemRegister( BASETIME_MONTH ) & 0x7F ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x0A ) & 0x7F ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x09 ) & 0x7F ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x08 ) ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x07 ) ) ), \
						   alarmTime1.getMilliSec() );

		alarmTime = intermediate;
	}
	else
	{
		date intermediate( \
						   calculateInverseTimeReg( ( getSystemRegister( BASETIME_YEAR ) ) ) + 2000,
						   ( MONTH ) calculateInverseTimeReg( ( getSystemRegister( BASETIME_MONTH ) & 0x7F ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x0D ) & 0x7F ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x0C ) & 0x7F ) ), \
						   calculateInverseTimeReg( ( getSystemRegister( 0x0B ) ) ), \
						   alarmTime2.getMilliSec() );

		alarmTime = intermediate;
	}
}


void rtc_ds3234::getAlarmTime( uint8_t& hour, uint8_t& minute, uint8_t& second )
{
	hour = getSystemRegister( 0x09 ) & 0x3F;
	minute = getSystemRegister( 0x08 );
	second = getSystemRegister( 0x07 );
}


void rtc_ds3234::getTemperature( float &temperature )
{
	uint8_t registerValue;

	registerValue = getSystemRegister( TEMP_MSB_ADDR );

	temperature = ( float )( registerValue & 0x7F );

	if ( registerValue & 0x80 )
		temperature *= ( -1 );

	registerValue = getSystemRegister( TEMP_MSB_ADDR );

	if ( registerValue & 0x80 )
		temperature += 0.5;
	if ( registerValue & 0x40 )
		temperature += 0.25;
}


void rtc_ds3234::writeToSRAM( uint8_t address, uint8_t data )
{
	setSystemRegister( SRAM_ADDR, address );
	setSystemRegister( SRAM_DATA, data );
}


uint8_t rtc_ds3234::readFromSRAM( const uint8_t address )
{
	setSystemRegister( SRAM_ADDR, address );
	return getSystemRegister( SRAM_DATA );
}



uint8_t rtc_ds3234::calculateTimeReg( uint8_t timeReg )
{
	// The user-defined time and date information is transfered form a Binary/Hex-Format into a Decimal
	// Data-Format which is used by the DS3234 for representation of Time and Date.
	uint8_t temp = timeReg % 10;
	timeReg = ( temp | ( ( timeReg / 10 ) << 4 ) );

	return timeReg;
}


uint8_t rtc_ds3234::calculateInverseTimeReg( uint8_t timeReg )
{
	uint8_t temp = ( timeReg & 0x0F );

	timeReg >>= 4;
	return ( temp + ( timeReg * 10 ) );
}


uint8_t rtc_ds3234::getSystemRegister( uint8_t address )
{
	if ( !( GPIO_PinInGet( RTC_VDD_PIN ) ) )
	{
		GPIO_PinOutSet( RTC_VDD_PIN );
		for ( volatile uint32_t i = 0; i < STABLE_WAIT; i++ );
	}


	if ( ( ( RTC_USART->ROUTE ) & _USART_ROUTE_LOCATION_MASK ) != RTC_USART_LOC )
		init();

	USART_TxDouble( RTC_USART, ( address << 8 ) );
	while ( !GPIO_PinInGet( RTC_CS_PIN ) );


	return ( uint8_t )USART_RxDouble( RTC_USART );
}


void rtc_ds3234::setSystemRegister( uint8_t address, uint8_t data )
{
	// Make sure the RTC supply is Enabled, when the SPI-Interface is accessed.
	// In case the RTC-VDD
	if ( !( GPIO_PinInGet( RTC_VDD_PIN ) ) )
	{
		GPIO_PinOutSet( RTC_VDD_PIN );
		for ( volatile uint32_t i = 0; i < STABLE_WAIT; i++ );
	}

	if ( ( ( RTC_USART->ROUTE ) & _USART_ROUTE_LOCATION_MASK ) != RTC_USART_LOC )
		init();

	// Transfer the 16-Bit Address and Data-Information to the SPI-Interface
	// Set the Shift the Address-Byte including the Read/Write-Information into the higher 8-Bit of an uint16_t variable
	// and then combine the result with the data to be written to the DS3234.
	USART_TxDouble( RTC_USART, ( ( address | 0x80 ) << 8 ) | data );

	// Wait for the Chip select to go high again, to avoid interferences between two Write-Commands to the DS3234
	// Make the pin selection dynamic to allow different Locations of the USART interface
	while ( !GPIO_PinInGet( RTC_CS_PIN ) );

	USART_RxDouble( RTC_USART );
}

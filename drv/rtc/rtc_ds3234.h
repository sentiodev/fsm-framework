/**
 * @file rtc_ds3234.h
 *
 * Driver: rtc_ds3234
 *
 * Driver for the external RTC DS3234
 */

#ifndef RTC_DS3234_H_
#define RTC_DS3234_H_

#include "mcu_efm32.h"
#include "date.h"
#include "base_system.h"

// Platform selection
#ifdef SENTIO_EM_ENABLE
#define _RTC_32KHZ_PORT    gpioPortB
#define _RTC_32KHZ_PIN     8
#define RTC_32KHZ_PIN      _RTC_32KHZ_PORT,_RTC_32KHZ_PIN

#define _RTC_INT_PORT      gpioPortA
#define _RTC_INT_PIN       7
#define RTC_INT_PIN        _RTC_INT_PORT,_RTC_INT_PIN

#define _RTC_VDD_PORT      gpioPortA
#define _RTC_VDD_PIN       8
#define RTC_VDD_PIN        _RTC_VDD_PORT,_RTC_VDD_PIN

#define _RTC_VBAT_PORT     gpioPortA
#define _RTC_VBAT_PIN      9
#define RTC_VBAT_PIN       _RTC_VBAT_PORT,_RTC_VBAT_PIN

#define _RTC_CLK_PORT      gpioPortC
#define _RTC_CLK_PIN       4
#define RTC_CLK_PIN       _RTC_CLK_PORT,_RTC_CLK_PIN

#define _RTC_CS_PORT       gpioPortC
#define _RTC_CS_PIN        5
#define RTC_CS_PIN         _RTC_CS_PORT,_RTC_CS_PIN

#define _RTC_RX_PORT       gpioPortC
#define _RTC_RX_PIN        3
#define RTC_RX_PIN         _RTC_RX_PORT,_RTC_RX_PIN

#define _RTC_TX_PORT       gpioPortC
#define _RTC_TX_PIN        2
#define RTC_TX_PIN         _RTC_TX_PORT,_RTC_TX_PIN

#define RTC_USART          USART2
#define RTC_USART_CLOCK    cmuClock_USART2
#define RTC_USART_LOC      USART_ROUTE_LOCATION_LOC0

#define RTC_ISR_MASK       0x0080
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef RTC_DS3234_ENABLE
#error Missing define in application_config.h: RTC_DS3234_ENABLE
#endif

// Crystal stabilization time
#define STABLE_WAIT 4000

// System Registers
#define BASETIME_SEC      0x00
#define BASETIME_MIN      0x01
#define BASETIME_HOUR     0x02
#define BASETIME_DAY      0x03
#define BASETIME_DATE     0x04
#define BASETIME_MONTH    0x05
#define BASETIME_YEAR     0x06

#define CONTROL_REG_ADDR  0x0E
#define STATUS_REG_ADDR   0x0F
#define AGING_COEF_ADDR   0x10
#define TEMP_MSB_ADDR     0x11
#define TEMP_LSB_ADDR     0x12
#define BAT_BACKED_ADDR   0x13
#define SRAM_ADDR         0x18
#define SRAM_DATA         0x19

/** Alarm timers */
typedef enum
{
	alarm1 = 0x01,
	alarm2 = 0x02
} ALARM_NUMBER;

/** Output waveforms */
typedef enum
{
	square1Hz    = 0x00,
	square1024Hz = 0x01,
	square4096Hz = 0x02,
	square8192Hz = 0x03
} SQUAREW;

/** Temperature compensation intervals */
typedef enum
{
	seconds64     = 0x00,
	seconds128    = 0x01,
	seconds256    = 0x02,
	seconds512    = 0x03
} TEMP_CONV;


struct RTC_CONFIG
{
	bool      enableAlarm1;
	bool      enableAlarm2;
	bool      enable32Khz;
	bool      keep32KhzRunning;
	bool      enableTempConv;
	TEMP_CONV convRate;
	SQUAREW   squareWaveFrequency;
};


typedef void ( *RTC_USER_INTERRUPT )();

/**
 * Driver class for the DS3234 external RTC
 *
 * The class contains the interface to interract with the external RTC (DS3234).
 */
class rtc_ds3234 : public base_system
{
private:
	static uint8_t statusReg;
	static uint8_t controlReg;

	date baseTime;
	date alarmTime1;
	date alarmTime2;

	static RTC_USER_INTERRUPT interruptAlarm1[MAX_NUM_APPS];
	static RTC_USER_INTERRUPT interruptAlarm2[MAX_NUM_APPS];

	uint8_t calculateTimeReg( uint8_t timeReg );
	uint8_t calculateInverseTimeReg( uint8_t timeReg );

	static void    setSystemRegister( uint8_t address, uint8_t data );
	static uint8_t getSystemRegister( uint8_t address );

public:
	rtc_ds3234();
	~rtc_ds3234() {}

	/**
	 * Initializes the interface to the microcontroller.
	 *
	 * The function initializes the SPI communication to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	static void init();

	/**
	 * Configures the RTC.
	 *
	 * The function configures the RTC with specified parameters. The RTC_CONFIG structure (see rtc_ds3234.h) is used to provide configurations.
	 *
	 * @param[in]  rtcConfig: the configuration to be used
	 * @param[out] none
	 * @return     void
	 */
	void setConfig( const RTC_CONFIG rtcConfig );

	/**
	 * Sets the time for the RTC.
	 *
	 * The function sets the time for the RTC. It uses the system time format (see time.h), which includes year, month, day, weekday, hour, minute and second.
	 *
	 * @param[in]  systemTime: the time to be set
	 * @param[out] none
	 * @return     void
	 */
	void setBaseTime( const date systemTime );

	/**
	 * Gets the time of the RTC.
	 *
	 * The function gets the current system time from the RTC and saves it in a time variable.
	 *
	 * @param[in]  none
	 * @param[out] systemTime: the current time
	 * @return     void
	 */
	void getBaseTime( date& systemTime );

	void getBaseTime( uint8_t& year, uint8_t& month, uint8_t& day, uint8_t& hour, uint8_t& minute, uint8_t& second );

	/**
	 * Returns the current system time.
	 *
	 * The function returns the current system time in time format.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     the current system time
	 */
	date now();

	/**
	 * Sets an alarm relative to the current system time.
	 *
	 * The function enables an alarm relative from the current system time.
	 *
	 * @param[in]  alarmPeriod: the period to wait until the alarm
	 *             alarm: the alarm timer to be used (alarm1 or alarm2)
	 * @param[out] none
	 * @return     void
	 */
	void setAlarmPeriod( const time alarmPeriod, const ALARM_NUMBER alarm );

	/**
	 * Sets an absolute alarm time.
	 *
	 * The function enables an alarm by providing the absolute time when the alarm should occur. In order to trigger an hardware interrupt,
	 * the alarm has to be enabled in the RTC_Config. If not enabled the alarm match will still cause the DS3234's status register to be set.
	 * Thus calling the function getAlarmStatus()/resetAlarmStatus(), will show if an alarm-match occurred.
	 *
	 * @param[in]  alarmTime: the time of the alarm
	 *             alarm: the alarm timer to be used (alarm1 or alarm2)
	 * @param[out] none
	 * @return     none
	 */
	void setAlarmTime( const date alarmTime, const ALARM_NUMBER alarm );


	void getAlarmTime( uint8_t& hour, uint8_t& minute, uint8_t& second );

	/**
	 * Gets the time of an alarm.
	 *
	 * The function gets the time for a currently configured alarm. In order to trigger an hardware interrupt,
	 * the alarm has to be enabled in the RTC_Config. If not enabled the alarm match will still cause the DS3234's status register to be set.
	 * Thus calling the function getAlarmStatus()/resetAlarmStatus(), will show if an alarm-match occurred.
	 *
	 * @param[in]  alarm: the alarm timer to check (alarm1 or alarm2)
	 * @param[out] alarmTime: the time the alarm will execute
	 * @return     void
	 */
	void getAlarmTime( date& alarmTime, const ALARM_NUMBER alarm );

	/**
	 * Stops an alarm timer.
	 *
	 * The function disables the specified alarm timer. The DS3234 Alarm interrupt is not disabled!, simply setting a new alarm time will trigger
	 *
	 * @param[in]  alarm: the alarm timer to deactivate (alarm1 or alarm2)
	 * @param[out] none
	 * @return     void
	 */
	void stopAlarm( const ALARM_NUMBER alarm );

	/**
	 * Sets an alarm relative to the current system time.
	 *
	 * The function enables an alarm relative from the current system time.
	 *
	 * @param[in]  alarmPeriod: the period to wait until the alarm
	 *             alarm: the alarm timer to be used (alarm1 or alarm2)
	 * @param[out] none
	 * @return     none
	 */
	void setAlarm( const time& alarmTime, ALARM_NUMBER alarm );

	/**
	 * Sets an absolute alarm time.
	 *
	 * The function enables an alarm by providing the absolute time when the alarm should occur.
	 *
	 * @param[in]  alarmTime: the time of the alarm
	 *             alarm: the alarm timer to be used (alarm1 or alarm2)
	 * @param[out] none
	 * @return     void
	 */
	void setAlarm( const date& alarmTime, ALARM_NUMBER alarm );

	/**
	 * Return the alarm status.
	 *
	 * This function returns the alarm status which tells weather (alarm1/2/both) got an alarm-match triggered. Calling this function
	 * will not reset the DS3234's status register and will thus not reset the hardware interrupt-pin of the RTC. In order to reset the alarm
	 * use resetAlarmStatus instead.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t alarm status (alarm1: 0x01, alarm2: 0x02)
	 */
	uint8_t getAlarmStatus();

	/**
	 * Resets the alarm status.
	 *
	 * This function returns the alarm status which tells weather (alarm1/2/both) got an alarm-match triggered. Calling this function
	 * will not reset the DS3234's status register and will thus not reset the hardware interrupt-pin of the RTC.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return uint8_t alarm status (alarm1: 0x01, alarm2: 0x02)
	 */
	static uint8_t resetAlarmStatus();

	/**
	 * Sets the external RTC to low power mode.
	 *
	 * The function sets the RTC to low power mode.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setLowPowerMode();

	/**
	 * Force a manual temperature conversion
	 *
	 * The internal temperature compensation mechanism will perform clock adjustment when this function is called.
	 * In case a previous adjustment cycle is not finished, the function will return without triggering an new cycle.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool: adjustment has been triggered/the RTC was busy (no new adjustment triggered)
	 */
	bool forceManualTempConv();

	/**
	 * Check if the current base-time reading is valid
	 *
	 * When the backup power supply of the DS3234 fails the oscillator stops which might result in an invalid system timing. This function
	 * checks the RTC status to see in a power failure causing the oscillator to stop occurred.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool: oscillator has stopped, due to power failure
	 */
	bool timeValid();

	/**
	 * Sets a crystal aging offset.
	 *
	 * The function specifies an aging offset for the crystal.
	 * "The aging offset register takes a user-provided value to add to or subtract from the oscillator capacitor array.
	 * The data is encoded in two’s complement, with bit 7 representing the SIGN bit. One LSB represents the
	 * smallest capacitor to be switched in or out of the capacitance array at the crystal pins. The aging offset
	 * register capacitance value is added or subtracted from the capacitance value that the device calculates for
	 * each temperature compensation." (See DS3234 Datasheet)
	 * @param[in]  agingConfig: the aging offset to be set
	 * @param[out] none
	 * @return     void
	 */
	void setCrystalAgingOffset( const uint8_t agingConfig );

	/**
	 * Returns the currently configured aging offset.
	 *
	 * The function returns the currently configured aging offset for the crystal.
	 * "The aging offset register takes a user-provided value to add to or subtract from the oscillator capacitor array.
	 * The data is encoded in two’s complement, with bit 7 representing the SIGN bit. One LSB represents the
	 * smallest capacitor to be switched in or out of the capacitance array at the crystal pins. The aging offset
	 * register capacitance value is added or subtracted from the capacitance value that the device calculates for
	 * each temperature compensation." (See DS3234 Datasheet)
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     the aging offset
	 */
	uint8_t getCrystalAgingOffset();

	/**
	 * Gets the internal temperature of the RTC.
	 *
	 * The function gets the internal temperature of the external RTC.
	 *
	 * @param[in]  none
	 * @param[out] temperature: the internal temperature in degrees C
	 * @return     void
	 */
	void getTemperature( float& temperature );

	/**
	 * Saves a byte in the RTCs memory.
	 *
	 * The function writes a byte to the internal memory of the RTC.
	 *
	 * @param[in]  address: the address where to write the byte (256x8bit width, address-range: 0x00 to 0xFF)
	* @param[in]  data: the byte to write
	 * @param[out] none
	 * @return     void
	 */
	void writeToSRAM( const uint8_t address, const uint8_t data );

	/**
	 * Read a byte from the RTCs memory.
	 *
	 * The function reads a byte from the internal memory of the RTC.
	 *
	 * @param[in]  address: the address to read from (256x8bit width, address-range: 0x00 to 0xFF)
	 * @param[out] none
	 * @return     the byte value
	 */
	uint8_t readFromSRAM( const uint8_t address );

};

#endif /* RTC_DS3234_H_ */

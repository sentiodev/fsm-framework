/*
 * radio_xbee_802_15_4.cpp
 *
 *  Created on: Dec 06, 2013
 *      Author: florian
 */

#include <string.h>
#include "radio_xbee_802_15_4.h"
#include "application_config.h"
#include "em_emu.h"
#include "em_cmu.h"


#ifdef SENTIO_EM_ENABLE
#include <sentio_em_io.h>

#define XBEE_UART              RADIO_USART
#define XBEE_CTS			   DEBUG_PIN_2

#define maskInterruptXbeeRadio      0x1000
#else
#error No Sensor Platform defined
#endif


#ifndef RADIO_XBEE_802_15_4_ENABLE
#error Missing define in application_config.h: RADIO_XBEE_802_15_4_ENABLE
#endif



/****************************************************************************************************************************************//**
 * @brief
 *  Declaration and Initialization of static members of the radio_xbee_802_15_4 driver-class
 *
 *******************************************************************************************************************************************/


//
//uint8_t radio_xbee_802_15_4::bufferCount;
//uint8_t radio_xbee_802_15_4::responseStatus;
//volatile bool    radio_xbee_802_15_4::responseReceived;
//
//LENGTH radio_xbee_802_15_4::temp;
//FRAMES radio_xbee_802_15_4::frame;



/****************************************************************************************************************************************//**
 * @brief
 *  Basic System-Variables are set-up, which do not need to be changed during Run-Time
 *
 * @details
 * The Parameterization of the Serial-Interface and the Selection of the Serial-Interface as well as the selection of Interface is done here!
 *
 * @note
 * The User should not change the Initialization-Values for System-Variables, which are not related to the Hardware-Interfaces
 *******************************************************************************************************************************************/

radio_xbee_802_15_4::radio_xbee_802_15_4()
    : radio_xbee()
{
    //****************************************************************************************************************************************
    //    !! NOTE !!  Do not change the Variables below
    //****************************************************************************************************************************************
    // Initialization of some Auxiliary Variables used in the XBee-driver module
    frame.atCommand.startDelimiter = 0x7E;

    responseReceived  = false; // Has the Command response frame been received?
    packetPending     = 0;     // Is the internal buffer of the Driver module empty?
    packetTransmitted = true;  // data-Packet Transmission finalized and Transmit-Response-frame was received.
    packetReceived    = false; // Has a data-Packet been received on the radio channel?
}


ptISR_Handler radio_xbee_802_15_4::getISR_FunctionPointer()
{
    return &wrapperXBeeUartRx;
}


/****************************************************************************************************************************************//**
 * @brief
 *  Send a data-Packet as unicast or broadcast message on the RF-channel
 *
 * @details
 * The user can send a data-packet with a specific payload length to the Xbee-Module, which is specified by the 64-bit MAC-address
 * given in the in the destination-address array. The transfer is timed and performed automatically by the 802.15.4 firmware. After
 * the transmission is perfomed or failed after a defined numbers ofretries, the XBee-module responds with a status-frame. The content
 * is available by calling the following driver-methods:
 * getDeliveryStatus(), getDiscoveryStatus(), getDiscoveryStatus()
 *
 * @param[in] data
 * Specifies the payload of the data-packet that is send on the RF-channel
 *
 * @param[in] payloadLength
 * Specifies the length of the payload
 *
 * @param[in] destinationAddress
 * Pointer to an uint8_t Array[8], which will contain the 64-bit source-address after a packet has been received on the RF-channel.
 * destinationAddress[0] contains Address-msb
 * destinationAddress[7] contains Address-lsb
 *
 * @param[in] broadcast
 * Transmission of Broadcast Messages
 *
 * @param[in] acknowledge
 * Enable/disable acknowledge
 *
 * @return
 * - True:  Packet has been transfered to the XBee-Module
 * - False: Previous Packet Transfer was not finalized, Module busy
 *
 *******************************************************************************************************************************************/

bool radio_xbee_802_15_4::sendPacket( uint8_t *data, uint8_t payloadLength, uint8_t broadcast, bool acknowledge )
{
    if ( packetTransmitted )
    {
        // Indicate Packet Transmission on-going
        packetTransmitted = false;

		uint8_t *destinationAddress = &receiverAddress[0];

        // Calculate the length of the API-frame without delimiter, length and checksum
        temp.length = 0x0B + payloadLength;

        frame.transmitPacket64.lengthMsb = temp.reg.msb;
        frame.transmitPacket64.lengthLsb = temp.reg.lsb;

        frame.transmitPacket64.frameType = 0x00;
        frame.transmitPacket64.frameId   = 0x53;

        // Copy the user-defined destination-address to the API-frame buffer
        memcpy( frame.transmitPacket64.destinationAddress64, destinationAddress, 8 );

        frame.transmitPacket64.options = ( ( uint8_t ) broadcast ) << 2 | ( ( ( uint8_t ) !acknowledge ) );

        // Copy the user-defined packet-payload to the API-frame buffer
        for ( volatile uint8_t i = 0; i < payloadLength; i++ )
            frame.transmitPacket64.radioData[i] = data[i];

        // The API-frame is send to the XBee-Module via the configured UART-interface
        for ( uint8_t i = 0; i <= ( temp.length + 2 ); i++ )
        {
            USART_Tx( XBEE_UART, frame.array[i] );
        }

        // When all data-Bytes are send out, the checksum is calculated and will be transfered at the END of the API-frame
        USART_Tx( XBEE_UART, processingChecksum() );

        // Wait in energy mode 1 for the complete reception of the XBee-module response and the processing of the Response-frame
        do
        {
            EMU_EnterEM1();
        } while ( !packetTransmitted );   // leave the loop when the Response-frame is received or if a
        // invalid API-frame or invalid Checksum is received

        return true;
    }

    else
        return false;
}


/****************************************************************************************************************************************//**
 * @brief
 *  The driver-method returns true, when the a packet has been received on the RF-channel
 *
 * @details
 * After the packet has been received, its payload is copied to the user-defined buffer, which has to be long enough
 * to fit the longest payload that is expected in the network.
 *
 * @return
 * - True:  packet is received by the XBee-Module
 * - False: no new packet is available in the buffer
 *
 *******************************************************************************************************************************************/

bool radio_xbee_802_15_4::getPacketReceived()
{
    if ( packetReceived )
    {
        // Reset the driver-status flag to allow an new packet to be transfered
        packetReceived = false;
        return true;
    }

    return false;
}


/****************************************************************************************************************************************//**
 * @brief
 *  When a packet-payload is not copied to the user--defined buffer. By calling this driver-method the user can copy the payload form the
 *  driver- to the applications-buffer "manually"
 *
 * @details
 * When a second packet is transfered to the XBee-Module before the application-code has processed the first packet, the user-defined buffer
 * is marked as not empty by an internal status flag of the driver. The payload of the new packet is kept in the drivers internal buffer and
 * will stay there until the user initiates the transfer or until a new API-frame is transfered by the XBee-Module
 *
 * @return
 * - True:  packet is received by the XBee-Module
 * - False: more than one packet was pending, just the last received packet is still available
 *
 *******************************************************************************************************************************************/

bool radio_xbee_802_15_4::getPendingPacketData()
{
    // Copy the data-Payload to the user-defined-buffer
    memcpy( userDataBuffer, frame.receivePacket64.receiveData, ( temp.length - 12 ) );

    // The msb is not contained in RF-Packet, due to the fact that it is always expected to be 0x00,
    // therefore set the byte manually
    *userSourceAddress = 0x00 ;

    // Copy the 7-Byte remaining Bytes from their position inside the API-frame-Buffer to the user-defined buffer
    memcpy( userSourceAddress + 1, frame.receivePacket64.sourceAdress64, 7 );

    // Pick the Status information from the correct position inside the frame and make
    // it available for the get-method()
    receiveOptions = frame.receivePacket64.options;

    // Indication of a pending packet in side the buffer,
    // a packetPending value of > 1 indicates a dropped packet!
    if ( packetPending <= 1 )
        return true;

    return false;
}



/****************************************************************************************************************************************//**
 * @brief
 * This method queries the MAC-Address of the locally attached XBee-Module.
 *
 * @param[in] (type: MAC_XBee) Address
 *
 * @return responseStatus
 * Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3), Driver related Error (0xFF)
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::getLocalXBeeMAC( MAC_XBee address )
{
    uint8_t i;
    uint8_t errorCount = 0;

    uint32_t temporary  = 0;
    uint32_t temporary2 = 0;

    errorCount += configRegisterAccess( serialNumberLow, temporary);

    for ( i = 7; i >= 4; i-- )
    {
        address[i] = ( uint8_t )temporary;
        temporary >>= 8;
    }

    errorCount += configRegisterAccess( serialNumberHigh, temporary2);

    for ( i = 3; i > 0; i-- )
    {
        address[i] = ( uint8_t )temporary2;
        temporary2 >>= 8;
    }

    return errorCount;
}


/****************************************************************************************************************************************//**
 * @brief
 *
 * @return responseStatus
 *  Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3), Driver related Error (0xFF)
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::getPAN_ID( uint16_t& pan_Id )
{
    return configRegisterAccess( panId, pan_Id );
}


/****************************************************************************************************************************************//**
 * @brief
 *
 * @return responseStatus
 *  Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3), Driver related Error (0xFF)
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::setPAN_ID( uint16_t pan_Id )
{
    //TODO: Check user Input valid?
    return configRegisterAccess( panId, pan_Id, false, true, true );
}


/****************************************************************************************************************************************//**
 * @brief
 *
 * @return responseStatus
 *  Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3), Driver related Error (0xFF)
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::getChannel( uint8_t &channelNumber )
{
    return configRegisterAccess( channel, channelNumber );
}


/****************************************************************************************************************************************//**
 * @brief
 *
 * @return responseStatus
 *  Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3), Driver related Error (0xFF)
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::setChannel( uint8_t channelNumber )
{
    return configRegisterAccess( channel, channelNumber, false, true, true );
}



/****************************************************************************************************************************************//**
 * @brief
 *                     !!! NOTE !!!
 *  Do not call this function within the Application-Code
 *
 * @details
 *  This function is called within the Interrupt Service Routine of the UART/UASRT Interface that is utilized to drive the XBee-Module. This
 *  interrupt routine is triggered, when the RXDATAV - interrupt occurs. The first steps in the processing of the API-Response frame is done.
 *
 * @param[in] inputChar
 *  Within the ISR the first byte of the UART/USART-RX Buffer is readout and transfered to this function for further processing
 *
 *******************************************************************************************************************************************/

void radio_xbee_802_15_4::wrapperXBeeUartRx( uint32_t inputChar )
{
    switch ( bufferCount )
    {
    // Verify the first byte, which is transfered to the USART. In this communication-cycle the expected Start-Delimiter is 0x7E
    // Otherwise stop reception of the frame.
    case 0:
        if ( ( uint8_t )inputChar != 0x7E ) // API-Response frame wrong
        {
            responseReceived = true;
            responseStatus = 0xFF;
            bufferCount = 0;
        }
        else
            bufferCount++;
        break;

    // Process the frame-length Information, send by the XBee-Module
    // The Union named temp is used to assign the msb/lsb information to an uint16_t
    case 1:
        temp.reg.msb = ( uint8_t )inputChar;
        bufferCount++;
        break;

    case 2:
        temp.reg.lsb = ( uint8_t )inputChar;
        bufferCount++;
        break;

    default:
        // Fill the SRAM-Buffer with the data-Bytes of the frame currently received on the UART
        if ( bufferCount < ( temp.length + 3 ) )
        {
            frame.array[bufferCount] = ( uint8_t )inputChar;
            bufferCount++;
        }

        // Reception of the data is completed, now the XBee-Module's Response-frame is processed.
        // Status-variables are set to default, to enable the reception of a new API-frame from the XBee-Module
        else
        {
            bufferCount = 0;
            receivedFrameChecksum = ( uint8_t )inputChar;
            responseStatus = processingResponse();
        }
    }
}



/****************************************************************************************************************************************//**
 * @brief
 *  Used in the Interrupt Service Routine !!!
 *  This method distinguishes the type of the Response frame, which was transfered by the XBee-Module to the MCU, and calls the dedicated
 *  method that is used to process the specific type of received frame
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::processingResponse()
{

    switch ( frame.atCommand.frameType )
    {
    case atCommandResponseFrame:
        return processingCommandResponse();

    case modemStatusFrame:
        processingModemStatusFrame();
        break;

    case transmitStatusFrame:
        processingTransmitStatus();
        break;

    case receivePacket64Frame:
        processingReceivePacket();
        break;

    default:
        return 0xFF;
        break;
    }

    // return value, which indicates that the frame is invalid!
    return 0xFE;

}


/****************************************************************************************************************************************//**
 * @brief
 *  Used in the Interrupt Service Routine !!!
 *  This function is called to process a Command Response API-frame, received on the UART/USART interface.
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::processingCommandResponse()
{

    // Check for a valid checksum of the received CommandResponse API-frame
    if ( ( processingChecksum() == receivedFrameChecksum ) )
    {
        // Set driver status flag to indicate the finalization of reception and processing of the status-frame
        responseReceived = true;

        return frame.atCommandResponse.status;
    }
    else
        // Indicate an invalid checksum
        return 0xFF;
}


/****************************************************************************************************************************************//**
 * @brief
 *  Used in the Interrupt Service Routine !!!
 *  This function is called to process a Modem Status API-frame, received on the UART/USART interface.
 *
 *******************************************************************************************************************************************/

void radio_xbee_802_15_4::processingModemStatusFrame()
{
    // Check for a valid checksum of the received ModemStatus API-frame
    if ( processingChecksum() == receivedFrameChecksum )
    {
        // Pick the Status information from the correct position inside the frame and make
        // it available for the get-method()
        modemStatus = frame.modemStatus.status;
    }
    else
        // Indicate an invalid checksum
        modemStatus = 0xFF;
}


/****************************************************************************************************************************************//**
 * @brief
 *  Used in the Interrupt Service Routine !!!
 *  This function is called to process a Transmit Response API-frame, received on the UART/USART interface.
 *
 *******************************************************************************************************************************************/

void radio_xbee_802_15_4::processingTransmitStatus()
{
    // Check for a valid checksum of the received transmitStatus API-frame
    if ( processingChecksum() == receivedFrameChecksum )
    {
        // Pick the Status information from the correct position inside the frame and make
        // it available for the get-methods()
        transmitStatus = frame.transmitStatus.status;

        // Set driver status flag to indicate the finalization of reception and processing of the status-frame
        packetTransmitted = true;
    }
    else
    {
        // Indicate an invalid Checksum
        transmitStatus = 0xFF;
    }
}


/****************************************************************************************************************************************//**
 * @brief
 *  Used in the Interrupt Service Routine !!!
 *  This function is called to process a Command Response API-frame, received on the UART/USART interface.
 *
 *******************************************************************************************************************************************/

void radio_xbee_802_15_4::processingReceivePacket()
{
    // Check for a valid checksum of the received receivePacket64 API-frame
    if ( ( processingChecksum() == receivedFrameChecksum ) && !packetReceived )
    {
        // Calculate and store the Payload-length of the received Packet
        *userPayloadLength = temp.length - 12;

        // Copy the data-Packets Payload to the user-defined buffer
        memcpy( userDataBuffer, frame.receivePacket64.receiveData, ( *userPayloadLength ) );

        // The msb is not contained in RF-Packet, due to the fact that it is always expected to be 0x00,
        // therefore set the byte manually
        *userSourceAddress = 0x00 ;

        // Copy the 7-Byte remaining Bytes from their position inside the API-frame-Buffer to the user-defined buffer
        memcpy( userSourceAddress + 1, frame.receivePacket64.sourceAdress64, 7 );

        // Pick the Status information from the correct position inside the frame and make
        // it available for the get-method()
        receiveOptions = frame.receivePacket64.options;

        // Set driver status flag to indicate the finalization of reception and processing of the status-frame
        packetReceived = true;
    }
    else
    {
        // Indication of a pending packet in sidethe buffer,
        // a packetPending value of > 1 indicates a dropped packet!
        packetPending++;
    }
}



/****************************************************************************************************************************************//**
 * @brief
 *
 * @return responseStatus
 *  Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3), (>3 && <=18) more than one register-access failed,
 *  Driver related Error (0xFF)
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee_802_15_4::getConfig( CONFIG& configuration )
{
    uint8_t errorCount = 0;

    errorCount += configRegisterAccess( destinationAddressHigh, configuration.destinationAddressHigh);
    errorCount += configRegisterAccess( destinationAddressLow, configuration.destinationAddressLow);

    errorCount += configRegisterAccess( serialNumberHigh, configuration.serialNumberHigh);
    errorCount += configRegisterAccess( serialNumberLow, configuration.serialNumberLow);

    errorCount += configRegisterAccess( panId, configuration.panId);
    errorCount += configRegisterAccess( xbeeRetries, configuration.xbeeRetries);
    errorCount += configRegisterAccess( macMode, configuration.macMode);

    errorCount += configRegisterAccess( powerLevel, configuration.powerLevel);

    errorCount += configRegisterAccess( coordinatorEnable, configuration.coordinatorEnable);
    errorCount += configRegisterAccess( receivedSignalStrength, configuration.receivedSignalStrength);
    errorCount += configRegisterAccess( hardwareVersion, configuration.hardwareVersion);
    errorCount += configRegisterAccess( firmwareVersion, configuration.firmwareVersion);
    errorCount += configRegisterAccess( ackFailures, configuration.ackFailures);

    errorCount += configRegisterAccess( sleepMode, configuration.sleepMode );
    errorCount += configRegisterAccess( sleepOptions, configuration.sleepOptions );
    errorCount += configRegisterAccess( timeBeforeSleep, configuration.timeBeforeSleep );
    errorCount += configRegisterAccess( cyclicSleepPeriod, configuration.cyclicSleepPeriod );

    return errorCount;
}



uint8_t radio_xbee_802_15_4::setConfig( CONFIG configuration )
{

    uint8_t errorCount = 0;

    errorCount += configRegisterAccess( destinationAddressHigh, configuration.destinationAddressHigh, false, true, true );

    errorCount += configRegisterAccess( destinationAddressLow, configuration.destinationAddressLow, false, true, true );

    errorCount += configRegisterAccess( panId, configuration.panId, false, true, true );

    if(  configuration.xbeeRetries <= 0x06 ) {
        errorCount += configRegisterAccess( xbeeRetries, configuration.xbeeRetries, false, true, true );
    }
    else return 0xFF;

    if(  configuration.macMode <= 0x03 ) {
        errorCount += configRegisterAccess( macMode, configuration.macMode, false, true, true );
    }
    else return 0xFF;


    if(  configuration.powerLevel <= 0x04 ) {
        errorCount += configRegisterAccess( powerLevel, configuration.powerLevel, false, true, true );
    }
    else return 0xFF;


    if(  configuration.coordinatorEnable <= 0x01 ) {
        errorCount += configRegisterAccess( coordinatorEnable, configuration.coordinatorEnable, false, true, true );
    }
    else return 0xFF;

    errorCount += configRegisterAccess( ackFailures, configuration.ackFailures, false, true, true );

    if(  configuration.sleepMode <= 0x05 ) {
        errorCount += configRegisterAccess( sleepMode, configuration.sleepMode, false, true, true );
    }
    else return 0xFF;

    if( ( configuration.sleepOptions != 0x03 ) && ( configuration.sleepOptions < 0x05 ) ) {
        errorCount += configRegisterAccess( sleepOptions, configuration.sleepOptions, false, true, true );
    }
    else return 0xFF;

    if( configuration.timeBeforeSleep > 0x00 ) {
        errorCount += configRegisterAccess( timeBeforeSleep, configuration.sleepOptions, false, true, true );
    }
    else return 0xFF;

    if( configuration.cyclicSleepPeriod <= 0x68B0 ) {
        errorCount += configRegisterAccess( cyclicSleepPeriod, configuration.cyclicSleepPeriod, false, true, true );
    }
    else return 0xFF;

    return errorCount;
}

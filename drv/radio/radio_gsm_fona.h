/***************************************************
  This is a library for our Adafruit FONA Cellular Module

  Designed specifically to work with the Adafruit FONA
  ----> http://www.adafruit.com/products/1946
  ----> http://www.adafruit.com/products/1963

  These displays use TTL Serial to communicate, 2 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "time_delay.h"
#include "date.h"

#include "em_gpio.h"
#include "em_usart.h"
#include "em_cmu.h"


// Platform selection
#if SENTIO_EM_ENABLE

#if OIL_IN_WATER_BOARD_ENABLE
#include "oil_in_water_board_io.h"

#define GSM_FONA_TX_PIN          GSM_TX_PIN
#define GSM_FONA_RX_PIN          GSM_RX_PIN
#define GSM_FONA_RSTN_PIN        GSM_RSTN_PIN
#define GSM_FONA_PWR_PIN         GSM_PWR_PIN
#define GSM_FONA_PWR_STATUS_PIN  GSM_PWR_STATUS_PIN
#define GSM_FONA_USART           GSM_UART
#define GSM_FONA_USART_CLOCK     GSM_USART_CLOCK
#define GSM_FONA_LOCATION        GSM_LOCATION
#define GSM_FONA_BAUDRATE        115200
#define GSM_FONA_REF_FREQ        MCU_CLOCK

#else
#include "sentio_em_io.h"

#define GSM_FONA_TX_PIN          SENSOR_UART_TX
#define GSM_FONA_RX_PIN          SENSOR_UART_RX
#define GSM_FONA_RSTN_PIN        SENSOR_PIN_1
#define GSM_FONA_PWR_PIN         SENSOR_PIN_2
#define GSM_FONA_PWR_STATUS_PIN  SENSOR_PIN_3
#define GSM_FONA_USART           SENSOR_UART
#define GSM_FONA_USART_CLOCK     SENSOR_UART_CLOCK
#define GSM_FONA_LOCATION        SENSOR_UART_LOC
#define GSM_FONA_BAUDRATE        115200
#define GSM_FONA_REF_FREQ        MCU_CLOCK
#endif

#else
#error No Sensor Platform defined
#endif

#define FONA_HEADSETAUDIO        0
#define FONA_EXTAUDIO            1

#define FONA_STTONE_DIALTONE     1
#define FONA_STTONE_BUSY         2
#define FONA_STTONE_CONGESTION   3
#define FONA_STTONE_PATHACK      4
#define FONA_STTONE_DROPPED      5
#define FONA_STTONE_ERROR        6
#define FONA_STTONE_CALLWAIT     7
#define FONA_STTONE_RINGING      8
#define FONA_STTONE_BEEP         16
#define FONA_STTONE_POSTONE      17
#define FONA_STTONE_ERRTONE      18
#define FONA_STTONE_INDIANDIALTONE 19
#define FONA_STTONE_USADIALTONE  20

#define FONA_DEFAULT_TIMEOUT_MS  9000000

#define prog_char const char


typedef const char SIM_PIN [5];


class radio_gsm_fona
{
public:
	radio_gsm_fona();

	void power( bool powerStatus );
	bool init();
	bool reset();

// Battery and ADC
	bool getADCVoltage( uint16_t *v );
	bool getBattPercent( uint16_t *p );
	uint8_t getBattVoltage( uint16_t *v );

// SIM query
	uint8_t unlockSim( SIM_PIN pin );
	uint8_t getSimCcid( char *ccid );
	uint8_t getNetworkStatus( void );
	uint8_t getRssi( void );

// IMEI
	uint8_t getImei( char *imei );

// SMS handling
	int8_t getNumSms( void );
	bool readSms( uint8_t i, char *smsbuff, uint16_t max, uint16_t *readsize );
	uint8_t sendSms( char *smsaddr, char *smsmsg );
	bool deleteSms( uint8_t i );
	bool getSmsSender( uint8_t i, char *sender, int senderlen );

// GPRS handling
	int8_t enableGPRS( bool onoff, const char *apn = 0, const char *username = 0, const char *password = 0 );
	uint8_t GPRSstate( void );
	bool getGSMLoc( uint16_t *replycode, char *buff, uint16_t maxlen );
//  void setGPRSNetworkSettings(  );

	int8_t openIpConnection( const char*, const char*, const char* );
	int8_t closeIpConnection();
	int8_t sendIpPacket( const char* data, const uint16_t length );
// // HTTP
//  bool HTTP_GET_start( char *url, uint16_t *status, uint16_t *datalen );
//  void HTTP_GET_end( void );
//  bool HTTP_POST_start( char *url, const char *contenttype, const uint8_t *postdata, uint16_t postdatalen,  uint16_t *status, uint16_t *datalen );
//  void HTTP_POST_end( void );
//  void setUserAgent( const char *useragent );

// HTTPS
	void setHTTPSRedirect( bool onoff );

// PWM (buzzer)
	bool PWM( uint16_t period, uint8_t duty = 50 );

// Time
	bool enableNetworkTimeSync( bool onoff );
	bool enableNTPTimeSync( bool onoff, const char *ntpserver = 0 );
	bool getTime( char *buff, uint16_t maxlen );
	bool getTime( date& localTime );

// RTC
	bool enableRTC( uint8_t i );
	bool readRTC( uint8_t *year, uint8_t *month, uint8_t *date, uint8_t *hr, uint8_t *min, uint8_t *sec );

private:
	static time_delay              delay;

	char replybuffer[255];
	bool httpsredirect;
	const char *useragent;

// HTTP helpers
	bool HTTP_initialize( char *url );
	bool HTTP_response( uint16_t *status, uint16_t *datalen );
	void HTTP_terminate( void );

	void flushInput();
	uint16_t readRaw( uint16_t b );
	uint8_t readline( uint32_t timeout = FONA_DEFAULT_TIMEOUT_MS, bool multiline = false );
	uint8_t getReply( char *send, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	uint8_t getReply( const char *send, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	uint8_t getReply( const char *prefix, char *suffix, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	uint8_t getReply( const char *prefix, int32_t suffix, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	uint8_t getReply( const char *prefix, int32_t suffix1, int32_t suffix2, uint16_t timeout ); // Don't set default value or else function call is ambiguous.
	uint8_t getReplyQuoted( const char *prefix, const char *suffix, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );

	bool sendCheckReply( const char *send, const char *reply, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	bool sendCheckReply( const char *prefix, char *suffix, const char *reply, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	bool sendCheckReply( const char *prefix, int32_t suffix, const char *reply, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	bool sendCheckReply( const char *prefix, int32_t suffix, int32_t suffix2, const char *reply, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );
	bool sendCheckReplyQuoted( const char *prefix, const char *suffix, const char *reply, uint16_t timeout = FONA_DEFAULT_TIMEOUT_MS );


	bool parseReply( const char *toreply, uint16_t *v, char divider  = ',', uint8_t index = 0 );
	bool parseReply( const char *toreply, char *v, char divider  = ',', uint8_t index = 0 );
	bool parseReplyQuoted( const char *toreply, char *v, int maxlen, char divider, uint8_t index );

	uint8_t sendParseReply( const char *tosend, const char *toreply, uint16_t *v, char divider = ',', uint8_t index = 0 );

	void print( const char send );
	void print( const char* send );
	void printLn( const char send );
	void printLn( const char* send );
	void printHex( int32_t input );
	void printDec( int32_t input );

	uint16_t  min( uint16_t a, uint16_t b );
	void write( const uint8_t* send, uint16_t sendLength );

// Stream
	int available( void );
	int write( uint8_t x );
	int read( void );
	int peek( void );
	void flush();

};

/**
 * Driver: radio_at86rf212b
 *
 * Driver Atmel AT86RF212B radio transceiver
 */

#include "radio_at86rf212b.h"


radio_at86rf212b::radio_at86rf212b()
{
	CMU_ClockEnable( cmuClock_GPIO, true );

	GPIO_PinModeSet( AT86RF212B_SPI_MOSI_PIN, gpioModePushPull, 1 );
	GPIO_PinModeSet( AT86RF212B_SPI_MISO_PIN, gpioModeInput, 1 );
	GPIO_PinModeSet( AT86RF212B_SPI_CLK_PIN, gpioModePushPull, 1 );
	GPIO_PinModeSet( AT86RF212B_SPI_CS_PIN, gpioModePushPull, 1 );

	GPIO_PinModeSet( AT86RF212B_SLP_TR, gpioModePushPull, 0 );

	GPIO_PinModeSet( AT86RF212B_IRQ, gpioModeInput, 0 );
	GPIO_PinModeSet( AT86RF212B_RSTN, gpioModePushPull, 1 );

	init();
}


void radio_at86rf212b::init()
{
	CMU_ClockEnable( cmuClock_USART0, true );

	interfaceInit.baudrate  = AT86RF212B_BAUDRATE;
	interfaceInit.databits  = AT86RF212B_DATABITS;
	interfaceInit.enable    = AT86RF212B_ENABLE;
	interfaceInit.refFreq   = AT86RF212B_REF_FREQ;
	interfaceInit.master    = AT86RF212B_MASTER;
	interfaceInit.msbf      = AT86RF212B_MSBF;
	interfaceInit.clockMode = AT86RF212B_CLOCK_MODE;

	USART_InitSync( AT86RF212B_USART, &interfaceInit );

	AT86RF212B_USART->ROUTE = USART_ROUTE_RXPEN  | // Enable the MISO-Pin
							  USART_ROUTE_TXPEN  | // Enable the MOSI-Pin
							  USART_ROUTE_CLKPEN | // Enable the SPI-Clock Pin
							  AT86RF212B_LOCATION;
}


void radio_at86rf212b::setConfig()
{
	AT86RF212B_CONFIG configDefault;

	configDefault.trxCtrl0.reg.clkMCtrl     = clockOff;
	configDefault.trxCtrl0.reg.clkMShaSel   = false;
	configDefault.trxCtrl0.reg.padIo        = current2mA;
	configDefault.trxCtrl0.reg.padIoClkM    = current2mA;

	configDefault.trxCtrl1.reg.extPaEnable  = false;
	configDefault.trxCtrl1.reg.extIrqEnable = true;
	configDefault.trxCtrl1.reg.txAutoCrcOn  = true;
	configDefault.trxCtrl1.reg.rxBlRtrl     = false;
	configDefault.trxCtrl1.reg.spiCmdMode   = monitorIrqStatus;
	configDefault.trxCtrl1.reg.irqMaskMode  = 1;
	configDefault.trxCtrl1.reg.irqPolarity  = 0;

	configDefault.phyTxPwr                  = dbm11;

	configDefault.phyCcCca.reg.channel      = ieee868Mhz;
	configDefault.phyCcCca.reg.ccaMode      = csOrEnergyAboveTh;
	configDefault.phyCcCca.reg.ccaRequest   = false;

	configDefault.ccaThres.reg.ccaEdThres   = 0x0F;
	configDefault.ccaThres.reg.ccaCsThres   = 0x0F;

	configDefault.clockJitterModule         = jcmEnable;

	configDefault.sdvValue                  = 0xA7;   // Start-of-Frame Delimiter (0xA7 is default for 802.15.4)

	configDefault.trxCtrl2.reg.rxSaveMode        = true;
	configDefault.trxCtrl2.reg.trxOffAvddEnable  = false;
	configDefault.trxCtrl2.reg.oQqpskScramEnable = true;
	configDefault.trxCtrl2.reg.altSpectrum       = true;
	configDefault.trxCtrl2.reg.bpskOqpsk         = true;
	configDefault.trxCtrl2.reg.subMode           = true;
	configDefault.trxCtrl2.reg.oQpskDataRate     = low400high1000;

	configDefault.xoscCtrl.reg.xtalMode      = internalOsc;
	configDefault.xoscCtrl.reg.xtalTrim      = 0x02;

	configDefault.ccNumber                   = 0x00;

	configDefault.ccCtrl1.reg.ccBand         = 0x00;

	configDefault.rxSyn.reg.rxPathLevel      = 0x00;
	configDefault.rxSyn.reg.rxOverride       = allDisabled;
	configDefault.rxSyn.reg.rxPathDisable    = false;

	configDefault.rfCtrl.reg.gainCalibOffset = offset2dbm;
	configDefault.rfCtrl.reg.paLeadTime      = paLeadTime2us;

	configDefault.xahCtrl1.reg.promiscuousMode           = false;
	configDefault.xahCtrl1.reg.ackResponseTime           = false;
	configDefault.xahCtrl1.reg.uploadReservedFrameTypes  = true;
	configDefault.xahCtrl1.reg.filterReservedFrameTypes  = true;
	configDefault.xahCtrl1.reg.csmaLbtModeUsed           = false;

	configDefault.csmaSeed0 = 0x3A;

	configDefault.csmaSeed1.reg.csmaSeed1       = 0x01;
	configDefault.csmaSeed1.reg.aackIamCoord    = false;
	configDefault.csmaSeed1.reg.aackDisable     = false;
	configDefault.csmaSeed1.reg.aackPendingData = false;
	configDefault.csmaSeed1.reg.fvnMode         = acceptFrameTypeZeroOrOne;

	configDefault.xahCtrl0.reg.slottetOperation = false;
	configDefault.xahCtrl0.reg.maxCsmaRetries   = 0x02;
	configDefault.xahCtrl0.reg.maxFrameRetries  = 0x02;

	configDefault.csmaBackoff.reg.minimal       = 0x02;
	configDefault.csmaBackoff.reg.maximal       = 0x02;

	configDefault.panId.val     = 0x3030;

	configDefault.shortAddr.val = 0x3030;

	configDefault.ieeeAddr[0] = 0x01;
	configDefault.ieeeAddr[1] = 0x02;
	configDefault.ieeeAddr[2] = 0x03;
	configDefault.ieeeAddr[3] = 0x04;
	configDefault.ieeeAddr[4] = 0x05;
	configDefault.ieeeAddr[5] = 0x06;
	configDefault.ieeeAddr[6] = 0x07;
	configDefault.ieeeAddr[7] = 0x08;

	setConfig( configDefault );
}


void radio_at86rf212b::setConfig( AT86RF212B_CONFIG &config )
{
	//Enable the Radio
	strobe( trxOffCmd );

	// Force a reset of the Radio chip
	reset();

	//Reading the Interrupt Status register resets the IRQs of the AT86RF212B
	getSystemReg( AT86RF212B_ADDR_IRQ_STATUS );

	// Enable the required Interrupts
	setSystemReg( AT86RF212B_ADDR_IRQ_MASK, frameRxStart | addressMatching | frameEndingRxTx );

	//Enable the MCUs IO Pin Interrupt
	GPIO_IntConfig( AT86RF212B_IRQ, true, false, true );

	setSystemReg( AT86RF212B_ADDR_TRX_CTRL_0,  config.trxCtrl0.val );
	setSystemReg( AT86RF212B_ADDR_TRX_CTRL_1,  config.trxCtrl1.val );

	setSystemReg( AT86RF212B_ADDR_PHY_TX_PWR,  config.phyTxPwr );
	setSystemReg( AT86RF212B_ADDR_PHY_CC_CCA,  config.phyCcCca.val );
	setSystemReg( AT86RF212B_ADDR_CCA_THRES,   config.ccaThres.val );
	setSystemReg( AT86RF212B_ADDR_RX_CTRL,     config.clockJitterModule );
	setSystemReg( AT86RF212B_ADDR_SFD_VALUE,   config.sdvValue );
	setSystemReg( AT86RF212B_ADDR_TRX_CTRL_2,  config.trxCtrl2.val );
	setSystemReg( AT86RF212B_ADDR_XOSC_CTRL,   config.xoscCtrl.val );
	setSystemReg( AT86RF212B_ADDR_CC_CTRL_0,   config.ccNumber );
	setSystemReg( AT86RF212B_ADDR_CC_CTRL_1,   config.ccCtrl1.val );
	setSystemReg( AT86RF212B_ADDR_RX_SYN,      config.rxSyn.val );
	setSystemReg( AT86RF212B_ADDR_RF_CTRL_0,   config.rfCtrl.val );
	setSystemReg( AT86RF212B_ADDR_XAH_CTRL_1,  config.xahCtrl1.val );
	setSystemReg( AT86RF212B_ADDR_XAH_CTRL_0,  config.xahCtrl0.val );
	setSystemReg( AT86RF212B_ADDR_CSMA_SEED_0, config.csmaSeed0 );
	setSystemReg( AT86RF212B_ADDR_CSMA_SEED_1, config.csmaSeed1.val );
	setSystemReg( AT86RF212B_ADDR_CSMA_BE,     config.csmaBackoff.val );

	setSystemReg( AT86RF212B_ADDR_SHORT_ADDR_0, config.shortAddr.reg.lsb );
	setSystemReg( AT86RF212B_ADDR_SHORT_ADDR_1, config.shortAddr.reg.msb );
	setSystemReg( AT86RF212B_ADDR_PAN_ID_0,     config.panId.reg.lsb );
	setSystemReg( AT86RF212B_ADDR_PAN_ID_1,     config.panId.reg.msb );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_0,  config.ieeeAddr[0] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_1,  config.ieeeAddr[1] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_2,  config.ieeeAddr[2] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_3,  config.ieeeAddr[3] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_4,  config.ieeeAddr[4] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_5,  config.ieeeAddr[5] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_6,  config.ieeeAddr[6] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_7,  config.ieeeAddr[7] );

	if ( config.trxCtrl2.reg.bpskOqpsk )
	{
		rssiBaseValue = -97;
	}
	else
	{
		rssiBaseValue = -100;
	}

	// Read the Interrupt Register of the AT86RF212B to reset the Interrupt Pin
	getInterruptStatus();

}


void radio_at86rf212b::setAddress( const uint16_t address, const uint16_t panId, uint8_t const * ieeeAddress )
{
	setSystemReg( AT86RF212B_ADDR_SHORT_ADDR_0, ( address & 0x00FF ) );
	setSystemReg( AT86RF212B_ADDR_SHORT_ADDR_1, ( ( address >> 8 ) & 0x00FF ) );

	setSystemReg( AT86RF212B_ADDR_PAN_ID_0, ( panId & 0x00FF ) );
	setSystemReg( AT86RF212B_ADDR_PAN_ID_1, ( ( panId >> 8 ) & 0x00FF ) );

	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_0,  ieeeAddress[0] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_1,  ieeeAddress[1] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_2,  ieeeAddress[2] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_3,  ieeeAddress[3] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_4,  ieeeAddress[4] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_5,  ieeeAddress[5] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_6,  ieeeAddress[6] );
	setSystemReg( AT86RF212B_ADDR_IEEE_ADDR_7,  ieeeAddress[7] );
}


void radio_at86rf212b::setReceiveMode()
{
	strobe( pllOnCmd );
	strobe( rxOnCmd );
}


void radio_at86rf212b::setSleepMode()
{
	strobe( forceTrxOffCmd );
	for ( volatile uint32_t i = 0; i < 20000; i++ );

	GPIO_PinOutSet( AT86RF212B_SLP_TR );
}


void radio_at86rf212b::setIdleMode()
{
	GPIO_PinOutClear( AT86RF212B_SLP_TR );

	strobe( pllOnCmd );
	for ( volatile uint32_t i = 0; i < 20000; i++ );
}


void radio_at86rf212b::sendPacket( uint8_t* payload, uint8_t payloadLength )
{
	strobe( pllOnCmd );

	for ( volatile uint32_t i = 0; i < 20000; i++ );

	// The Packet is transfered using the 8-Bit SPI Mode
	GPIO_PinOutClear( AT86RF212B_SPI_CS_PIN );

	// Write the Register Address to the AT86RF212B
	USART_Tx( AT86RF212B_USART, AT86RF212B_FRAME_BUF_WRITE );
	USART_Rx( AT86RF212B_USART );

	// Write the payload to the radio chip
	for ( uint32_t i = 0; i < payloadLength - 1; i++ )
	{
		USART_Tx( AT86RF212B_USART, payload[i] );
		USART_Rx( AT86RF212B_USART );
	}

	GPIO_PinOutSet( AT86RF212B_SPI_CS_PIN );

	// Enable the Tramsmission
	strobe( txStartCmd );
}


void radio_at86rf212b::readPacketDirect( uint8_t* payload, uint8_t payloadMax, uint8_t &payloadLength )
{
	GPIO_PinOutClear( AT86RF212B_SPI_CS_PIN );

	// Write the Register Address to the AT86RF212B
	USART_Tx( AT86RF212B_USART, AT86RF212B_FRAME_BUF_READ );
	USART_Rx( AT86RF212B_USART );

	// Access the SRAM buffer that holdes the Packe Frame Length, with an dummy write to the spi module
	USART_Tx( AT86RF212B_USART, 0xFF );
	payloadLength = USART_Rx( AT86RF212B_USART ) - 2;

 	if ( payloadLength < payloadMax )
	{
		for ( uint8_t i = 0; i < payloadLength + 3; i ++ )
		{
			USART_Tx( AT86RF212B_USART, 0xFF );
			payload [i] = USART_Rx( AT86RF212B_USART );
		}
	}

	GPIO_PinOutSet( AT86RF212B_SPI_CS_PIN );
}


void radio_at86rf212b::strobe( TRX_CMD strobeCommand )
{
	setSystemReg( AT86RF212B_ADDR_TRX_STATE, strobeCommand );
}


uint8_t radio_at86rf212b::getStatus()
{
	return getSystemReg( AT86RF212B_ADDR_TRX_STATUS );
}


void radio_at86rf212b::reset()
{
	GPIO_PinOutClear( AT86RF212B_RSTN );

	for ( volatile uint32_t i = 0; i < 10000; i++ );


	GPIO_PinOutSet( AT86RF212B_RSTN );
	for ( volatile uint32_t i = 0; i < 2000000; i++ );
}


uint8_t radio_at86rf212b::getInterruptStatus()
{
	return getSystemReg( AT86RF212B_ADDR_IRQ_STATUS );
}


int8_t radio_at86rf212b::getRssi()
{
	return ( ( ( getSystemReg( AT86RF212B_ADDR_PHY_RSSI ) & 0x1F ) * 3 ) + rssiBaseValue );
}


int8_t radio_at86rf212b::getEdLevel()
{
	return ( getSystemReg( AT86RF212B_ADDR_PHY_ED_LEVEL ) + rssiBaseValue );
}


void radio_at86rf212b::setSystemReg( const uint8_t address, const uint8_t data )
{
	GPIO_PinOutClear( AT86RF212B_SPI_CS_PIN );

	USART_TxDouble( AT86RF212B_USART, ( ( address | AT86RF212B_REG_WRITE ) | ( data << 8 ) ) );

	USART_RxDouble( AT86RF212B_USART );
	GPIO_PinOutSet( AT86RF212B_SPI_CS_PIN );
}


uint8_t radio_at86rf212b::getSystemReg( const uint8_t address )
{
	GPIO_PinOutClear( AT86RF212B_SPI_CS_PIN );

	USART_TxDouble( AT86RF212B_USART, ( address | AT86RF212B_REG_READ ) );

	uint8_t intermediate = ( USART_RxDouble( AT86RF212B_USART ) >> 8 );

	GPIO_PinOutSet( AT86RF212B_SPI_CS_PIN );

	return intermediate;
}

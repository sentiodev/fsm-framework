/*
 * radio_xbee.cpp
 */

#include <string.h>
#include "radio_xbee.h"
#include "application_config.h"
#include "em_emu.h"
#include "em_cmu.h"

#ifdef SENTIO_EM_ENABLE
#include <sentio_em_io.h>
#endif

/****************************************************************************************************************************************//**
 * @brief
 *  Declaration and Initialization of static members of the radio_xbee_dm driver-class
 *
 *******************************************************************************************************************************************/

USART_InitAsync_TypeDef radio_xbee::initXBeeUart;

uint8_t *radio_xbee::userDataBuffer;
uint8_t *radio_xbee::userSourceAddress;
uint8_t *radio_xbee::userPayloadLength;

uint8_t radio_xbee::modemStatus;
uint8_t radio_xbee::receiveOptions;
uint8_t radio_xbee::transmitStatus;

uint8_t radio_xbee::bufferCount;
uint8_t radio_xbee::responseStatus;
uint32_t radio_xbee::receivedFrameChecksum;

volatile uint8_t radio_xbee::packetTransmitted;
volatile bool    radio_xbee::responseReceived;

bool    radio_xbee::packetReceived;
bool    radio_xbee::packetPending;

LENGTH radio_xbee::temp;
FRAMES radio_xbee::frame;

/****************************************************************************************************************************************//**
 * @brief
 *  Basic System-Variables are set-up, which do not need to be changed during Run-Time
 *
 * @details
 * The Parameterization of the Serial-Interface and the Selection of the Serial-Interface as well as the selection of Interface is done here!
 *
 * @note
 * The User should not change the Initialization-Values for System-Variables, which are not related to the Hardware-Interfaces
 *******************************************************************************************************************************************/

radio_xbee::radio_xbee()
{
    // Setup the Parameters used to configure the selected UART/USART module
    initXBeeUart.enable       = usartEnable;      // Enable the UART after the Configuration is finalized
    initXBeeUart.refFreq      = MCU_CLOCK;        // The Default HF-Clock Frequency (see CMU) is used to calculate the Baudrate-Clock-Divider Setting
    initXBeeUart.baudrate     = 38400;            // Resulting Baudrate at which the UART module runs
    initXBeeUart.oversampling = usartOVS4;        // RX-Pin Over-sampling-Rate
    initXBeeUart.databits     = usartDatabits8;   // Number of data-Bits
    initXBeeUart.parity       = usartNoParity;    // Parity-Bit setup
    initXBeeUart.stopbits     = usartStopbits1;   // Number of Stop-Bits

    // Configure the MCU-Pin which drives the Enable-Pin of the LDO-Voltage Regulator used to power the XBee
    GPIO_PinModeSet( XBEE_POWER_ENABLE_PIN, gpioModePushPull, 0 );

    // Pin forces the XBEE module into low-power sleep
    GPIO_PinModeSet( XBEE_SLEEP_RQ_PIN, gpioModePushPull, 0 );

    init();
}


/****************************************************************************************************************************************//**
 * @brief
 *  Enables/Disables the Power-Supply of the XBee-Radio and also disables the HF-Clock to the Serial-Peripheral-Module (UART/USART) connected
 *  to the XBee-Module
 *
 * @details
 *
 *
 * @param[in] status
 * - True:  enable the Power-Supply/Clock
 * - False: disable the Power-Supply/Clock
 *
 * @note
 * When the HF-Clock is disabled no write-operation can be executed and the MCU code execution stops. This is a efficient way to prevent
 * accessing an disabled module, what might cause hardware damage.
 *******************************************************************************************************************************************/

void radio_xbee::powerRadio( bool status )
{
    if ( status ) // Enable the Power-Supply/Clock
    {
        GPIO_PinOutSet( XBEE_POWER_ENABLE_PIN );
    }

    else         // Disable the Power-Supply/Clock
    {
        GPIO_PinOutClear( XBEE_POWER_ENABLE_PIN );
    }
}



/****************************************************************************************************************************************//**
 * @brief
 *  Sets up GPIO-Pins, the Serial-Peripheral-Module (UART/USART) utilized by the Xbee 802.15.2 and buffer allocation.
 *
 *
 * @return
 *  - True: Initialization Successful
 *  - False Initialization failed, chosen Interface invalid
 *
 * @note
 *  The parameterization of the Serial-Peripheral-Module and the initialization of basic system-variables
 *  has to be done in the CPP-Object constructor!
 *
 *******************************************************************************************************************************************/

void radio_xbee::init()
{
    // Configuration of the GPIO-Pins which belong to the UART/USART configured. Furthermore the RX-Interrupt
    // Service Routine of the UART is enabled.
    // In a first switch-case statement the UART/USART Module is selected. Then the RX/TX Pins which have
    // to be activated are selected

    // Enable the required periphery clock
    CMU_ClockEnable( XBEE_CLOCK, true );

    // Enable the RX-Interrupt Service Routine
    NVIC_EnableIRQ( XBEE_ISR_NUMBER );

    USART_InitAsync( XBEE_UART, &initXBeeUart );

    // The Routing Register of the selected UART/USART Register is configured. The following register-access
    // enables the UART modules RX/TX shift-register and furthermore selects the one of the possible Locations of the modules IO-Pins
    //
    // NOTE!!!
    // Beside setting a modules Routing Register the functionality of the GPIO-Pins IO-Port-driver has to be configured separately

    XBEE_UART->ROUTE = USART_ROUTE_RXPEN |
                       USART_ROUTE_TXPEN |
                       XBEE_LOCATION;

    // Configure the IO-Port driver of the EFM32-GPIO Pins which were selected before
    GPIO_PinModeSet( XBEE_RX_PIN, gpioModeInputPullFilter, 1 ); // RX needs to be a Input
    GPIO_PinModeSet( XBEE_TX_PIN, gpioModePushPull, 0 );        // TX needs to be a Output


    // Configure the Interrupt
    XBEE_UART->IFC = ~0;                 // Clear pending Interrupts
    XBEE_UART->IEN = USART_IEN_RXDATAV;  // Enable the RX-Interrupt when One-Byte is in the buffer,
    // Flag is cleared when the RX buffer is read-out

    setBroadcastAddress();
    setReceiverAddress( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF );

    userDataBuffer = getReceiveDataBuffer();
    userSourceAddress = getSourceAddress();
    userPayloadLength = getReceivePayloadLength();
}


void radio_xbee::enableRadioSm1()
{
    GPIO_PinOutClear( XBEE_SLEEP_RQ_PIN );

    for ( uint32_t volatile i = 0; i < 2000000; i++ );
}


void radio_xbee::disableRadioSm1()
{
    GPIO_PinOutSet( XBEE_SLEEP_RQ_PIN );
}



/****************************************************************************************************************************************//**
 * @brief
 *  Sets the receiver address for XBee communication
 *
 * @params
 *  byte1 - byte8 of the receiver address
 *
 *******************************************************************************************************************************************/

void radio_xbee::setReceiverAddress( uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4, uint8_t byte5, uint8_t byte6, uint8_t byte7, uint8_t byte8 )
{
    receiverAddress[0] = byte1;
    receiverAddress[1] = byte2;
    receiverAddress[2] = byte3;
    receiverAddress[3] = byte4;
    receiverAddress[4] = byte5;
    receiverAddress[5] = byte6;
    receiverAddress[6] = byte7;
    receiverAddress[7] = byte8;
}



/****************************************************************************************************************************************//**
 * @brief
 *  Sets the broadcast address for XBee communication
 *
 *******************************************************************************************************************************************/

void radio_xbee::setBroadcastAddress()
{
    receiverAddress[0] = 0x00;
    receiverAddress[1] = 0x00;
    receiverAddress[2] = 0x00;
    receiverAddress[3] = 0x00;
    receiverAddress[4] = 0x00;
    receiverAddress[5] = 0x00;
    receiverAddress[6] = 0xFF;
    receiverAddress[7] = 0xFF;
}


/****************************************************************************************************************************************//**
 * @brief
 *	Gets the address of the receiver address
 *
 * @return receiverAddress
 *  Pointer to the receiver address buffer
 *
 *******************************************************************************************************************************************/

uint8_t *radio_xbee::getReceiverAddress()
{
    return receiverAddress;
}


/****************************************************************************************************************************************//**
 * @brief
 *	Gets the address of the broadcast address
 *
 * @return broadcastAddress
 *  Pointer to the broadcast address buffer
 *
 *******************************************************************************************************************************************/

uint8_t *radio_xbee::getBroadcastAddress()
{
    return broadcastAddress;
}


/****************************************************************************************************************************************//**
 * @brief
 *	Gets the address of the source address
 *
 * @return sourceAddress
 *  Pointer to the source address buffer
 *
 *******************************************************************************************************************************************/

uint8_t *radio_xbee::getSourceAddress()
{
    return sourceAddress;
}


/****************************************************************************************************************************************//**
 * @brief
 *	Gets the address of the receive data buffer
 *
 * @return receiveDataBuffer
 *  Pointer to the receive data buffer
 *
 *******************************************************************************************************************************************/

uint8_t *radio_xbee::getReceiveDataBuffer()
{
    return receiveDataBuffer;
}


/****************************************************************************************************************************************//**
 * @brief
 *	Gets the address of the receive payload length
 *
 * @return receivePayloadLength
 *  Pointer to the receive payload length
 *
 *******************************************************************************************************************************************/

uint8_t *radio_xbee::getReceivePayloadLength()
{
    return &receivePayloadLength;
}




/****************************************************************************************************************************************//**
 * @brief
 *  When a packet-payload is not copied to the user--defined buffer. By calling this driver-method the user can copy the payload form the
 *  driver- to the applications-buffer "manually"
 *
 * @details
 *  When a second packet is transfered to the XBee-Module before the application-code has processed the first packet, the user-defined buffer
 *  is marked as not empty by an internal status flag of the driver. The payload of the new packet, is kept in the drivers internal buffer and
 *  will stay there until the user initiates the transfer or until an new API-frame is transfered by the XBee-Module
 *
 * @param[in] *command
 *  Character Array contains the ASCII representation of the command name
 *
 * @param[in] *parameter
 *  Specifies the Parameter-Value
 *
 * @return responseStatus
 *  Command Status is returned OK(0), ERROR(1), Invalid Command(2), Invalid Parameter(3)
 *
 *******************************************************************************************************************************************/


/****************************************************************************************************************************************//**
 * @brief
 *  Calculation of the API-frame checksum as specified in the device's documentation
 *
 * @details
 *  This method is used to calculate the Checksum, which is attached at the end of API-Frames that are send to the XBee-Module,
 *  and is also used to verify the Checksum that is part of every response frame of the XBee-Module
 *
 * @return
 *  Returns the value of the checksum calculated over the current contend of the drivers SRAM-Buffer
 *
 *******************************************************************************************************************************************/

uint8_t radio_xbee::processingChecksum()
{
    uint32_t tempChecksum = 0;

    // Calculation of the Checksum
    for( uint8_t i = 3; i < ( temp.length + 3); i++ )
    {
        // In the first step the Hex-Values of all "PAYLOAD-BYTES" (excluding Start-Delimiter, length msb/lsb,
        // and the Checksum itself are summed up
        tempChecksum += frame.array[i];
    }

    // Masking the Result of the Checksum Calculation with 0x000000FF. The Low-Byte of the Checksum remains unchanged, while the upper 24-bit
    // are forced to Zero. The Outcome of the bitwise AND - Operation is send to the XBee-Module at the end of the frame.
    return ( 0xFF - ( (uint8_t) ( tempChecksum & 0x000000FF ) ) );
}

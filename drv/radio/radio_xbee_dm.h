/**
 * @file radio_xbee_dm.h
 *
 * Driver: radio_xbee_dm
 *
 * Driver for the XBee transceiver module with Digimesh firmware
 */

#ifndef RADIO_XBEE_DM_H_
#define RADIO_XBEE_DM_H_

#include <stdint.h>
#include <radio_xbee.h>
#include "em_gpio.h"
#include "em_usart.h"

// Platform selection
#ifdef SENTIO_EM_ENABLE
#include <sentio_em_io.h>

#define XBEE_UART              RADIO_USART
#define XBEE_LOCATION          RADIO_USART_LOC

#define XBEE_ISR_NUMBER        USART0_RX_IRQn
#define XBEE_CLOCK             cmuClock_USART0

#define XBEE_POWER_ENABLE_PIN  RADIO_PWR_PIN
#define XBEE_WAKEUP_PIN        DEBUG_PIN_2
#define XBEE_SLEEP_RQ_PIN      DEBUG_PIN_8

#define XBEE_TX_PIN            RADIO_TX
#define XBEE_RX_PIN            RADIO_RX

#define maskInterruptXbeeRadio 0x1000
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef RADIO_XBEE_DM_ENABLE
#error Missing define in application_config.h: RADIO_XBEE_DM_ENABLE
#endif

const char WriteValues[2]            = {'W', 'R'};
const char RestoreDefaults[2]        = {'R', 'E'};
const char SoftwareReset[2]          = {'F', 'R'};
const char ApplyChanges[2]           = {'A', 'C'};
const char VersionLong[2]            = {'V', 'L'};
const char DestinationAddressHigh[2] = {'D', 'H'};
const char DestinationAddressLow[2]  = {'D', 'L'};
const char DeviceTypeIdentifier[2]   = {'D', 'D'};
const char SerialNumberHigh[2]       = {'S', 'H'};
const char SerialNumberLow[2]        = {'S', 'L'};
const char ClusterIdentifier[2]      = {'C', 'I'};
const char NetworkIdentifier[2]      = {'I', 'D'};
const char MaximumRF_PayloadBytes[2] = {'N', 'P'};
const char Channel[2]                = {'C', 'H'};
const char Coordinator_Enddevice[2]  = {'C', 'E'};
const char FirmwareVersion[2]        = {'V', 'R'};
const char HardwareVersion[2]        = {'H', 'V'};
const char ConfigurationCode[2]      = {'C', 'K'};
const char RF_Errors[2]              = {'E', 'R'};
const char GoodPackets[2]            = {'G', 'D'};
const char RSSI_PWM_timer[2]         = {'R', 'P'};
const char TransmissionErrors[2]     = {'T', 'R'};
const char Temperature[2]            = {'T', 'P'};
const char ReceivedSignalStrength[2] = {'D', 'B'};
const char SupplyVoltage[2]          = {'%', 'V'};
const char BroadcastMultiTransmit[2] = {'M', 'T'};
const char UnicastMAC_Retries[2]     = {'R', 'R'};
const char PowerLevel[2]             = {'P', 'L'};
const char NetworkHops[2]            = {'N', 'H'};
const char NetworkDelaySlots[2]      = {'N', 'N'};
const char MeshNetworkRetries[2]     = {'M', 'R'};
const char BroadcastRadius[2]        = {'B', 'H'};
const char NodeType[2]               = {'C', 'E'};
const char SleepMode[2]              = {'S', 'M'};
const char SleepOptions[2]           = {'S', 'O'};
const char WakeTime[2]               = {'S', 'T'};
const char SleepPeriod[2]            = {'S', 'P'};
const char NumberOfSleepPeriods[2]   = {'S', 'N'};
const char WakeHost[2]               = {'W', 'H'};
const char NumberOfMissedSyncs[2]    = {'M', 'S'};
const char MissedSyncCount[2]        = {'S', 'Q'};
const char SleepStatus[2]            = {'S', 'S'};
const char OperationalSleepPeriod[2] = {'O', 'S'};
const char OperationalWakePeriod[2]  = {'O', 'W'};

struct MAC_LEVEL_CONFIG
{
	uint8_t broadcastMultiTransmit;
	uint8_t unicastMacRetries;
	uint8_t powerLevel;
};

struct DIGIMESH_CONFIG
{
	uint8_t networkHops;
	uint8_t networkDelaySlots;
	uint8_t meshNetworkRetries;
	uint8_t broadcastRadius;
	uint8_t nodeType;
};

struct SLEEP_CONFIG
{
	uint8_t  sleepMode;
	uint8_t  sleepOptions;
	uint32_t wakeTime;
	uint32_t sleepPeriod;
	uint16_t numberOfSleepPeriods;
	uint16_t wakeHost;
};


typedef uint8_t MAC[8];
typedef void ( *ptISR_Handler )( uint32_t );

/**
 * Driver class for the XBee radio transceiver module with Digimesh firmware
 *
 * The class contains all member functions to interact with the XBee radio with Digimesh firmware.
 */
class radio_xbee_dm : public radio_xbee
{
private:
	static uint8_t transmitRetryCount;
	static uint8_t deliveryStatus;
	static uint8_t discoveryStatus;
	static uint8_t receiveOptions;

	typedef enum
	{
		AT_CommandResponseFrame = 0x88,
		ModemStatusFrame        = 0x8A,
		TransmitStatusFrame     = 0x8B,
		ReceivePacketFrame      = 0x90
	} FRAME_ID;

	static uint8_t processingChecksum();
	static uint8_t processingResponse();
	static uint8_t processingCommandResponse();
	static void processingModemStatusFrame();
	static void processingTransmitStatus();
	static void processingReceivePacket();

	static void wrapper_XBeeUART_RX( uint32_t inputChar );

public:
	radio_xbee_dm();
	~radio_xbee_dm() {}

	/**
	 * Sets up GPIO-Pins and the Serial-Peripheral-Module (UART/USART) utilized by the Xbee-Digimesh.
	 *
	 * The function initializes the UART communication to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     function success (true/false)
	 */
	void init();

	//Not needed anymore: void initializeSystemBuffer( uint8_t *buffer, uint8_t *sourceAddress, uint8_t *payloadLength );

	ptISR_Handler getISR_FunctionPointer();

	/**
	 * Enables the radio.
	 *
	 * The function enables the radio module when it is configured with asynchronous pin sleep.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void enableRadio_SM1();

	/**
	 * Disables the radio.
	 *
	 * The function disables the radio module when it is configured with asynchronous pin sleep.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void disableRadio_SM1();

	/**
	 * Sends a packet to the specified receiver address.
	 *
	 * The function sends a packet to a specified receiver. The receiver can be specified by calling setReceiverAddress prior to this function.
	 *
	 * @param[in]  data: the data to be send
	* @param[in]  payloadLength: the amount of data to be transmited
	* @param[in]  broadcastRadius: TODO: ?
	* @param[in]  acknowledge: use acknowledgements (default enabled)
	* @param[in]  discovery: TODO: ?

	 * @return     transmission success (true/false)
	 */
	bool sendPacket( const uint8_t* data, const uint8_t payloadLength, const uint8_t broadcastRadius = 0, const bool acknowledge = true, const bool discovery = true );

	/**
	 * Checks for available packets.
	 *
	 * The function checks if there are available packets that need to be handled.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     available packet (true/false)
	 */
	bool getPacketReceived();

	/**
	 * Gets the data of available packets.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     true/false
	 */
	bool getPendingPacketData();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getModemStatus();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getTransmitRetryCount();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getDeliveryStatus();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getDiscoveryStatus();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getReceiveOptions();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getFirmwareVersion( uint32_t &firmwareVersion );


	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t sendRestoreDefaultsComand();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t sendSoftwareResetComand();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t sendApplyChangesComand();

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t sendVersionLongComand();


	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getHardwareVersion( uint16_t &hardwareVersion );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getConfigurationCode( uint32_t &configurationCode );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getRF_Errors( uint16_t &rfErrors );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getGoodPackets( uint16_t &goodPackets );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getTransmissionErrors( uint16_t &transmissionErrors );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getTemperature( uint16_t &temperature );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getReceivedSignal( uint8_t& rssi );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getSupplyVoltage( uint16_t& supplyVoltage );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setRSSI_PWM_Timer( const uint8_t rssiTime );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getRSSI_PWM_Timer( uint8_t& rssiTime );
//
//   /**
//    * Gets the MAC address of the XBee module.
//    *
//    * The function gets the MAC address of the local XBee module.
//    *
//    * @param[in]  none
//    * @param[out] xBeeMAC_Address: the address of the module
//    * @return     ?
//    */
//	uint8_t getLocalXBeeMAC( MAC xBeeMAC_Address );
//
//   /**
//    * Gets the destination MAC address.
//    *
//    * details.
//    *
//    * @param[in]  none
//    * @param[out] xBeeMAC_Address: the destination MAC address
//    * @return     ?
//    */
//	uint8_t getDestinationXBeeMAC( MAC xBeeMAC_Address );

	/**
	 * Gets the MAC address of the XBee module.
	 *
	 * The function gets the MAC address of the local XBee module.
	 *
	 * @param[in]  none
	 * @param[out] xBeeMAC_Address: the address of the module
	 * @return     TODO: ?
	 */
	uint8_t getLocalXBeeMAC( MAC xBeeMAC_Address );

	/**
	 * Gets the destination MAC address.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] xBeeMAC_Address: the destination MAC address
	 * @return     TODO: ?
	 */
	uint8_t getDestinationXBeeMAC( MAC xBeeMAC_Address );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getDeviceTypeIdentifier( uint32_t& deviceTypeIdentifier );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getMaximumRfPayload( uint16_t& maximumPayloadBytes );

	/**
	 * Get the current PAN ID (Network Identifier).
	 *
	 * The function gets the PAN ID (Network Identifier) the module is currently configured for.
	 *
	 * @param[in]  none
	 * @param[out] PAN_ID: the pan id
	 * @return     TODO: ?
	 */
	uint8_t getPanId( uint16_t& panId );

	/**
	 * Sets the PAN ID (Network Identifier).
	 *
	 * The function sets the PAN ID, which configures the module to be used in the specified network.
	 *
	 * @param[in]  PAN_ID: the pan id to be set
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setPanId( const uint16_t panId );

	/**
	 * Gets the current channel configuration.
	 *
	 * The function gets the channel number the module is currently set to operate in.
	 *
	 * @param[in]  none
	 * @param[out] channel: the channel number
	 * @return     TODO: ?
	 */
	uint8_t getChannel( uint8_t& channel );

	/**
	 * Sets the channel number.
	 *
	 * The function configures the module to operate in the specified channel.
	 *
	 * @param[in]  channel: the channel number to be used
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setChannel( const uint8_t channel );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getModuleType( uint8_t& type );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setModuleType( const uint8_t type );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getMAC_LayerConfig( MAC_LEVEL_CONFIG& configuration );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setMAC_LayerConfig( const MAC_LEVEL_CONFIG& configuration );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getDigiMeshConfig( DIGIMESH_CONFIG &configuration );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setDigiMeshConfig( const DIGIMESH_CONFIG& configuration );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getSleepConfig( SLEEP_CONFIG& configuration );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t setSleepConfig( const SLEEP_CONFIG& configuration );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getNumberOfMissedSyncs( uint16_t& numberOfSyncCount );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getMissedSyncCount( uint16_t &missedSyncCount );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getSleepStatus( uint8_t& sleepStatus );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getOperationalSleepPeriod( uint16_t& operationalSleepPeriod );

	/**
	 * TODO: brief.
	 *
	 * TODO: details.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     TODO: ?
	 */
	uint8_t getOperationalWakePeriod( uint16_t& operationalWakePeriod );

	/**
	 * Sets the receiver MAC address.
	 *
	 * The function saves a MAC address which should be used for unicast transmission.
	 *
	 * @param[in]  address: the address of the receiver
	 * @param[out] none
	 * @return     void
	 */
	void setReceiverAddress( uint8_t *address );

	/**
	 * Gets the currently configured receiver address.
	 *
	 * The function gets the receiver address, which is currently used for unicast transmissions.
	 *
	 * @param[in]  none
	 * @param[out] address: the current receiver address
	 * @return     void
	 */
	void getReceiverAddress( uint8_t *address );

	/**
	 * Gets the broadcast address.
	 *
	 * The function gets the broadcast address, which is statically defined as 000000000000FFFF.
	 *
	 * @param[in]  none
	 * @param[out] address: the broadcast address (000000000000FFFF)
	 * @return     void
	 */
	void getBroadcastAddress( uint8_t *address );

	/**
	 * Gets the source address.
	 *
	 * The function gets the MAC address of the transmitter that has sent the last packet.
	 *
	 * @param[in]  none
	 * @param[out] address: the source address
	 * @return     void
	 */
	void getSourceAddress( uint8_t *address );

	/**
	 * Gets the size of the last payload.
	 *
	 * The function returns the size of the payload that was included in the last packet.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     the payload size
	 */
	uint8_t getReceivePayloadLength();

};



#endif /* XBEE_RADIO_H_ */

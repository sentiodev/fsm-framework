/**
 * Driver: radio_xbee_dm
 *
 * Driver for the XBee transceiver module with Digimesh firmware
 */

#include <string.h>
#include "radio_xbee_dm.h"
#include "em_emu.h"
#include "em_cmu.h"

uint8_t radio_xbee_dm::transmitRetryCount;
uint8_t radio_xbee_dm::deliveryStatus;
uint8_t radio_xbee_dm::discoveryStatus;
uint8_t radio_xbee_dm::receiveOptions;


radio_xbee_dm::radio_xbee_dm()
{
	//****************************************************************************************************************************************
	//    !! NOTE !!  Do not change the Variables below
	//****************************************************************************************************************************************
	// Initialization of some Auxiliary Variables used in the XBee-driver module
	frame.atCommand.startDelimiter = 0x7E;

	responseReceived  = false; // Has the Command response Frame been received?
	packetPending     = 0;     // Is the internal buffer of the Driver module empty?
	packetTransmitted = true;  // Data-Packet Transmission finalized and Transmit-Response-Frame was received.
	packetReceived    = false; // Has a Data-Packet been received on the radio channel?

	// Configure the MCU-Pin which drives the Enable-Pin of the LDO-Voltage Regulator used to power the XBee
	GPIO_PinModeSet( XBEE_POWER_ENABLE_PIN, gpioModePushPull, 0 );

	// Pin forces the XBEE module into low-power sleep
	GPIO_PinModeSet( XBEE_SLEEP_RQ_PIN, gpioModePushPull, 0 );
}


ptISR_Handler radio_xbee_dm::getISR_FunctionPointer()
{
	return &wrapper_XBeeUART_RX;
}


bool radio_xbee_dm::sendPacket( const uint8_t* data, const uint8_t payloadLength, const uint8_t broadcastRadius, const bool acknowledge, const bool discovery )
{
	if ( packetTransmitted )
	{
		// Indicate Packet Transmission on-going
		packetTransmitted = false;


		// Calculate the Length of the API-Frame
		temp.length = 0x0E + payloadLength;

		frame.transmitPacketDm.lengthMsb = temp.reg.msb;
		frame.transmitPacketDm.lengthLsb = temp.reg.lsb;

		frame.transmitPacketDm.frameType = 0x10;
		frame.transmitPacketDm.frameId   = 0x01;

		// Copy the user-defined destination-address to the API-Frame buffer
		memcpy( frame.transmitPacketDm.destinationAddress, receiverAddress, 8 );

		// Set Reserve-Bytes to the required default values
		frame.transmitPacketDm.reserve[0] = 0xFF;
		frame.transmitPacketDm.reserve[1] = 0xFE;

		frame.transmitPacketDm.broadcastRadius = broadcastRadius;
		frame.transmitPacketDm.transmitOptions = ( ( uint8_t ) !discovery ) << 1 | ( ( uint8_t ) !acknowledge );

		// Copy the user-defined packet-payload to the API-Frame buffer
		memcpy(frame.transmitPacketDm.radioData,data,payloadLength);

		// The API-Frame is send to the XBee-Module via the configured UART-interface
		for ( uint8_t i = 0; i <= ( temp.length + 2 ); i++ )
		{
			USART_Tx( XBEE_UART, frame.array[i] );
		}

		// When all Data-Bytes are send out the Checksum, is calculated and will be transfered at the END of the API-Frame
		USART_Tx( XBEE_UART, processingChecksum() );

		// Wait in Energy-Mode 1 for the complete reception of the XBee-module response and the processing of the
		do
		{

			EMU_EnterEM1();

		}
		while ( !packetTransmitted );   // leave the loop when the Response-Frame is received or if a
										// invalid API-Frame or invalid Checksum is received

		return true;
	}

	else
		return false;
}


bool radio_xbee_dm::getPacketReceived()
{
	if ( packetReceived )
	{
		// Reset the driver-status flag to allow an new packet to be transfered
		packetReceived = false;
		return true;
	}

	return false;
}


bool radio_xbee_dm::getPendingPacketData()
{
	// Copy the Data-Payload to the user-defined-buffer
	memcpy( userDataBuffer, frame.receivePacketDm.receiveData, ( temp.length - 12 ) );

	// The MSB is not contained in RF-Packet, due to the fact that it is always expected to be 0x00,
	// therefore set the byte manually
	*userSourceAddress = 0x00 ;

	// Copy the 7-Byte remaining Bytes from their position inside the API-Frame-Buffer to the user-defined buffer
	memcpy( userSourceAddress + 1, frame.receivePacketDm.sourceAdress64, 7 );

	// Pick the Status information from the correct position inside the frame and make
	// it available for the get-method()
	receiveOptions = frame.receivePacketDm.receiveOptions;

	// Indication of a pending packet in side the buffer,
	// a packetPending value of > 1 indicates a dropped packet!
	if ( packetPending <= 1 )
		return true;

	return false;
}


uint8_t radio_xbee_dm::getModemStatus()
{
	return modemStatus;
}


uint8_t radio_xbee_dm::getTransmitRetryCount()
{
	return transmitRetryCount;
}


uint8_t radio_xbee_dm::getDeliveryStatus()
{
	return deliveryStatus;
}


uint8_t radio_xbee_dm::getDiscoveryStatus()
{
	return discoveryStatus;
}


uint8_t radio_xbee_dm::getReceiveOptions()
{
	return receiveOptions;
}
/*

uint8_t radio_xbee_dm::sendWriteValuesComand()
{
	return configRegisterAccess( WriteValues, 0x00 );
}


uint8_t radio_xbee_dm::sendRestoreDefaultsComand()
{
	return configRegisterAccess( RestoreDefaults );
}


uint8_t radio_xbee_dm::sendSoftwareResetComand()
{
	return configRegisterAccess( SoftwareReset );
}


uint8_t radio_xbee_dm::sendApplyChangesComand()
{
	return configRegisterAccess( ApplyChanges );
}


uint8_t radio_xbee_dm::sendVersionLongComand()
{
	return 0xFF;
	//return configRegisterAccess( VersionLong );
}*/


uint8_t radio_xbee_dm::getFirmwareVersion( uint32_t &firmwareVersion )
{
	return configRegisterAccess( FirmwareVersion, firmwareVersion );
}


uint8_t radio_xbee_dm::getHardwareVersion( uint16_t &hardwareVersion )
{
	return configRegisterAccess( HardwareVersion, hardwareVersion );
}


uint8_t radio_xbee_dm::getConfigurationCode( uint32_t &configurationCode )
{
	return configRegisterAccess( ConfigurationCode, configurationCode );
}


uint8_t radio_xbee_dm::getRF_Errors( uint16_t &rf_Errors )
{
	return configRegisterAccess( RF_Errors, rf_Errors );
}


uint8_t radio_xbee_dm::getGoodPackets( uint16_t &good_Packets )
{
	return configRegisterAccess( GoodPackets, good_Packets );
}


uint8_t radio_xbee_dm::getTransmissionErrors( uint16_t &transmissionErrors )
{
	return configRegisterAccess( TransmissionErrors, transmissionErrors );
}


uint8_t radio_xbee_dm::getTemperature( uint16_t &temperature )
{
	return configRegisterAccess( Temperature, temperature );
}


uint8_t radio_xbee_dm::getReceivedSignal( uint8_t &rssi )
{
	return configRegisterAccess( ReceivedSignalStrength, rssi );
}


uint8_t radio_xbee_dm::getSupplyVoltage( uint16_t& )
{
	return 0xFF;
	//return configRegisterAccess( SupplyVoltage, (uint32_t*) supplyVoltage, 2 );
}


uint8_t radio_xbee_dm::setRSSI_PWM_Timer( const uint8_t rssiTime )
{
	return configRegisterAccess( SupplyVoltage, const_cast<uint8_t&> (rssiTime), false, true, true );
}


uint8_t radio_xbee_dm::getRSSI_PWM_Timer( uint8_t &rssiTime )
{
	return configRegisterAccess( RSSI_PWM_timer, rssiTime );
}

//
//uint8_t radio_xbee_dm::getLocalXBeeMAC( MAC Address )
//{
//	uint8_t i;
//	uint8_t errorCount = 0;
//
//	uint32_t temporary  = 0;
//	uint32_t temporary2 = 0;
//
//	errorCount += configRegisterAccess( SerialNumberLow, &temporary, 4 );
//
//	for ( i = 7; i >= 4; i-- )
//	{
//		Address[i] = ( uint8_t )temporary;
//		temporary >>= 8;
//	}
//
//	errorCount += configRegisterAccess( SerialNumberHigh, &temporary2, 4 );
//
//	for ( i = 3; i > 0; i-- )
//	{
//		Address[i] = ( uint8_t )temporary2;
//		temporary2 >>= 8;
//	}
//
//	return errorCount;
//}
//
//
//uint8_t radio_xbee_dm::getDestinationXBeeMAC( MAC Address )
//{
//	uint8_t i;
//	uint8_t errorCount = 0;
//
//	uint32_t temporary  = 0;
//	uint32_t temporary2 = 0;
//
//	errorCount += configRegisterAccess( DestinationAddressLow, &temporary, 4 );
//
//	for ( i = 7; i >= 4; i-- )
//	{
//		Address[i] = ( uint8_t )temporary;
//		temporary >>= 8;
//	}
//
//	errorCount += configRegisterAccess( DestinationAddressHigh, &temporary2, 4 );
//
//	for ( i = 3; i > 0; i-- )
//	{
//		Address[i] = ( uint8_t )temporary2;
//		temporary2 >>= 8;
//	}
//
//
//	return errorCount;
//}


uint8_t radio_xbee_dm::getDeviceTypeIdentifier( uint32_t &deviceTypeIdentifier )
{
	return configRegisterAccess( DeviceTypeIdentifier, deviceTypeIdentifier );
}


uint8_t radio_xbee_dm::getMaximumRfPayload( uint16_t &maximumPayloadBytes )
{
	return configRegisterAccess( MaximumRF_PayloadBytes, maximumPayloadBytes );
}


uint8_t radio_xbee_dm::getPanId( uint16_t &panId )
{
	return configRegisterAccess( NetworkIdentifier, panId );
}


uint8_t radio_xbee_dm::setPanId( const uint16_t panId )
{
	return configRegisterAccess( NetworkIdentifier, const_cast<uint16_t&> (panId), false, true, true );
}


uint8_t radio_xbee_dm::getChannel( uint8_t &channel )
{
	return configRegisterAccess( Channel, channel );
}


uint8_t radio_xbee_dm::setChannel( const uint8_t channel )
{
	return configRegisterAccess( Channel, const_cast<uint8_t&> (channel), false, true, true );
}


uint8_t radio_xbee_dm::getModuleType( uint8_t& type )
{
	return configRegisterAccess( Coordinator_Enddevice, type );
}


uint8_t radio_xbee_dm::setModuleType( const uint8_t type )
{
	if ( type == 0 || type == 2 )
		return configRegisterAccess( Coordinator_Enddevice, const_cast<uint8_t&> (type), false, true, true );
	else
		return 0xFF;
}


uint8_t radio_xbee_dm::getMAC_LayerConfig( MAC_LEVEL_CONFIG& configuration )
{
	uint8_t errorCount = 0;

	errorCount += configRegisterAccess( BroadcastMultiTransmit, configuration.broadcastMultiTransmit );

	errorCount += configRegisterAccess( UnicastMAC_Retries, configuration.unicastMacRetries );

	errorCount += configRegisterAccess( PowerLevel, configuration.powerLevel );


	return errorCount;
}


uint8_t radio_xbee_dm::setMAC_LayerConfig( const MAC_LEVEL_CONFIG& configuration )
{
	uint8_t errorCount = 0;

	if ( configuration.broadcastMultiTransmit <= 0x0F )
		errorCount += configRegisterAccess( BroadcastMultiTransmit, const_cast<uint8_t&> (configuration.broadcastMultiTransmit), false, true, true );
	else
		return 0xFF;

	if ( configuration.unicastMacRetries <= 0x0F )
		errorCount += configRegisterAccess( UnicastMAC_Retries, const_cast<uint8_t&> (configuration.unicastMacRetries), false, true, true );
	else
		return 0xFF;

	if ( configuration.powerLevel <= 0x04 )
		errorCount += configRegisterAccess( PowerLevel, const_cast<uint8_t&> (configuration.powerLevel), false, true, true );
	else
		return 0xFF;

	return errorCount;
}


uint8_t radio_xbee_dm::getDigiMeshConfig( DIGIMESH_CONFIG &configuration )
{
	uint8_t errorCount = 0;

	errorCount += configRegisterAccess( NetworkHops, configuration.networkHops );

	errorCount += configRegisterAccess( NetworkDelaySlots, configuration.networkDelaySlots );

	errorCount += configRegisterAccess( MeshNetworkRetries, configuration.meshNetworkRetries );

	errorCount += configRegisterAccess( BroadcastRadius, configuration.broadcastRadius );

	errorCount += configRegisterAccess( NodeType, configuration.nodeType );

	return errorCount;
}


uint8_t radio_xbee_dm::setDigiMeshConfig( const DIGIMESH_CONFIG& configuration )
{
	uint8_t errorCount = 0;

	if ( configuration.networkHops >= 1 )
		errorCount += configRegisterAccess( NetworkHops, const_cast<uint8_t&> (configuration.networkHops), false, true, true );
	else
		return 0xFF;

	if ( configuration.networkDelaySlots <= 0x0A )
		errorCount += configRegisterAccess( NetworkDelaySlots, const_cast<uint8_t&> (configuration.networkDelaySlots), false, true, true );
	else
		return 0xFF;

	if ( configuration.meshNetworkRetries <= 0x07 )
		errorCount += configRegisterAccess( MeshNetworkRetries, const_cast<uint8_t&> (configuration.meshNetworkRetries), false, true, true );
	else
		return 0xFF;

	if ( configuration.broadcastRadius <= 0x20 )
		errorCount += configRegisterAccess( BroadcastRadius, const_cast<uint8_t&> (configuration.broadcastRadius), false, true, true );
	else
		return 0xFF;

	if ( configuration.nodeType == 0x02 )
		errorCount += configRegisterAccess( NodeType, const_cast<uint8_t&> (configuration.nodeType), false, true, true );
	else
		return 0xFF;

	return errorCount;
}


uint8_t radio_xbee_dm::getSleepConfig( SLEEP_CONFIG &configuration )
{
	uint8_t errorCount = 0;

	errorCount += configRegisterAccess( SleepMode, configuration.sleepMode );

	errorCount += configRegisterAccess( SleepOptions, configuration.sleepOptions );

	errorCount += configRegisterAccess( WakeTime, configuration.wakeTime );

	errorCount += configRegisterAccess( SleepPeriod, configuration.sleepPeriod );

	errorCount += configRegisterAccess( NumberOfSleepPeriods, configuration.numberOfSleepPeriods );

	errorCount += configRegisterAccess( WakeHost, configuration.wakeHost );

	return errorCount;
}


uint8_t radio_xbee_dm::setSleepConfig( const SLEEP_CONFIG& configuration )
{
	uint8_t errorCount = 0;

	if ( ( configuration.sleepMode < 0x09 ) && ( configuration.sleepMode != 0x02 ) && ( configuration.sleepMode != 0x03 ) && ( configuration.sleepMode != 0x06 ) )
		errorCount += configRegisterAccess( NetworkHops, const_cast<uint8_t&> (configuration.sleepMode), false, true, true );
	else
		return 0xFF;

	if ( ( ( configuration.sleepOptions & 0x01 ) && !( configuration.sleepOptions & 0x02 ) ) || ( !( configuration.sleepOptions & 0x01 ) && ( configuration.sleepOptions & 0x02 ) ) )
		errorCount += configRegisterAccess( NetworkHops, const_cast<uint8_t&> (configuration.sleepOptions), false, true, true );
	else
		return 0xFF;

	if ( ( configuration.wakeTime > 0x45 ) && ( configuration.wakeTime < 0x0036EE80 ) )
		errorCount += configRegisterAccess( NetworkHops, const_cast<uint32_t&> (configuration.wakeTime), false, true, true );
	else
		return 0xFF;

	if ( configuration.sleepPeriod > 0 )
		errorCount += configRegisterAccess( SleepPeriod, const_cast<uint32_t&> (configuration.sleepPeriod), false, true, true );
	else
		return 0xFF;

	if ( configuration.numberOfSleepPeriods > 0 )
		errorCount += configRegisterAccess( NumberOfSleepPeriods, const_cast<uint16_t&> (configuration.numberOfSleepPeriods), false, true, true );
	else
		return 0xFF;

	errorCount += configRegisterAccess( WakeHost, const_cast<uint16_t&> (configuration.wakeHost), false, true, true );


	return errorCount;
}


uint8_t radio_xbee_dm::getNumberOfMissedSyncs( uint16_t& numberOfSyncCount )
{
	return configRegisterAccess( NumberOfMissedSyncs, numberOfSyncCount );
}


uint8_t radio_xbee_dm::getMissedSyncCount( uint16_t& missedSyncCount )
{
	return configRegisterAccess( MissedSyncCount, missedSyncCount );
}


uint8_t radio_xbee_dm::getSleepStatus( uint8_t& sleepStatus )
{
	return configRegisterAccess( SleepStatus, sleepStatus );
}


uint8_t radio_xbee_dm::getOperationalSleepPeriod( uint16_t& operationalSleepPeriod )
{
	return configRegisterAccess( OperationalSleepPeriod, operationalSleepPeriod );
}


uint8_t radio_xbee_dm::getOperationalWakePeriod( uint16_t& operationalWakePeriod )
{
	return configRegisterAccess( OperationalWakePeriod, operationalWakePeriod );
}


void radio_xbee_dm::setReceiverAddress( uint8_t *address )
{
	memcpy( &receiverAddress[0], address, 8 );
}


void radio_xbee_dm::getReceiverAddress( uint8_t *address )
{
	memcpy( address, &receiverAddress[0], 8 );
}


void radio_xbee_dm::getBroadcastAddress( uint8_t *address )
{
	memcpy( address, &broadcastAddress[0], 8 );
}


void radio_xbee_dm::getSourceAddress( uint8_t *address )
{
	memcpy( address, &sourceAddress[0], 8 );
}


uint8_t radio_xbee_dm::getReceivePayloadLength()
{
	return receivePayloadLength;
}


void radio_xbee_dm::wrapper_XBeeUART_RX( uint32_t inputChar )
{

	switch ( bufferCount )
	{
	// Verify the first byte, which is transfered to the USART in this communication-cycle is the expected Start-Delimiter 0x7E
	// Otherwise stop reception of the Frame.
	case 0:
		if ( ( uint8_t )inputChar != 0x7E ) // API-Response Frame wrong
		{
			responseReceived = true;
			responseStatus = 0xFF;
			bufferCount = 0;
		}
		else
			bufferCount++;
		break;

	// Process the Frame-Length Information, send by the XBee-Module
	// The Union named temp is used to assign the MSB/LSB information to an uint16_t
	case 1:
		temp.reg.msb = ( uint8_t )inputChar;
		bufferCount++;
		break;

	case 2:
		temp.reg.lsb = ( uint8_t )inputChar;
		bufferCount++;
		break;

	default:

		// Fill the SRAM-Buffer with the Data-Bytes of the Frame currently received on the UART
		if ( bufferCount < ( temp.length + 3 ) )
		{
			frame.array[bufferCount] = ( uint8_t )inputChar;
			bufferCount++;
		}

		// Reception of the Data is completed, now the XBee-Module's Response-Frame is processed.
		// Status-variables are set to default, to enable the reception of a new API-Frame from the XBee-Module
		else
		{
			bufferCount = 0;
			receivedFrameChecksum = ( uint8_t )inputChar;
			responseStatus = processingResponse();
		}
		break;
	}
}


uint8_t radio_xbee_dm::processingChecksum()
{
	uint32_t tempChecksum = 0;

	// Calculation of the Checksum
	for ( uint8_t i = 3; i < ( temp.length + 3 ); i++ )
	{
		// In the first step the Hex-Values of all "PAYLOAD-BYTES" (excluding Start-Delimiter, Length MSB/LSB,
		// and the Checksum itself are summed up
		tempChecksum += frame.array[i];
	}

	// Masking the Result of the Checksum Calculation with 0x000000FF. The Low-Byte of the Checksum remains unchanged, while the upper 24-bit
	// are forced to Zero. The Outcome of the bitwise AND - Operation is send to the XBee-Module at the end of the Frame.
	return ( 0xFF - ( ( uint8_t )( tempChecksum & 0x000000FF ) ) );
}


uint8_t radio_xbee_dm::processingResponse()
{

	switch ( frame.atCommand.frameType )
	{
	case AT_CommandResponseFrame:
		return processingCommandResponse();

	case ModemStatusFrame:
		processingModemStatusFrame();
		break;

	case TransmitStatusFrame:
		processingTransmitStatus();
		break;

	case ReceivePacketFrame:
		processingReceivePacket();
		break;

	default:
		return 0xFF;
	}

	// return value, which is indicates, the Frame is invalid!
	return 0xFE;
}


uint8_t radio_xbee_dm::processingCommandResponse()
{

	// Check for a valid checksum of the received CommandResponse API-Frame
	if ( ( processingChecksum() == receivedFrameChecksum ) )
	{
		// Set driver status flag to indicate the finalization of reception and processing of the status-frame
		responseReceived = true;

		return frame.atCommandResponse.status;
	}

	else
		// Indicate an invalid checksum
		return 0xFF;
}


void radio_xbee_dm::processingModemStatusFrame()
{
	// Check for a valid checksum of the received ModemStatus API-Frame
	if ( radio_xbee_dm::processingChecksum() == receivedFrameChecksum )
	{
		// Pick the Status information from the correct position inside the frame and make
		// it available for the get-method()
		modemStatus = frame.modemStatus.status;
	}
	else
		// Indicate an invalid checksum
		modemStatus = 0xFF;
}


void radio_xbee_dm::processingTransmitStatus()
{
	// Check for a valid checksum of the received TransmitStatus API-Frame
	if ( processingChecksum() == receivedFrameChecksum )
	{
		// Pick the Status information from the correct position inside the frame and make
		// it available for the get-methods()
		transmitRetryCount = frame.transmitStatusDm.transmitRetryCount;
		deliveryStatus     = frame.transmitStatusDm.deliveryStatus;
		discoveryStatus    = frame.transmitStatusDm.discoveryStatus;

		// Set driver status flag to indicate the finalization of reception and processing of the status-frame
		packetTransmitted = true;
	}
	else
	{
		// Indicate an invalid Checksum
		transmitRetryCount = deliveryStatus = discoveryStatus = 0xFF;
	}
}


void radio_xbee_dm::processingReceivePacket()
{

	// Check for a valid checksum of the received ReceivePacket API-Frame
	if ( ( processingChecksum() == receivedFrameChecksum ) && !packetReceived )
	{
		// Calculate and store the Payload-Length of the received Packet
		*userPayloadLength = temp.length - 12;

		// Copy the Data-Packets Payload to the user-defined buffer
		memcpy( userDataBuffer, frame.receivePacketDm.receiveData, ( *userPayloadLength ) );

		// The MSB is not contained in RF-Packet, due to the fact that it is always expected to be 0x00,
		// therefore set the byte manually
		*userSourceAddress = 0x00 ;

		// Copy the 7-Byte remaining Bytes from their position inside the API-Frame-Buffer to the user-defined buffer
		memcpy( userSourceAddress + 1, frame.receivePacketDm.sourceAdress64, 7 );

		// Pick the Status information from the correct position inside the frame and make
		// it available for the get-method()
		receiveOptions = frame.receivePacketDm.receiveOptions;

		// Set driver status flag to indicate the finalization of reception and processing of the status-frame
		packetReceived = true;
	}

	else
	{
		// Indication of a pending packet in side the buffer,
		// a packetPending value of > 1 indicates a dropped packet!
		packetPending++;
	}
}


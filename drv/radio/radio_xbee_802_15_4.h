/*
 * radio_xbee_802_15_4.h
 */

#ifndef RADIO_XBEE_802_15_4_H_
#define RADIO_XBEE_802_15_4_H_

#include <stdint.h>
#include <radio_xbee.h>
#include "em_gpio.h"
#include "em_usart.h"


/****************************************************************************************************************************************//**
 * @brief
 *  Definition of XBEE-Parameter-naming
 *
 *******************************************************************************************************************************************/

const char writeValues[2]            = {'W', 'R'};
const char restoreDefaults[2]        = {'R', 'E'};
const char softwareReset[2]          = {'F', 'R'};

const char channel[2]                = {'C', 'H'};
const char panId[2]                  = {'I', 'D'};
const char xbeeRetries[2]            = {'R', 'R'};
const char destinationAddressHigh[2] = {'D', 'H'};
const char destinationAddressLow[2]  = {'D', 'L'};
const char serialNumberHigh[2]       = {'S', 'H'};
const char serialNumberLow[2]        = {'S', 'L'};

const char macMode[2]                = {'M', 'M'};
const char coordinatorEnable[2]      = {'C', 'E'};

const char sleepMode[2]              = {'S', 'M'};
const char timeBeforeSleep[2]        = {'S', 'T'};
const char sleepOptions[2]           = {'S', 'O'};
const char cyclicSleepPeriod[2]      = {'S', 'P'};

const char powerLevel[2]             = {'P', 'L'};

const char firmwareVersion[2]        = {'V', 'R'};
const char hardwareVersion[2]        = {'H', 'V'};
const char receivedSignalStrength[2] = {'D', 'B'};
const char ackFailures[2]            = {'E', 'A'};

const char applyChanges[2]           = {'A', 'C'};


/****************************************************************************************************************************************//**
 * @brief
 *  Definition of the configuration
 *
 *******************************************************************************************************************************************/

struct CONFIG
{
	uint32_t  destinationAddressHigh;
	uint32_t  destinationAddressLow;

	uint16_t  serialNumberHigh;
	uint16_t  serialNumberLow;

	uint16_t  panId;
	uint8_t   xbeeRetries;
	uint8_t   macMode;

	uint8_t   powerLevel;

	uint8_t   coordinatorEnable;
	uint8_t   receivedSignalStrength;
	uint16_t  firmwareVersion;
	uint16_t  hardwareVersion;
	uint16_t  ackFailures;

	uint8_t   sleepMode;
	uint8_t   sleepOptions;
	uint16_t  timeBeforeSleep;
	uint16_t  cyclicSleepPeriod;
};




/****************************************************************************************************************************************//**
 * @brief
 *  Declaration of the radio_xbee_802_15_4-class as radio_xbee-subclass.
 *
 *******************************************************************************************************************************************/

class radio_xbee_802_15_4 : public radio_xbee
{
private:
	typedef enum
	{
		atCommandResponseFrame  = 0x88,
		transmitStatusFrame     = 0x89,
		modemStatusFrame        = 0x8A,
		receivePacket64Frame    = 0x80
	} FRAME_TYPE;

	static uint8_t processingResponse();
	static uint8_t processingCommandResponse();
	static void processingModemStatusFrame();
	static void processingTransmitStatus();
	static void processingReceivePacket();

	static void wrapperXBeeUartRx( uint32_t inputChar );


public:
	radio_xbee_802_15_4();
	~radio_xbee_802_15_4() {}

	void initializeSystemBuffer( uint8_t *buffer, uint8_t *sourceAddress, uint8_t *payloadLength );
	ptISR_Handler getISR_FunctionPointer();

	bool sendPacket( uint8_t *data, uint8_t payloadLength, uint8_t broadcast = 0, bool acknowledge = true );
	bool getPacketReceived();
	bool getPendingPacketData();

	uint8_t getLocalXBeeMAC( MAC_XBee address );
	uint8_t getPAN_ID( uint16_t& panId );
	uint8_t setPAN_ID( uint16_t panId );
	uint8_t getChannel( uint8_t& channelNumber );
	uint8_t setChannel( uint8_t channelNumber );

	uint8_t getConfig( CONFIG& configuration );
	uint8_t setConfig( CONFIG configuration );
};

#endif /* XBEE_RADIO_802_15_4_ */

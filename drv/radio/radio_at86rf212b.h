/**
 * @file radio_at86rf212b.h
 *
 * Driver: radio_at86rf212b
 *
 * Driver Atmel AT86RF212B radio transceiver
 */

#ifndef RADIO_AT86RF212B_H_
#define RADIO_AT86RF212B_H_


// Platform selection
#if SENTIO_EM_ENABLE
#include "sentio_em_io.h"
#include "mcu_efm32.h"

#define AT86RF212B_SPI_MOSI_PIN    RADIO_TX
#define AT86RF212B_SPI_MISO_PIN    RADIO_RX
#define AT86RF212B_SPI_CLK_PIN     RADIO_CLK
#define AT86RF212B_SPI_CS_PIN      RADIO_CS
#define AT86RF212B_SLP_TR          RADIO_DI0
#define AT86RF212B_DIG1            RADIO_DI1
#define AT86RF212B_DIG2            RADIO_DI2
#define AT86RF212B_DIG3            RADIO_DI3
#define AT86RF212B_DIG4            RADIO_DI4
#define AT86RF212B_IRQ             RADIO_DI5
#define AT86RF212B_RSTN            RADIO_RESET_PIN

#define AT86RF212B_ISR_MASK        0b0000001000000000

#define AT86RF212B_USART           RADIO_USART
#define AT86RF212B_USART_CLOCK     RADIO_USART_CLOCK
#define AT86RF212B_LOCATION        RADIO_USART_LOC

#define AT86RF212B_CLOCK_MODE      usartClockMode0
#define AT86RF212B_BAUDRATE        4000000
#define AT86RF212B_DATABITS        usartDatabits8
#define AT86RF212B_ENABLE          usartEnable
#define AT86RF212B_REF_FREQ        MCU_CLOCK
#define AT86RF212B_MASTER          true
#define AT86RF212B_MSBF            true
#elif SENTIO_GW_ENABLE
#include "sentio_gw_io.h"
#include "mcu_efm32.h"

#define AT86RF212B_SPI_MOSI_PIN    RADIO_TX
#define AT86RF212B_SPI_MISO_PIN    RADIO_RX
#define AT86RF212B_SPI_CLK_PIN     RADIO_CLK
#define AT86RF212B_SPI_CS_PIN      RADIO_CS
#define AT86RF212B_SLP_TR          RADIO_XBEE_AD0_PIN
#define AT86RF212B_DIG1            RADIO_XBEE_AD1_PIN
#define AT86RF212B_DIG2            RADIO_XBEE_AD2_PIN
#define AT86RF212B_DIG3            RADIO_XBEE_AD3_PIN
#define AT86RF212B_DIG4            RADIO_XBEE_AD4_PIN
#define AT86RF212B_IRQ             RADIO_XBEE_DO8_PIN
#define AT86RF212B_RSTN            RADIO_RESET_PIN

#define AT86RF212B_ISR_MASK        0b0000010000000000

#define AT86RF212B_USART           RADIO_USART
#define AT86RF212B_USART_CLOCK     RADIO_USART_CLOCK
#define AT86RF212B_LOCATION        RADIO_USART_LOC

#define AT86RF212B_CLOCK_MODE      usartClockMode0
#define AT86RF212B_BAUDRATE        4000000
#define AT86RF212B_DATABITS        usartDatabits8
#define AT86RF212B_ENABLE          usartEnable
#define AT86RF212B_REF_FREQ        MCU_CLOCK
#define AT86RF212B_MASTER          true
#define AT86RF212B_MSBF            true
#elif SENTIO_CAM_ENABLE
#include "sentio_cam_io.h"
#include "mcu_efm32.h"

#define AT86RF212B_SPI_MOSI_PIN    RADIO_TX_PIN
#define AT86RF212B_SPI_MISO_PIN    RADIO_RX_PIN
#define AT86RF212B_SPI_CLK_PIN     RADIO_CLK_PIN
#define AT86RF212B_SPI_CS_PIN      RADIO_CS_PIN
#define AT86RF212B_SLP_TR          RADIO_SLP_TR_PIN
#define AT86RF212B_DIG1            RADIO_DIG1_PIN
#define AT86RF212B_DIG2            RADIO_DIG2_PIN
#define AT86RF212B_DIG3            RADIO_DIG3_PIN
#define AT86RF212B_DIG4            RADIO_DIG4_PIN
#define AT86RF212B_IRQ             RADIO_IRQ_PIN
#define AT86RF212B_RSTN            RADIO_RSTN_PIN

#define AT86RF212B_ISR_MASK        0b0000001000000000

#define AT86RF212B_USART           RADIO_USART
#define AT86RF212B_USART_CLOCK     RADIO_USART_CLOCK
#define AT86RF212B_LOCATION        RADIO_USART_LOC

#define AT86RF212B_CLOCK_MODE      usartClockMode0
#define AT86RF212B_BAUDRATE        4000000
#define AT86RF212B_DATABITS        usartDatabits8
#define AT86RF212B_ENABLE          usartEnable
#define AT86RF212B_REF_FREQ        MCU_CLOCK
#define AT86RF212B_MASTER          true
#define AT86RF212B_MSBF            true
#else
#error No Sensor Platform defined in application_config.h
#endif

// Application configuration check
#ifndef RADIO_AT86RF212B_ENABLE
#error Missing define in application_config.h: RADIO_AT86RF212B_ENABLE
#endif

#define AT86RF212B_REG_READ           0x80
#define AT86RF212B_REG_WRITE          0xC0
#define AT86RF212B_FRAME_BUF_READ     0x20
#define AT86RF212B_FRAME_BUF_WRITE    0x60
#define AT86RF212B_SRAM_READ          0x00
#define AT86RF212B_SRAM_WRITE         0x40

#define AT86RF212B_ADDR_TRX_STATUS    0x01 // get Status
#define AT86RF212B_ADDR_TRX_STATE     0x02 // strobe
#define AT86RF212B_ADDR_TRX_CTRL_0    0x03 //
#define AT86RF212B_ADDR_TRX_CTRL_1    0x04 //
#define AT86RF212B_ADDR_PHY_TX_PWR    0x05
#define AT86RF212B_ADDR_PHY_RSSI      0x06 // Read
#define AT86RF212B_ADDR_PHY_ED_LEVEL  0x07 // Read
#define AT86RF212B_ADDR_PHY_CC_CCA    0x08 //
#define AT86RF212B_ADDR_CCA_THRES     0x09 //
#define AT86RF212B_ADDR_RX_CTRL       0x0A //
#define AT86RF212B_ADDR_SFD_VALUE     0x0B //
#define AT86RF212B_ADDR_TRX_CTRL_2    0x0C //
#define AT86RF212B_ADDR_ANT_DIV       0x0D //
#define AT86RF212B_ADDR_IRQ_MASK      0x0E // 
#define AT86RF212B_ADDR_IRQ_STATUS    0x0F // get Interrupt Status
#define AT86RF212B_ADDR_VREG_CTRL     0x10 // just debugging
#define AT86RF212B_ADDR_BATMON        0x11 //
#define AT86RF212B_ADDR_XOSC_CTRL     0x12 //
#define AT86RF212B_ADDR_CC_CTRL_0     0x13 //
#define AT86RF212B_ADDR_CC_CTRL_1     0x14 //
#define AT86RF212B_ADDR_RX_SYN        0x15 //
#define AT86RF212B_ADDR_RF_CTRL_0     0x16 //
#define AT86RF212B_ADDR_XAH_CTRL_1    0x17 //
#define AT86RF212B_ADDR_FTN_CTRL      0x18 // trigger
#define AT86RF212B_ADDR_PLL_CF        0x1A // trigger
#define AT86RF212B_ADDR_PLL_DCU       0x1B // trigger
#define AT86RF212B_ADDR_PART_NUM      0x1C //
#define AT86RF212B_ADDR_VERSION_NUM   0x1D //
#define AT86RF212B_ADDR_MAN_ID_0      0x1E //
#define AT86RF212B_ADDR_MAN_ID_1      0x1F //
#define AT86RF212B_ADDR_SHORT_ADDR_0  0x20 //
#define AT86RF212B_ADDR_SHORT_ADDR_1  0x21 //
#define AT86RF212B_ADDR_PAN_ID_0      0x22 //
#define AT86RF212B_ADDR_PAN_ID_1      0x23 //
#define AT86RF212B_ADDR_IEEE_ADDR_0   0x24 //
#define AT86RF212B_ADDR_IEEE_ADDR_1   0x25 //
#define AT86RF212B_ADDR_IEEE_ADDR_2   0x26 //
#define AT86RF212B_ADDR_IEEE_ADDR_3   0x27 //
#define AT86RF212B_ADDR_IEEE_ADDR_4   0x28 //
#define AT86RF212B_ADDR_IEEE_ADDR_5   0x29 //
#define AT86RF212B_ADDR_IEEE_ADDR_6   0x2A //
#define AT86RF212B_ADDR_IEEE_ADDR_7   0x2B //
#define AT86RF212B_ADDR_XAH_CTRL_0    0x2C //
#define AT86RF212B_ADDR_CSMA_SEED_0   0x2D
#define AT86RF212B_ADDR_CSMA_SEED_1   0x2E
#define AT86RF212B_ADDR_CSMA_BE       0x2F

#define AT86RF212B_NOP_CMD           0x00
#define AT86RF212B_TX_START_CMD      0x02
#define AT86RF212B_FORCE_TRX_OFF_CMD 0x03
#define AT86RF212B_FORCE_PLL_ON_CMD  0x04
#define AT86RF212B_RX_ON_CMD         0x06
#define AT86RF212B_TRX_OFF_CMD       0x08
#define AT86RF212B_PLL_ON_CMD        0x09
#define AT86RF212B_RX_AACK_ON_CMD    0x16
#define AT86RF212B_TX_ARET_ON_CMD    0x19

/** Commands */
enum TRX_CMD
{
	nopCmd         = AT86RF212B_NOP_CMD,
	txStartCmd     = AT86RF212B_TX_START_CMD,
	forceTrxOffCmd = AT86RF212B_FORCE_TRX_OFF_CMD,
	forcePllOnCmd  = AT86RF212B_FORCE_PLL_ON_CMD,
	rxOnCmd        = AT86RF212B_RX_ON_CMD,
	trxOffCmd      = AT86RF212B_TRX_OFF_CMD,
	pllOnCmd       = AT86RF212B_PLL_ON_CMD,
	rxAackCmd      = AT86RF212B_RX_AACK_ON_CMD,
	txAretCmd      = AT86RF212B_TX_ARET_ON_CMD
};

#define AT86RF212B_P_ON_STATUS                  0x00
#define AT86RF212B_BUSY_RX_STATUS               0x01
#define AT86RF212B_BUSY_TX_STATUS               0x02
#define AT86RF212B_RX_ON_STATUS                 0x06
#define AT86RF212B_TRX_OFF_STATUS               0x08
#define AT86RF212B_PLL_ON_STATUS                0x09
#define AT86RF212B_SLEEP_STATUS                 0x0F
#define AT86RF212B_BUSY_RX_AACK_STATUS          0x11
#define AT86RF212B_BUSY_TX_ARET_STATUS          0x12
#define AT86RF212B_RX_AACK_ON_STATUS            0x16
#define AT86RF212B_TX_ARET_ON_STATUS            0x19
#define AT86RF212B_RX_ON_NOCLK_STATUS           0x1C
#define AT86RF212B_RX_AACK_ON_NOCLK_STATUS      0x1D
#define AT86RF212B_BUSY_RX_AACK_NOCLK_STATUS    0x1E
#define AT86RF212B_TRANSITION_IN_PROGRESS       0x1F

/** Status definitions */
enum TRX_STATUS
{
	pOnStatus             = AT86RF212B_P_ON_STATUS,
	busyRxStatus          = AT86RF212B_BUSY_RX_STATUS,
	busyTxStatus          = AT86RF212B_BUSY_TX_STATUS,
	trxOffStatus          = AT86RF212B_TRX_OFF_STATUS,
	pllOnStatus           = AT86RF212B_PLL_ON_STATUS,
	rxOnStatus            = AT86RF212B_RX_ON_STATUS,
	trxOff                = AT86RF212B_TRX_OFF_STATUS,
	sleepStatus           = AT86RF212B_SLEEP_STATUS,
	busyRxAackStatus      = AT86RF212B_BUSY_RX_AACK_STATUS,
	busyTxAretStatus      = AT86RF212B_BUSY_TX_ARET_STATUS,
	rxAackOnStatus        = AT86RF212B_RX_AACK_ON_STATUS,
	txAretOnStatus        = AT86RF212B_TX_ARET_ON_STATUS,
	rxOnNoClkStatus       = AT86RF212B_RX_ON_NOCLK_STATUS,
	rxAackNoClkStatus     = AT86RF212B_RX_AACK_ON_NOCLK_STATUS,
	busyRxAackNoClkStatus = AT86RF212B_BUSY_RX_AACK_NOCLK_STATUS,
	transInProgressStatus = AT86RF212B_TRANSITION_IN_PROGRESS
};

#define AT86RF212B_PAD_IO_CURRENT_2             0x00
#define AT86RF212B_PAD_IO_CURRENT_4             0x01
#define AT86RF212B_PAD_IO_CURRENT_6             0x02
#define AT86RF212B_PAD_IO_CURRENT_8             0x03

/** Output Current Limit */
enum PAD_IO_CURRENT
{
	current2mA = AT86RF212B_PAD_IO_CURRENT_2,
	current4mA = AT86RF212B_PAD_IO_CURRENT_4,
	current6mA = AT86RF212B_PAD_IO_CURRENT_6,
	current8mA = AT86RF212B_PAD_IO_CURRENT_8
};

#define AT86RF212B_CLKM_CTRL_OFF                0x00
#define AT86RF212B_CLKM_CTRL_1MHZ               0x01
#define AT86RF212B_CLKM_CTRL_2MHZ               0x02
#define AT86RF212B_CLKM_CTRL_4MHZ               0x03
#define AT86RF212B_CLKM_CTRL_8MHZ               0x04
#define AT86RF212B_CLKM_CTRL_16MHZ              0x05
#define AT86RF212B_CLKM_CTRL_250KHZ             0x06
#define AT86RF212B_CLKM_CTRL_IEEE_SYM           0x07

/** Clock options */
enum CLKM_CTRL
{
	clockOff       = AT86RF212B_CLKM_CTRL_OFF,
	clock1Mhz      = AT86RF212B_CLKM_CTRL_1MHZ,
	clock2Mhz      = AT86RF212B_CLKM_CTRL_2MHZ,
	clock4Mhz      = AT86RF212B_CLKM_CTRL_4MHZ,
	clock8Mhz      = AT86RF212B_CLKM_CTRL_8MHZ,
	clock16Mhz     = AT86RF212B_CLKM_CTRL_16MHZ,
	clock250Khz    = AT86RF212B_CLKM_CTRL_250KHZ,
	ieeeSymbolrate = AT86RF212B_CLKM_CTRL_IEEE_SYM,
};

#define AT86RF212B_SPI_CMD_MODE_DEFAULT         0x00
#define AT86RF212B_SPI_CMD_MODE_MON_TRX_STATUS  0x01
#define AT86RF212B_SPI_CMD_MODE_MON_PHY_RSSI    0x02
#define AT86RF212B_SPI_CMD_MODE_IRQ_STATUS      0x03

/** Define the Chips return value, when MCU sends the Register address */
enum SPI_CMD_MODE
{
	allBitsZero      = AT86RF212B_SPI_CMD_MODE_DEFAULT,
	monitorTrxStatus = AT86RF212B_SPI_CMD_MODE_MON_TRX_STATUS,
	monitorPhyRssi   = AT86RF212B_SPI_CMD_MODE_MON_PHY_RSSI,
	monitorIrqStatus = AT86RF212B_SPI_CMD_MODE_IRQ_STATUS
};

#define AT86RF212B_EUROPE_BAND_11     0xA0
#define AT86RF212B_EUROPE_BAND_10     0x80
#define AT86RF212B_EUROPE_BAND_9      0xE4
#define AT86RF212B_EUROPE_BAND_8      0xE6
#define AT86RF212B_EUROPE_BAND_7      0xE7
#define AT86RF212B_EUROPE_BAND_6      0xE8
#define AT86RF212B_EUROPE_BAND_5      0xE9
#define AT86RF212B_EUROPE_BAND_4      0xEA
#define AT86RF212B_EUROPE_BAND_3      0xCB
#define AT86RF212B_EUROPE_BAND_2      0xCC
#define AT86RF212B_EUROPE_BAND_1      0xCD
#define AT86RF212B_EUROPE_BAND_0      0xAD

#define AT86RF212B_EUROPE_BAND_M1     0x47
#define AT86RF212B_EUROPE_BAND_M2     0x48
#define AT86RF212B_EUROPE_BAND_M3     0x49
#define AT86RF212B_EUROPE_BAND_M4     0x29
#define AT86RF212B_EUROPE_BAND_M5     0x90
#define AT86RF212B_EUROPE_BAND_M6     0x91
#define AT86RF212B_EUROPE_BAND_M7     0x93
#define AT86RF212B_EUROPE_BAND_M8     0x94
#define AT86RF212B_EUROPE_BAND_M9     0x2F
#define AT86RF212B_EUROPE_BAND_M10    0x30
#define AT86RF212B_EUROPE_BAND_M11    0x31
#define AT86RF212B_EUROPE_BAND_M12    0x0F

#define AT86RF212B_EUROPE_BAND_M13    0x10
#define AT86RF212B_EUROPE_BAND_M14    0x11
#define AT86RF212B_EUROPE_BAND_M15    0x12
#define AT86RF212B_EUROPE_BAND_M16    0x13
#define AT86RF212B_EUROPE_BAND_M17    0x14
#define AT86RF212B_EUROPE_BAND_M18    0x15
#define AT86RF212B_EUROPE_BAND_M19    0x17
#define AT86RF212B_EUROPE_BAND_M20    0x18
#define AT86RF212B_EUROPE_BAND_M21    0x19
#define AT86RF212B_EUROPE_BAND_M22    0x1A
#define AT86RF212B_EUROPE_BAND_M23    0x1B
#define AT86RF212B_EUROPE_BAND_M24    0x1C
#define AT86RF212B_EUROPE_BAND_M25    0x1D

/** Power level definitions */
enum TX_PWR
{
	dbm11 = AT86RF212B_EUROPE_BAND_11,
	dbm10 = AT86RF212B_EUROPE_BAND_10,
	dbm9  = AT86RF212B_EUROPE_BAND_9,
	dbm8  = AT86RF212B_EUROPE_BAND_8,
	dbm7  = AT86RF212B_EUROPE_BAND_7,
	dbm6  = AT86RF212B_EUROPE_BAND_6,
	dbm5  = AT86RF212B_EUROPE_BAND_5,
	dbm4  = AT86RF212B_EUROPE_BAND_4,
	dbm3  = AT86RF212B_EUROPE_BAND_3,
	dbm2  = AT86RF212B_EUROPE_BAND_2,
	dbm1  = AT86RF212B_EUROPE_BAND_1,
	dbm0  = AT86RF212B_EUROPE_BAND_0
};

#define AT86RF212B_LEAD_TIME_2US    0x00
#define AT86RF212B_LEAD_TIME_4US    0x01
#define AT86RF212B_LEAD_TIME_6US    0x02
#define AT86RF212B_LEAD_TIME_8US    0x03

/** Power amplifier timings */
enum PA_LT
{
	paLeadTime2us = AT86RF212B_LEAD_TIME_2US,
	paLeadTime4us = AT86RF212B_LEAD_TIME_4US,
	paLeadTime6us = AT86RF212B_LEAD_TIME_6US,
	paLeadTime8us = AT86RF212B_LEAD_TIME_8US
};

#define AT86RF212B_GC_TX_OFFS_0     0x00
#define AT86RF212B_GC_TX_OFFS_1     0x01
#define AT86RF212B_GC_TX_OFFS_2     0x02

/** Offset between the TX power control word */
enum GC_TX_OFFS
{
	offset0dbm = AT86RF212B_GC_TX_OFFS_0,
	offset1dbm = AT86RF212B_GC_TX_OFFS_1,
	offset2dbm = AT86RF212B_GC_TX_OFFS_2
};

#define AT86RF212B_CCA_MODE_CS_OR_ENERGY   0x00
#define AT86RF212B_CCA_MODE_ENERGY         0x01
#define AT86RF212B_CCA_MODE_CS             0x02
#define AT86RF212B_CCA_MODE_CS_AND_ENERGY  0x03

/** CCA mode options */
enum CCA_MODE
{
	csOrEnergyAboveTh  = AT86RF212B_CCA_MODE_CS_OR_ENERGY,
	energyAboveTh      = AT86RF212B_CCA_MODE_ENERGY,
	csAboveTh          = AT86RF212B_CCA_MODE_CS,
	csAndEnergyAboveTh = AT86RF212B_CCA_MODE_CS_AND_ENERGY
};

/** Digital clock jitter module enable */
enum JCM_EN
{
	jcmEnable  = 0x02,
	jcmDisable = 0x00

};

#define AT86RF212B_CHANNEL                0x00

/** Channel definition */
enum CHANNEL
{
	ieee868Mhz = AT86RF212B_CHANNEL
};

#define AT86RF212B_OQPSK_DATA_RATE_100_250     0x00
#define AT86RF212B_OQPSK_DATA_RATE_200_500     0x01
#define AT86RF212B_OQPSK_DATA_RATE_400_1000    0x02
#define AT86RF212B_OQPSK_DATA_RATE_RES_500     0x03

/** Data rate options */
enum OQPSK_DATA_RATE
{
	low100high250  = AT86RF212B_OQPSK_DATA_RATE_100_250,
	low200high500  = AT86RF212B_OQPSK_DATA_RATE_200_500,
	low400high1000 = AT86RF212B_OQPSK_DATA_RATE_400_1000,
	lowReshigh500  = AT86RF212B_OQPSK_DATA_RATE_RES_500
};

#define AT86RF212B_IRQ_BAT_LOW              0x80
#define AT86RF212B_IRQ_TRX_UR               0x40
#define AT86RF212B_IRQ_AMI                  0x20
#define AT86RF212B_IRQ_CCA_ED_DONE          0x10
#define AT86RF212B_IRQ_TRX_END              0x08
#define AT86RF212B_IRQ_RX_START             0x04
#define AT86RF212B_IRQ_PLL_UNLOCK           0x02
#define AT86RF212B_IRQ_PLL_LOCK             0x01

/** Interrupt masks */
enum IRQ_MASK {
	batteryLow            = AT86RF212B_IRQ_BAT_LOW,
	bufferAccessViolation = AT86RF212B_IRQ_TRX_UR,
	addressMatching       = AT86RF212B_IRQ_AMI,
	ccaEdDone             = AT86RF212B_IRQ_CCA_ED_DONE,
	frameEndingRxTx       = AT86RF212B_IRQ_TRX_END,
	frameRxStart          = AT86RF212B_IRQ_RX_START,
	pllUnlock             = AT86RF212B_IRQ_PLL_UNLOCK,
	pllLock               = AT86RF212B_IRQ_PLL_LOCK
};

#define AT86RF212B_XTAL_MODE_INTERN  0x0F
#define AT86RF212B_XTAL_MODE_EXTERN  0x04

/** Clock mode options */
enum XTAL_MODE
{
	internalOsc = AT86RF212B_XTAL_MODE_INTERN,
	externalOSc = AT86RF212B_XTAL_MODE_EXTERN
};

#define AT86RF212B_RX_OVERRIDE_DISABLE            0x00
#define AT86RF212B_RX_OVERRIDE_IPAN_ED_LQ_ENABLE  0x06

/** Control the RXO functions during RX phase */
enum RX_OVERRIDE
{
	allDisabled     = AT86RF212B_RX_OVERRIDE_DISABLE,
	ipanEdLqEnabled = AT86RF212B_RX_OVERRIDE_IPAN_ED_LQ_ENABLE
};

#define AT86RF212B_AACK_FVN_MODE_FRAME_0       0x00
#define AT86RF212B_AACK_FVN_MODE_FRAME_0_1     0x01
#define AT86RF212B_AACK_FVN_MODE_FRAME_0_1_2   0x02
#define AT86RF212B_AACK_FVN_MODE_ALL           0x03

/** Controls the ACK behavior */
enum AACK_FVN_MODE
{
	acceptFrameTypeZero           = AT86RF212B_AACK_FVN_MODE_FRAME_0,
	acceptFrameTypeZeroOrOne      = AT86RF212B_AACK_FVN_MODE_FRAME_0_1,
	acceptFrameTypeZeroOrOneOrTwo = AT86RF212B_AACK_FVN_MODE_FRAME_0_1_2,
	acceptAllFrames               = AT86RF212B_AACK_FVN_MODE_ALL
};

struct AT86RF212B_CONFIG
{
	union {
		struct AT86RF212B_REG_TRX_CTRL_0
		{
			CLKM_CTRL      clkMCtrl   : 3;
			bool           clkMShaSel : 1;
			PAD_IO_CURRENT padIo      : 2;
			PAD_IO_CURRENT padIoClkM  : 2;
		} reg;
		uint8_t val;
	} trxCtrl0;

	union {
		struct AT86RF212B_REG_TRX_CTRL_1
		{
			bool         irqPolarity  : 1;
			bool         irqMaskMode  : 1;
			SPI_CMD_MODE spiCmdMode   : 2;
			bool         rxBlRtrl     : 1;
			bool         txAutoCrcOn  : 1;
			bool         extIrqEnable : 1;
			bool         extPaEnable  : 1;
		} reg;
		uint8_t val;
	} trxCtrl1;

	TX_PWR  phyTxPwr;

	union {
		struct AT86RF212B_REG_PHY_CC_CCA
		{
			CHANNEL  channel    : 5;
			CCA_MODE ccaMode    : 2;
			bool     ccaRequest : 1;
		} reg;
		uint8_t val;
	} phyCcCca;

	union {
		struct AT86RF212B_REG_CCA_THRES
		{
			uint8_t ccaEdThres : 4;
			uint8_t ccaCsThres : 4;
		} reg;
		uint8_t val;
	} ccaThres;

	JCM_EN  clockJitterModule;

	uint8_t sdvValue;

	union {
		struct AT86RF212B_REG_TRX_CTRL_2
		{
			uint8_t oQpskDataRate   : 2;
			bool subMode            : 1;
			bool bpskOqpsk          : 1;
			bool altSpectrum        : 1;
			bool oQqpskScramEnable  : 1;
			bool trxOffAvddEnable   : 1;
			bool rxSaveMode         : 1;
		} reg;
		uint8_t val;
	} trxCtrl2;

	union {
		struct AT86RF212B_REG_XOSC_CTRL
		{
			uint8_t   xtalTrim      : 4;
			XTAL_MODE xtalMode      : 4;
		} reg;
		uint8_t val;
	} xoscCtrl;

	uint8_t ccNumber;

	union {
		struct AT86RF212B_REG_CC_CTRL_1
		{
			uint8_t ccBand          : 3;
			uint8_t reservedA       : 5;
		} reg;
		uint8_t val;
	} ccCtrl1;

	union {
		struct AT86RF212B_REG_RX_SYN
		{
			uint8_t     rxPathLevel   : 4;
			RX_OVERRIDE rxOverride    : 3;
			bool        rxPathDisable : 1;
		} reg;
		uint8_t val;
	} rxSyn;

	union {
		struct AT86RF212B_REG_RF_CTRL_0
		{
			GC_TX_OFFS gainCalibOffset : 2;
			uint8_t    reservedB       : 2;
			uint8_t    reservedC       : 2;
			PA_LT      paLeadTime      : 2;
		} reg;
		uint8_t val;
	} rfCtrl;

	union {
		struct AT86RF212B_REG_XAH_CTRL_1
		{
			bool reservedD                 : 1;
			bool promiscuousMode           : 1;
			bool ackResponseTime           : 1;
			bool reservedE                 : 1;
			bool uploadReservedFrameTypes : 1;
			bool filterReservedFrameTypes  : 1;
			bool csmaLbtModeUsed           : 1;
			bool reservedF                 : 1;

		} reg;
		uint8_t val;
	} xahCtrl1;

	union
	{
		struct AT86RF212B_REG_CSMA_SEED_1
		{
			uint8_t       csmaSeed1       : 3;
			bool          aackIamCoord    : 1;
			bool          aackDisable     : 1;
			bool          aackPendingData : 1;
			AACK_FVN_MODE fvnMode         : 2;
		} reg;
		uint8_t val;
	} csmaSeed1;

	uint8_t csmaSeed0;

	union
	{
		struct AT86RF212B_REG_XAH_CTRL_0
		{
			bool    slottetOperation : 1;
			uint8_t maxCsmaRetries   : 3;
			uint8_t maxFrameRetries  : 4;
		} reg;
		uint8_t val;
	} xahCtrl0;

	union
	{
		struct AT86RF212B_REG_CSMA_BE
		{
			uint8_t minimal  : 4;
			uint8_t maximal  : 4;
		} reg;
		uint8_t val;
	} csmaBackoff;

	union
	{
		struct PAN_ID
		{
			uint8_t lsb;
			uint8_t msb;
		} reg;
		uint16_t val;
	} panId;

	union
	{
		struct SHORT_ADDR
		{
			uint8_t lsb;
			uint8_t msb;
		} reg;
		uint16_t val;
	} shortAddr;

	uint8_t ieeeAddr[8];
};

#define IEEE_802_15_4_FRAME_TYPE_BEACON      0x00
#define IEEE_802_15_4_FRAME_TYPE_DATA        0x01
#define IEEE_802_15_4_FRAME_TYPE_ACKNOWLEDGE 0x02
#define IEEE_802_15_4_FRAME_TYPE_MAC_COMMAND 0x03

/** Frame type definitions */
enum FRAME_TYPE
{
	beacon = IEEE_802_15_4_FRAME_TYPE_BEACON,
	data   = IEEE_802_15_4_FRAME_TYPE_DATA,
	ack    = IEEE_802_15_4_FRAME_TYPE_ACKNOWLEDGE,
	macCom = IEEE_802_15_4_FRAME_TYPE_MAC_COMMAND
};

#define IEEE_802_15_4_NO_PAN_NO_ADDRESS 0x00
#define IEEE_802_15_4_16_BIT_ADDRESS    0x02
#define IEEE_802_15_4_64_BIT_ADDRESS    0x03

/** Addressing modes */
enum ADDRESSING
{
	noPanNoAddress = IEEE_802_15_4_NO_PAN_NO_ADDRESS,
	address16Bit   = IEEE_802_15_4_16_BIT_ADDRESS,
	address64Bit   = IEEE_802_15_4_64_BIT_ADDRESS
};

#define IEEE_802_15_4_VERSION_2003  0x00
#define IEEE_802_15_4_VERSION_2006  0x01

/** Frame versions */
enum FRAME_VERSION
{
	version2003 = IEEE_802_15_4_VERSION_2003,
	version2006 = IEEE_802_15_4_VERSION_2006
};

union DATA_FRAME_DEFAULT
{
	struct FRAME_HEADER
	{
		uint8_t frameLength         : 8;

		FRAME_TYPE frameType        : 3;
		bool secEnable              : 1;
		bool framePending           : 1;
		bool ackRequest             : 1;
		bool panIdCompression       : 1;
		uint8_t reservedX           : 1;

		uint8_t reservedY           : 2;
		ADDRESSING destAddressing   : 2;
		FRAME_VERSION frameVersion  : 2;
		ADDRESSING sourceAddressing : 2;

		uint8_t sequenceNumber;

		uint16_t destAddress;
		uint16_t destPanId;
		uint16_t sourceAddress;
		uint16_t sourcePanId;

		uint8_t payload[114];
	} packet;

	uint8_t array[127];
};


/**
 * Driver class for the AT86RF212B radio
 *
 * The class contains the interface to interact with the radio transceiver.
 */
class radio_at86rf212b
{
private:
	USART_InitSync_TypeDef  interfaceInit;
	int8_t rssiBaseValue;

	void setSystemReg( const uint8_t address, const uint8_t data );
	uint8_t getSystemReg( const uint8_t address );

	void strobe( TRX_CMD strobeCommand );

public:
	radio_at86rf212b();
	~radio_at86rf212b() {}

	/**
	 * Initializes the interface to the host MCU.
	 *
	 * The function initializes the SPI interface and GPIO pin configurations between the transceiver and the host MCU.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Resets the radio transceiver.
	 *
	 * The function performs a chip reset by pulling the RST hardware pin of the radio low.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void reset();

	/**
	 * Sets the default configuration.
	 *
	 * The function configures the transceiver with a default configuration.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setConfig();

	/**
	 * Sets a user configuration.
	 *
	 * The function configures the transceiver according to a user configuration given in AT86RF212B_CONFIG format (see radio_at86rf212b.h).
	 *
	 * @param[in]  config: the configuration to be used
	 * @param[out] none
	 * @return     void
	 */
	void setConfig( AT86RF212B_CONFIG &config );

	/**
	 * Set the IEEE 802.15.4 receiver addresses
	 *
	 * This function sets the 16-bit short address, PAN ID and 64-bit address
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setAddress( const uint16_t address, const uint16_t panId, uint8_t const * ieeeAddress );

	/**
	 * Sets the transceiver to receive mode.
	 *
	 * The function performs a switch to receive state, so the transceiver starts listening for incoming packages.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setReceiveMode();

	/**
	 * Sets the transceiver to sleep mode.
	 *
	 * The function performs a switch to sleep state, which deactivates all communication abilities, but conserves energy.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setSleepMode();

	/**
	 * Sets the transceiver to idle mode.
	 *
	 * The function performs a switch to sleep state, which deactivates all communication abilities, but conserves energy.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void setIdleMode();

	/**
	 * Sends a packet.
	 *
	 * The function sends a packet with given payload.
	 *
	 * @param[in]  payload: the payload to be transmitted
	 * @param[in]  payloadLength: the length of the payload
	 * @param[out] none
	 * @return     void
	 */
	void sendPacket( uint8_t* payload, const uint8_t payloadLength );

	/**
	 * Read the payload
	 *
	 * After a frameEndingRxTx interrupt occurred a data packet is valid and can be readout.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void readPacketDirect( uint8_t* payload, uint8_t payloadMax, uint8_t &payloadLength );

	/**
	 * Read the status byte of the AT86RF212B
	 *
	 * This function returns the status byte of the AT86RF212B radio
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t AT86RF212B status byte
	 */
	uint8_t getStatus();

	/**
	 * Read the interrupt status byte of the AT86RF212B
	 *
	 * This function reads the interrupt status, which tells type of interrupt that has been triggered by the AT86RF212B radio.
	 * Read this register to reset the interrupt pin and to control the MCU program flow.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t interrupt status/type of interrupt source
	 */
	uint8_t getInterruptStatus();

	/**
	 * Gets the current RSSI value.
	 *
	 * The function returns the received signal strength indicator of the last received packet. This can
	 * be used as an indicator for the link quality.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     int8_t rssi value
	 */
	int8_t getRssi();

	/**
	 * Get the ED (Energy Detection) measurement result
	 *
	 * The receiver ED measurement (ED scan procedure) can be used as a part of a channel selection algorithm.
	 * It is an estimation of the received signal power within the bandwidth of an IEEE 802.15.4 channel.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     int8_t ed value
	 */
	int8_t getEdLevel();
};

#endif  /*AT86RF212B RADIO */

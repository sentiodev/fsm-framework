/***************************************************
  This is a library for our Adafruit FONA Cellular Module

  Designed specifically to work with the Adafruit FONA
  ----> http://www.adafruit.com/products/1946
  ----> http://www.adafruit.com/products/1963

  These displays use TTL Serial to communicate, 2 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include "radio_gsm_fona.h"

time_delay       radio_gsm_fona::delay;

radio_gsm_fona::radio_gsm_fona()
{
	CMU_ClockEnable( cmuClock_GPIO, true );
	CMU_ClockEnable( GSM_FONA_USART_CLOCK, true );
	
	// Configure the IO-Port driver of the EFM32-GPIO Pins which were selected before
	GPIO_PinModeSet( GSM_FONA_RX_PIN,   gpioModeInput, 0 ); // RX needs to be a Input
	GPIO_PinModeSet( GSM_FONA_TX_PIN,   gpioModePushPull, 1 );        // TX needs to be a Output
	GPIO_PinModeSet( GSM_FONA_RSTN_PIN, gpioModePushPull, 1 );
	GPIO_PinModeSet( GSM_FONA_PWR_PIN,  gpioModePushPull, 1 );
	GPIO_PinModeSet( GSM_FONA_PWR_STATUS_PIN,  gpioModeInput, 0 );
	
}


void radio_gsm_fona::power(bool powerStatus)
{
	if( powerStatus )
	{
		if( !GPIO_PinInGet(GSM_FONA_PWR_STATUS_PIN) )
		{
			GPIO_PinOutClear( GSM_FONA_PWR_PIN );
			delay.milli( 4000 );
			GPIO_PinOutSet( GSM_FONA_PWR_PIN );
			
			while(!GPIO_PinInGet(GSM_FONA_PWR_STATUS_PIN));
			delay.milli( 2000 );
			
			init();
		}
	}
	else
	{
		if(GPIO_PinInGet(GSM_FONA_PWR_STATUS_PIN) )
		{
			if( GPIO_PinInGet(GSM_FONA_PWR_STATUS_PIN) )
			{
				GPIO_PinOutClear( GSM_FONA_PWR_PIN );
				delay.milli( 2000 );
				GPIO_PinOutSet( GSM_FONA_PWR_PIN );
				while(GPIO_PinInGet(GSM_FONA_PWR_STATUS_PIN));
			}
		}
	}
}


bool radio_gsm_fona::init()
{
// // turn off Echo!
	USART_InitAsync_TypeDef initGsmUart;
	initGsmUart.enable       = usartEnable;
	initGsmUart.refFreq      = GSM_FONA_REF_FREQ;
	initGsmUart.baudrate     = GSM_FONA_BAUDRATE;
	initGsmUart.databits     = usartDatabits8;
	initGsmUart.oversampling = usartOVS16;
	initGsmUart.parity       = usartNoParity;
	initGsmUart.stopbits     = usartStopbits1;
	USART_InitAsync( GSM_FONA_USART, &initGsmUart );

	// The Routing Register of the selected UART/USART Register is configured. The following register-access
	// enables the UART modules RX/TX shift-register and furthermore selects the one of the possible Locations of the modules IO-Pins
	//
	// NOTE!!!
	// Beside setting a modules Routing Register the functionality of the GPIO-Pins IO-Port-driver has to be configured separately

	GSM_FONA_USART->ROUTE = USART_ROUTE_RXPEN |
							USART_ROUTE_TXPEN |
							GSM_FONA_LOCATION;
	sendCheckReply( "ATE0", "OK" );
	delay.milli( 100 );

	if ( ! sendCheckReply( "ATE0", "OK" ) )
	{
		return false;
	}

	return true;
}

bool radio_gsm_fona::reset()
{
	GPIO_PinOutSet( GSM_FONA_RSTN_PIN );
	delay.milli( 10 );
	GPIO_PinOutClear( GSM_FONA_RSTN_PIN );
	delay.milli( 100 );
	GPIO_PinOutSet( GSM_FONA_RSTN_PIN );

	// give 3 seconds to reboot
	delay.milli( 5000 );
// 	while(!GPIO_PinInGet(GSM_FONA_PWR_STATUS_PIN));

	sendCheckReply( "AT", "OK" );
	delay.milli( 100 );
	sendCheckReply( "AT", "OK" );
	delay.milli( 100 );
	sendCheckReply( "AT", "OK" );
	delay.milli( 100 );
}

/********* Real Time Clock ********************************************/

/* returns value in mV (uint16_t) */
bool radio_gsm_fona::readRTC( uint8_t *year, uint8_t *, uint8_t *, uint8_t *, uint8_t *, uint8_t * )
{
	uint16_t v;
	sendParseReply( "AT+CCLK?", "+CCLK: ", &v, '/', 0 );
	*year = v;

	return true;
}

bool radio_gsm_fona::enableRTC( uint8_t i )
{
	if ( ! sendCheckReply( "AT+CLTS=", i, "OK" ) )
		return false;

	return sendCheckReply( "AT&W", "OK" );
}


// /********* BATTERY & ADC ********************************************/

/* returns value in mV (uint16_t) */
uint8_t radio_gsm_fona::getBattVoltage( uint16_t *v )
{
	return sendParseReply( "AT+CBC", "+CBC: ", v, ',', 2 );
}

/* returns the percentage charge of battery as reported by sim800 */
bool radio_gsm_fona::getBattPercent( uint16_t *p )
{
	return sendParseReply( "AT+CBC", "+CBC: ", p, ',', 1 );
}

bool radio_gsm_fona::getADCVoltage( uint16_t *v )
{
	return sendParseReply( "AT+CADC?", "+CADC: 1,", v );
}

// /********* SIM ***********************************************************/
uint8_t radio_gsm_fona::unlockSim( SIM_PIN pin )
{
	char sendbuff[14] = "AT+CPIN=";
	sendbuff[8] = pin[0];
	sendbuff[9] = pin[1];
	sendbuff[10] = pin[2];
	sendbuff[11] = pin[3];
	sendbuff[12] = '\0';

	int8_t intermediate = !sendCheckReply( sendbuff, "OK" );
	
	readline();
	const char answer[] = "+CPIN: READY";
	for(uint32_t i =0; answer[i]!= '\0'; i++)
	{
		if( answer[i] != replybuffer[i])
		{
// 			USART_Tx(UART0,answer[i]);
			intermediate = -1;
		}
	}

	readline();
	const char answer1[] = "Call Ready";
	for(uint32_t i =0; answer1[i]!= '\0'; i++)
	{
		if( answer1[i] != replybuffer[i])
			intermediate = -2;
	}

	readline(); 
	const char answer2[] = "SMS Ready";
	for(uint32_t i =0; answer2[i]!= '\0'; i++)
	{
		if( answer2[i] != replybuffer[i])
			intermediate = -3;
	}

	return intermediate;
}

uint8_t radio_gsm_fona::getSimCcid( char *ccid )
{
	getReply( "AT+CCID" );

// up to 20 chars
	strncpy( ccid, replybuffer, 20 );
	ccid[20] = '\0';

	readline(); // eat 'OK'

	return strlen( ccid );
}

/********* IMEI **********************************************************/

uint8_t radio_gsm_fona::getImei( char *imei )
{
	getReply( "AT+GSN" );

// up to 15 chars
	strncpy( imei, replybuffer, 15 );
	imei[15] = '\0';

	readline(); // eat 'OK'

	return strlen( imei );
}

/********* NETWORK *******************************************************/

uint8_t radio_gsm_fona::getNetworkStatus( void )
{
	uint16_t status;

	if ( ! sendParseReply( "AT+CREG?", "+CREG: ", &status, ',', 1 ) ) return 0;

	return status;
}


uint8_t radio_gsm_fona::getRssi( void )
{
	uint16_t reply;

	if ( ! sendParseReply( "AT+CSQ", "+CSQ: ", &reply ) ) return 0;

	return reply;
}

/********* PWM/BUZZER **************************************************/

bool radio_gsm_fona::PWM( uint16_t period, uint8_t duty ) 
{
	if ( period > 2000 ) return false;
	if ( duty > 100 ) return false;

	return sendCheckReply( "AT+SPWM=0," , period, duty, "OK" );
}

/********* SMS **********************************************************/

int8_t radio_gsm_fona::getNumSms( void ) 
{
	uint16_t numsms;

	if ( ! sendCheckReply( "AT+CMGF=1", "OK" ) ) return -1;
// ask how many sms are stored

	if ( ! sendParseReply( "AT+CPMS?", "+CPMS: \"SM_P\",", &numsms ) ) return -1;

	return numsms;
}

// Reading SMS's is a bit involved so we don't use helpers that may cause delays or debug
// printouts!
bool radio_gsm_fona::readSms( uint8_t i, char *smsbuff, uint16_t maxlen, uint16_t *readlen )
{
// text mode
	if ( ! sendCheckReply( "AT+CMGF=1", "OK" ) ) return false;

// show all text mode parameters
	if ( ! sendCheckReply( "AT+CSDH=1", "OK" ) ) return false;

// parse out the SMS len
	uint16_t thesmslen = 0;

//getReply(F("AT+CMGR="), i, 1000);  //  do not print debug!
	print( "AT+CMGR=" );
	printLn( i );
	readline( 1000 ); // timeout

// parse it out...
	parseReply( "+CMGR:", &thesmslen, ',', 11 );

	readRaw( thesmslen );

	flushInput();

	uint16_t thelen = min( maxlen, strlen( replybuffer ) );
	strncpy( smsbuff, replybuffer, thelen );
	smsbuff[thelen] = 0; // end the string

	*readlen = thelen;
	return true;
}

// Retrieve the sender of the specified SMS message and copy it as a string to
// the sender buffer.  Up to senderlen characters of the sender will be copied
// and a null terminator will be added if less than senderlen charactesr are
// copied to the result.  Returns true if a result was successfully retrieved,
// otherwise false.
bool radio_gsm_fona::getSmsSender( uint8_t i, char *sender, int senderlen )
{
// Ensure text mode and all text mode parameters are sent.
	if ( ! sendCheckReply( "AT+CMGF=1" , "OK" ) ) return false;
	if ( ! sendCheckReply( "AT+CSDH=1" , "OK" ) ) return false;
// Send command to retrieve SMS message and parse a line of response.
	print( "AT+CMGR=" );
	printLn( i );
	readline( 1000 );
// Parse the second field in the response.
	bool result = parseReplyQuoted( "+CMGR:", sender, senderlen, ',', 1 );
// Drop any remaining data from the response.
	flushInput();

	return result;
}

uint8_t radio_gsm_fona::sendSms( char *smsaddr, char *smsmsg )
{
	if ( !sendCheckReply( "AT+CMGF=1", "OK" ) )
		return -2;

	delay.milli(1000);

	char sendcmd[30] = "AT+CMGS=\"";
	strncpy( sendcmd + 9, smsaddr, 30 - 9 - 2 ); // 9 bytes beginning, 2 bytes for close quote + null
	sendcmd[strlen( sendcmd )] = '\"';

	if ( ! sendCheckReply( sendcmd, "> " ) )
		return -3;

	delay.milli(1000);

	printLn( smsmsg );
	printLn("");
	write( 0x1A );

	readline( 10000 ); // read the +CMGS reply, wait up to 10 seconds!!!
	
	if ( strstr( replybuffer, "+CMGS" ) == 0 ) 
	{
		return -4;
	}
	readline( 1000 ); // read OK

	if ( strcmp( replybuffer, "OK" ) != 0 ) 
	{
		return -5;
	}

	return 0;
}


bool radio_gsm_fona::deleteSms( uint8_t i )
{
	if ( ! sendCheckReply( "AT+CMGF=1", "OK" ) ) return -1;
// read an sms
	char sendbuff[12] = "AT+CMGD=000";
	sendbuff[8] = ( i / 100 ) + '0';
	i %= 100;
	sendbuff[9] = ( i / 10 ) + '0';
	i %= 10;
	sendbuff[10] = i + '0';

	return sendCheckReply( sendbuff, "OK", 2000 );
}

/********* TIME **********************************************************/

bool radio_gsm_fona::enableNetworkTimeSync( bool onoff )
{
	if ( onoff ) {
		if ( ! sendCheckReply( "AT+CLTS=1" , "OK" ) )
			return false;
	} else {
		if ( ! sendCheckReply( "AT+CLTS=0" , "OK" ) )
			return false;
	}

	flushInput(); // eat any 'Unsolicted Result Code'

	return true;
}

bool radio_gsm_fona::enableNTPTimeSync( bool onoff, const char *ntpserver )
{
	if ( onoff )
	{
		if ( ! sendCheckReply( "AT+CNTPCID=1", "OK" ) )
			return false;

		print( "AT+CNTP=\"" );
		if ( ntpserver != 0 ) 
		{
			print( "83.168.200.199" );
		} else {
			print( "83.168.200.199" );
		}
		printLn( "\",0" );
		readline( FONA_DEFAULT_TIMEOUT_MS );
		if ( strcmp( replybuffer, "OK" ) != 0 )
			return false;

		if ( ! sendCheckReply( "AT+CNTP", "OK" , 10000 ) )
			return false;

		uint16_t status;
		readline( 10000 );
		if ( ! parseReply( "+CNTP:", &status ) )
			return false;
	}
	else
	{
		if ( ! sendCheckReply( "AT+CNTPCID=0", "OK" ) )
			return false;
	}

	return true;
}


bool radio_gsm_fona::getTime( char *buff, uint16_t maxlen )
{
	getReply( "AT+CCLK?", ( uint16_t ) 10000 );
	if ( strncmp( replybuffer, "+CCLK: ", 7 ) != 0 )
		return false;

	char *p = replybuffer + 7;
	uint16_t lentocopy = min( maxlen - 1, strlen( p ) );
	strncpy( buff, p, lentocopy + 1 );
	buff[lentocopy] = 0;

	readline(); // eat OK

	return true;
}


bool radio_gsm_fona::getTime(date& localTime)
{
	getReply( "AT+CCLK?", ( uint16_t ) 10000 );

		if ( strncmp( replybuffer, "+CCLK: ", 7 ) != 0 )
		return false;
	/*
	for(uint8_t i  = 8; i <sizeof("YYxMMxDDxhhxmmxss")+8; i++)
		USART_Tx(UART0, replybuffer[i]);*/
	
	date intermediate(&replybuffer[8],"YYxMMxDDxhhxmmxss",sizeof("YYxMMxDDxhhxmmxss"));
	
	localTime = intermediate;
}


/********* GPRS **********************************************************/

int8_t radio_gsm_fona::enableGPRS( bool onoff, const char *apnUsp, const char *usernameUsp, const char *passwordUsp ) 
{

	if ( onoff ) 
	{
		if ( !sendCheckReply( "AT+CGATT=1", "OK" ) )
			return -1;

		// set bearer profile! connection type GPRS
		if ( !sendCheckReply( "AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"", "OK", 10000 ) )
			return -2;

		// set bearer profile access point name
		if ( apnUsp )
		{
			// Send command AT+SAPBR=3,1,"APN","<apn value>" where <apn value> is the configured APN value.
			if ( ! sendCheckReplyQuoted( "AT+SAPBR=3,1,\"APN\",", apnUsp, "OK", 10000 ) )
				return -3;

			// set username/password
			if ( usernameUsp )
			{
				// Send command AT+SAPBR=3,1,"USER","<user>" where <user> is the configured APN username.
				if ( ! sendCheckReplyQuoted( "AT+SAPBR=3,1,\"USER\",", usernameUsp, "OK" ), 10000 )
					return -4;
			}
			if ( passwordUsp )
			{
				// Send command AT+SAPBR=3,1,"PWD","<password>" where <password> is the configured APN password.
				if ( ! sendCheckReplyQuoted( "AT+SAPBR=3,1,\"PWD\",", passwordUsp, "OK" ), 10000 )
					return -5;
			}
		}

		// open GPRS context
		if ( ! sendCheckReply( "AT+SAPBR=1,1", "OK", 10000 ) )
			return -6;
	}
	else 
	{
		// close GPRS context
		if ( ! sendCheckReply( "AT+SAPBR=0,1", "OK", 10000 ) )
			return -7;

		if ( ! sendCheckReply( "AT+CGATT=0", "OK", 10000 ) )
			return -8;

	}

	return 0;
}

uint8_t radio_gsm_fona::GPRSstate( void )
{
	uint16_t state;
	if ( ! sendParseReply( "AT+CGATT?", "+AT+CGATT: ", &state ) )
		return -1;

	return state;
}


int8_t radio_gsm_fona::openIpConnection( const char* ipAddress, const char* port, const char* mode )
{
	char sendBuffer[50] = "AT+CIPSTART=\"";
	uint8_t j = 13;
	
	for(uint8_t i = 0; i < 3; i++, j++)
	{
		sendBuffer[j] = mode[i];
	}

	sendBuffer[j] = '\"';
	j++;
	sendBuffer[j] = ',';
	j++;
	sendBuffer[j] = '\"';
	j++;
	
	for( uint8_t i = 0; ipAddress[i] != '\0'; i++, j++)
	{
		sendBuffer[j] = ipAddress[i];
	}

	sendBuffer[j] = '\"';
	j++;
	sendBuffer[j] = ',';
	j++;
	sendBuffer[j] = '\"';
	j++;

	for( uint8_t i = 0; port[i]!= '\0'; i++, j++)
	{
		sendBuffer[j] = port[i];
	}

	sendBuffer[j] = '\"';

	if(!sendCheckReply("AT+CIPMUX=0","OK", 10000 ))
		return -1;

	if(!sendCheckReply(sendBuffer,"OK", 10000 ))
		return -2;

	readline();

	return 0;
}


int8_t radio_gsm_fona::sendIpPacket( const char* data, const uint16_t length )
{
	char sendbuffer[20] = "AT+CIPSEND=";
	char temp[4];
	uint8_t i = 0;
	uint16_t intermediate = length;

	do
	{
		temp[i] = intermediate % 10;
		intermediate /= 10;
		i++;
	}
	while ( intermediate );
	
	for(uint8_t j = 11;i>0 ;j++,i--)
		sendbuffer[j] = temp[i-1]+'0';

	if(!sendCheckReply(sendbuffer,"> ", 10000 ))
		return -1;

	if(!sendCheckReply( data, "SEND OK", 10000 ))
		return -2;


	return 0;
}


int8_t radio_gsm_fona::closeIpConnection()
{
	if(!sendCheckReply("AT+CIPCLOSE=0","CLOSE OK", 10000 ))
		return -1;
	
	return 0;
}


bool radio_gsm_fona::getGSMLoc( uint16_t *errorcode, char *buff, uint16_t maxlen )
{

	getReply( "AT+CIPGSMLOC=1,1", ( uint16_t )10000 );

	if ( ! parseReply( "+CIPGSMLOC: ", errorcode ) )
		return -1;

	char *p = replybuffer + 14;
	uint16_t lentocopy = min( maxlen - 1, strlen( p ) );
	strncpy( buff, p, lentocopy + 1 );

	readline(); // eat OK

	return 0;
}

// bool radio_gsm_fona::HTTP_GET_start( char *url, uint16_t *status, uint16_t *datalen )
// {
// 	if ( ! HTTP_initialize( url ) )
// 		return false;
// 
// // HTTP GET
// 	if ( ! sendCheckReply( "AT+HTTPACTION=0", "OK" ) )
// 		return false;
// 
// // HTTP response
// 	if ( ! HTTP_response( status, datalen ) )
// 		return false;
// 
// 	return true;
// }
// 
// void radio_gsm_fona::HTTP_GET_end( void )
// {
// 	HTTP_terminate();
// }
// 
// bool radio_gsm_fona::HTTP_POST_start( char *url,
// 									  const char *contenttype,
// 									  const uint8_t *postdata, uint16_t postdatalen,
// 									  uint16_t *status, uint16_t *datalen ) {
// 	if ( ! HTTP_initialize( url ) )
// 		return false;
// 
// 	if ( ! sendCheckReplyQuoted( "AT+HTTPPARA=\"CONTENT\",", contenttype, "OK", 10000 ) )
// 		return false;
// 
// // HTTP POST data
// 	flushInput();
// 	print( "AT+HTTPDATA=" );
// 	print( postdatalen );
// 	printLn( ",10000" );
// 	readline( FONA_DEFAULT_TIMEOUT_MS );
// 	if ( strcmp( replybuffer, "DOWNLOAD" ) != 0 )
// 		return false;
// 
// 	write( postdata, postdatalen );
// 	readline( 10000 );
// 	if ( strcmp( replybuffer, "OK" ) != 0 )
// 		return false;
// 
// // HTTP POST
// 	if ( ! sendCheckReply( "AT+HTTPACTION=1", "OK" ) )
// 		return false;
// 
// 	if ( ! HTTP_response( status, datalen ) )
// 		return false;
// 
// 	return true;
// }
// 
// void radio_gsm_fona::HTTP_POST_end( void )
// {
// 	HTTP_terminate();
// }
// 
// void radio_gsm_fona::setUserAgent( const char *useragentUsp )
// {
// 	useragent = useragentUsp;
// }
// 
// void radio_gsm_fona::setHTTPSRedirect( bool onoff )
// {
// 	httpsredirect = onoff;
// }
// 
// /********* HTTP HELPERS ****************************************/
// 
// bool radio_gsm_fona::HTTP_initialize( char *url )
// {
// // Handle any pending
// 	HTTP_terminate();
// 
// // Initialize and set parameters
// 	if ( ! sendCheckReply( "AT+HTTPINIT", "OK" ) )
// 		return false;
// 	if ( ! sendCheckReply( "AT+HTTPPARA=\"CID\",1", "OK" ) )
// 		return false;
// 	if ( ! sendCheckReplyQuoted( "AT+HTTPPARA=\"UA\",", useragent, "OK", 10000 ) )
// 		return false;
// 
// 	flushInput();
// 	print( "AT+HTTPPARA=\"URL\",\"" );
// 	print( url );
// 	printLn( "\"" );
// 	readline( FONA_DEFAULT_TIMEOUT_MS );
// 	if ( strcmp( replybuffer, "OK" ) != 0 )
// 		return false;
// 
// // HTTPS redirect
// 	if ( httpsredirect )
// 	{
// 		if ( ! sendCheckReply( "AT+HTTPPARA=\"REDIR\",1", "OK" ) )
// 			return false;
// 
// 		if ( ! sendCheckReply( "AT+HTTPSSL=1", "OK" ) )
// 			return false;
// 	}
// 
// 	return true;
// }
// 
// bool radio_gsm_fona::HTTP_response( uint16_t *status, uint16_t *datalen )
// {
// // Read response status
// 	readline( 10000 );
// 
// 	if ( ! parseReply( "+HTTPACTION:", status, ',', 1 ) )
// 		return false;
// 	if ( ! parseReply( "+HTTPACTION:", datalen, ',', 2 ) )
// 		return false;
// 
// // Read response
// 	getReply( "AT+HTTPREAD" );
// 
// 	return true;
// }
// 
// void radio_gsm_fona::HTTP_terminate( void )
// {
// 	flushInput();
// 	sendCheckReply( "AT+HTTPTERM", "OK" );
// }

// /********* LOW LEVEL *******************************************/

inline int radio_gsm_fona::available( void )
{
	return !( GSM_FONA_USART->STATUS & USART_STATUS_RXDATAV );
}

inline int radio_gsm_fona::write( uint8_t x )
{
	USART_Tx( GSM_FONA_USART, x );
	return 1;
}

inline int radio_gsm_fona::read( void )
{
	return GSM_FONA_USART->RXDATA;
}

inline int radio_gsm_fona::peek( void )
{
	return ( GSM_FONA_USART->RXDATAXP ) & 0x0F;
}

inline void radio_gsm_fona::flush()
{
	GSM_FONA_USART->CMD |= USART_CMD_CLEARTX;
}

void radio_gsm_fona::flushInput()
{
// Read all available serial input to flush pending data.
	GSM_FONA_USART->CMD |= USART_CMD_CLEARRX;
}

uint16_t radio_gsm_fona::readRaw( uint16_t b )
{
	uint16_t idx = 0;

	while ( b && ( idx < sizeof( replybuffer ) - 1 ) )
	{
		if ( !( GSM_FONA_USART->STATUS & USART_STATUS_RXDATAV ) )
		{
			replybuffer[idx] = GSM_FONA_USART->RXDATA;
			idx++;
			b--;
		}
	}
	replybuffer[idx] = 0;

	return idx;
}


uint8_t radio_gsm_fona::readline( uint32_t timeout, bool multiline )
{
	uint8_t replyidx = 0;

	uint32_t xx = 9000000;
	while ( xx-- )
	{
		if ( replyidx >= 254 )
		{
			break;
		}

		if ( GSM_FONA_USART->STATUS & USART_STATUS_RXDATAV )
		{
			char c = GSM_FONA_USART->RXDATA;
			if ( c == '\r' )
				continue;

			if ( c == '\n' )
			{
				if ( replyidx == 0 ) // the first 0x0A is ignored
					continue;
				if ( !multiline )
				{
					xx = 0; // the second 0x0A is the end of the line
				}
			}

			replybuffer[replyidx] = c;
			replyidx++;
		}
	}


	replybuffer[replyidx] = '\0';
	
	for(uint8_t i= 0;replybuffer[i] != '\0';i++)
		USART_Tx(UART0,replybuffer[i]);

	return replyidx;
}


uint8_t radio_gsm_fona::getReply( const char *send, uint16_t timeout )
{
	flushInput();

	printLn( send );

	uint8_t l = readline( timeout );

	return l;
}


// Send prefix, suffix, suffix2, and newline. Return response (and also set replybuffer with response).
uint8_t radio_gsm_fona::getReply( const char *prefix, int32_t suffix1, int32_t suffix2, uint16_t timeout )
{
	flushInput();

	print( prefix );
	printHex( suffix1 );
	print( ',' );
	printDec( suffix2 );
	print( "\r\n" );

	uint8_t l = readline( timeout );

	return l;
}


// Send prefix, ", suffix, ", and newline. Return response (and also set replybuffer with response).
uint8_t radio_gsm_fona::getReplyQuoted( const char *prefix, const char *suffix, uint16_t timeout )
{
	flushInput();

	print( prefix );
	print( "\"" );
	print( suffix );
	printLn( "\"" );

	uint8_t l = readline( timeout );

	return l;
}


bool radio_gsm_fona::sendCheckReply( const char *send, const char *reply, uint16_t timeout )
{
	getReply( send, timeout );
/*	
	USART_Tx(UART0, 'T');
	USART_Tx(UART0, 'T');
	
	for(uint32_t i =0; replybuffer[i]!= '\0'; i++)
		USART_Tx(UART0, replybuffer[i]);
		
	USART_Tx(UART0, 'T');
	USART_Tx(UART0, 'T');
	for(uint32_t i =0; reply[i]!= '\0'; i++)
		USART_Tx(UART0, reply[i]);*/

	bool test = true;

	for(uint32_t i =0; reply[i]!= '\0'&& replybuffer[i]!= '\0'; i++)
	{
		if(reply[i] != replybuffer[i])
			test = false;
	}

// 	if(test)
// 		USART_Tx(UART0, 'B');
// 
// 	USART_Tx(UART0, 'U');
// 	USART_Tx(UART0, 'T');
	
	return ( test );
}

// Send prefix, suffix, and newline.  Verify FONA response matches reply parameter.
bool radio_gsm_fona::sendCheckReply( const char *prefix, char *suffix, const char *reply, uint16_t timeout )
{
	getReply( prefix, suffix, timeout );
	
// 	USART_Tx(UART0, 'T');
// 	USART_Tx(UART0, 'T');
// 	
	for(uint32_t i =0; replybuffer[i]!= '\0'; i++)
		USART_Tx(UART0, replybuffer[i]);
// 		
// 	USART_Tx(UART0, 'T');
// 	USART_Tx(UART0, 'T');
	for(uint32_t i =0; reply[i]!= '\0'; i++)
		USART_Tx(UART0, reply[i]);
// 	
// 	USART_Tx(UART0, 'T');
// 	USART_Tx(UART0, 'T');
	
	bool test = true;

	for(uint32_t i =0; reply[i]!= '\0'&& replybuffer[i]!= '\0'; i++)
	{
		if(reply[i] != replybuffer[i])
			test = false;
	}

	return test;
}

// Send prefix, suffix, and newline.  Verify FONA response matches reply parameter.
bool radio_gsm_fona::sendCheckReply( const char *prefix, int32_t suffix, const char *reply, uint16_t timeout )
{
	getReply( prefix, suffix, timeout );

// 	for(uint32_t i =0; replybuffer[i]!= '\0'; i++)
// 		USART_Tx(UART0, replybuffer[i]);
// 
// 	for(uint32_t i =0; reply[i]!= '\0'; i++)
// 		USART_Tx(UART0, reply[i]);

	bool test = true;

	for(uint32_t i =0; reply[i]!= '\0'&& replybuffer[i]!= '\0'; i++)
	{
		if(reply[i] != replybuffer[i])
			test = false;
	}

	return test;
}

// Send prefix, suffix, suffix2, and newline.  Verify FONA response matches reply parameter.
bool radio_gsm_fona::sendCheckReply( const char *prefix, int32_t suffix1, int32_t suffix2, const char *reply, uint16_t timeout )
{
	getReply( prefix, suffix1, suffix2, timeout );
	
	for(uint32_t i =0; replybuffer[i]!= '\0'; i++)
		USART_Tx(UART0, replybuffer[i]);
	
	for(uint32_t i =0; reply[i]!= '\0'; i++)
		USART_Tx(UART0, reply[i]);

	bool test = true;

	for(uint32_t i =0; reply[i]!= '\0'&& replybuffer[i]!= '\0'; i++)
	{
		if(reply[i] != replybuffer[i])
			test = false;
	}

	return test;
}

// Send prefix, ", suffix, ", and newline.  Verify FONA response matches reply parameter.
bool radio_gsm_fona::sendCheckReplyQuoted( const char *prefix, const char *suffix, const char *reply, uint16_t timeout )
{
	getReplyQuoted( prefix, suffix, timeout );
	
	for(uint32_t i =0; replybuffer[i]!= '\0'; i++)
		USART_Tx(UART0, replybuffer[i]);
	
	for(uint32_t i =0; reply[i]!= '\0'; i++)
		USART_Tx(UART0, reply[i]);
	
	bool test = true;

	for(uint32_t i =0; reply[i]!= '\0'&& replybuffer[i]!= '\0'; i++)
	{
		if(reply[i] != replybuffer[i])
			test = false;
	}

	return test;
}


bool radio_gsm_fona::parseReply( const char *toreply, uint16_t *v, char divider, uint8_t index )
{
	char *p = strstr( replybuffer, toreply ); // get the pointer to the voltage
	if ( p == 0 ) return false;
	p += strlen( ( prog_char* )toreply );

	for ( uint8_t i = 0; i < index; i++ )
	{
		// increment dividers
		p = strchr( p, divider );
		if ( !p ) return false;
		p++;
	}

	*v = atoi( p );

	return true;
}


bool radio_gsm_fona::parseReply( const char *toreply, char *v, char divider, uint8_t index )
{
	uint8_t i = 0;
	char *p = strstr( replybuffer, ( prog_char* )toreply );
	if ( p == 0 ) return false;
	p += strlen( ( prog_char* )toreply );

	for ( i = 0; i < index; i++ )
	{
//      increment dividers
		p = strchr( p, divider );
		if ( !p ) return false;
		p++;
	}

	for ( i = 0; i < strlen( p ); i++ )
	{
		if ( p[i] == divider )
			break;
		v[i] = p[i];
	}

	v[i] = '\0';

	return true;
}

// Parse a quoted string in the response fields and copy its value (without quotes)
// to the specified character array (v).  Only up to maxlen characters are copied
// into the result buffer, so make sure to pass a large enough buffer to handle the
// response.
bool radio_gsm_fona::parseReplyQuoted( const char *toreply, char *v, int maxlen, char divider, uint8_t index )
{
	uint8_t i = 0, j;
// Verify response starts with toreply.
	char *p = strstr( replybuffer, ( prog_char* )toreply );
	if ( p == 0 ) return false;
	p += strlen( ( prog_char* )toreply );

// Find location of desired response field.
	for ( i = 0; i < index; i++ ) {
		// increment dividers
		p = strchr( p, divider );
		if ( !p ) return false;
		p++;
	}

// Copy characters from response field into result string.
	for ( i = 0, j = 0; j < maxlen && i < strlen( p ); ++i ) {
		// Stop if a divier is found.
		if ( p[i] == divider )
			break;
		// Skip any quotation marks.
		else if ( p[i] == '"' )
			continue;
		v[j++] = p[i];
	}

// Add a null terminator if result string buffer was not filled.
	if ( j < maxlen )
		v[j] = '\0';

	return true;
}

uint8_t radio_gsm_fona::sendParseReply( const char *tosend, const char *toreply, uint16_t *v, char divider, uint8_t index )
{
	getReply( tosend );

	if ( ! parseReply( toreply, v, divider, index ) ) return false;

	readline(); // eat 'OK'

	return true;
}

void radio_gsm_fona::print( const char* send )
{
	for ( uint32_t i = 0; send[i] != '\0'; i++ )
	{
		USART_Tx( GSM_FONA_USART, send[i] );
	}
}


void radio_gsm_fona::printLn( const char* send )
{
	for ( uint32_t i = 0; send[i] != '\0'; i++ )
	{
		USART_Tx( GSM_FONA_USART, send[i] );
	}

	USART_Tx( GSM_FONA_USART, '\r' );
	USART_Tx( GSM_FONA_USART, '\n' );
}


void radio_gsm_fona::printHex( int32_t input )
{
	uint32_t i = sizeof( int32_t ) << 1;
	uint8_t temp;

	for ( ; i > 0; i-- )
	{
		// Shift and masking the MSB of the input variable
		temp = ( ( uint8_t )( input >> ( ( i - 1 ) << 2 ) ) ) & 0x0F;

		USART_Tx( GSM_FONA_USART, ( temp + ( ( temp <= 0x09 ) ? '0' : '7' ) ) );
	}
}

void radio_gsm_fona::printDec( int32_t input )
{
	uint8_t temp[11];

	int32_t intermediate = input;

	uint32_t i = 0;
	uint8_t  shorten = 0;

	if ( input < 0 )
	{
		intermediate *= -1;
		i++;
	}

	do
	{
		temp[i] = intermediate % 10;
		intermediate /= 10;
		i++;
	}
	while ( intermediate );

	if ( input < 0 )
	{
		USART_Tx( GSM_FONA_USART, '-' );
		shorten = 1;
	}

	do
	{
		i--;
		USART_Tx( GSM_FONA_USART, temp[i] + '0' );
	}
	while ( i - shorten );
}

uint16_t radio_gsm_fona:: min( uint16_t a, uint16_t b )
{
	if ( a < b )
		return a;
	return b;
}

void radio_gsm_fona::write( const uint8_t* send, uint16_t sendLength )
{
	for ( uint8_t i = 0; send[i] < sendLength; i++ )
	{
		USART_Tx( GSM_FONA_USART, send[i] );
	}
}

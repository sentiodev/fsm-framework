/*
 * radio_xbee.h
 */

#ifndef RADIO_XBEE_H_
#define RADIO_XBEE_H_


#include <stdint.h>
#include "em_gpio.h"
#include "em_usart.h"
#include "radio_xbee.h"
#include "em_emu.h"
#include "em_cmu.h"

#include "sentio_em_io.h"

#define XBEE_UART              RADIO_USART
#define XBEE_LOCATION          RADIO_USART_LOC

#define XBEE_ISR_NUMBER        USART0_RX_IRQn
#define XBEE_CLOCK             cmuClock_USART0

#define XBEE_POWER_ENABLE_PIN  RADIO_PWR_PIN
#define XBEE_SLEEP_RQ_PIN      DEBUG_PIN_8

#define XBEE_TX_PIN            RADIO_TX
#define XBEE_RX_PIN            RADIO_RX


/****************************************************************************************************************************************//**
 * @brief
 *  Type-Definition: used to define the Interrupt-Service Routine. Used to operate the module.
 *
 *******************************************************************************************************************************************/

typedef uint8_t MAC_XBee[8];
typedef void ( *ptISR_Handler )( uint32_t );



/****************************************************************************************************************************************//**
 * @brief
 *  Auxiliary-Struct used to handle the Packet-Length
 *
 *******************************************************************************************************************************************/

union LENGTH
{
	struct
	{
		uint8_t lsb;
		uint8_t msb;
	} reg;
	uint16_t length;
};


/****************************************************************************************************************************************//**
 * @brief
 *  Auxiliary-Struct used to handle the different Packet-Frame Types
 *
 *******************************************************************************************************************************************/

union FRAMES
{
	// Identical frames
	struct AT_COMMAND_FRAME
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t atCommand[2];
		uint8_t parameter[4];
	} atCommand;

	struct MODEM_STATUS
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t status;
	} modemStatus;

	struct AT_COMMAND_RESPONSE
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t atCommand[2];
		uint8_t status;
		uint8_t commandData[4];
	} atCommandResponse;

	// XBee 802.15.4 specific frames
	struct RECEIVE_PACKET_64
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t sourceAdress64[8];
		uint8_t rssi;
		uint8_t options;
		uint8_t receiveData[100];
	} receivePacket64;

	struct RECEIVE_PACKET_16
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t sourceAdress16[2];
		uint8_t rssi;
		uint8_t options;
		uint8_t receiveData[100];
	} receivePacket16;

	struct TRANSMIT_REQUEST_64
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t destinationAddress64[8];
		uint8_t options;
		uint8_t radioData[100];
	} transmitPacket64;

	struct TRANSMIT_REQUEST_16
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t destinationAddress16[2];
		uint8_t options;
		uint8_t radioData[100];
	} transmitPacket16;

	struct TRANSMIT_STATUS
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t status;
	} transmitStatus;

	// XBee DigiMesh specific frames
	struct TRANSMIT_STATUS_DM
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t reserveA;
		uint8_t reserveB;
		uint8_t transmitRetryCount;
		uint8_t deliveryStatus;
		uint8_t discoveryStatus;
	} transmitStatusDm;

	struct RECEIVE_PACKET_DM
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t sourceAdress64[7];
		uint8_t reserveA;
		uint8_t reserveB;
		uint8_t receiveOptions;
		uint8_t receiveData[88];
	} receivePacketDm;

	struct TRANSMIT_PACKET_DM
	{
		uint8_t startDelimiter;
		uint8_t lengthMsb;
		uint8_t lengthLsb;
		uint8_t frameType;
		uint8_t frameId;
		uint8_t destinationAddress[8];
		uint8_t reserve[2];
		uint8_t broadcastRadius;
		uint8_t transmitOptions;
		uint8_t radioData[88];
	} transmitPacketDm;

	uint8_t array[114];
};


class radio_xbee
{
protected:
	static LENGTH temp;
	static FRAMES frame;

	static uint8_t bufferCount;
	static uint8_t responseStatus;
	static volatile bool    responseReceived;

	static uint32_t receivedFrameChecksum;

	static uint8_t receiveOptions;
	static uint8_t modemStatus;
	static uint8_t transmitStatus;

	static uint8_t *userDataBuffer;
	static uint8_t *userSourceAddress;
	static uint8_t *userPayloadLength;

	static volatile uint8_t packetTransmitted;

	static bool    packetReceived;
	static bool    packetPending;

	uint8_t sourceAddress[8];
	uint8_t receiverAddress[8];
	uint8_t broadcastAddress[8];
	uint8_t receiveDataBuffer[50];
	uint8_t receivePayloadLength;

	static USART_InitAsync_TypeDef initXBeeUart;


public:
	radio_xbee();
	~radio_xbee() {}

	void init();

	void powerRadio( bool status );

	/**
	* Enables the radio.
	*
	* The function enables the radio module when it is configured with asynchronous pin sleep.
	*
	* @param[in]  none
	* @param[out] none
	* @return     none
	*/
	void enableRadioSm1();

	/**
	* Disables the radio.
	*
	* The function disables the radio module when it is configured with asynchronous pin sleep.
	*
	* @param[in]  none
	* @param[out] none
	* @return     none
	*/
	void disableRadioSm1();

	void setReceiverAddress( uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4, uint8_t byte5, uint8_t byte6, uint8_t byte7, uint8_t byte8 );
	void setBroadcastAddress();
	uint8_t *getReceiverAddress();
	uint8_t *getBroadcastAddress();
	uint8_t *getSourceAddress();
	uint8_t *getReceiveDataBuffer();
	uint8_t *getReceivePayloadLength();

	static uint8_t processingChecksum();

	template<typename T>
	uint8_t configRegisterAccess( const char *command, T& parameter, const bool queueing = false, const bool moduleResponse = true, const bool setRegister = false )
	{
		uint8_t size = sizeof( T );
		uint8_t i;

		bufferCount = 0;
		responseStatus = 0x00;
		responseReceived = false; // signaling the reception of a Response frame from the XBee-Module

		// Configure for set and querying the register correctly
		if ( setRegister )
		{
			temp.length = 0x04 + size;
			frame.atCommand.frameId = 0x4D;
		}
		else
		{
			temp.length = 0x04;
			frame.atCommand.frameId = 0x52;
		}

		frame.atCommand.lengthMsb = temp.reg.msb;
		frame.atCommand.lengthLsb = temp.reg.lsb;

		// The Command frame-Type and frame-Identification is set.
		if ( queueing )
		{
			frame.atCommand.frameType = 0x09; // The parameter value is queued until the AT-Command "Apply Changes" (AC) is issued.
		}

		else
		{
			frame.atCommand.frameType = 0x08; // The XBee-Module executes the command immediately
		}

		// When the XBee-Module response is not required for secured operation of the Application-Code, the User can disable this response.
		// The frame-Identification has to be set to 0x00 regardless which type of frame is transfered to the XBee-Module.
		if ( !moduleResponse )
			frame.atCommand.frameId = 0x00;

		// Set the AT-Command Bytes
		frame.atCommand.atCommand[0] = command[0];
		frame.atCommand.atCommand[1] = command[1];

		// Set the Command Parameter
		// The value of the parameter is not sent in case the user queries the current status
		for ( i = 0; i < size; i++ )
		{
			frame.atCommand.parameter[size - i - 1] = ( uint8_t )( parameter  >> ( i * 8 ) );
		}

		// The data-frame is send to the XBee-Module via the configured UART-interface
		for ( i = 0; i <= ( temp.length + 2 ); i++ )
		{
			USART_Tx( XBEE_UART, frame.array[i] );
		}

		// When all data-Bytes are send out the Checksum, is calculated and will be transfered at the END of the Command-frame

		USART_Tx( XBEE_UART, processingChecksum() );

		// If a Command-Response frame is expected from the XBee-Module the micro-controller is waiting in Low-power mode until the entire
		// Response frame has been transfered to the SRAM-buffer.
		if ( moduleResponse )
		{
			do
			{
				EMU_EnterEM1(); // Maintain the Peripheral-Clock in order to enable the UART-RX mode
			} while ( !responseReceived );
		}

		union PARAMETER
		{
			uint8_t driverPar[ sizeof( T ) ];
			T       userPar;
		} para;

		// When the user queries the value that is currently set, the XBee-Module will respond with
		// a Command Response frame that contains the current value of the register beginning from the 6th "PAYLOAD" byte in the AT-Response frame.
		if ( !setRegister )
		{
			para.userPar = 0;

			uint8_t offset = ( temp.length - 5 );

			for ( i = 0; i < offset; i++ )
			{
				para.driverPar[i] = frame.atCommandResponse.commandData[offset - i - 1];
			}

			parameter = para.userPar;
		}

		for ( volatile uint32_t t = 0; t < 60000; t++ );

		return responseStatus;
	}
};


#endif /* RADIO_XBEE_H_ */

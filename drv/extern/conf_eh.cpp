/**
 * Extension: conf_eh
 *
 * Driver for the ConfEH extension board
 */

#include "conf_eh.h"
#include "em_gpio.h"
#include "em_cmu.h"

adc_ltc2990 conf_eh::ltc2990;

conf_eh::conf_eh()
{
	init();
}

void conf_eh::init()
{
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	// Turn all load resistors off
	setResistor_1( false );
	setResistor_2( false );
	setResistor_3( false );

	// Enable the level translator
	GPIO_PinModeSet( CONFEH_TXS0102EN_PIN, gpioModePushPull, 1 );

	ltcConfig_CVV.temperature = celsius;
	ltcConfig_CVV.mode        = V1_V2V3V4;
	ltcConfig_CVV.aquire      = single;
	ltcConfig_CVV.shuntV12    = ltc2990_shuntOhm;
	ltcConfig_CVV.shuntV34    = 0;
	ltcConfig_CVV.address     = chip0;

	ltcConfig_VVVV.temperature = celsius;
	ltcConfig_VVVV.mode        = V1V2V3V4;
	ltcConfig_VVVV.aquire      = single;
	ltcConfig_VVVV.shuntV12    = ltc2990_shuntOhm;
	ltcConfig_VVVV.shuntV34    = 0;
	ltcConfig_VVVV.address     = chip0;
}

void conf_eh::setConfig( CONFEH_CONFIG confehConfig )
{
	boardConfig = confehConfig;

	switch ( confehConfig )
	{
	case dlc_direct:
		GPIO_PinModeSet( CONFEH_LTC3105EN_PIN, gpioModePushPull, 0 );
		GPIO_PinModeSet( CONFEH_MAX17710AE_PIN, gpioModePushPull, 0 );
		GPIO_PinModeSet( CONFEH_MAX17710LCE_PIN, gpioModePushPull, 0 );
		break;
	case dlc_ltc3105:
		GPIO_PinModeSet( CONFEH_LTC3105EN_PIN, gpioModePushPull, 1 );
		GPIO_PinModeSet( CONFEH_MAX17710AE_PIN, gpioModePushPull, 0 );
		GPIO_PinModeSet( CONFEH_MAX17710LCE_PIN, gpioModePushPull, 0 );
		break;
	case tfb_direct:
		GPIO_PinModeSet( CONFEH_LTC3105EN_PIN, gpioModePushPull, 0 );
		GPIO_PinModeSet( CONFEH_MAX17710AE_PIN, gpioModePushPull, 1 );
		GPIO_PinModeSet( CONFEH_MAX17710LCE_PIN, gpioModePushPull, 0 );
		break;
	case tfb_ltc3105:
		GPIO_PinModeSet( CONFEH_LTC3105EN_PIN, gpioModePushPull, 1 );
		GPIO_PinModeSet( CONFEH_MAX17710AE_PIN, gpioModePushPull, 1 );
		GPIO_PinModeSet( CONFEH_MAX17710LCE_PIN, gpioModePushPull, 0 );
		break;
	default:
		GPIO_PinModeSet( CONFEH_LTC3105EN_PIN, gpioModePushPull, 0 );
		GPIO_PinModeSet( CONFEH_MAX17710AE_PIN, gpioModePushPull, 0 );
		GPIO_PinModeSet( CONFEH_MAX17710LCE_PIN, gpioModePushPull, 0 );
		break;
	}
}

void conf_eh::getMeasurements( float &storageVoltage, float &solarVoltage, float &solarCurrent )
{
	// Configure LTC2990 for Current-Voltage-Voltage measurement
	ltc2990.setConfig( ltcConfig_CVV );
	ltc2990.trigger();

	// Read the storage voltage
	if ( boardConfig == dlc_direct || boardConfig == dlc_ltc3105 )
		ltc2990.getVoltage( storageVoltage, voltageV3 );
	else if ( boardConfig == tfb_direct || boardConfig == tfb_ltc3105 )
		ltc2990.getVoltage( storageVoltage, voltageV4 );

	// Read the solar current
	ltc2990.getCurrent( solarCurrent, currentV12 );

	// Check for LTC3105 reset condition
	if ( solarCurrent < 0.001 )
	{
		// Reset LTC3105
		GPIO_PinOutClear( CONFEH_LTC3105EN_PIN );
		GPIO_PinOutSet( CONFEH_LTC3105EN_PIN );
	}

	// Re-configure LTC2990 for pure voltage measurement
	ltc2990.setConfig( ltcConfig_VVVV );
	ltc2990.trigger();

	// Read the solar voltage
	ltc2990.getVoltage( solarVoltage, voltageV1 );
}

void conf_eh::getSupplyVoltage( float &supplyVoltage )
{
	// Configure LTC2990 for Current-Voltage-Voltage measurement
	ltc2990.setConfig( ltcConfig_CVV );

	// Read the solar current
	ltc2990.getSupplyVoltage( supplyVoltage );
}

void conf_eh::setResistor_1( bool state )
{
	GPIO_PinModeSet( CONFEH_RESISTOR1_PIN, gpioModePushPull, state );
}

void conf_eh::setResistor_2( bool state )
{
	GPIO_PinModeSet( CONFEH_RESISTOR2_PIN, gpioModePushPull, state );
}

void conf_eh::setResistor_3( bool state )
{
	GPIO_PinModeSet( CONFEH_RESISTOR3_PIN, gpioModePushPull, state );
}

/**
 * @file oil_in_water_board.h
 *
 * Extension: oil_in_water_board
 *
 * Driver for the oil_in_water_board extension board
 */

#ifndef OIL_IN_WATER_BOARD_H_
#define OIL_IN_WATER_BOARD_H_

#include "adc_ad7xxx.h"
#include "sensor_sht15.h"

struct OIL_IN_WATER_DATA
{
	float temperature;
	float humidity;
	float oilConcentration;
	uint32_t numberOfSamples;
	union {
		struct {
			bool shtReadingOk    : 1;
			bool ad7988ReadingOk : 1;
		} statusFields;
		uint8_t status;
	} system;
};

class oil_in_water_board
{
public:
	oil_in_water_board();
	~oil_in_water_board() {}

	void getMeasurement( OIL_IN_WATER_DATA& data );
	void startSampling();
	void resetSampling();
	uint32_t getNumberOfSamples();


private:
	adc_ad7xxx   ad7988;
	sensor_sht15 sht;
	
	uint32_t countSamplesTaken;
};

#endif /* OIL_IN_WATER_BOARD_H_ */

/*
 * emulatorbord.cpp
 *
 *  Created on: Sep 26, 2011
 *      Author: Matthias Kr�mer
 */

#include "SystemConfig.h"
#include "Emulatorbord.h"
#include "efm32_gpio.h"


void Emulatorbord::initializeInterface()
{
	// Setup the Pins to Enable Solar Panel 2 and 3
	GPIO_PinModeSet( solarPanel_1, gpioModePushPull, 0 );
	GPIO_PinModeSet( solarPanel_2, gpioModePushPull, 0 );
	GPIO_PinModeSet( solarPanel_3, gpioModePushPull, 0 );

	// Setup the Pins to Configure the Harvesting System
	GPIO_PinModeSet( enableHarvester, gpioModePushPull, 0 );
	GPIO_PinModeSet( enableMPPT,      gpioModePushPull, 0 );

	// Setup the Pins to Select the Energy-Storage
	GPIO_PinModeSet( selectStorageIN,     gpioModePushPull, 0 );
	GPIO_PinModeSet( selectStorageOUT,    gpioModePushPull, 0 );
	GPIO_PinModeSet( enableBattery_LDO,   gpioModePushPull, 0 );
	GPIO_PinModeSet( enableCapacitor_1F,  gpioModePushPull, 0 );
	GPIO_PinModeSet( enableCapacitor_10F, gpioModePushPull, 0 );
	GPIO_PinModeSet( enableCapacitor_22F, gpioModePushPull, 0 );

	initializeInterfaceLTC();
	initializeInterfaceLoad();
}


bool Emulatorbord::setInputSolarCell( uint8_t selection )
{
	switch( selection )
	{
	case disableSolar:
		GPIO_PinOutClear( solarPanel_1 );
		GPIO_PinOutClear( solarPanel_2 );
		GPIO_PinOutClear( solarPanel_3 );

		GPIO_PinOutClear( enableHarvester );
		return true;

	case solarCell_1:
		GPIO_PinOutClear( solarPanel_2 );
		GPIO_PinOutClear( solarPanel_3 );
		GPIO_PinOutSet( solarPanel_1);
		return true;

	case solarCell_2:
		GPIO_PinOutClear( solarPanel_1 );
		GPIO_PinOutClear( solarPanel_3 );
		GPIO_PinOutSet( solarPanel_2 );
		return true;

	case solarCell_3:
		GPIO_PinOutClear( solarPanel_1 );
		GPIO_PinOutClear( solarPanel_2 );
		GPIO_PinOutSet( solarPanel_3 );
		return true;

	case solarCell_12:
		GPIO_PinOutClear( solarPanel_3 );
		GPIO_PinOutSet( solarPanel_1 );
		GPIO_PinOutSet( solarPanel_2 );
		return true;

	case solarCell_13:
		GPIO_PinOutClear( solarPanel_2 );
		GPIO_PinOutSet( solarPanel_1 );
		GPIO_PinOutSet( solarPanel_3 );
		return true;

	case solarCell_23:
		GPIO_PinOutClear( solarPanel_1 );
		GPIO_PinOutSet( solarPanel_2 );
		GPIO_PinOutSet( solarPanel_3 );
		return true;

	case solarCell_123:
		GPIO_PinOutSet( solarPanel_1 );
		GPIO_PinOutSet( solarPanel_2 );
		GPIO_PinOutSet( solarPanel_3 );
		return true;


	default:
		return false;
	}
}


bool Emulatorbord::setHarvester( uint8_t enable )
{
	switch(enable)
	{
	case disableHarvester:
		GPIO_PinOutClear( enableHarvester );
		GPIO_PinOutClear( enableMPPT );
		return true;

	case enableLT_3105:
		GPIO_PinOutSet( enableHarvester );
		GPIO_PinOutSet( enableMPPT );
		return true;

	case enableLT_3105_MPPT:
		GPIO_PinOutSet( enableHarvester );
		GPIO_PinOutClear( enableMPPT );
		return true;

	default:
		return false;
	}
}


void Emulatorbord::powerHarvester( bool enable )
{
	if( enable )
		GPIO_PinOutSet( enableHarvester );
	else
	{
		GPIO_PinOutClear( solarPanel_1 );
		GPIO_PinOutClear( solarPanel_2 );
		GPIO_PinOutClear( solarPanel_3 );

		GPIO_PinOutClear( enableHarvester );
	}
}


bool Emulatorbord::setEnergyStorage( uint16_t type )
{
	switch( type )
	{
	case disable:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutClear( enableCapacitor_22F );

		GPIO_PinOutClear( selectStorageIN );
		GPIO_PinOutSet( selectStorageOUT );
		GPIO_PinOutClear( enableBattery_LDO );
		return true;

	case bat_bat:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutClear( enableCapacitor_22F );

		GPIO_PinOutSet( selectStorageIN );
		GPIO_PinOutSet( selectStorageOUT );
		GPIO_PinOutSet( enableBattery_LDO );
		return true;

	case bat_only:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutClear( enableCapacitor_22F );

		GPIO_PinOutClear( selectStorageIN  );
		GPIO_PinOutSet( selectStorageOUT );
		GPIO_PinOutSet( enableBattery_LDO );
		return true;

	case cap_cap_1F:
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutClear( enableCapacitor_22F );
		GPIO_PinOutSet( enableCapacitor_1F );

		GPIO_PinOutClear( selectStorageIN  );
		GPIO_PinOutClear( selectStorageOUT );
		GPIO_PinOutClear( enableBattery_LDO );
		return true;

	case cap_cap_10F:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_22F );
		GPIO_PinOutSet( enableCapacitor_10F );

		GPIO_PinOutClear( selectStorageIN  );
		GPIO_PinOutClear( selectStorageOUT );
		GPIO_PinOutClear( enableBattery_LDO );
		return true;

	case cap_cap_22F:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutSet( enableCapacitor_22F );

		GPIO_PinOutClear( selectStorageIN  );
		GPIO_PinOutClear( selectStorageOUT );
		GPIO_PinOutClear( enableBattery_LDO );
		return true;

	case cap_1F_bat:
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutClear( enableCapacitor_22F );
		GPIO_PinOutSet( enableCapacitor_1F );

		GPIO_PinOutClear( selectStorageIN );
		GPIO_PinOutSet( selectStorageOUT );
		GPIO_PinOutSet( enableBattery_LDO );
		return true;

	case cap_10F_bat:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_22F );
		GPIO_PinOutSet( enableCapacitor_10F );

		GPIO_PinOutSet( selectStorageIN  );
		GPIO_PinOutSet( selectStorageOUT );
		GPIO_PinOutSet( enableBattery_LDO );
		return true;

	case cap_22F_bat:
		GPIO_PinOutClear( enableCapacitor_1F  );
		GPIO_PinOutClear( enableCapacitor_10F );
		GPIO_PinOutSet( enableCapacitor_22F );

		GPIO_PinOutClear( selectStorageIN );
		GPIO_PinOutSet( selectStorageOUT  );
		GPIO_PinOutSet( enableBattery_LDO );
		return true;

	default:
		return false;
	}
}

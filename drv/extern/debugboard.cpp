/**
 * Extension: debugboard
 *
 * Driver for the debugboard
 */

#include "debugboard.h"

#include "em_cmu.h"


debugboard::debugboard()
{
	init();
}

void debugboard::init()
{
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	GPIO_PinModeSet( DIGI0, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI1, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI2, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI3, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI4, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI5, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI6, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI7, gpioModePushPull, 0 );
	GPIO_PinModeSet( DIGI8, gpioModePushPull, 0 );

	GPIO_PinModeSet( BUTTON0, gpioModeInputPullFilter, 0 );
	GPIO_PinModeSet( BUTTON1, gpioModeInputPullFilter, 0 );
	GPIO_PinModeSet( BUTTON2, gpioModeInputPullFilter, 0 );
	GPIO_PinModeSet( BUTTON3, gpioModeInputPullFilter, 0 );
}

void debugboard::setPin( GPIO_Port_TypeDef port, unsigned int pin )
{
	GPIO->P[port].DOUTSET = 1 << pin;
}

void debugboard::tglPin( GPIO_Port_TypeDef port, unsigned int pin )
{
	GPIO->P[port].DOUTTGL = 1 << pin;
}

void debugboard::clrPin( GPIO_Port_TypeDef port, unsigned int pin )
{
	GPIO->P[port].DOUTCLR = 1 << pin;
}

bool debugboard::getPin( GPIO_Port_TypeDef port, unsigned int pin )
{
	return GPIO_PinInGet( port, pin );
}
/**
 * @file smartlight_board.h
 *
 * Extension: smartlight_board_io
 *
 * Driver for the smart traffic light extension board
 */

#ifndef SMART_LIGHT_BOARD_H_
#define SMART_LIGHT_BOARD_H_

#include "application_config.h"
#include "adc_ltc2990.h"
#include "sensor_sht15.h"

struct SMARTLIGHT_DATA
{
	float temperature;
	float humidity;
	float windSpeed;
	float windDirection;
	float solarRadiation;
	float backupBattery;
	float analogSupply;
	float noiseLevel;

	uint8_t regulatorDataArray[18];

	union {
		struct {
			bool analogV1             : 1;
			bool analogV2             : 1;
			bool analogV3             : 1;
			bool analogV4             : 1;
			bool shtReadingOk         : 1;
			bool rs232InterfaceStatus : 1;
			bool analogSupply         : 1;
			bool regulatorDataReadOk  : 1;
		} statusFields;
		uint8_t status;
	} system;
};

enum SMARTLIGHT_VOLTAGE_REG_STATUS
{
	sleepMode     = 0,
	highPowerMode = 1
};

class smartlight_board
{
public:
	smartlight_board();
	~smartlight_board() {}

	void startWindMeasurement();
	void startSolarMeasurement();
	void getMeasurement( SMARTLIGHT_DATA& data );

	void setVoltageRegStatus( SMARTLIGHT_VOLTAGE_REG_STATUS status );

	void callbackWindSpeed();

private:
	adc_ltc2990   ltc2990;
	sensor_sht15  sht;

	static uint32_t countWindSpeed;
	static uint32_t countSamples;
};

#endif /* SMART_LIGHT_BOARD_H_ */

/**
 * \file debugboard.h
 *
 * Extension: debugboard
 *
 * Driver for the debugboard
 */

#ifndef DEBUGBOARD_H_
#define DEBUGBOARD_H_

#include "em_gpio.h"

// Platform selection
#ifdef SENTIO_EM_ENABLE
#include <sentio_em_io.h>

#define DIGI0   DEBUG_PIN_13
#define DIGI1   DEBUG_PIN_15
#define DIGI2   DEBUG_PIN_16
#define DIGI3   DEBUG_PIN_17
#define DIGI4   DEBUG_PIN_18
#define DIGI5   DEBUG_PIN_21
#define DIGI6   DEBUG_PIN_22
#define DIGI7   DEBUG_PIN_24
#define DIGI8   DEBUG_PIN_28

#define BUTTON0 DEBUG_PIN_29
#define BUTTON1 DEBUG_PIN_29
#define BUTTON2 DEBUG_PIN_29
#define BUTTON3 DEBUG_PIN_29

#define maskInterruptButton1 0x0800
#define maskInterruptButton2 0x1000
#define maskInterruptButton3 0x2000
#define maskInterruptButton4 0x4000
#else
#error No Sensor Platform defined
#endif

/**
 * Driver class for the debugboard
 *
 * The class contains the interface to interract with the debugboard.
 */
class debugboard
{
private:

public:
	debugboard();
	~debugboard() {}

	/**
	 * Initializes the GPIO pins.
	 *
	 * The function initializes the GPIO pins used on the debugboard. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Sets a GPIO pin.
	 *
	 * The function sets a GPIO pin. The pins can be addressed via debugboard definitions.
	 *
	 * @param[in]  port: the port of the pin
	 * @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void setPin( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Toggles a GPIO pin.
	 *
	 * The function toggles a GPIO pin. The pins can be addressed via debugboard definitions.
	 *
	 * @param[in]  port: the port of the pin
	 * @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void tglPin( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Clears a GPIO pin.
	 *
	 * The function clears a GPIO pin. The pins can be addressed via debugboard definitions.
	 *
	 * @param[in]  port: the port of the pin
	 * @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void clrPin( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Gets the binary value of a GPIO pin.
	 *
	 * The function reads the value from the hardware pad of a GPIO pin. The pins can be addressed via debugboard definitions.
	 *
	 * @param[in]  port: the port of the pin
	 * @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     Status of the IO Pin
	 */
	bool getPin( GPIO_Port_TypeDef port, unsigned int pin );
};


#endif /* DEBUGBOARD_H_ */

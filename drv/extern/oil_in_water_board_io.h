/**
 * @file water_in_oil_board_io.h
 *
 * Extension: water_in_oil
 *
 *  Describes connector interfaces of the water_in_oil extension board
 */

// Platform selection
#if SENTIO_EM_ENABLE
#include "sentio_em_io.h"
#define GSM_TX_PIN          SENSOR_UART_TX
#define GSM_RX_PIN          SENSOR_UART_RX
#define GSM_RSTN_PIN        SENSOR_PIN_4
#define GSM_PWR_PIN         SENSOR_PIN_5
#define GSM_PWR_STATUS_PIN  SENSOR_PIN_1
#define GSM_UART            SENSOR_UART
#define GSM_USART_CLOCK     SENSOR_UART_CLOCK
#define GSM_LOCATION        SENSOR_UART_LOC

#define GSM_FONA_BAUDRATE   115200
#define GSM_FONA_REF_FREQ   MCU_CLOCK

#define ADC_SDO_PIN         SENSOR_USART_RX
#define ADC_CLK_PIN         SENSOR_USART_CLK
#define ADC_CNV_PIN         SENSOR_USART_CS

#define ADC_USART           SENSOR_USART
#define ADC_USART_CLOCK     SENSOR_USART_CLOCK
#define ADC_LOCATION        SENSOR_USART_LOC

#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef OIL_IN_WATER_BOARD_ENABLE
#error Missing define in Makefile: OIL_IN_WATER_BOARD_ENABLE
#endif
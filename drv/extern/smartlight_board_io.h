/**
 * @file smartlight_board_io.h
 *
 * Extension: smartlight_board
 *
 *  Describes connector interfaces of the smartlight extension board
 */

// Platform selection
#if SENTIO_EM_ENABLE
#include "sentio_em_io.h"
#define SMARTLIGHT_SHT_ENABLE      POWER_PIN_9
#define SMARTLIGHT_SHT_DATA_PIN    SENSOR_PIN_2
#define SMARTLIGHT_SHT_SCK_PIN     SENSOR_PIN_3

// Control of the PowerSupply and BatteryCharger
#define SMARTLIGHT_TPS_SLEEP_PIN   SENSOR_PIN_13
#define SMARTLIGHT_TPS_PG_PIN      SENSOR_PIN_18
#define SMARTLIGHT_CHARGE_STAT_PIN
#define SMARTLIGHT_CHARGE_PROG_PIN

// PIR Presence Sensors
#define SMARTLIGHT_PIR_ENABLE_PIN
#define SMARTLIGHT_PIR1_PIN
#define SMART_LIGHT_PIR2_PIN

// Wind Sensor (Anemometer)
#define SMARTLIGHT_WIND_ENABLE_PIN  POWER_PIN_3
#define SMARTLIGHT_WINDSPEED_PIN    SENSOR_PIN_17
#define SMARTLIGHT_WINDSPEED_MASK   0x01

// Solar Radiation Sensor
#define SMARTLIGHT_SOLAR_ENABLE_PIN POWER_PIN_1

// Noise LevelSensor (Microphone)
#define SMARTLIGHT_NOISE_ENABLE_PIN POWER_PIN_2

// RS232 Interface
#define SMARTLIGHT_RS232_ENABLE     SENSOR_PIN_4
#define SMARTLIGHT_RS232_FORCE_ON   SENSOR_PIN_8
#define SMARTLIGHT_RS232_FORCE_OFF  SENSOR_PIN_5
#define SMARTLIGHT_RS232_INVALID    SENSOR_PIN_10
#define SMARTLIGHT_RS232_TX         SENSOR_UART_TX
#define SMARTLIGHT_RS232_RX         SENSOR_UART_RX
#define SMARTLIGHT_RS232_CTS        SENSOR_PIN_1
#define SMARTLIGHT_RS232_RTS        SENSOR_PIN_9
#define SMARTLIGHT_RS232            SENSOR_UART
#define SMARTLIGHT_RS232_CLOCK      SENSOR_UART_CLOCK
#define SMARTLIGHT_RS232_RX_ISR     SENSOR_UART_RX_ISR
#define SMARTLIGHT_RS232_LOC        SENSOR_UART_LOC

#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef SMARTLIGHT_ENABLE
#error Missing define in application_config.h: SMARTLIGHT_ENABLE
#endif
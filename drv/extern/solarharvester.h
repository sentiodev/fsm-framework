/*
 * solarharvester.h
 *
 *  Created on: 23 jan 2014
 *      Author: flge1200
 */

#ifndef SOLARHARVESTER_H_
#define SOLARHARVESTER_H_

#include "adc_ltc2990.h"

#ifdef SENTIO_EM_ENABLE
#define _SOLARHARVESTER_TXS0102EN_PORT       gpioPortE
#define _SOLARHARVESTER_TXS0102EN_PIN        7
#define SOLARHARVESTER_TXS0102EN_PIN         _SOLARHARVESTER_TXS0102EN_PORT,_SOLARHARVESTER_TXS0102EN_PIN

#define _SOLARHARVESTER_LTC3129PGOOD_PORT     gpioPortD
#define _SOLARHARVESTER_LTC3129PGOOD_PIN      5
#define SOLARHARVESTER_LTC3129PGOOD_PIN       _SOLARHARVESTER_LTC3129PGOOD_PORT,_SOLARHARVESTER_LTC3129PGOOD_PIN

#define _SOLARHARVESTER_BQ25504VBATOK_PORT   gpioPortE
#define _SOLARHARVESTER_BQ25504VBATOK_PIN    6
#define SOLARHARVESTER_BQ25504VBATOK_PIN     _SOLARHARVESTER_BQ25504VBATOK_PORT,_SOLARHARVESTER_BQ25504VBATOK_PIN

#define _SOLARHARVESTER_RESISTOR1_PORT       gpioPortE
#define _SOLARHARVESTER_RESISTOR1_PIN        3
#define SOLARHARVESTER_RESISTOR1_PIN         _SOLARHARVESTER_RESISTOR1_PORT,_SOLARHARVESTER_RESISTOR1_PIN

#define _SOLARHARVESTER_RESISTOR2_PORT       gpioPortE
#define _SOLARHARVESTER_RESISTOR2_PIN        4
#define SOLARHARVESTER_RESISTOR2_PIN         _SOLARHARVESTER_RESISTOR2_PORT,_SOLARHARVESTER_RESISTOR2_PIN

#define _SOLARHARVESTER_RESISTOR3_PORT       gpioPortE
#define _SOLARHARVESTER_RESISTOR3_PIN        5
#define SOLARHARVESTER_RESISTOR3_PIN         _SOLARHARVESTER_RESISTOR3_PORT,_SOLARHARVESTER_RESISTOR3_PIN
#else
#error No Sensor Platform defined
#endif

#ifndef SOLARHARVESTER_H_
#error Missing define in application_config.h: SOLARHARVESTER_H_
#endif


typedef enum
{
	ltc3129  = 0x00,
	bq25504  = 0x01,
} HARVESTER;


class solarharvester
{
private:
	print<print_serial> *msg;

	static adc_ltc2990 ltc2990U5;
	static adc_ltc2990 ltc2990U6;

	LTC2990_CONFIG ltcConfig_CVV1;
	LTC2990_CONFIG ltcConfig_CVV2;

	HARVESTER harvesterType;

	static const uint8_t ltc2990_shuntOhm = 1;

	bool pinLtc3129Pgood;
	bool pinBq25504VbatOk;
	bool pinResistor_1;
	bool pinResistor_2;
	bool pinResistor_3;

public:
	solarharvester( print<print_serial> *sysMsg );
	~solarharvester() {}

	void init();
	void setConfig( HARVESTER harvester );
	void getMeasurements( float &solarVoltage, float &solarCurrent, float &chargingCurrent, float &capacitorVoltage );

	void setResistor_1( bool state );
	void setResistor_2( bool state );
	void setResistor_3( bool state );

	void getStorageStatus( bool &status );
};


#endif /* SOLARHARVESTER_H_ */

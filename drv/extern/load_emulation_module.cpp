/*
 * LoadEmulationModule.cpp
 *
 *  Created on: Nov 10, 2011
 *      Author: matthias
 */

#include "LoadEmulationModule.h"
#include "SystemConfig.h"
#include "efm32_timer.h"
#include "efm32_gpio.h"
#include "efm32_emu.h"
#include "efm32_cmu.h"
#include "efm32_usart.h"

uint32_t LoadEmulationModule::lengthProfile;
volatile uint32_t LoadEmulationModule::profileIndex = 0;
uint16_t *LoadEmulationModule::current;
uint16_t *LoadEmulationModule::timeing;
uint16_t LoadEmulationModule::idle;


LoadEmulationModule::LoadEmulationModule()
{

}


void LoadEmulationModule::initializeInterfaceLoad()
{
	GPIO_PinModeSet( PinResistorA, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorB, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorC, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorD, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorE, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorF, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorG, gpioModePushPull, 0 );
	GPIO_PinModeSet( PinResistorH, gpioModePushPull, 0 );

	initTimer.clkSel     = timerClkSelHFPerClk;
	initTimer.prescale   = timerPrescale1;
	initTimer.dmaClrAct  = false;
	initTimer.debugRun   = false;
	initTimer.enable     = false;
	initTimer.sync	     = true;
	initTimer.quadModeX4 = false;
	initTimer.mode       = timerModeUp;
	initTimer.fallAction = timerInputActionNone;
	initTimer.riseAction = timerInputActionNone;

	initTimer1.clkSel     = timerClkSelCascade;
	initTimer1.prescale   = timerPrescale1;
	initTimer1.dmaClrAct  = false;
	initTimer1.debugRun   = false;
	initTimer1.enable     = false;
	initTimer1.sync	      = true;
	initTimer1.quadModeX4 = false;
	initTimer1.mode       = timerModeUp;
	initTimer1.fallAction = timerInputActionNone;
	initTimer1.riseAction = timerInputActionNone;

	CMU_ClockEnable( cmuClock_TIMER0, true);
	CMU_ClockEnable( cmuClock_TIMER1, true);

	TIMER_Reset( TIMER0 );
	TIMER_Reset( TIMER1 );

	TIMER_Init( TIMER0, &initTimer );
	TIMER_Init( TIMER1, &initTimer1 );

	TIMER_IntClear( TIMER1, ~0 );
	TIMER_IntEnable( TIMER1, TIMER_IF_OF );

	NVIC_EnableIRQ( TimerLoadEmulation );

	CMU_ClockEnable( cmuClock_TIMER0, false);
	CMU_ClockEnable( cmuClock_TIMER1, false);
}


void LoadEmulationModule::startActivePeriod( uint16_t *load, uint16_t *time, uint32_t length, uint16_t resolution, uint16_t idleCurrent )
{
	current   = load;
	timeing   = time;
	idle      = idleCurrent;

	lengthProfile = length;

	GPIO_PinModeSet(gpioPortD,8,gpioModePushPull,0);

	CMU_ClockEnable( cmuClock_TIMER0, true);
	CMU_ClockEnable( cmuClock_TIMER1, true);

	TIMER_TopSet( TIMER0, resolution );

	TIMER_TopSet( TIMER1, (uint32_t)timeing[profileIndex]-1 );

	GPIO_PortOutSetVal( mainIO_Port, (uint32_t)current[profileIndex], 0x000000FE );

	if( current[profileIndex] & 0x0001 )
		GPIO_PinOutSet( PinResistorH );
	else
		GPIO_PinOutClear( PinResistorH );

	profileIndex++;

	TIMER_TopBufSet( TIMER1, timeing[profileIndex]-1 );

	TIMER_Enable( TIMER0, true );

	profileIndex++;
}


void LoadEmulationModule::_wrapper_Timer_Interrupt( uint32_t temp )
{
	if(profileIndex <= lengthProfile)
	{
		GPIO_PortOutSetVal( mainIO_Port, (uint32_t)current[profileIndex - 1], 0x000000FE );

		if( current[profileIndex - 1] & 0x0001 )
			GPIO_PinOutSet( auxIO_Port, _resistor_H );
		else
			GPIO_PinOutClear( auxIO_Port, _resistor_H );
	}
	else
	{
		GPIO_PortOutSetVal( mainIO_Port, idle, 0x000000FE );

		if( idle & 0x0001 )
			GPIO_PinOutSet( auxIO_Port, _resistor_H );
		else
			GPIO_PinOutClear( auxIO_Port, _resistor_H );
	}

	if( profileIndex < lengthProfile +1 )
		TIMER_TopBufSet( TIMER1, (uint32_t)timeing[profileIndex]-1 );

	TIMER_IntClear( TIMER1, ~0 );

	profileIndex++;
}


void LoadEmulationModule::setIdleCurrent( uint16_t idleCurrent )
{
	idle = idleCurrent;

	GPIO_PortOutSetVal( mainIO_Port, idleCurrent, 0x000000FE );

	if( idleCurrent & 0x0001 )
		GPIO_PinOutSet( auxIO_Port, _resistor_H );
	else
		GPIO_PinOutClear( auxIO_Port, _resistor_H );
}


bool LoadEmulationModule::resetLoadModule()
{
	TIMER_Enable( TIMER0, false );
	TIMER1->CNT = 0;

	CMU_ClockEnable( cmuClock_TIMER0, false);
	CMU_ClockEnable( cmuClock_TIMER1, false);

	profileIndex = 0;

	GPIO_PortOutSetVal( mainIO_Port, idle, 0x000000FE );

	if( idle & 0x0001 )
		GPIO_PinOutSet( auxIO_Port, _resistor_H );
	else
		GPIO_PinOutClear( auxIO_Port, _resistor_H );

	return 0;
}


bool LoadEmulationModule::getLoadStatus()
{
	return (profileIndex <= lengthProfile +1);
}


ptISR_Handler LoadEmulationModule::getISR_FunctionPointer()
{
	return _wrapper_Timer_Interrupt;
}

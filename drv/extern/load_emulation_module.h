/*
 * LoadEmulationModule.h
 *
 *  Created on: Nov 10, 2011
 *      Author: matthias
 */

#ifndef LOADEMULATIONMODULE_H_
#define LOADEMULATIONMODULE_H_

#include "efm32_timer.h"


struct LOAD_STATUS {
	uint16_t timing;
	uint16_t loadCurrent;
};


typedef void ( *ptISR_Handler )( uint32_t );

class LoadEmulationModule {

private:
	TIMER_Init_TypeDef initTimer;
	TIMER_Init_TypeDef initTimer1;
	ptISR_Handler Timer_InterruptHandler;

	static volatile uint32_t profileIndex;
	static uint32_t lengthProfile;
	static uint16_t idle;

	static uint16_t *current;
	static uint16_t *timeing;
	static bool     externalReset;
	static bool     timeout;

	static void _wrapper_Timer_Interrupt( uint32_t temp );
	void setCurrentValue();

public:
	void initializeInterfaceLoad();
	void startActivePeriod( uint16_t *load, uint16_t *time, uint32_t length, uint16_t resolution, uint16_t idleCurrent );
	void setIdleCurrent( uint16_t idleCurrent );
	bool resetLoadModule();
	bool getLoadStatus();

	ptISR_Handler getISR_FunctionPointer();
	LoadEmulationModule();
	~LoadEmulationModule() {}
};

#endif /* LOADEMULATIONMODULE_H_ */

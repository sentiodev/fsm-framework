/*
 * emulatorbord.h
 *
 *  Created on: Sep 26, 2011
 *      Author: Matthias Kr�mer
 */

#ifndef EMULATORBORD_H_
#define EMULATORBORD_H_

#include "efm32.h"
#include "LTC2990.h"
#include "LoadEmulationModule.h"


typedef enum {
	disableSolar  = 0x00,
	solarCell_1   = 0x01,
	solarCell_2   = 0x02,
	solarCell_3   = 0x03,
	solarCell_12  = 0x04,
	solarCell_13  = 0x05,
	solarCell_23  = 0x06,
	solarCell_123 = 0x07
} INPUT_SOLAR;

typedef enum {
	disable     = 0x00,
	bat_bat     = 0x01,
	bat_only    = 0x02,
	cap_cap_1F  = 0x03,
	cap_cap_10F = 0x04,
	cap_cap_22F = 0x05,
	cap_1F_bat  = 0x06,
	cap_10F_bat = 0x07,
	cap_22F_bat = 0x08
} ENERGY_STORAGE;

typedef enum {
	disableHarvester   = 0x00,
	enableLT_3105      = 0x01,
	enableLT_3105_MPPT = 0x02
};

class Emulatorbord : public LTC2990, public LoadEmulationModule
{
private:


public:
	Emulatorbord() {}
	~Emulatorbord() {}

	void initializeInterface();

	bool setInputSolarCell( uint8_t selection );
	bool setHarvester( uint8_t enable );
	void powerHarvester( bool enable );
	bool setEnergyStorage( uint16_t type );
	void setEnergyStorageDrained( ENERGY_STORAGE type );

};

#endif /* EMULATORBORD_H_ */

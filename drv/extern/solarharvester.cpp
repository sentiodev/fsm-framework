/*
 * solarharvester.cpp
 *
 *  Created on: 23 jan 2014
 *      Author: flge1200
 */


#include "solarharvester.h"
#include "em_gpio.h"
#include "em_cmu.h"

adc_ltc2990 solarharvester::ltc2990U5;
adc_ltc2990 solarharvester::ltc2990U6;


solarharvester::solarharvester(print<print_serial> *sysMsg) : msg(sysMsg)
{
    init();
}


void solarharvester::init()
{
    // Enable the GPIO-Pins of the EFM32 MCU
    CMU_ClockEnable( cmuClock_GPIO, true );

	// Turn all load resistors off
    setResistor_1( false );
    setResistor_2( false );
    setResistor_3( false );

    // Enable the level translator
	GPIO_PinModeSet( SOLARHARVESTER_TXS0102EN_PIN, gpioModePushPull, 1 );

    // Configure both LTC2990 for Current-Voltage-Voltage measurement (using 1 ohm sense resistor)
	ltcConfig_CVV1.temperature = celsius;
	ltcConfig_CVV1.mode        = V1_V2V3V4;
	ltcConfig_CVV1.aquire      = single;
	ltcConfig_CVV1.shuntV12    = ltc2990_shuntOhm;
	ltcConfig_CVV1.shuntV34    = 0;
	ltcConfig_CVV1.address     = chip0;

	ltcConfig_CVV2.temperature = celsius;
	ltcConfig_CVV2.mode        = V1_V2V3V4;
	ltcConfig_CVV2.aquire      = single;
	ltcConfig_CVV2.shuntV12    = ltc2990_shuntOhm;
	ltcConfig_CVV2.shuntV34    = 0;
	ltcConfig_CVV2.address     = chip3;

}

void solarharvester::setConfig( HARVESTER harvester )
{
	harvesterType = harvester;

	if (harvesterType == ltc3129)
		GPIO_PinModeSet(SOLARHARVESTER_LTC3129PGOOD_PIN, gpioModeInput,1);
	else if (harvesterType == bq25504)
		GPIO_PinModeSet(SOLARHARVESTER_BQ25504VBATOK_PIN, gpioModeInput,1);
}

void solarharvester::getMeasurements( float &solarVoltage, float &solarCurrent, float &chargingCurrent, float &capacitorVoltage )
{
    // Configure LTC2990s for Current-Voltage-Voltage measurement
	ltc2990U5.setConfig( ltcConfig_CVV1 );
	ltc2990U5.trigger();
	ltc2990U6.setConfig( ltcConfig_CVV2 );
	ltc2990U6.trigger();

    // Read solar and charging current
	ltc2990U5.getCurrent( solarCurrent, currentV12 );
	ltc2990U6.getCurrent( chargingCurrent, currentV34 );

    // Read solar and supercap voltage
	ltc2990U5.getVoltage( capacitorVoltage, voltageV3 );
	ltc2990U6.getVoltage( solarVoltage, voltageV3 );
}


void solarharvester::getStorageStatus( bool &status )
{
	if (harvesterType == ltc3129)
		status = GPIO_PinInGet(SOLARHARVESTER_LTC3129PGOOD_PIN);
	else if (harvesterType == bq25504)
		status = GPIO_PinInGet(SOLARHARVESTER_BQ25504VBATOK_PIN);
}


void solarharvester::setResistor_1( bool state )
{
	GPIO_PinModeSet( SOLARHARVESTER_RESISTOR1_PIN, gpioModePushPull, state );
}

void solarharvester::setResistor_2( bool state )
{
	GPIO_PinModeSet( SOLARHARVESTER_RESISTOR2_PIN, gpioModePushPull, state );
}

void solarharvester::setResistor_3( bool state )
{
	GPIO_PinModeSet( SOLARHARVESTER_RESISTOR3_PIN, gpioModePushPull, state );
}

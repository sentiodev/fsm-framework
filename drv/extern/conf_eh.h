/**
 * @file conf_eh.h
 *
 * Extension: conf_eh
 *
 * Driver for the ConfEH extension board
 */

#ifndef CONF_EH_H_
#define CONF_EH_H_

#include "adc_ltc2990.h"

// Platform selection
#ifdef SENTIO_EM_ENABLE
#define _CONFEH_TXS0102EN_PORT    gpioPortE
#define _CONFEH_TXS0102EN_PIN     7
#define CONFEH_TXS0102EN_PIN      _CONFEH_TXS0102EN_PORT,_CONFEH_TXS0102EN_PIN

#define _CONFEH_MAX17710AE_PORT   gpioPortD
#define _CONFEH_MAX17710AE_PIN    4
#define CONFEH_MAX17710AE_PIN     _CONFEH_MAX17710AE_PORT,_CONFEH_MAX17710AE_PIN

#define _CONFEH_MAX17710LCE_PORT  gpioPortD
#define _CONFEH_MAX17710LCE_PIN   5
#define CONFEH_MAX17710LCE_PIN    _CONFEH_MAX17710LCE_PORT,_CONFEH_MAX17710LCE_PIN

#define _CONFEH_LTC3105EN_PORT    gpioPortE
#define _CONFEH_LTC3105EN_PIN     6
#define CONFEH_LTC3105EN_PIN      _CONFEH_LTC3105EN_PORT,_CONFEH_LTC3105EN_PIN

#define _CONFEH_RESISTOR1_PORT    gpioPortE
#define _CONFEH_RESISTOR1_PIN     3
#define CONFEH_RESISTOR1_PIN      _CONFEH_RESISTOR1_PORT,_CONFEH_RESISTOR1_PIN

#define _CONFEH_RESISTOR2_PORT    gpioPortE
#define _CONFEH_RESISTOR2_PIN     4
#define CONFEH_RESISTOR2_PIN      _CONFEH_RESISTOR1_PORT,_CONFEH_RESISTOR1_PIN

#define _CONFEH_RESISTOR3_PORT    gpioPortE
#define _CONFEH_RESISTOR3_PIN     5
#define CONFEH_RESISTOR3_PIN      _CONFEH_RESISTOR1_PORT,_CONFEH_RESISTOR1_PIN
#else
#error No Sensor Platform defined
#endif

// Application configuration check
#ifndef CONF_EH_ENABLE
#error Missing define in application_config.h: CONF_EH_ENABLE
#endif

/** Board configuration options */
typedef enum
{
	dlc_direct  = 0x00,
	tfb_direct  = 0x01,
	dlc_ltc3105 = 0x02,
	tfb_ltc3105 = 0x03
} CONFEH_CONFIG;

/**
 * Driver class for the ConfEH extension board
 *
 * The class contains the interface to interract with the ConfEH extension board.
 */
class conf_eh
{
public:
	conf_eh();
	~conf_eh() {}

	/**
	 * Initializes the components and interfaces on the board.
	 *
	 * The function initializes the component configurations and the interface to the host MCU. It is automatically called in the constructor.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init();

	/**
	 * Sets the board configuration.
	 *
	 * The function selects in configuration the board is used. The board has four configuration options:
	*    - dlc_direct: equipped with directly coupled DLC
	*    - dlc_ltc3105: equipped with DLC charged via the LTC3105 harvesting IC
	*    - tfb_direct: equipped with TFB charged only with the MAX17710 charger
	*    - tfb_ltc3105: equipped with TFB charged via additional LTC3105 harvesting IC
	* The board configuration needs to match the actual hardware implementation of the board
	 *
	 * @param[in]  confehConfig: the config to be used
	 * @param[out] none
	 * @return     void
	 */
	void setConfig( const CONFEH_CONFIG confehConfig );

	/**
	 * Gets the measurement parameters.
	 *
	 * The function reads the storage voltage, solar voltage and solar current.
	 *
	 * @param[in]  none
	 * @param[out] storageVoltage: the terminal voltage of the storage device
	* @param[out] solarVoltage: the terminal voltage of the solar panel
	* @param[out] solarCurrent: the charge current (output current of the solar panel)
	 * @return     void
	 */
	void getMeasurements( float &storageVoltage, float &solarVoltage, float &solarCurrent );

	/**
	 * Gets the supply voltage of the LTC2990.
	 *
	 * The function reads the supply voltage of the LTC2990 measurement IC.
	 *
	 * @param[in]  none
	 * @param[out] supplyVoltage: the supply voltage value in float
	 * @return     none
	 */
	void getSupplyVoltage( float &supplyVoltage );

	/**
	 * Controls the load resistor 1.
	 *
	 * The function connects (true) or disconnects (false) the load resistor 1.
	 *
	 * @param[in]  state: connect (true) or disconnect (false) resistor 1 as load
	 * @param[out] none
	 * @return     void
	 */
	void setResistor_1( bool state );

	/**
	 * Controls the load resistor 2.
	 *
	 * The function connects (true) or disconnects (false) the load resistor 2.
	 *
	 * @param[in]  state: connect (true) or disconnect (false) resistor 2 as load
	 * @param[out] none
	 * @return     void
	 */
	void setResistor_2( bool state );

	/**
	 * Controls the load resistor 3.
	 *
	 * The function connects (true) or disconnects (false) the load resistor 3.
	 *
	 * @param[in]  state: connect (true) or disconnect (false) resistor 3 as load
	 * @param[out] none
	 * @return     void
	 */
	void setResistor_3( bool state );
private:
	static adc_ltc2990 ltc2990;

	LTC2990_CONFIG ltcConfig_CVV;
	LTC2990_CONFIG ltcConfig_VVVV;

	static const uint8_t ltc2990_shuntOhm = 1;

	CONFEH_CONFIG boardConfig;

};


#endif /* CONF_EH_H_ */

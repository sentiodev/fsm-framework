/**
 * Extension: smartlight_board
 *
 * Driver for the smart traffic light extension board
 */

#include "smartlight_board.h"

uint32_t smartlight_board::countWindSpeed;

smartlight_board::smartlight_board( print<print_serial> *sysMsg ) : msg( sysMsg )
{
	DBG_MSG( "Initialize SmartLight Driver... " );
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );

	// Enable the Power-Pin of the SHT15 Sensor
	GPIO_PinModeSet( SMARTLIGHT_SHT_ENABLE, gpioModePushPull, 1 );

	// Configure Analog Measurement
	LTC2990_CONFIG ltc2990config;
	ltc2990config.temperature   = celsius;
	ltc2990config.mode          = V1V2V3V4;
	ltc2990config.aquire        = single;
	ltc2990config.shuntV12      = 0;
	ltc2990config.shuntV34      = 0;
	ltc2990config.address       = chip0;

	ltc2990.setConfig( ltc2990config );

	// Configure the Windspeed Meassurement Pin
	GPIO_PinModeSet( SMARTLIGHT_WINDSPEED_PIN, gpioModeInputPullFilter , 1 );
	GPIO_PinModeSet( SMARTLIGHT_WIND_ENABLE_PIN, gpioModePushPull , 0 );

	// Enable the Power-Pin of the Solar Radiation Sensor
	GPIO_PinModeSet( SMARTLIGHT_SOLAR_ENABLE_PIN, gpioModePushPull , 0 );

	// Enable the Power-Pin of the Noise Level Sensor
	// GPIO_PinModeSet( SMARTLIGHT_NOISE_ENABLE_PIN, gpioModePushPull , 0 );

	//Configure RS232 Interface
	GPIO_PinModeSet( SMARTLIGHT_RS232_ENABLE,    gpioModePushPull , 0 );
	GPIO_PinModeSet( SMARTLIGHT_RS232_FORCE_ON,  gpioModePushPull , 1 );
	GPIO_PinModeSet( SMARTLIGHT_RS232_FORCE_OFF, gpioModePushPull , 1 );
	GPIO_PinModeSet( SMARTLIGHT_RS232_INVALID,   gpioModeInputPullFilter , 0 );

	GPIO_PinModeSet( SMARTLIGHT_RS232_TX,        gpioModePushPull , 1 );
	GPIO_PinModeSet( SMARTLIGHT_RS232_RX,        gpioModeInputPullFilter , 0 );
	GPIO_PinModeSet( SMARTLIGHT_RS232_RTS,       gpioModePushPull , 0 );
	GPIO_PinModeSet( SMARTLIGHT_RS232_CTS,       gpioModeInputPullFilter , 0 );

	//  Setup the Parameters used to configure the selected UART/USART module
	USART_InitAsync_TypeDef  initUart =
	{
		usartEnable,     // Enable the UART after the Configuration is finalized
		MCU_CLOCK,       // The HF-Clock Frequency is used to calculate the Baudrate-Clock-Divider Setting
		9600, // Resulting Baudrate at which the UART module runs
		usartOVS4,      // RX-Pin Over-sampling-Rate
		usartDatabits8,  // Number of Data-Bits
		usartNoParity,   // Parity-Bit setup
		usartStopbits2   // Number of Stop-Bits
	};

	// Enable the required periphery clock
	CMU_ClockEnable( SMARTLIGHT_RS232_CLOCK, true );

	USART_InitAsync( SMARTLIGHT_RS232, &initUart );

	SMARTLIGHT_RS232->ROUTE = USART_ROUTE_RXPEN |
							  USART_ROUTE_TXPEN |
							  SMARTLIGHT_RS232_LOC;

//  GPIO_PinModeSet( SMARTLIGHT_TPS_PG_PIN, gpioModeInputPullFilter , 1 );
//  GPIO_PinModeSet( SMARTLIGHT_TPS_SLEEP_PIN, gpioModePushPull , 1 );  CMU_ClockEnable( SMARTLIGHT_RS232_CLOCK, true );

}


void smartlight_board::setVoltageRegStatus( SMARTLIGHT_VOLTAGE_REG_STATUS status )
{
	if ( status )
		GPIO_PinOutSet( SMARTLIGHT_TPS_SLEEP_PIN );
	else
		GPIO_PinOutClear( SMARTLIGHT_TPS_SLEEP_PIN );
}


void smartlight_board::startSolarMeasurement()
{
	// Enable the solar radiation sensor to allow the output-value to settle
	GPIO_PinOutSet( SMARTLIGHT_SOLAR_ENABLE_PIN );
}


void smartlight_board::startWindMeasurement()
{
	// Start the windspeed measurement, reset the pulseCount then enable counting again
	countWindSpeed = 0;

	GPIO_IntConfig( SMARTLIGHT_WINDSPEED_PIN, true, false, true );

	GPIO_PinOutSet( SMARTLIGHT_WIND_ENABLE_PIN );
}


void smartlight_board::getMeasurement( SMARTLIGHT_DATA& data )
{
	//Stop the Windspeed measurement
	GPIO_IntConfig( SMARTLIGHT_WINDSPEED_PIN, false, false, false );

	// Enable noise-level measurement
	GPIO_PinOutSet( SMARTLIGHT_NOISE_ENABLE_PIN );

	float    intermediate = 0;
	uint32_t countV1      = 0;
	uint32_t countV2      = 0;
	uint32_t countV4      = 0;

	data.system.status    = 0xFF;

	for ( uint32_t i = 0; i < 100; i++ )
	{
		ltc2990.trigger();

		if ( !ltc2990.getVoltage( intermediate, voltageV1 ) )
		{
			data.system.statusFields.analogV1 = false;
			data.windDirection = 99999;
		}
		else
		{
			if ( data.system.statusFields.analogV1 )
			{
				data.windDirection += intermediate;
				countV1++;
			}
		}

		if ( !ltc2990.getVoltage( intermediate, voltageV2 ) )
		{
			data.system.statusFields.analogV2 = false;
			data.solarRadiation = 99999;
		}
		else
		{
			if ( data.system.statusFields.analogV2 )
			{
				data.solarRadiation += intermediate;
				countV2++;
			}
		}

		if ( !ltc2990.getVoltage( intermediate, voltageV4 ) )
		{
			data.system.statusFields.analogV4 = false;
			data.noiseLevel = 99999;
		}
		else
		{
			if ( data.system.statusFields.analogV4 )
			{
				data.noiseLevel += intermediate;
				countV4++;
			}
		}
	}

	GPIO_PinOutClear( SMARTLIGHT_SOLAR_ENABLE_PIN );
	GPIO_PinOutClear( SMARTLIGHT_WIND_ENABLE_PIN );
	GPIO_PinOutClear( SMARTLIGHT_NOISE_ENABLE_PIN );

	data.windDirection  /= countV1;
	data.solarRadiation /= countV2;
	data.noiseLevel     /= countV4;

	if ( !sht.getMeasurement( data.humidity, data.temperature ) )
	{
		data.system.statusFields.shtReadingOk = false;
		data.humidity    = 99999;
		data.temperature = 99999;
	}

	if ( !ltc2990.getVoltage( data.backupBattery, voltageV3 ) )
	{
		data.system.statusFields.analogV3 = false;
		data.backupBattery = 99999;
	}
	else
	{
		// Assuming the battery Voltage is measured using an 2:1 voltage devider
		data.backupBattery *= 2;
	}

	if ( !ltc2990.getSupplyVoltage( data.analogSupply ) )
	{
		data.system.statusFields.analogSupply = false;
		data.analogSupply = 99999;
	}

	data.windSpeed = countWindSpeed;
	data.system.statusFields.regulatorDataReadOk = true;

	data.system.statusFields.rs232InterfaceStatus = ( bool ) GPIO_PinInGet( SMARTLIGHT_RS232_INVALID );

	if ( data.system.statusFields.rs232InterfaceStatus )
	{
		uint32_t timeout = 0;

		USART_InitAsync_TypeDef  initUart =
		{
			usartEnable,     // Enable the UART after the Configuration is finalized
			MCU_CLOCK,       // The HF-Clock Frequency is used to calculate the Baudrate-Clock-Divider Setting
			9600, // Resulting Baudrate at which the UART module runs
			usartOVS4,      // RX-Pin Over-sampling-Rate
			usartDatabits8,  // Number of Data-Bits
			usartNoParity,   // Parity-Bit setup
			usartStopbits2   // Number of Stop-Bits
		};

		if ( ( ( SMARTLIGHT_RS232->ROUTE ) & _USART_ROUTE_LOCATION_MASK ) != SMARTLIGHT_RS232_LOC )
		{
			USART_InitAsync( SMARTLIGHT_RS232, &initUart );

			SMARTLIGHT_RS232->ROUTE = USART_ROUTE_RXPEN |
									  USART_ROUTE_TXPEN |
									  SMARTLIGHT_RS232_LOC;
		}

		// Send the Dataframe-request to the Solar Charge Regulator via RS232
		USART_Tx( SMARTLIGHT_RS232, 0x01 ); // Regulator Address
		USART_Tx( SMARTLIGHT_RS232, 0x80 ); // Command
		USART_Tx( SMARTLIGHT_RS232, 0x80 ); // Checksum

		for ( uint32_t j = 0; j < 18; j++ )
		{
			while ( !( SMARTLIGHT_RS232->STATUS & USART_STATUS_RXDATAV ) && ( timeout < 10000000 ) )
			{
				timeout++;
			}

			if ( timeout >= 10000000 )
			{
				for ( uint32_t i = 0; i < 18; i++ )
				{
					data.regulatorDataArray[i] = 0x00;
				}
				data.system.statusFields.regulatorDataReadOk = false;
				break;
			}

			data.regulatorDataArray[j] = SMARTLIGHT_RS232->RXDATA;
		}
	}
	else
	{
		data.system.statusFields.regulatorDataReadOk = false;

		for ( uint32_t i = 0; i < 18; i++ )
		{
			data.regulatorDataArray[i] = 0x00;
		}
	}
}


void smartlight_board::callbackWindSpeed()
{
	countWindSpeed++;
}
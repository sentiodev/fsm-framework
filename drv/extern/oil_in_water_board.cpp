/**
 * Extension: oil_in_water_board
 *
 * Driver for the oil_in_water_board extension board
 */

#include "oil_in_water_board.h"

oil_in_water_board::oil_in_water_board()
{
	
	GPIO_PinModeSet( gpioPortE, 2, gpioModePushPull, 0 );
	GPIO_PinModeSet( gpioPortE, 7, gpioModePushPull, 0 );
	
	AD7XXX_CONFIG config;
	config.refVoltage = 5.0;
	config.daisyChain = 1;
	config.precision  = prec16bit;

	ad7988.setConfig( config );

	countSamplesTaken = 0;
}


void oil_in_water_board::getMeasurement( OIL_IN_WATER_DATA& data )
{
	if ( !sht.getMeasurement( data.humidity, data.temperature ) )
	{
		data.system.statusFields.shtReadingOk = false;
		data.humidity    = 99999;
		data.temperature = 99999;
	}

	// Oversampling the External ADC 
	float storage      = 0;
	float currentValue = 0;
	for(uint32_t i = 0; i < 16; i++)
	{
		ad7988.getValue( currentValue );

		storage += currentValue;
	}

	// Disable ADC and Sensor PowerSupply
	GPIO_PinModeSet( gpioPortE, 7, gpioModePushPull, 0 );

	data.oilConcentration = storage / 16;

	// Increase the Counter of Measuremets that are taken
	data.numberOfSamples = countSamplesTaken;
	countSamplesTaken++;
}

void oil_in_water_board::startSampling()
{
	GPIO_PinModeSet( gpioPortE, 2, gpioModePushPull, 1 );

	GPIO_PinModeSet( gpioPortE, 7, gpioModePushPull, 1 );
}

void oil_in_water_board::resetSampling()
{
	countSamplesTaken = 0;
}

uint32_t oil_in_water_board::getNumberOfSamples()
{
	return countSamplesTaken;
}
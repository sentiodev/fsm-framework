/**
 * @file sentio_em_io.h
 *
 * Interface: sentio_em_io
 *
 * Describes connector interfaces of the SENTIO-em platform (V3.2)
 */


#if SENTIO_EM_ENABLE

/* Sensor Connector */

#define _SENSOR_PORT_1     gpioPortB                           ///< Sensor connector PIN1: Port
#define _SENSOR_PIN_1      7                                   ///< Sensor connector PIN1: Pin number
#define SENSOR_PIN_1       _SENSOR_PORT_1,_SENSOR_PIN_1        ///< Sensor connector PIN1

#define _SENSOR_PORT_2     gpioPortA                           ///< Sensor connector PIN2: Port
#define _SENSOR_PIN_2      11                                  ///< Sensor connector PIN2: Pin number
#define SENSOR_PIN_2       _SENSOR_PORT_2,_SENSOR_PIN_2        ///< Sensor connector PIN2

#define _SENSOR_PORT_3     gpioPortA                           ///< Sensor connector PIN3: Port
#define _SENSOR_PIN_3      12                                  ///< Sensor connector PIN3: Pin number
#define SENSOR_PIN_3       _SENSOR_PORT_3,_SENSOR_PIN_3        ///< Sensor connector PIN3

#define _SENSOR_PORT_4     gpioPortA                           ///< Sensor connector PIN4: Port
#define _SENSOR_PIN_4      13                                  ///< Sensor connector PIN4: Pin number
#define SENSOR_PIN_4       _SENSOR_PORT_4,_SENSOR_PIN_4        ///< Sensor connector PIN4

#define _SENSOR_PORT_5     gpioPortB                           ///< Sensor connector PIN5: Port
#define _SENSOR_PIN_5      9                                   ///< Sensor connector PIN5: Pin number
#define SENSOR_PIN_5       _SENSOR_PORT_5,_SENSOR_PIN_5        ///< Sensor connector PIN5

#define _SENSOR_PORT_6     gpioPortC                           ///< Sensor connector PIN6: Port
#define _SENSOR_PIN_6      1                                   ///< Sensor connector PIN6: Pin number
#define SENSOR_PIN_6       _SENSOR_PORT_6,_SENSOR_PIN_6        ///< Sensor connector PIN6

#define _SENSOR_PORT_7     gpioPortC                           ///< Sensor connector PIN7: Port
#define _SENSOR_PIN_7      0                                   ///< Sensor connector PIN7: Pin number
#define SENSOR_PIN_7       _SENSOR_PORT_7,_SENSOR_PIN_7        ///< Sensor connector PIN7

#define _SENSOR_PORT_8     gpioPortA                           ///< Sensor connector PIN8: Port
#define _SENSOR_PIN_8      6                                   ///< Sensor connector PIN8: Pin number
#define SENSOR_PIN_8       _SENSOR_PORT_8,_SENSOR_PIN_8        ///< Sensor connector PIN8

#define _SENSOR_PORT_9     gpioPortA                           ///< Sensor connector PIN9: Port
#define _SENSOR_PIN_9      5                                   ///< Sensor connector PIN9: Pin number
#define SENSOR_PIN_9       _SENSOR_PORT_9,_SENSOR_PIN_9        ///< Sensor connector PIN9

#define _SENSOR_PORT_10    gpioPortA                           ///< Sensor connector PIN10: Port
#define _SENSOR_PIN_10     14                                  ///< Sensor connector PIN10: Pin number
#define SENSOR_PIN_10      _SENSOR_PORT_10,_SENSOR_PIN_10      ///< Sensor connector PIN10

#define _SENSOR_PORT_11    gpioPortB                           ///< Sensor connector PIN11: Port
#define _SENSOR_PIN_11     11                                  ///< Sensor connector PIN11: Pin number
#define SENSOR_PIN_11      _SENSOR_PORT_11,_SENSOR_PIN_11      ///< Sensor connector PIN11

#define _SENSOR_PORT_12    gpioPortC                           ///< Sensor connector PIN12: Port
#define _SENSOR_PIN_12     6                                   ///< Sensor connector PIN12: Pin number
#define SENSOR_PIN_12      _SENSOR_PORT_12,_SENSOR_PIN_12      ///< Sensor connector PIN12

#define _SENSOR_PORT_13    gpioPortB                           ///< Sensor connector PIN13: Port
#define _SENSOR_PIN_13     12                                  ///< Sensor connector PIN13: Pin number
#define SENSOR_PIN_13      _SENSOR_PORT_13,_SENSOR_PIN_13      ///< Sensor connector PIN13

#define _SENSOR_PORT_14    gpioPortC                           ///< Sensor connector PIN14: Port
#define _SENSOR_PIN_14     7                                   ///< Sensor connector PIN14: Pin number
#define SENSOR_PIN_14      _SENSOR_PORT_14,_SENSOR_PIN_14      ///< Sensor connector PIN14

#define _SENSOR_PORT_17    gpioPortD                           ///< Sensor connector PIN17: Port
#define _SENSOR_PIN_17     0                                   ///< Sensor connector PIN17: Pin number
#define SENSOR_PIN_17      _SENSOR_PORT_17,_SENSOR_PIN_17      ///< Sensor connector PIN17

#define _SENSOR_PORT_18    gpioPortD                           ///< Sensor connector PIN18: Port
#define _SENSOR_PIN_18     1                                   ///< Sensor connector PIN18: Pin number
#define SENSOR_PIN_18      _SENSOR_PORT_18,_SENSOR_PIN_18      ///< Sensor connector PIN18

#define _SENSOR_PORT_19    gpioPortD                           ///< Sensor connector PIN19: Port
#define _SENSOR_PIN_19     2                                   ///< Sensor connector PIN19: Pin number
#define SENSOR_PIN_19      _SENSOR_PORT_19,_SENSOR_PIN_19      ///< Sensor connector PIN19

#define _SENSOR_PORT_20    gpioPortD                           ///< Sensor connector PIN20: Port
#define _SENSOR_PIN_20     3                                   ///< Sensor connector PIN20: Pin number
#define SENSOR_PIN_20      _SENSOR_PORT_20,_SENSOR_PIN_20      ///< Sensor connector PIN20

#define SENSOR_LEUART_RX   SENSOR_PIN_8                        ///< Sensor connector LEUART RX pin
#define SENSOR_LEUART_TX   SENSOR_PIN_9                        ///< Sensor connector LEUART TX pin
#define SENSOR_LEUART      LEUART1                             ///< Sensor connector LEUART module
#define SENSOR_LEUART_LOC  LEUART_ROUTE_LOCATION_LOC0          ///< Sensor connector LEUART location

#define SENSOR_UART_RX     SENSOR_PIN_6                        ///< Sensor connector UART RX pin
#define SENSOR_UART_TX     SENSOR_PIN_7                        ///< Sensor connector UART TX pin
#define SENSOR_UART        USART1                              ///< Sensor connector UART module
#define SENSOR_UART_CLOCK  cmuClock_USART1                     ///< Sensor connector UART clock
#define SENSOR_UART_RX_ISR USART1_RX_IRQn                      ///< Sensor connector UART interrupt
#define SENSOR_UART_LOC    USART_ROUTE_LOCATION_LOC0           ///< Sensor connector UART location

#define SENSOR_I2C_SDA     SENSOR_PIN_12                       ///< Sensor connector I2C DATA pin
#define SENSOR_I2C_SCL     SENSOR_PIN_14                       ///< Sensor connector I2C CLK pin
#define SENSOR_I2C         I2C0                                ///< Sensor connector I2C module
#define SENSOR_I2C_LOC     I2C_ROUTE_LOCATION_LOC2             ///< Sensor connector I2C location

#define SENSOR_USART_RX    SENSOR_PIN_18                       ///< Sensor connector USART RX pin
#define SENSOR_USART_TX    SENSOR_PIN_17                       ///< Sensor connector USART TX pin
#define SENSOR_USART_CS    SENSOR_PIN_20                       ///< Sensor connector USART CS pin
#define SENSOR_USART_CLK   SENSOR_PIN_19                       ///< Sensor connector USART CLK pin
#define SENSOR_USART       USART1                              ///< Sensor connector USART module
#define SENSOR_USART_CLOCK  cmuClock_USART1                    ///< Sensor connector USART clock
#define SENSOR_USART_RX_ISR USART1_RX_IRQn                     ///< Sensor connector USART interrupt
#define SENSOR_USART_LOC   USART_ROUTE_LOCATION_LOC1           ///< Sensor connector USART location

#define SENSOR_ADC_0       SENSOR_PIN_17                       ///< Sensor connector ADC0 pin
#define SENSOR_ADC_1       SENSOR_PIN_18                       ///< Sensor connector ADC1 pin
#define SENSOR_ADC_2       SENSOR_PIN_19                       ///< Sensor connector ADC2 pin
#define SENSOR_ADC_3       SENSOR_PIN_20                       ///< Sensor connector ADC3 pin

/* Power Connector */

#define _POWER_PORT_1      gpioPortD                           ///< Power connector PIN1: Port
#define _POWER_PIN_1       7                                   ///< Power connector PIN1: Pin number
#define POWER_PIN_1        _POWER_PORT_1,_POWER_PIN_1          ///< Power connector PIN1

#define _POWER_PORT_2      gpioPortD                           ///< Power connector PIN2: Port
#define _POWER_PIN_2       5                                   ///< Power connector PIN2: Pin number
#define POWER_PIN_2        _POWER_PORT_2,_POWER_PIN_2          ///< Power connector PIN2

#define _POWER_PORT_3      gpioPortD                           ///< Power connector PIN3: Port
#define _POWER_PIN_3       6                                   ///< Power connector PIN3: Pin number
#define POWER_PIN_3        _POWER_PORT_3,_POWER_PIN_3          ///< Power connector PIN3

#define _POWER_PORT_4      gpioPortD                           ///< Power connector PIN4: Port
#define _POWER_PIN_4       4                                   ///< Power connector PIN4: Pin number
#define POWER_PIN_4        _POWER_PORT_4,_POWER_PIN_4          ///< Power connector PIN4

#define _POWER_PORT_6      gpioPortD                           ///< Power connector PIN6: Port
#define _POWER_PIN_6       8                                   ///< Power connector PIN6: Pin number
#define POWER_PIN_6        _POWER_PORT_6,_POWER_PIN_6          ///< Power connector PIN6

#define _POWER_PORT_7      gpioPortE                           ///< Power connector PIN7: Port
#define _POWER_PIN_7       1                                   ///< Power connector PIN7: Pin number
#define POWER_PIN_7        _POWER_PORT_7,_POWER_PIN_7          ///< Power connector PIN7

#define _POWER_PORT_8      gpioPortE                           ///< Power connector PIN8: Port
#define _POWER_PIN_8       2                                   ///< Power connector PIN8: Pin number
#define POWER_PIN_8        _POWER_PORT_8,_POWER_PIN_8          ///< Power connector PIN8

#define _POWER_PORT_9      gpioPortE                           ///< Power connector PIN9: Port
#define _POWER_PIN_9       3                                   ///< Power connector PIN9: Pin number
#define POWER_PIN_9        _POWER_PORT_9,_POWER_PIN_9          ///< Power connector PIN9

#define _POWER_PORT_10     gpioPortE                           ///< Power connector PIN10: Port
#define _POWER_PIN_10      4                                   ///< Power connector PIN10: Pin number
#define POWER_PIN_10       _POWER_PORT_10,_POWER_PIN_10        ///< Power connector PIN10

#define _POWER_PORT_11     gpioPortE                           ///< Power connector PIN11: Port
#define _POWER_PIN_11      5                                   ///< Power connector PIN11: Pin number
#define POWER_PIN_11       _POWER_PORT_11,_POWER_PIN_11        ///< Power connector PIN11

#define _POWER_PORT_12     gpioPortE                           ///< Power connector PIN12: Port
#define _POWER_PIN_12      6                                   ///< Power connector PIN12: Pin number
#define POWER_PIN_12       _POWER_PORT_12,_POWER_PIN_12        ///< Power connector PIN12

#define _POWER_PORT_13     gpioPortE                           ///< Power connector PIN13: Port
#define _POWER_PIN_13      7                                   ///< Power connector PIN13: Pin number
#define POWER_PIN_13       _POWER_PORT_13,_POWER_PIN_13        ///< Power connector PIN13

#define POWER_LEUART_RX   POWER_PIN_2                          ///< Power connector LEUART RX pin
#define POWER_LEUART_TX   POWER_PIN_4                          ///< Power connector LEUART TX pin
#define POWER_LEUART      LEUART0                              ///< Power connector LEUART module
#define POWER_LEUART_LOC  LEUART_ROUTE_LOCATION_LOC0           ///< Power connector LEUART location

#define POWER_UART_RX     POWER_PIN_12                         ///< Power connector UART RX pin
#define POWER_UART_TX     POWER_PIN_13                         ///< Power connector UART TX pin
#define POWER_UART        USART0                               ///< Power connector UART module
#define POWER_UART_LOC    USART_ROUTE_LOCATION_LOC1            ///< Power connector UART location

#define POWER_I2C_SDA     POWER_PIN_1                          ///< Power connector I2C DATA pin
#define POWER_I2C_SCL     POWER_PIN_3                          ///< Power connector I2C CLK pin
#define POWER_I2C         I2C0                                 ///< Power connector I2C module
#define POWER_I2C_LOC     I2C_ROUTE_LOCATION_LOC1              ///< Power connector I2C location

#define POWER_USART_RX    POWER_PIN_12                         ///< Power connector USART RX pin
#define POWER_USART_TX    POWER_PIN_13                         ///< Power connector USART TX pin
#define POWER_USART_CS    POWER_PIN_10                         ///< Power connector USART CS pin
#define POWER_USART_CLK   POWER_PIN_11                         ///< Power connector USART CLK pin
#define POWER_USART       USART0                               ///< Power connector USART module
#define POWER_USART_LOC   USART_ROUTE_LOCATION_LOC1            ///< Power connector USART location

#define POWER_ADC_4       POWER_PIN_4                          ///< Power connector ADC4 pin
#define POWER_ADC_5       POWER_PIN_2                          ///< Power connector ADC5 pin
#define POWER_ADC_6       POWER_PIN_3                          ///< Power connector ADC6 pin
#define POWER_ADC_7       POWER_PIN_1                          ///< Power connector ADC7 pin

/* Debug Connector */

#define _DEBUG_PORT_1     gpioPortD                            ///< Debug connector PIN1: Port
#define _DEBUG_PIN_1      10                                   ///< Debug connector PIN1: Pin number
#define DEBUG_PIN_1       _DEBUG_PORT_1,_DEBUG_PIN_1           ///< Debug connector PIN1

#define _DEBUG_PORT_2     gpioPortA                            ///< Debug connector PIN2: Port
#define _DEBUG_PIN_2      15                                   ///< Debug connector PIN2: Pin number
#define DEBUG_PIN_2       _DEBUG_PORT_2,_DEBUG_PIN_2           ///< Debug connector PIN2

#define _DEBUG_PORT_3     gpioPortD                            ///< Debug connector PIN3: Port
#define _DEBUG_PIN_3      9                                    ///< Debug connector PIN3: Pin number
#define DEBUG_PIN_3       _DEBUG_PORT_3,_DEBUG_PIN_3           ///< Debug connector PIN3

#define _DEBUG_PORT_5     gpioPortE                            ///< Debug connector PIN5: Port
#define _DEBUG_PIN_5      14                                   ///< Debug connector PIN5: Pin number
#define DEBUG_PIN_5       _DEBUG_PORT_5,_DEBUG_PIN_5           ///< Debug connector PIN5

#define _DEBUG_PORT_6     gpioPortE                            ///< Debug connector PIN6: Port
#define _DEBUG_PIN_6      15                                   ///< Debug connector PIN6: Pin number
#define DEBUG_PIN_6       _DEBUG_PORT_6,_DEBUG_PIN_6           ///< Debug connector PIN6

#define _DEBUG_PORT_7     gpioPortE                            ///< Debug connector PIN7: Port
#define _DEBUG_PIN_7      12                                   ///< Debug connector PIN7: Pin number
#define DEBUG_PIN_7       _DEBUG_PORT_7,_DEBUG_PIN_7           ///< Debug connector PIN7

#define _DEBUG_PORT_8     gpioPortE                            ///< Debug connector PIN8: Port
#define _DEBUG_PIN_8      13                                   ///< Debug connector PIN8: Pin number
#define DEBUG_PIN_8       _DEBUG_PORT_8,_DEBUG_PIN_8           ///< Debug connector PIN8

#define _DEBUG_PORT_9     gpioPortE                            ///< Debug connector PIN9: Port
#define _DEBUG_PIN_9      10                                   ///< Debug connector PIN9: Pin number
#define DEBUG_PIN_9       _DEBUG_PORT_9,_DEBUG_PIN_9           ///< Debug connector PIN9

#define _DEBUG_PORT_10     gpioPortE                           ///< Debug connector PIN10: Port
#define _DEBUG_PIN_10      11                                  ///< Debug connector PIN10: Pin number
#define DEBUG_PIN_10       _DEBUG_PORT_10,_DEBUG_PIN_10        ///< Debug connector PIN10

#define _DEBUG_PORT_11     gpioPortB                           ///< Debug connector PIN11: Port
#define _DEBUG_PIN_11      2                                   ///< Debug connector PIN11: Pin number
#define DEBUG_PIN_11       _DEBUG_PORT_11,_DEBUG_PIN_11        ///< Debug connector PIN11

#define _DEBUG_PORT_12     gpioPortB                           ///< Debug connector PIN12: Port
#define _DEBUG_PIN_12      0                                   ///< Debug connector PIN12: Pin number
#define DEBUG_PIN_12       _DEBUG_PORT_12,_DEBUG_PIN_12        ///< Debug connector PIN12

#define _DEBUG_PORT_13     gpioPortA                           ///< Debug connector PIN13: Port
#define _DEBUG_PIN_13      0                                   ///< Debug connector PIN13: Pin number
#define DEBUG_PIN_13       _DEBUG_PORT_13,_DEBUG_PIN_13        ///< Debug connector PIN13

#define _DEBUG_PORT_14     gpioPortB                           ///< Debug connector PIN14: Port
#define _DEBUG_PIN_14      1                                   ///< Debug connector PIN14: Pin number
#define DEBUG_PIN_14      _DEBUG_PORT_14,_DEBUG_PIN_14         ///< Debug connector PIN14

#define _DEBUG_PORT_15     gpioPortA                           ///< Debug connector PIN15: Port
#define _DEBUG_PIN_15      2                                   ///< Debug connector PIN15: Pin number
#define DEBUG_PIN_15       _DEBUG_PORT_15,_DEBUG_PIN_15        ///< Debug connector PIN15

#define _DEBUG_PORT_16     gpioPortA                           ///< Debug connector PIN16: Port
#define _DEBUG_PIN_16      1                                   ///< Debug connector PIN16: Pin number
#define DEBUG_PIN_16       _DEBUG_PORT_16,_DEBUG_PIN_16        ///< Debug connector PIN16

#define _DEBUG_PORT_17     gpioPortA                           ///< Debug connector PIN17: Port
#define _DEBUG_PIN_17      4                                   ///< Debug connector PIN17: Pin number
#define DEBUG_PIN_17       _DEBUG_PORT_15,_DEBUG_PIN_17        ///< Debug connector PIN17

#define _DEBUG_PORT_18     gpioPortA                           ///< Debug connector PIN18: Port
#define _DEBUG_PIN_18      3                                   ///< Debug connector PIN18: Pin number
#define DEBUG_PIN_18       _DEBUG_PORT_18,_DEBUG_PIN_18        ///< Debug connector PIN18

#define _DEBUG_PORT_19     gpioPortF                           ///< Debug connector PIN19: Port
#define _DEBUG_PIN_19      6                                   ///< Debug connector PIN19: Pin number
#define DEBUG_PIN_19       _DEBUG_PORT_19,_DEBUG_PIN_19        ///< Debug connector PIN19

#define _DEBUG_PORT_20     gpioPortF                           ///< Debug connector PIN20: Port
#define _DEBUG_PIN_20      7                                   ///< Debug connector PIN20: Pin number
#define DEBUG_PIN_20       _DEBUG_PORT_20,_DEBUG_PIN_20        ///< Debug connector PIN20

#define _DEBUG_PORT_21     gpioPortF                           ///< Debug connector PIN21: Port
#define _DEBUG_PIN_21      4                                   ///< Debug connector PIN21: Pin number
#define DEBUG_PIN_21       _DEBUG_PORT_21,_DEBUG_PIN_21        ///< Debug connector PIN21

#define _DEBUG_PORT_22     gpioPortF                           ///< Debug connector PIN22: Port
#define _DEBUG_PIN_22      5                                   ///< Debug connector PIN22: Pin number
#define DEBUG_PIN_22       _DEBUG_PORT_22,_DEBUG_PIN_22        ///< Debug connector PIN22

#define _DEBUG_PORT_24     gpioPortF                           ///< Debug connector PIN24: Port
#define _DEBUG_PIN_24      3                                   ///< Debug connector PIN24: Pin number
#define DEBUG_PIN_24       _DEBUG_PORT_24,_DEBUG_PIN_24        ///< Debug connector PIN24

#define _DEBUG_PORT_28     gpioPortC                           ///< Debug connector PIN28: Port
#define _DEBUG_PIN_28      15                                  ///< Debug connector PIN28: Pin number
#define DEBUG_PIN_28       _DEBUG_PORT_20,_DEBUG_PIN_20        ///< Debug connector PIN28

#define _DEBUG_PORT_29     gpioPortC                           ///< Debug connector PIN28: Port
#define _DEBUG_PIN_29      14                                  ///< Debug connector PIN28: Pin number
#define DEBUG_PIN_29       _DEBUG_PORT_29,_DEBUG_PIN_29        ///< Debug connector PIN28

#define _DEBUG_PORT_30     gpioPortC                           ///< Debug connector PIN30: Port
#define _DEBUG_PIN_30      13                                  ///< Debug connector PIN30: Pin number
#define DEBUG_PIN_30       _DEBUG_PORT_30,_DEBUG_PIN_30        ///< Debug connector PIN30

#define _DEBUG_PORT_31     gpioPortC                           ///< Debug connector PIN31: Port
#define _DEBUG_PIN_31      12                                  ///< Debug connector PIN31: Pin number
#define DEBUG_PIN_31       _DEBUG_PORT_31,_DEBUG_PIN_31        ///< Debug connector PIN31

#define _DEBUG_PORT_32     gpioPortC                           ///< Debug connector PIN32: Port
#define _DEBUG_PIN_32      11                                  ///< Debug connector PIN32: Pin number
#define DEBUG_PIN_32       _DEBUG_PORT_32,_DEBUG_PIN_32        ///< Debug connector PIN32

#define DEBUG_LEUART_RX    POWER_PIN_6                         ///< Debug connector LEUART RX pin
#define DEBUG_LEUART_TX    POWER_PIN_5                         ///< Debug connector LEUART TX pin
#define DEBUG_LEUART       LEUART0                             ///< Debug connector LEUART module
#define DEBUG_LEUART_LOC   LEUART_ROUTE_LOCATION_LOC2          ///< Debug connector LEUART location

#define DEBUG_UART_RX      DEBUG_PIN_20                        ///< Debug connector UART RX pin
#define DEBUG_UART_TX      DEBUG_PIN_19                        ///< Debug connector UART TX pin
#define DEBUG_UART_CLOCK   cmuClock_UART0                      ///< Debug connector UART clock
#define DEBUG_UART         UART0                               ///< Debug connector UART module
#define DEBUG_UART_LOC     UART_ROUTE_LOCATION_LOC0            ///< Debug connector UART location

/* Radio Connector */

#define _RADIO_PWR_PORT    gpioPortD                           ///< Radio connector PWR pin: Port
#define _RADIO_PWR_PIN     12                                  ///< Radio connector PWR pin: Pin number
#define RADIO_PWR_PIN      _RADIO_PWR_PORT,_RADIO_PWR_PIN      ///< Radio connector PWR pin

#define _RADIO_RXTX_PORT   gpioPortF                           ///< Radio connector RXTX pin: Port
#define _RADIO_RXTX_PIN    8                                   ///< Radio connector RXTX pin: Pin number
#define RADIO_RXTX_PIN     _RADIO_RXTX_PORT,_RADIO_RXTX_PIN    ///< Radio connector RXTX pin

#define _RADIO_RESET_PORT  gpioPortE                           ///< Radio connector Reset pin: Port
#define _RADIO_RESET_PIN   9                                   ///< Radio connector Reset pin: Pin number
#define RADIO_RESET_PIN    _RADIO_RESET_PORT,_RADIO_RESET_PIN  ///< Radio connector Reset pin

#define RADIO_DI0          DEBUG_PIN_11                        ///< Radio connector IO PIN0
#define RADIO_DI1          DEBUG_PIN_14                        ///< Radio connector IO PIN1
#define RADIO_DI2          DEBUG_PIN_12                        ///< Radio connector IO PIN2
#define RADIO_DI3          DEBUG_PIN_2                         ///< Radio connector IO PIN3
#define RADIO_DI4          DEBUG_PIN_3                         ///< Radio connector IO PIN4
#define RADIO_DI5          DEBUG_PIN_1                         ///< Radio connector IO PIN5

#define RADIO_CLK          DEBUG_PIN_7                         ///< Radio connector USART CLK pin
#define RADIO_CS           DEBUG_PIN_8                         ///< Radio connector USART CS pin
#define RADIO_TX           DEBUG_PIN_9                         ///< Radio connector USART TX pin
#define RADIO_RX           DEBUG_PIN_10                        ///< Radio connector USART RX pin
#define RADIO_USART_CLOCK  cmuClock_USART0                     ///< Radio connector USART clock
#define RADIO_USART        USART0                              ///< Radio connector USART module
#define RADIO_USART_LOC    USART_ROUTE_LOCATION_LOC0           ///< Radio connector USART location

/* SD card Interface */

#define _SD_PWR_PORT    gpioPortA                              ///< SD card interface PWR pin: Port
#define _SD_PWR_PIN     10                                     ///< SD card interface PWR pin: Pin number
#define SD_PWR_PIN      _SD_PWR_PORT,_SD_PWR_PIN               ///< SD card interface PWR pin

#define _SD_DET_PORT    gpioPortD                              ///< SD card interface detection pin: Port
#define _SD_DET_PIN     8                                      ///< SD card interface detection pin: Pin number
#define SD_DET_PIN      _SD_DET_PORT,_SD_DET_PIN               ///< SD card interface detection pin

#define _SD_CLK_PORT    gpioPortB                              ///< SD card interface USART CLK pin: Port
#define _SD_CLK_PIN     5                                      ///< SD card interface USART CLK pin: Pin number
#define SD_CLK_PIN      _SD_CLK_PORT,_SD_CLK_PIN               ///< SD card interface USART CLK pin

#define _SD_CS_PORT     gpioPortB                              ///< SD card interface USART CS pin: Port
#define _SD_CS_PIN      6                                      ///< SD card interface USART CS pin: Pin number
#define SD_CS_PIN       _SD_CS_PORT,_SD_CS_PIN                 ///< SD card interface USART CS pin

#define _SD_RX_PORT     gpioPortB                              ///< SD card interface USART RX pin: Port
#define _SD_RX_PIN      4                                      ///< SD card interface USART RX pin: Pin number
#define SD_RX_PIN       _SD_RX_PORT,_SD_RX_PIN                 ///< SD card interface USART RX pin

#define _SD_TX_PORT     gpioPortB                              ///< SD card interface USART TX pin: Port
#define _SD_TX_PIN      3                                      ///< SD card interface USART TX pin: Pin number
#define SD_TX_PIN       _SD_TX_PORT,_SD_TX_PIN                 ///< SD card interface USART TX pin

#define SD_USART_CLOCK  cmuClock_USART2                        ///< SD card interface USART clock
#define SD_USART        USART2                                 ///< SD card interface USART module
#define SD_USART_LOC    USART_ROUTE_LOCATION_LOC1              ///< SD card interface USART location

/* Onboard IO (LEDs, Buttons, Switches) */

#define _RED_PORT       gpioPortC                              ///< Red LED: Port
#define _RED_PIN        8                                      ///< Red LED: Pin number
#define RED             _RED_PORT,_RED_PIN                     ///< Red LED

#define _ORANGE_PORT    gpioPortC                              ///< Orange LED: Port
#define _ORANGE_PIN     9                                      ///< Orange LED: Pin number
#define ORANGE          _ORANGE_PORT,_ORANGE_PIN               ///< Orange LED

#define _GREEN_PORT     gpioPortC                              ///< Orange LED: Port
#define _GREEN_PIN      10                                     ///< Orange LED: Pin number
#define GREEN           _GREEN_PORT,_GREEN_PIN                 ///< Orange LED

#define _BUTTON_PORT    gpioPortC                              ///< User Button: Port
#define _BUTTON_PIN     11                                     ///< User Button: Pin number
#define BUTTON          _BUTTON_PORT,_BUTTON_PIN               ///< User Button
#define BUTTON_ISR_MASK 0x0800                                 ///< User Button interrupt mask

#define _REED_PORT      gpioPortB                              ///< Reed Switch: Port
#define _REED_PIN       10                                     ///< Reed Switch: Pin number
#define REED_PORT      _REED_PORT,_REED_PIN                    ///< Reed Switch

/* Clock Output */

#define _CMU_HF_PORT    gpioPortC                              ///< High frequency clock output: Port
#define _CMU_HF_PIN     12                                     ///< High frequency clock output: Pin number
#define CMU_HF_PIN      _CMU_HF_PORT,_CMU_HF_PIN               ///< High frequency clock output

#define _CMU_LF_PORT    gpioPortD                              ///< Low frequency clock output: Port
#define _CMU_LF_PIN     8                                      ///< Low frequency clock output: Pin number
#define CMU_LF_PIN     _CMU_LF_PORT,_CMU_LF_PIN                ///< Low frequency clock output
#endif

/**
 * Board: sentio_em
 *
 * Driver for the SENTIO-em board
 */

#include "sentio_em.h"

mcu_efm32  sentio_em::mcu;

#if RTC_DS3234_ENABLE
rtc_ds3234  sentio_em::rtc;
#endif

#if RADIO_AT86RF212B_ENABLE
radio_at86rf212b    sentio_em::radio;
#endif

#if RADIO_CC1101_ENABLE
radio_cc1101        sentio_em::radio;
#endif

#if RADIO_XBEE_DM_ENABLE
radio_xbee_dm       sentio_em::radio;
#endif

#if RADIO_XBEE_802_15_4_ENABLE
radio_xbee_802_15_4 sentio_em::radio;
#endif

#if RADIO_GSM_FONA_ENABLE
radio_gsm_fona      sentio_em::gsm;
#endif

#if MICRO_SD_ENABLE
memory_micro_sd     sentio_em::microSD;
#endif

#if MCU_CLOCK == 32000000
#define OSC_ON cmuOsc_HFXO
#elif MCU_CLOCK == 1000000
#define OSC_FREQ cmuHFRCOBand_1MHz
#elif MCU_CLOCK == 7000000
#define OSC_FREQ cmuHFRCOBand_7MHz
#elif MCU_CLOCK == 11000000
#define OSC_FREQ cmuHFRCOBand_11MHz
#elif MCU_CLOCK == 14000000
#define OSC_FREQ cmuHFRCOBand_14MHz
#elif MCU_CLOCK == 21000000
#define OSC_FREQ cmuHFRCOBand_21MHz
#elif MCU_CLOCK == 28000000
#define OSC_FREQ cmuHFRCOBand_28MHz
#else
#error Invalid MCU_CLOCK in application_config
#endif

sentio_em::sentio_em()
{
	//Apply EFM32 chip dependent Bug-Fixes
	CHIP_Init();
	// Low Frequencies are generated using the internal RC oscillator
#if (MCU_CLOCK <= 28000000)
	CMU_HFRCOBandSet( OSC_FREQ );
	CMU_ClockSelectSet( cmuClock_HF, cmuSelect_HFRCO );
	CMU_OscillatorEnable( cmuOsc_HFXO, false, false );
#else
	CMU_ClockSelectSet( cmuClock_HF, cmuSelect_HFXO );
	CMU_OscillatorEnable( cmuOsc_HFRCO, false, false );
#endif
//  DBG_MSG( "\n\r******************\n\r* Sentio Startup *\n\r******************\n\rMain Clock: " );
//  DBG_MSG( ( uint32_t )MCU_CLOCK );
//  DBG_MSG( "\n\r" );
	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );
	// Disable SD Card Interface, prevent floating of the analog switch
	GPIO_PinModeSet( SD_PWR_PIN, gpioModePushPull, 0 );

#if LF_CLOCK
	CMU_OscillatorEnable( cmuOsc_LFRCO, true, true );
	CMU_ClockSelectSet( cmuClock_LFA, cmuSelect_LFRCO );
	CMU_ClockSelectSet( cmuClock_LFB, cmuSelect_LFRCO );
	CMU_ClockEnable( cmuClock_CORELE, true );
//  DBG_MSG( "Enable LF Clock\n\r" );
#else
//  DBG_MSG( "->Disable LF Clock\n\r" );
#endif

#if CLOCK_TEST_PINS
	CMU->ROUTE = CMU_ROUTE_LOCATION_LOC1 | CMU_ROUTE_CLKOUT1PEN | CMU_ROUTE_CLKOUT0PEN;
	CMU->CTRL |= CMU_CTRL_CLKOUTSEL0_HFCLK16;

	GPIO_PinModeSet( CMU_LF_PIN, gpioModePushPull, 0 );
	GPIO_PinModeSet( CMU_HF_PIN, gpioModePushPull, 0 );
//  DBG_MSG( "Enable Clock Test Pins\n\r" );
#else
//  DBG_MSG( "->Disable Clock Test Pins\n\r" );
#endif

#if ONBOARD_LEDS
	GPIO_PinModeSet( RED,    gpioModePushPull, 0 );
	GPIO_PinModeSet( ORANGE, gpioModePushPull, 0 );
	GPIO_PinModeSet( GREEN,  gpioModePushPull, 0 );
//  DBG_MSG( "Enable Onboard LEDs\n\r" );
#else
//  DBG_MSG( "->Disable Onboard LEDs\n\r" );
#endif

#if ONBOARD_BUTTON
	GPIO_PinModeSet( BUTTON, gpioModeInputPullFilter, 1 );
	GPIO_IntConfig( BUTTON, true, false, true );
//  DBG_MSG( "Enable Onboard Button\n\r" );
#else
//  DBG_MSG( "->Disable Onboard Button\n\r" );
#endif
}

#if ONBOARD_LEDS
void sentio_em::setLed( GPIO_Port_TypeDef port, unsigned int pin )
{
	GPIO->P[port].DOUTSET = 1 << pin;
}

void sentio_em::tglLed( GPIO_Port_TypeDef port, unsigned int pin )
{
	GPIO->P[port].DOUTTGL = 1 << pin;
}

void sentio_em::clrLed( GPIO_Port_TypeDef port, unsigned int pin )
{
	GPIO->P[port].DOUTCLR = 1 << pin;
}
#else
void sentio_em::setLed( GPIO_Port_TypeDef, unsigned int )
{
	;
}

void sentio_em::tglLed( GPIO_Port_TypeDef, unsigned int )
{
	;
}

void sentio_em::clrLed( GPIO_Port_TypeDef, unsigned int )
{
	;
}
#endif
/**
 * @file sentio_gw.h
 *
 * Board: sentio_gw
 *
 * Driver for the SENTIO gateway board
 */

#ifndef SENTIO_GW_H_
#define SENTIO_GW_H_

#include "sentio_gw_io.h"
#include "mcu_efm32.h"

// Module selection (based on the application_config.h)

#if RTC_DS3234_ENABLE
#error RTC3234 not supported on Sentio Gateway
#endif

#if RADIO_AT86RF212B_ENABLE
#include "radio_at86rf212b.h"
#endif

#if RADIO_CC1101_ENABLE
#include "radio_cc1101.h"
#endif

#if RADIO_XBEE_DM_ENABLE
#include "radio_xbee_dm.h"
#include "radio_xbee.h"
#endif

#if RADIO_XBEE_802_15_4_ENABLE
#include "radio_xbee_802_15_4.h"
#include "radio_xbee.h"
#endif

#if MICRO_SD_ENABLE
#error Micro SD card not supportd on Sentio Gateway
#endif

/**
 * Driver class for the SENTIO gateway board
 *
 * The class contains the interface to interact with the SENTIO gateway board.
 */
class sentio_gw
{
public:
	sentio_gw();
	~sentio_gw() {}

	static mcu_efm32  mcu;

#if RADIO_AT86RF212B_ENABLE
	static radio_at86rf212b radio;
#endif

#if RADIO_CC1101_ENABLE
	static radio_cc1101     radio;
#endif

#if RADIO_XBEE_DM_ENABLE
	static radio_xbee_dm    radio;
#endif

	/**
	 * Sets an onboard LED.
	 *
	 * The function sets a LED on the SENTIO gateway board. Board specific LED definitions can be used in this function (see sentio_gw_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void setLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Toggles an onboard LED.
	 *
	 * The function toggles a LED on the SENTIO gateway board. Board specific LED definitions can be used in this function (see sentio_gw_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void tglLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Clears an onboard LED.
	 *
	 * The function clears a LED on the SENTIO gateway board. Board specific LED definitions can be used in this function (see sentio_gw_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void clrLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Returns the state of the onboard button.
	 *
	 * The function returns the state of the button switch. This can be used to adjust application behavior based on the switch position.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     the button state
	 */
	bool getButtonState();

};

#endif /* SENTIO_GW_H_ */

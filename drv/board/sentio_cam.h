/**
 * @file sentio_cam.h
 *
 * Board: sentio_cam
 *
 * Driver for the SENTIO-cam board
 */

#ifndef SENTIO_CAM_H_
#define SENTIO_CAM_H_

#include "sentio_cam_io.h"
#include "mcu_efm32.h"

#include "print.h"
#include "radio_at86rf212b.h"

// Module selection (based on the application_config.h)

#if RTC_DS3234_ENABLE
#error RTC3234 not supported on Sentio Cam
#endif

#if RADIO_CC1101_ENABLE
#error CC1101 not supportd on Sentio Cam
#endif

#if RADIO_XBEE_DM_ENABLE
#error XBEE DM not supportd on Sentio Cam
#endif

#if MICRO_SD_ENABLE
#error Micro SD card not supportd on Sentio Cam
#endif

/**
 * Driver class for the SENTIO-cam board
 *
 * The class contains all member functions to interact with the SENTIO-cam board.
 */
class sentio_cam
{
public:
	sentio_cam();
	~sentio_cam() {}

	static mcu_efm32  mcu;

	static radio_at86rf212b radio;

	/**
	 * Sets an onboard LED.
	 *
	 * The function sets a LED on the SENTIO-cam board. Board specific LED definitions can be used in this function (see sentio_cam_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void setLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Toggles an onboard LED.
	 *
	 * The function toggles a LED on the SENTIO-em board. Board specific LED definitions can be used in this function (see sentio_em_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void tglLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Clears an onboard LED.
	 *
	 * The function clears a LED on the SENTIO-em board. Board specific LED definitions can be used in this function (see sentio_em_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void clrLed( GPIO_Port_TypeDef port, unsigned int pin );

};

#endif /* SENTIO_CAM_H_ */

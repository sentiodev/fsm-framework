/**
 * @file sentio_em.h
 *
 * Board: sentio_em
 *
 * Driver for the SENTIO-em board
 */

#ifndef SENTIO_EM_H_
#define SENTIO_EM_H_

#include "sentio_em_io.h"
#include "mcu_efm32.h"


// Module selection (based on the application_config.h)
#if RTC_DS3234_ENABLE
#include "rtc_ds3234.h"
#endif

#if RADIO_AT86RF212B_ENABLE
#include "radio_at86rf212b.h"
#endif

#if RADIO_CC1101_ENABLE
#include "radio_cc1101.h"
#endif

#if RADIO_XBEE_DM_ENABLE
#include "radio_xbee_dm.h"
#include "radio_xbee.h"
#endif

#if RADIO_XBEE_802_15_4_ENABLE
#include "radio_xbee_802_15_4.h"
#include "radio_xbee.h"
#endif

#if RADIO_GSM_FONA_ENABLE
#include "radio_gsm_fona.h"
#endif

#if MICRO_SD_ENABLE
#include "memory_micro_sd.h"
#endif


/**
 * Driver class for the SENTIO-em board
 *
 * The class contains all member functions to interact with the SENTIO-em board.
 */
class sentio_em
{
public:
	sentio_em();
	~sentio_em() {}

	static mcu_efm32             mcu;

#if RTC_DS3234_ENABLE
	static rtc_ds3234            rtc;
#endif

#if RADIO_AT86RF212B_ENABLE
	static radio_at86rf212b      radio;
#endif

#if RADIO_CC1101_ENABLE
	static radio_cc1101          radio;
#endif

#if RADIO_XBEE_DM_ENABLE
	static radio_xbee_dm         radio;
#endif

#if RADIO_XBEE_802_15_4_ENABLE
	static radio_xbee_802_15_4   radio;
#endif

#if RADIO_GSM_FONA_ENABLE
	static radio_gsm_fona        gsm;
#endif

#if MICRO_SD_ENABLE
	static memory_micro_sd       microSD;
#endif

	/**
	 * Sets an onboard LED.
	 *
	 * The function sets a LED on the SENTIO-em board. Board specific LED definitions can be used in this function (see sentio_em_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void setLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Toggles an onboard LED.
	 *
	 * The function toggles a LED on the SENTIO-em board. Board specific LED definitions can be used in this function (see sentio_em_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void tglLed( GPIO_Port_TypeDef port, unsigned int pin );

	/**
	 * Clears an onboard LED.
	 *
	 * The function clears a LED on the SENTIO-em board. Board specific LED definitions can be used in this function (see sentio_em_io.h).
	 *
	 * @param[in]  port: the port of the LED
	* @param[in]  pin: the pin number
	 * @param[out] none
	 * @return     void
	 */
	void clrLed( GPIO_Port_TypeDef port, unsigned int pin );

};

#endif /* SENTIO_EM_H_ */

/**
 * @file sentio_gw_io.h
 *
 * Interface: sentio_gw_io
 *
 * Describes connector interfaces of the SENTIO gateway platform
 */

#if SENTIO_GW_ENABLE

/* Debug Connector */

#define _DEBUG_PORT_3        gpioPortC                                 ///< Debug connector PIN3: Port
#define _DEBUG_PIN_3         0                                         ///< Debug connector PIN3: Pin number
#define DEBUG_PIN_3          _DEBUG_PORT_3,_DEBUG_PIN_3                ///< Debug connector PIN3

#define _DEBUG_PORT_4        gpioPortC                                 ///< Debug connector PIN4: Port
#define _DEBUG_PIN_4         1                                         ///< Debug connector PIN4: Pin number
#define DEBUG_PIN_4          _DEBUG_PORT_4,_DEBUG_PIN_4                ///< Debug connector PIN4

#define DEBUG_UART_RX        DEBUG_PIN_4                               ///< Debug connector UART RX pin
#define DEBUG_UART_TX        DEBUG_PIN_3                               ///< Debug connector UART TX pin
#define DEBUG_UART_CLOCK     cmuClock_USART1                           ///< Debug connector UART clock
#define DEBUG_UART           USART1                                    ///< Debug connector UART module
#define DEBUG_UART_LOC       USART_ROUTE_LOCATION_LOC0                 ///< Debug connector UART location

/* Radio Connector */

#define _RADIO_PWR_PORT      gpioPortX                                 ///< Radio connector PWR: Port (undefined in Version 1)
#define _RADIO_PWR_PIN       XX                                        ///< Radio connector PWR: Pin number (undefined in Version 1)
#define RADIO_PWR_PIN        _RADIO_PWR_PORT,_RADIO_PWR_PIN            ///< Radio connector PWR (undefined in Version 1)

#define _RADIO_PWM1_PORT     gpioPortC                                 ///< Radio connector XBEE-PWM output: Port
#define _RADIO_PWM1_PIN      15                                        ///< Radio connector XBEE-PWM output: Pin number
#define RADIO_PWM1_PIN       _RADIO_PWM1_PORT,_RADIO_PWM1_PIN          ///< Radio connector XBEE-PWM output

#define _RADIO_RSSI_PORT     gpioPortC                                 ///< Radio connector XBEE-RSSI output: Port
#define _RADIO_RSSI_PIN      14                                        ///< Radio connector XBEE-RSSI output: Pin number
#define RADIO_RSSI_PIN       _RADIO_RSSI_PORT,_RADIO_RSSI_PIN          ///< Radio connector XBEE-RSSI output

#define _RADIO_RESET_PORT    gpioPortC                                 ///< Radio connector Reset: Port
#define _RADIO_RESET_PIN     13                                        ///< Radio connector Reset: Pin number
#define RADIO_RESET_PIN      _RADIO_RESET_PORT,_RADIO_RESET_PIN        ///< Radio connector Reset

#define _RADIO_XBEE_DO8_PORT gpioPortC                                 ///< Radio connector XBEE-Digital IO 8: Port
#define _RADIO_XBEE_DO8_PIN  11                                        ///< Radio connector XBEE-Digital IO 8: Pin Number
#define RADIO_XBEE_DO8_PIN   _RADIO_XBEE_DO8_PORT,_RADIO_XBEE_DO8_PIN  ///< Radio connector XBEE-Digital IO 8

#define _RADIO_XBEE_AD0_PORT gpioPortC                                 ///< Radio connector XBEE Analog-IO 0: Port
#define _RADIO_XBEE_AD0_PIN  9                                         ///< Radio connector XBEE Analog-IO 0: Pin number
#define RADIO_XBEE_AD0_PIN   _RADIO_XBEE_AD0_PORT,_RADIO_XBEE_AD0_PIN  ///< Radio connector XBEE Analog-IO

#define _RADIO_XBEE_AD1_PORT gpioPortC                                 ///< Radio connector XBEE Analog-IO 1: Port
#define _RADIO_XBEE_AD1_PIN  8                                         ///< Radio connector XBEE Analog-IO 1: Pin number 
#define RADIO_XBEE_AD1_PIN   _RADIO_XBEE_AD1_PORT,_RADIO_XBEE_AD1_PIN  ///< Radio connector XBEE Analog-IO

#define _RADIO_XBEE_AD2_PORT gpioPortC                                 ///< Radio connector XBEE Analog-IO 2: Port
#define _RADIO_XBEE_AD2_PIN  10                                        ///< Radio connector XBEE Analog-IO 2: Pin number
#define RADIO_XBEE_AD2_PIN   _RADIO_XBEE_AD2_PORT,_RADIO_XBEE_AD2_PIN  ///< Radio connector XBEE Analog-IO

#define _RADIO_XBEE_AD3_PORT gpioPortD                                 ///< Radio connector XBEE Analog-IO 3: Port
#define _RADIO_XBEE_AD3_PIN  7                                         ///< Radio connector XBEE Analog-IO 3: Pin number 
#define RADIO_XBEE_AD3_PIN   _RADIO_XBEE_AD3_PORT,_RADIO_XBEE_AD3_PIN  ///< Radio connector XBEE Analog-IO

#define _RADIO_XBEE_AD4_PORT gpioPortA                                 ///< Radio connector XBEE Analog-IO 4: Port
#define _RADIO_XBEE_AD4_PIN  8                                         ///< Radio connector XBEE Analog-IO 4: Pin number
#define RADIO_XBEE_AD4_PIN   _RADIO_XBEE_AD4_PORT,_RADIO_XBEE_AD4_PIN  ///< Radio connector XBEE Analog-IO 4

#define _RADIO_XBEE_AD5_PORT gpioPortD                                 ///< Radio connector XBEE Analog-IO 5: Port
#define _RADIO_XBEE_AD5_PIN  5                                         ///< Radio connector XBEE Analog-IO 5: Pin number
#define RADIO_XBEE_AD5_PIN   _RADIO_XBEE_AD5_PORT,_RADIO_XBEE_AD5_PIN  ///< Radio connector XBEE Analog-IO 5

#define _RADIO_XBEE_RTS_PORT gpioPortC                                 ///< Radio connector XBEE UART RTS: Port
#define _RADIO_XBEE_RTS_PIN  14                                        ///< Radio connector XBEE UART RTS: Pin number
#define RADIO_XBEE_RTS_PIN   _RADIO_XBEE_RTS_PORT,_RADIO_XBEE_RTS_PIN  ///< Radio connector XBEE UART RTS

#define _RADIO_XBEE_CTS_PORT gpioPortA                                 ///< Radio connector XBEE UART CTS: Port
#define _RADIO_XBEE_CTS_PIN  9                                         ///< Radio connector XBEE UART CTS: Pin number
#define RADIO_XBEE_CTS_PIN   _RADIO_XBEE_CTS_PORT,_RADIO_XBEE_CTS_PIN

#define _RADIO_XBEE_ON_PORT  gpioPortA                                 ///< Radio connector XBEE-ON (module wakeup from sleep): Port
#define _RADIO_XBEE_ON_PIN   10                                        ///< Radio connector XBEE-ON (module wakeup from sleep): Pin number
#define RADIO_XBEE_ON_PIN    _RADIO_XBEE_ON_PORT,_RADIO_XBEE_ON_PIN    ///< Radio connector XBEE-ON (module wakeup from sleep)
#define _RADIO_CLK_PORT      gpioPortE                                 ///< Radio connector USART SPI clock (SPI CLK): Port
#define _RADIO_CLK_PIN       12                                        ///< Radio connector USART SPI clock (SPI CLK): Pin number
#define RADIO_CLK            _RADIO_CLK_PORT,_RADIO_CLK_PIN            ///< Radio connector USART SPI clock (SPI CLK)

#define _RADIO_CS_PORT       gpioPortE                                 ///< Radio connector USART SPI chip select (SPI CS): Port
#define _RADIO_CS_PIN        13                                        ///< Radio connector USART SPI chip select (SPI CS): Pin number
#define RADIO_CS             _RADIO_CS_PORT,_RADIO_CS_PIN              ///< Radio connector USART SPI chip select (SPI CS)

#define _RADIO_TX_PORT       gpioPortE                                 ///< Radio connector USART TX (SPI MOSI): Port
#define _RADIO_TX_PIN        10                                        ///< Radio connector USART TX (SPI MOSI): Pin number
#define RADIO_TX             _RADIO_TX_PORT,_RADIO_TX_PIN              ///< Radio connector USART TX (SPI MOSI)

#define _RADIO_RX_PORT       gpioPortE                                 ///< Radio connector USART RX (SPI MOSI): Port
#define _RADIO_RX_PIN        11                                        ///< Radio connector USART RX (SPI MOSI): Pin number
#define RADIO_RX             _RADIO_RX_PORT,_RADIO_RX_PIN              ///< Radio connector USART RX (SPI MOSI)

#define RADIO_USART_CLOCK    cmuClock_USART0                           ///< Radio connector USART clock
#define RADIO_USART          USART0                                    ///< Radio connector USART module
#define RADIO_USART_LOC      USART_ROUTE_LOCATION_LOC0                 ///< Radio connector USART location

/* Onboard IO (LEDs, Buttons, Switches) */

#define _RED_PORT            gpioPortB                                 ///< Red LED: Port
#define _RED_PIN             8                                         ///< Red LED: Pin
#define RED                  _RED_PORT,_RED_PIN                        ///< Red LED

#define _GREEN_PORT          gpioPortB                                 ///< Green LED: Port
#define _GREEN_PIN           7                                         ///< Green LED: Pin number
#define GREEN                _GREEN_PORT,_GREEN_PIN                    ///< Green LED

#define _SWITCH_PORT         gpioPortA                                 ///< User Switch: Port
#define _SWITCH_PIN          0                                         ///< User Switch: Pin number
#define SWITCH               _SWITCH_PORT,_SWITCH_PIN                  ///< User Button

#define SWITCH_ISR_MASK      0x0001                                    ///< User Switch interrupt mask

#endif

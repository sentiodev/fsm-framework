/**
 * Board: sentio_gw
 *
 * Driver for the SENTIO gateway board
 */

#include "sentio_gw.h"

mcu_efm32  sentio_gw::mcu;

#if RADIO_XBEE_DM_ENABLE
radio_xbee_dm   sentio_gw::radio;
#endif

#if RADIO_CC1101_ENABLE
radio_cc1101    sentio_gw::radio;
#endif

#if RADIO_AT86RF212B_ENABLE
radio_at86rf212b    sentio_gw::radio;
#endif

#if MCU_CLOCK > 28000000
#error Maximal Clock Speed 28MHz
#elif MCU_CLOCK == 1000000
#define OSC_FREQ cmuHFRCOBand_1MHz
#elif MCU_CLOCK == 7000000
#define OSC_FREQ cmuHFRCOBand_7MHz
#elif MCU_CLOCK == 11000000
#define OSC_FREQ cmuHFRCOBand_11MHz
#elif MCU_CLOCK == 14000000
#define OSC_FREQ cmuHFRCOBand_14MHz
#elif MCU_CLOCK == 21000000
#define OSC_FREQ cmuHFRCOBand_21MHz
#elif MCU_CLOCK == 28000000
#define OSC_FREQ cmuHFRCOBand_28MHz
#else
#error Invalid MCU_CLOCK in application_config
#endif


sentio_gw::sentio_gw()
{
	//Apply EFM32 chip dependent Bug-Fixes
	CHIP_Init();
	// Low Frequencies are generated using the internal RC oscillator

	CMU_HFRCOBandSet( OSC_FREQ );
	CMU_ClockSelectSet( cmuClock_HF, cmuSelect_HFRCO );
	CMU_OscillatorEnable( cmuOsc_HFXO, false, false );

	// Enable the GPIO-Pins of the EFM32 MCU
	CMU_ClockEnable( cmuClock_GPIO, true );


#if LF_CLOCK
	CMU_OscillatorEnable( cmuOsc_LFRCO, true, true );
	CMU_ClockSelectSet( cmuClock_LFA, cmuSelect_LFRCO );
	CMU_ClockEnable( cmuClock_CORELE, true );
#endif

#if CLOCK_TEST_PINS
	CMU->ROUTE = CMU_ROUTE_LOCATION_LOC1 | CMU_ROUTE_CLKOUT1PEN | CMU_ROUTE_CLKOUT0PEN;

	GPIO_PinModeSet( CMU_LF_PIN, gpioModePushPull, 0 );
	GPIO_PinModeSet( CMU_HF_PIN, gpioModePushPull, 0 );
#endif

#if ONBOARD_LEDS
	GPIO_PinModeSet( RED,    gpioModePushPull, 0 );
	GPIO_PinModeSet( GREEN, gpioModePushPull, 0 );
#endif

#if ONBOARD_BUTTON
	GPIO_PinModeSet( SWITCH, gpioModeInput, 0 );
#endif
}


#if ONBOARD_LEDS
	void sentio_gw::setLed( GPIO_Port_TypeDef port, unsigned int pin )
	{
		GPIO->P[port].DOUTSET = 1 << pin;
	}

	void sentio_gw::tglLed( GPIO_Port_TypeDef port, unsigned int pin )
	{
		GPIO->P[port].DOUTTGL = 1 << pin;
	}

	void sentio_gw::clrLed( GPIO_Port_TypeDef port, unsigned int pin )
	{
		GPIO->P[port].DOUTCLR = 1 << pin;
	}
#else
	void sentio_gw::setLed( GPIO_Port_TypeDef, unsigned int )
	{
		;
	}

	void sentio_gw::tglLed( GPIO_Port_TypeDef, unsigned int )
	{
		;
	}

	void sentio_gw::clrLed( GPIO_Port_TypeDef, unsigned int )
	{
		;
	}
#endif


bool sentio_gw::getButtonState()
{
#if ONBOARD_BUTTON
	return GPIO_PinInGet(SWITCH);
#else
	return false;	
#endif
}


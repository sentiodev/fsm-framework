/**
 * @file sentio_cam_io.h
 *
 * Interface: sentio_cam_io
 *
 * Describes connector interfaces of the SENTIO-cam platform (V1.0,2.0)
 */


#if SENTIO_CAM_ENABLE

/* Debug Connector */

#define _DEBUG_PORT_4        gpioPortD                                ///< Debug connector PIN4: Port
#define _DEBUG_PIN_4         4                                        ///< Debug connector PIN4: Pin number
#define DEBUG_PIN_4          _DEBUG_PORT_4,_DEBUG_PIN_4               ///< Debug connector PIN4

#define _DEBUG_PORT_6        gpioPortD                                ///< Debug connector PIN5: Port
#define _DEBUG_PIN_6         5                                        ///< Debug connector PIN5: Pin number
#define DEBUG_PIN_6          _DEBUG_PORT_6,_DEBUG_PIN_6               ///< Debug connector PIN5

#define DEBUG_UART_RX        DEBUG_PIN_6                              ///< Debug connector UART RX pin
#define DEBUG_UART_TX        DEBUG_PIN_4                              ///< Debug connector UART TX pin

#define DEBUG_UART           LEUART0                                  ///< Debug connector USART module
#define DEBUG_UART_CLOCK     cmuClock_LEUART0                         ///< Debug connector USART clock
#define DEBUG_UART_LOC       LEUART_ROUTE_LOCATION_LOC0               ///< Debug connector USART location

/* Radio Interface */

#define _RADIO_SLP_TR_PORT   gpioPortE                                ///<  Radio AT86RF212B sleep pin: Port
#define _RADIO_SLP_TR_PIN    14                                       ///<  Radio AT86RF212B sleep pin: Pin number
#define RADIO_SLP_TR_PIN     _RADIO_SLP_TR_PORT,_RADIO_SLP_TR_PIN     ///<  Radio AT86RF212B sleep pin

#define _RADIO_DIG1_PORT     gpioPortA                                ///< Radio AT86RF212B IO pin 1: Port
#define _RADIO_DIG1_PIN      0                                        ///< Radio AT86RF212B IO pin 1: Pin number
#define RADIO_DIG1_PIN       _RADIO_DIG1_PORT,_RADIO_DIG1_PIN         ///< Radio AT86RF212B IO pin 1: Pin

#define _RADIO_DIG2_PORT     gpioPortE                                ///< Radio AT86RF212B IO pin 2: Port
#define _RADIO_DIG2_PIN      15                                       ///< Radio AT86RF212B IO pin 2: Pin number
#define RADIO_DIG2_PIN       _RADIO_DIG2_PORT,_RADIO_DIG2_PIN         ///< Radio AT86RF212B IO pin 2: Pin

#define _RADIO_DIG3_PORT     gpioPortE                                ///< Radio AT86RF212B IO pin 3: Port
#define _RADIO_DIG3_PIN      8                                        ///< Radio AT86RF212B IO pin 3: Pin number
#define RADIO_DIG3_PIN       _RADIO_DIG3_PORT,_RADIO_DIG3_PIN         ///< Radio AT86RF212B IO pin 3: Pin

#define _RADIO_DIG4_PORT     gpioPortF                                ///< Radio AT86RF212B IO pin 4: Port
#define _RADIO_DIG4_PIN      5                                        ///< Radio AT86RF212B IO pin 4: Pin number
#define RADIO_DIG4_PIN       _RADIO_DIG4_PORT,_RADIO_DIG4_PIN         ///< Radio AT86RF212B IO pin 4: Pin

#define _RADIO_IRQ_PORT      gpioPortE                                ///< Radio AT86RF212B Interrupt pin: Port
#define _RADIO_IRQ_PIN       9                                        ///< Radio AT86RF212B Interrupt pin: Pin number
#define RADIO_IRQ_PIN        _RADIO_IRQ_PORT,_RADIO_IRQ_PIN           ///< Radio AT86RF212B Interrupt pin

#define _RADIO_RSTN_PORT     gpioPortA                                ///< Radio AT86RF212B Reset pin: Port
#define _RADIO_RSTN_PIN      1                                        ///< Radio AT86RF212B Reset pin: Pin number
#define RADIO_RSTN_PIN       _RADIO_RSTN_PORT,_RADIO_RSTN_PIN         ///< Radio AT86RF212B Reset pin

#define _RADIO_CLK_PORT      gpioPortE                                ///< Radio AT86RF212B USART SPI clock (SPI CLK): Port
#define _RADIO_CLK_PIN       12                                       ///< Radio AT86RF212B USART SPI clock (SPI CLK): Pin number
#define RADIO_CLK_PIN        _RADIO_CLK_PORT,_RADIO_CLK_PIN           ///< Radio AT86RF212B USART SPI clock (SPI CLK)

#define _RADIO_CS_PORT       gpioPortE                                ///< Radio AT86RF212B USART SPI chip select (SPI CS): Port
#define _RADIO_CS_PIN        13                                       ///< Radio AT86RF212B USART SPI chip select (SPI CS): Pin number
#define RADIO_CS_PIN         _RADIO_CS_PORT,_RADIO_CS_PIN             ///< Radio AT86RF212B USART SPI chip select (SPI CS)

#define _RADIO_TX_PORT       gpioPortE                                ///< Radio AT86RF212B USART TX (SPI MOSI): Port
#define _RADIO_TX_PIN        10                                       ///< Radio AT86RF212B USART TX (SPI MOSI): Pin
#define RADIO_TX_PIN         _RADIO_TX_PORT,_RADIO_TX_PIN             ///< Radio AT86RF212B USART TX (SPI MOSI)

#define _RADIO_RX_PORT       gpioPortE                                ///< Radio AT86RF212B USART RX (SPI MOSI): Port
#define _RADIO_RX_PIN        11                                       ///< Radio AT86RF212B USART RX (SPI MOSI): Pin
#define RADIO_RX_PIN         _RADIO_RX_PORT,_RADIO_RX_PIN             ///< Radio AT86RF212B USART RX (SPI MISO)

#define RADIO_USART_CLOCK    cmuClock_USART0                          ///< Radio AT86RF212B USART clock
#define RADIO_USART          USART0                                   ///< Radio AT86RF212B USART module
#define RADIO_USART_LOC      USART_ROUTE_LOCATION_LOC0                ///< Radio AT86RF212B USART location

/* Sensor Interface */

#define _SENSOR_PWR_PORT     gpioPortA                                ///< Sensor VC0706 PWR enable pin: Port
#define _SENSOR_PWR_PIN      10                                       ///< Sensor VC0706 PWR enable pin: Pin
#define SENSOR_PWR           _SENSOR_PWR_PORT,_SENSOR_PWR_PIN         ///< Sensor VC0706 PWR enable pin

#define _SENSOR_UART_RX_PORT gpioPortC                                ///< Sensor VC0706 USART RX: Port
#define _SENSOR_UART_RX_PIN  3                                        ///< Sensor VC0706 USART RX: Pin number
#define SENSOR_UART_RX       _SENSOR_UART_RX_PORT,_SENSOR_UART_RX_PIN ///< Sensor VC0706 USART RX pin

#define _SENSOR_UART_TX_PORT gpioPortC                                ///< Sensor VC0706 USART RX: Port
#define _SENSOR_UART_TX_PIN  2                                        ///< Sensor VC0706 USART TX: Pin number
#define SENSOR_UART_TX       _SENSOR_UART_TX_PORT,_SENSOR_UART_TX_PIN ///< Sensor VC0706 USART TX pin

#define SENSOR_UART_CLOCK    cmuClock_USART2                          ///< Sensor VC0706 USART clock
#define SENSOR_UART          USART2                                   ///< Sensor VC0706 USART module
#define SENSOR_UART_LOC      USART_ROUTE_LOCATION_LOC0                ///< Sensor VC0706 USART location
#define SENSOR_UART_RX_ISR   USART2_RX_IRQn                           ///< Sensor VC0706 USART interrupt number

/* On-board LEDS (Camera Illumination)*/

#define _LED1_PORT        gpioPortA                                   ///< Illumination LED group 1: Port
#define _LED1_PIN         3                                           ///< Illumination LED group 1: Pin number
#define LED1              _LED1_PORT,_LED1_PIN                        ///< Illumination LED group 1 

#define _LED2_PORT        gpioPortA                                   ///< Illumination LED group 2: Port
#define _LED2_PIN         2                                           ///< Illumination LED group 2: Pin number
#define LED2              _LED2_PORT,_LED2_PIN                        ///< Illumination LED group 2

#define _LED3_PORT        gpioPortC                                   ///< Illumination LED group 3: Port
#define _LED3_PIN         15                                          ///< Illumination LED group 3: Pin number
#define LED3              _LED3_PORT,_LED3_PIN                        ///< Illumination LED group 3

#define _LED4_PORT        gpioPortD                                   ///< Illumination LED group 4: Port
#define _LED4_PIN         3                                           ///< Illumination LED group 4: Pin number
#define LED4              _LED4_PORT,_LED4_PIN                        ///< Illumination LED group 4

#endif

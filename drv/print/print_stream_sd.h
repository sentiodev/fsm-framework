
#ifndef _PRINT_STREAM_SD_H_
#define _PRINT_STREAM_SD_H_

#include <stdint.h>

#include "ff.h"
#include "microsd.h"
#include "diskio.h"


#define BUFFER_LENGTH_SD 1000

class print_stream_sd
{
private:
	FATFS   Fatfs;                /* File system specific */
	UINT    br, bw;                /* File read/write count */
	FIL     file;
	FRESULT res;

	char buffer[BUFFER_LENGTH_SD];
	uint32_t index;
	bool overflow;

	bool mount( uint8_t logicalDrive )
	{
		if ( f_mount( logicalDrive, &Fatfs ) != FR_OK )
		{
			return false;
		}
		else
		{
			return true;
		}
	}

public:
	inline void transmit( uint8_t input )
	{
		if ( input != '\0' )
		{
			if ( index >= BUFFER_LENGTH_SD - 1 )
			{
				flush();
				overflow = true;
				index = 0;

			}

			buffer[index] = input;
			index++;
		}
	}

	inline char receive()
	{
		return buffer[index--];
	}

	inline bool getStatus()
	{
		return overflow;
	}

	void flush()
	{
		res = f_open( &file, "test.txt",  FA_WRITE );

		if ( res != FR_OK )
		{
			/*  If file does not exist create it*/
			res = f_open( &file, "test.txt", FA_CREATE_ALWAYS | FA_WRITE );
			if ( res != FR_OK )
			{
				return;
			}
		}

		res = f_lseek( &file, file.fsize );

		if ( res != FR_OK )
		{
			return;
		}

		f_write( &file, buffer, index, &bw );
		f_close( &file );

		power_off();

		index    = 0;
		overflow = false;

		return;
	}

	void init()
	{
		MICROSD_init( MCU_CLOCK );
		mount( 0 );

		index    = 0;
		overflow = false;
	}

	print_stream_sd() {}
	~print_stream_sd() {}
};

#endif // _PRINT_STREAM__SD_H_

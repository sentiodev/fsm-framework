/**
 * @file print.h
 *
 * Driver: print to serial streams such as uart, ram-printBuffer or SD card
 *
 * Common Interface class that enables to read and write serial data streams to different devices using an dedicated hardware abstraction
 * layer implemented in the base that is inherited by this common interface. Thus the user can print to various different devices using
 * the same high level interface.
 *
 */

#ifndef _PRINT_h_
#define _PRINT_h_

#if PRINT_SERIAL_ENABLE
#include "print_serial.h"
#endif

#if PRINT_STREAM_ENABLE || ISR_MSG_ENABLE
#include "print_stream.h"
#endif

#if PRINT_STREAM_SD_ENABLE
#include "print_stream_sd.h"
#endif

#include "time.h"
#include "date.h"

/** Format of 8,16,32-Bit Integers */
enum FORMAT {
	hex,
	dec,
	bin,
	raw
};

/** Format used to print date and time objects */
enum TIME_FORMAT {
	hhmmss,
	yymmddhhmmss
};

struct SERIAL_CONFIG
{
	FORMAT      format;
	TIME_FORMAT timeFormat;
	uint8_t     debugLevel;
	uint8_t     minimalWidth;
	uint8_t     floatLength;
	char        fill;
};

const char endl[3] = "\n"; ///< Specify End of Line
const char spl[3]  = " ";    ///< Specify Default Separator

/**
 * High Level Abstraction to provide a common interface to different stream objects
 *
 * The class contains the interface to write a formated ASCII string to any kind of serial interface that is
 * available on the micro-controller
 */
template <class C>
class print : public C
{
public:
	print();
	~print() {}

	void setConfig( SERIAL_CONFIG config );

	/**
	 * Print an ASCII string to the serial interface
	 *
	 * The function performs an byte-wise transfer of the input string to the serial interface. No reformatting is done.
	 *
	 * @param[in]  const char*: String to be written
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const char *input );

	/**
	 * Print an ASCII character to the serial interface
	 *
	 * The function performs an byte-wise transfer of the input string to the serial interface. No reformatting is done.
	 *
	 * @param[in]  const char: Character to be written
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const char input );

	/**
	 * Print a float value as ASCII representation to the serial interface
	 *
	 * The function performs a conversion of the given float-value and will then perform a byte-wise transfer of the resulting
	 * string to the serial interface.
	 *
	 * @param[in]  const float: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const float input );

	/**
	 * Print a 32-Bit unsigned integer to serial interface
	 *
	 * The function performs a conversion of the given 32-Bit integer value and will then perform a byte-wise transfer of the resulting
	 * string to the serial interface.
	 *
	 * @param[in]  const uint32_t: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const uint32_t input );

	/**
	 * Print a 16-Bit unsigned integer to serial interface
	 *
	 * The function performs a conversion of the given 16-Bit integer value and will then perform a byte-wise transfer of the resulting
	 * string to the serial interface.
	 *
	 * @param[in]  const uint16_t: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const uint16_t input );

	/**
	 * Print a 8-Bit unsigned integer to serial interface
	 *
	 * The function performs a conversion of the given 8-Bit integer value and will then perform a byte-wise transfer of the resulting
	 * string to the serial interface.
	 *
	 * @param[in]  const uint8_t: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const uint8_t input );

	/**
	 * Print a 32-Bit integer to serial interface
	 *
	 * The function performs a conversion of the given 32-Bit integer value and will then perform a byte-wise transfer of the resulting
	 * string to the serial interface.
	 *
	 * @param[in]  const int32_t: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const int32_t input );

	/**
	 * Print a 16-Bit integer to serial interface
	 *
	 * The function performs a conversion of the given 16-Bit integer value and will then perform a byte-wise transfer of the resulting
	 * string to the serial interface.
	 *
	 * @param[in]  const int16_t: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const int16_t input );

	/**
	 * Print a 8-Bit integer to serial interface
	 *
	 * The function performs a conversion of the given 8-Bit integer value and will then perform a byte-wise
	 * transfer of the resulting string to the serial interface.
	 *
	 * @param[in]  const int8_t: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const int8_t input );

	/**
	 * Print a bool variable to serial interface
	 *
	 * The function performs a conversion of the given bool value and will then perform a byte
	 * transfer of the resulting string to the serial interface.
	 *
	 * @param[in]  const bool: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const bool input );

	/**
	 * Print date to serial interface
	 *
	 * The function performs a conversion of the given date information (according to the formatting currently set)
	 * value and will then perform a byte-wise transfer of the resulting string to the serial interface.
	 *
	 * @param[in]  const &date: reference to date, which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const date& input );
	/**
	 * Print time to serial interface
	 *
	 * The function performs a conversion of the given time information (according to the formatting currently set)
	 * value and will then perform a byte-wise transfer of the resulting string to the serial interface.
	 *
	 * @param[in]  const &time: reference to date, which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const time& input );

	/**
	 * Set the output format used when printing integer and float
	 *
	 * @param[in]  const FORMAT: valid formats are raw, bin, hex, dec
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const FORMAT input );

	/**
	 * Set the format used to print time and date objects
	 *
	 * @param[in]  const TIME_FORMAT: value which will be written to the serial interface
	 * @param[out] none
	 * @return     &print: Reference to print in order to allow operator chaining
	 */
	print& operator << ( const TIME_FORMAT input ) {
		timeFormat = input;
		return *this;
	};

	/**
	 * Set the output format used when printing integer and float
	 *
	 * @param[in]  const FORMAT: valid formats are raw, bin, hex, dec
	 * @param[out] none
	 * @return     void
	 */
	void setFormat( const FORMAT input ) {
		format = input;
	}

	/**
	 * Set the format used to print time and date objects.
	 *
	 * @param[in]  const TIME_FORMAT: value which will be written to the serial interface
	 * @param[out] none
	 * @return     void
	 */
	void setTimeFormat( const TIME_FORMAT input ) {
		timeFormat = input;
	}

	/**
	 * Set the minimal with integers and float values.
	 *
	 * This function sets the minimal number of digits, which float and integer values will occupy
	 * in the resulting string. In case the resulting string would be shorter that the minimal required
	 * width, the remaining spaces will be filled using ASCII character specified using the setFill()
	 * function or the .fill-parameter in the SERIAL_CONFIG. The resulting string is always right-hand aligned.
	 * If setting minimal width shorter that the resulting string the result will not be cut or rounded,
	 * it will consult more space in the output string.
	 *
	 * @param[in]  const uint8_t: value minimal width is set to
	 * @param[out] none
	 * @return     void
	 */
	void setWidth( const uint8_t input ) {
		minimalWidth = input;
	}

	/**
	 * Set the debug level that the print object is using
	 *
	 * This function sets the debug level that the print object is using, if set to a lower level than specified in then
	 * application_config.h the object will not generate any output and the print functions will return directly.
	 *
	 * @param[in]  const uint8_t: value to which the debug level will be set
	 * @param[out] none
	 * @return     void
	 */
	void setDbgLevel( const uint8_t input ) {
		debugLevel = input;
	}

	/**
	 * Set the number of digits that a float-value will occupy.
	 *
	 * This function sets the number of digits, which float values will occupy in the resulting string. In case the
	 * float-length is set shorter that the minimal width the remaining space will be filled using th ASCII specified using the setFill()
	 * function or the .fill-parameter in the SERIAL_CONFIG. The resulting string is always right-hand aligned.
	 * If setting minimal width shorter that the resulting string the result will not be cut or rounded,
	 * it will consult more space in the output string.
	 *
	 * @param[in]  const uint8_t: value to which the float-length will be set
	 * @param[out] none
	 * @return     void
	 */
	void setFloatLength( const uint8_t input ) {
		floatLength = input;
	}

	/**
	 * Set the ASCII character used for filling resulting strings of float and integer values.
	 *
	 * In case the resulting string of a number to string conversion would be shorter than specified, using the minimal width parameter,
	 * the character set using this function will be used to fill the empty places in the string.
	 *
	 * @param[in]  const char: ASCII character used for filling strings
	 * @param[out] none
	 * @return     void
	 */
	void setFill( const char input ) {
		fill = input;
	}

private:
	template<typename T> inline void printHex( T input );
	template<typename T> inline uint32_t printDec( T input );
	template<typename T> inline void printBin( T input );
	template<typename T> inline void printRaw( T input );

	FORMAT  format;
	TIME_FORMAT timeFormat;
	uint8_t debugLevel;
	uint8_t minimalWidth;
	uint8_t floatLength;
	char fill;
};

template<class C>
print<C>::print() : format( dec ),        \
	timeFormat( hhmmss ), \
	debugLevel( 0 ),      \
	minimalWidth( 3 ),    \
	floatLength( 6 ),     \
	fill( ' ' )
{
	// Call the hardware initialization function specified in the base class.
	C::init();
}

template<class C>
template<typename T>
uint32_t print<C>::printDec( T input )
{
	uint8_t temp[11];

	int32_t intermediate = input;

	uint32_t length = 0;
	uint32_t i = 0;
	uint8_t  shorten = 0;

	if ( input < 0 )
	{
		intermediate *= -1;
		i++;
	}

	do
	{
		temp[i] = intermediate % 10;
		intermediate /= 10;
		i++;
	}
	while ( intermediate );

	if ( i < minimalWidth )
		for ( uint32_t j = 0; j < minimalWidth - i; j++ )
		{
			C::transmit( fill );
		}

	if ( input < 0 )
	{
		C::transmit( '-' );
		shorten = 1;
	}

	length = i - shorten;

	do
	{
		i--;
		C::transmit( temp[i] + '0' );
	}
	while ( i - shorten );

	return length;
}

template<class C>
print<C>& print<C>::operator << ( const char *input )
{
#if SERIAL_OUTPUT_ENABLE
	uint8_t i = 0;

	while ( input[i] != '\0' )
	{
		C::transmit( input[i] );
		i++;
	}
#endif
	return *this;
}


template<class C>
print<C>& print<C>::operator << ( const char input )
{
#if SERIAL_OUTPUT_ENABLE
	C::transmit( input );
#endif
	return *this;
}


template<class C>
print<C>& print<C>::operator << ( float input )
{
#if SERIAL_OUTPUT_ENABLE

	if ( format != raw )
	{
		uint32_t length  = 0;
		float  printBuffer        = input;

		uint8_t storeWidth = minimalWidth;
		minimalWidth = 0;

		length = printDec <const int32_t>( printBuffer );

		minimalWidth = storeWidth;

		if ( printBuffer < 0 )
		{
			printBuffer *= -1;
			length++;
		}


		if ( length <= floatLength )
			C::transmit( '.' );

		while ( length < floatLength )
		{
			printBuffer = printBuffer - ( uint32_t ) printBuffer;

			printBuffer *= 10;
			C::transmit( ( ( uint8_t ) printBuffer ) + ( ( ( uint8_t ) printBuffer > 0x09 ) ? '7' : '0' ) );

			length++;
		}
	}
	else
	{
		for ( uint32_t i = 4; i > 0; i-- )
		{
			// Shift and masking the MSB of the input variable and print
			C::transmit( ( ( ( uint8_t )( ( uint32_t ) input >> ( ( i - 1 ) << 3 ) ) ) ) );
		}
	}
#endif

	return *this;
}

template<class C>
template<typename T>
void print<C>::printRaw( T input )
{
	uint32_t i = sizeof( T );

	for ( ; i > 0; i-- )
	{
		// Shift and masking the MSB of the input variable and print
		C::transmit( ( ( ( uint8_t )( input >> ( ( i - 1 ) << 3 ) ) ) ) );
	}
}

template<class C>
template<typename T>
void print<C>::printHex( T input )
{
	uint32_t i = sizeof( T ) << 1;
	uint8_t temp;

	for ( ; i > 0; i-- )
	{
		// Shift and masking the MSB of the input variable
		temp = ( ( uint8_t )( input >> ( ( i - 1 ) << 2 ) ) ) & 0x0F;

		C::transmit( ( temp + ( ( temp <= 0x09 ) ? '0' : '7' ) ) );
	}
}

template<class C>
template<typename T>
void print<C>::printBin( T input )
{
#if SERIAL_OUTPUT_ENABLE
	uint32_t i = sizeof( T ) << 3 ;
	uint8_t temp;

	for ( ; i > 0; i-- )
	{
		// Shift and masking the MS-Bit of the input variable
		temp = ( ( uint8_t )( input >> ( ( i - 1 ) ) ) ) & 0x01;

		C::transmit( ( temp + ( ( temp <= 0x09 ) ? '0' : '7' ) ) );
	}
#endif
}


template<class C>
print<C>& print<C>::operator << ( const uint32_t input )
{
#if SERIAL_OUTPUT_ENABLE
	switch ( format )
	{
	case hex:
		printHex<const uint32_t>( input );
		break;
	case dec:
		printDec<const uint32_t>( input );
		break;
	case bin:
		printBin<const uint32_t>( input );
		break;
	case raw:
		printRaw<const uint32_t>( input );
		break;
	default:
		break;
	}
#endif
	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const uint16_t input )
{
#if SERIAL_OUTPUT_ENABLE
	switch ( format )
	{
	case hex:
		printHex<const uint16_t>( input );
		break;
	case dec:
		printDec<const uint16_t>( input );
		break;
	case bin:
		printBin<const uint16_t>( input );
		break;
	case raw:
		printRaw<const uint16_t>( input );
		break;
	default:
		break;
	}
#endif
	return *this;
}

template<class C>
print<C>& print<C>::operator << ( uint8_t input )
{
#if SERIAL_OUTPUT_ENABLE
	switch ( format )
	{
	case hex:
		printHex<const uint8_t>( input );
		break;
	case dec:
		printDec<const uint8_t>( input );
		break;
	case bin:
		printBin<const uint8_t>( input );
		break;
	case raw:
		printRaw<const uint8_t>( input );
		break;
	default:
		break;
	}
#endif
	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const int32_t input )
{
#if SERIAL_OUTPUT_ENABLE
	switch ( format )
	{
	case hex:
		printHex<const int32_t>( input );
		break;
	case dec:
		printDec<const int32_t>( input );
		break;
	case bin:
		printBin<const int32_t>( input );
		break;
	case raw:
		printRaw<const int32_t>( input );
		break;
	default:
		break;
	}
#endif

	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const int16_t input )
{
#if SERIAL_OUTPUT_ENABLE
	switch ( format )
	{
	case hex:
		printHex<const int16_t>( input );
		break;
	case dec:
		printDec<const int16_t>( input );
		break;
	case bin:
		printBin<const int16_t>( input );
		break;
	case raw:
		printRaw<const int16_t>( input );
		break;

	default:
		break;
	}
#endif

	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const int8_t input )
{
#if SERIAL_OUTPUT_ENABLE
	switch ( format )
	{
	case hex:
		printHex<const int8_t>( input );
		break;
	case dec:
		printDec<const int8_t >( input );
		break;
	case bin:
		printBin<const int8_t>( input );
		break;
	case raw:
		printRaw<const int8_t>( input );
		break;
	default:
		break;
	}
#endif

	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const bool input )
{
#if SERIAL_OUTPUT_ENABLE
	C::transmit( input + '0' );
#endif

	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const FORMAT input )
{
#if SERIAL_OUTPUT_ENABLE
	format = input;
#endif

	return *this;
}

template<class C>
print<C>& print<C>::operator << ( const date& input )
{
#if SERIAL_OUTPUT_ENABLE
	FORMAT  tempFormat = format;
	uint8_t tempMinWidth = minimalWidth;
	char    tempFill    = fill;

	format       = dec;
	minimalWidth = 2;
	fill = '0';

	switch ( timeFormat )
	{
	default:
		*this << ( uint16_t )( input.getYear() );
		*this << "-";
		*this << ( uint8_t ) input.getMonth();
		*this << "-";
		*this << ( uint8_t ) input.getDate();
		*this << " T";
		*this << ( uint8_t ) input.getHour();
		*this << ":";
		*this << input.getMinute();
		*this << ":";
		*this << ( uint8_t ) input.getSecond();
		*this << "  ";
		*this << ( uint16_t ) input.getMilliSec();
		break;
	}
	minimalWidth = tempMinWidth;
	format       = tempFormat;
	fill = tempFill;
#endif

	return *this;
}


template<class C>
print<C>& print<C>::operator << ( const time& input )
{
#if SERIAL_OUTPUT_ENABLE
	FORMAT  tempFormat = format;
	uint8_t tempMinWidth = minimalWidth;
	char    tempFill    = fill;

	format       = dec;
	minimalWidth = 2;
	fill = '0';

	switch ( timeFormat )
	{
	default:
		*this << input.getHour();
		*this << ":";
		*this << input.getMinute();
		*this << ":";
		*this << input.getSecond();
		*this << ".";
		minimalWidth = 3;
		*this << input.getMilliSec();

		break;
	}
	minimalWidth = tempMinWidth;
	format       = tempFormat;
	fill = tempFill;
#endif

	return *this;
}


#endif // _PRINT_h_

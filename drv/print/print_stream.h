/**
 * @file print_stream.h
 *
 * Driver: print_stream
 *
 * Driver to read write a data string to a RAM buffer
 */

#ifndef _PRINT_STREAM_H_
#define _PRINT_STREAM_H_

#include <stdint.h>

#define BUFFER_LENGTH_STREAM 3000  ///< Define the length of the ram-buffer

/**
 * Driver to read write a data string to a RAM buffer
 *
 * This class provides access to an ram segment via the high level interface implemented in print.h
 */
class print_stream
{
public:
	print_stream() {}
	~print_stream() {}

	/**
	 * Initializes the ram buffer of the micro-controller.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init()
	{
		index    = 0;
		overflow = false;
		interator = buffer;
	}

	/**
	 * Transfer a 8-bit value to the ram buffer
	 *
	 * @param[in]  uint8_t data to be transmitted
	 * @param[out] none
	 * @return     void
	 */
	inline void transmit( uint8_t input )
	{
		if ( input != '\0' )
		{
			if ( index == BUFFER_LENGTH_STREAM - 1 )
			{
				overflow = true;
				index = 0;
			}

			buffer[index] = input;
			index++;
		}
	}

	/**
	 * Obtain a 8-bit value to the ram buffer
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     char received data
	 */
	inline char receive()
	{
		return buffer[index--];
	}

	/**
	 * Test if the ram buffer overflew
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool overflow status
	 */
	inline bool getStatus()
	{
		return overflow;
	}

	/**
	 * Get an pointer to the first element of the buffer.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t received data
	 */
	char const* flush()
	{
		// Insert String End
		buffer[index] = '\0';
		// Reset buffer counter
		index = 0;
		// Reset overflow status
		overflow = true;

		return buffer;
	}


	char const* end()
	{
		// Insert String End
		buffer[index] = '\0';
		return &buffer[index];
	}
	
	uint32_t size()
	{
		// Insert String End
		buffer[index] = '\0';
		return index;
	}
	
	char const* begin()
	{
		// Insert String End
		buffer[index] = '\0';
		return buffer;
	}

private:
	char     buffer[BUFFER_LENGTH_STREAM];
	
	char*    interator;
	uint32_t index;
	bool     overflow;
};

#endif // _PRINT_STREAM_H_

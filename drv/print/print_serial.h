/**
 * @file print_serial.h
 *
 * Driver: print_serial
 *
 * Driver to access the LEUART/UART of the MCU to output debug messages
 */

#ifndef _PRINT_SERIAL_H_
#define _PRINT_SERIAL_H_

#include "mcu_efm32.h"

// Platform selection
#if SENTIO_EM_ENABLE
#include "sentio_em_io.h"
#if MESHLIUM_INTERFACE_ENABLE
#define SERIAL_UART_TX      SENSOR_UART_TX
#define SERIAL_UART_RX      SENSOR_UART_RX
#define SERIAL_UART_CLOCK   SENSOR_UART_CLOCK
#define SERIAL_UART         SENSOR_UART
#define SERIAL_UART_LOC     SENSOR_UART_LOC
#define SERIAL_UART_RX_ISR  SENSOR_UART_RX_ISR
#else
#define SERIAL_UART_TX      DEBUG_UART_TX
#define SERIAL_UART_RX      DEBUG_UART_RX
#define SERIAL_UART_CLOCK   DEBUG_UART_CLOCK
#define SERIAL_UART         DEBUG_UART
#define SERIAL_UART_LOC     DEBUG_UART_LOC
#define SERIAL_UART_RX_ISR  DEBUG_UART_RX_ISR
#endif
#elif SENTIO_GW_ENABLE
#include "sentio_gw_io.h"
#define SERIAL_UART_TX      DEBUG_UART_TX
#define SERIAL_UART_RX      DEBUG_UART_RX
#define SERIAL_UART_CLOCK   DEBUG_UART_CLOCK
#define SERIAL_UART         DEBUG_UART
#define SERIAL_UART_LOC     DEBUG_UART_LOC
#define SERIAL_UART_RX_ISR  DEBUG_UART_RX_ISR
#elif SENTIO_CAM_ENABLE
#include "sentio_cam_io.h"
#else
#error No Sensor Platform defined
#endif


/**
* Driver: print_serial
 *
 * The class provides read/write access to access the LEUART/UART of the MCU to output debug messages
 * on the platforms serial interface.
 *
 */
class print_serial
{
private:

public:
	print_serial() {}
	~print_serial() {}

#if SENTIO_EM_ENABLE || SENTIO_GW_ENABLE
	/**
	 * Initializes the interface to the micro-controller.
	 *
	 * The function initializes the serial interface on platforms that use the standard UART to transmit debug messages.
	 * It is automatically called in the constructor of the derived class.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init()
	{
		// Setup the Parameters used to configure the selected UART/USART module
		USART_InitAsync_TypeDef  initUart =
		{
			usartEnable,     // Enable the UART after the Configuration is finalized
			MCU_CLOCK,       // The HF-Clock Frequency is used to calculate the Baudrate-Clock-Divider Setting
			SERIAL_BAUDRATE, // Resulting Baudrate at which the UART module runs
			usartOVS4,      // RX-Pin Over-sampling-Rate
			usartDatabits8,  // Number of Data-Bits
			SERIAL_PARITY,   // Parity-Bit setup
			SERIAL_STOP,      // Number of Stop-Bits
#if defined(_EFM32_GIANT_FAMILY) || defined(_EFM32_TINY_FAMILY) || defined(_EFM32_WONDER_FAMILY)
			false,
			false,
			usartPrsRxCh0
#endif
		};

		// Enable the IO Pins of the MCU
		CMU_ClockEnable( cmuClock_GPIO, true );

		//Enable the required periphery clock
		CMU_ClockEnable( SERIAL_UART_CLOCK, true );

		USART_InitAsync( SERIAL_UART, &initUart );

		// The Routing Register of the selected UART/USART Register is configured. The following register-access
		// enables the UART modules RX/TX shift-register and furthermore selects the one of the possible Locations of the modules IO-Pins
		//
		// NOTE!!!
		// Beside setting a modules Routing Register the functionality of the GPIO-Pins IO-Port-driver has to be configured separately

		SERIAL_UART->ROUTE = USART_ROUTE_RXPEN |
							 USART_ROUTE_TXPEN |
							 SERIAL_UART_LOC;

		// Configure the IO-Port driver of the EFM32-GPIO Pins which were selected before
		GPIO_PinModeSet( SERIAL_UART_RX, gpioModeInputPullFilter, 1 );
		GPIO_PinModeSet( SERIAL_UART_TX, gpioModePushPull, 0 );
	}

	/**
	 * Transmit a 8-bit value via the UART interface of the MCU.
	 *
	 * @param[in]  uint8_t data to be transmitted
	 * @param[out] none
	 * @return     void
	 */
	void transmit( uint8_t data ) {
		USART_Tx( SERIAL_UART, data );
	}

	/**
	 * Receive a 8-bit value via the UART interface of the MCU. Function is stalling until a byte is received.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     char data received via the UART
	 */
	char receive() {
		while ( !( SERIAL_UART->STATUS & USART_STATUS_RXDATAV ) );
		return SERIAL_UART->RXDATA;
	}

	/**
	 * Receive a 8-bit value via the UART interface of the MCU. Function is stalling until a byte is received.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t received data
	 */
	char receive( uint32_t timeout )
	{
		uint32_t i = timeout;
		for ( i = timeout; ( !( SERIAL_UART->STATUS & USART_STATUS_RXDATAV ) &&  i ); i-- );
		if ( i )
			return SERIAL_UART->RXDATA;
		else
			return 0;

	}

	/**
	 * Test if a single 8-bit value has been received via the UART interface.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     bool status of the receive buffer (filled/empty)
	 */
	bool dataValid()              {
		return ( SERIAL_UART->STATUS & USART_STATUS_RXDATAV );
	}

	/**
	 * Wait until the TX buffer is empty.
	 *
	 * Stall the program until the entire data is transmitted and the buffers are empty.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t received data
	 */
	char const * flush()           {
		while ( !( SERIAL_UART->STATUS & USART_STATUS_TXC ) );
		return "Serial-Port Flush!";
	}

#elif SENTIO_CAM_ENABLE
	/**
	 * Initializes the interface to the micro-controller.
	 *
	 * The function initializes the serial interface on platforms that use the LEUART to transmit debug messages.
	 * It is automatically called in the constructor of the derived class.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     void
	 */
	void init()
	{
		// Setup the Parameters used to configure the selected UART/USART module
		LEUART_Init_TypeDef  initUart;
		initUart.enable      = leuartEnable;    // Enable the UART after the Configuration is finalized
		initUart.refFreq     = MCU_CLOCK / 2;     // The HF-Clock Frequency is used to calculate the Baudrate-Clock-Divider Setting
		initUart.baudrate    = SERIAL_BAUDRATE; // Resulting Baudrate at which the UART module runs
		initUart.databits    = leuartDatabits8;  // Number of Data-Bits
		initUart.parity      = leuartNoParity;   // Parity-Bit setup
		initUart.stopbits    = leuartStopbits1;     // Number of Stop-Bits

		// Enable the IO Pins of the MCU
		CMU_ClockEnable( cmuClock_GPIO, true );

		//Enable the required periphery clock
		CMU_ClockEnable( DEBUG_UART_CLOCK, true );

		LEUART_Init( DEBUG_UART, &initUart );

		// The Routing Register of the selected UART/USART Register is configured. The following register-access
		// enables the UART modules RX/TX shift-register and furthermore selects the one of the possible Locations of the modules IO-Pins
		//
		// NOTE!!!
		// Beside setting a modules Routing Register the functionality of the GPIO-Pins IO-Port-driver has to be configured separately
		DEBUG_UART->ROUTE = USART_ROUTE_RXPEN |
							USART_ROUTE_TXPEN |
							DEBUG_UART_LOC;

		// Configure the IO-Port driver of the EFM32-GPIO Pins which were selected before
		GPIO_PinModeSet( DEBUG_UART_RX, gpioModeInputPullFilter, 1 );
		GPIO_PinModeSet( DEBUG_UART_TX, gpioModePushPull, 0 );
	}

	/**
	 * Transmit a 8-bit value via the UART interface of the MCU.
	 *
	 * @param[in]  uint8_t data to be transmitted
	 * @param[out] none
	 * @return     void
	 */
	void transmit( uint8_t data ) {
		LEUART_Tx( DEBUG_UART, data );
	}

	/**
	 * Stall the program until the entire data is transmitted and the buffers are empty.
	 *
	 * @param[in]  none
	 * @param[out] none
	 * @return     uint8_t received data
	 */
	char const * flush()           {
		while ( !( DEBUG_UART->STATUS & USART_STATUS_TXC ) );
		return "Serial-Port Flush!";
	}
#endif
};

#endif // _PRINT_SERIAL_H_

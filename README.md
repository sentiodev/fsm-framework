FSM Framework
==================

This file contains the kernel, hardware abstraction layer and libraries for
development on the Sentio platform.


Clone the project
-----------------

This code is intended to be included as a submodule in a Sentio application.
The code can be neither compiled nor executed on its own. Nevertheless, to
inspect the code or make changes, it can be cloned.

    git clone git@bitbucket.org:sentiodev/fsm-framework.git

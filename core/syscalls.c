#include "em_device.h"
#include "core_cm3.h"

int _getpid()
{
	return 0;
}

void _exit( int status )
{
	NVIC_SystemReset();

	while ( 1 );
}

int _kill( int pid, int sig )
{
	return -1;
}

int _sbrk()
{
	return 0;
}


#include <stdint.h>

/** Micro-controller sleep modes */
enum CPU_MODE
{
	on       = 0,
	sleep    = 1,
	stop     = 2,
	deepStop = 3,
	off      = 4
};

struct STATUS_BLOCK
{
	CPU_MODE   mode;
	uint32_t   applicationId;
	uint32_t   nextState;
	uint32_t   initialState;
};


enum ANCHOR_ISR
{
	useInAllFsm  = 1,
	useInThisFsm = 0
};

typedef void ( *ISR_HANDLER )();

class base_system
{
protected:
	static STATUS_BLOCK *currentStatusBlock;
};


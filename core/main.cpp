/*
 * main.cpp
 *
 *  Created on: May 15, 2011
 *      Author: Matthias Krämer
 */

#include "events.h"

events    fsmSystem;

/****************************************************************************************************************************************//**
 * @brief Main Application-Code
 * Initialize the Main-System and setup/execute the Application
 *******************************************************************************************************************************************/

int main( void )
{
	fsmSystem.execute();

	fsmSystem.codeError();

	return 0;
}

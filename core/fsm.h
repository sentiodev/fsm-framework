/**
 * Core: Sentio Software Framework - Finite State Machine Support
 *
 *
 */

#ifndef FSM_H_
#define FSM_H_

#include "driver_interface.h"

// Application configuration check
#ifndef MAX_NUM_STATES
#error Missing define in application_config.h: MAX_NUM_STATES
#endif

#ifndef MAX_NUM_APPS
#error Missing define in application_config.h: MAX_NUM_APPS
#endif

#define FSM_STOP false
#define FSM_CONT true

typedef bool ( *STATE_FUNCTION )();


// Provide "C- Prototypes" of Interrupt Service Routines to make them visible to the Linker
extern "C" {
	void GPIO_EVEN_IRQHandler() __attribute__( ( used, externally_visible ) );
	void GPIO_ODD_IRQHandler()  __attribute__( ( used, externally_visible ) );

	void TIMER0_IRQHandler()    __attribute__( ( used, externally_visible ) );
	void TIMER1_IRQHandler()    __attribute__( ( used, externally_visible ) );
	void TIMER2_IRQHandler()    __attribute__( ( used, externally_visible ) );
	void TIMER3_IRQHandler()    __attribute__( ( used, externally_visible ) );

	void USART0_RX_IRQHandler() __attribute__( ( used, externally_visible ) );
	void USART0_TX_IRQHandler() __attribute__( ( used, externally_visible ) );
	void USART1_RX_IRQHandler() __attribute__( ( used, externally_visible ) );
	void USART1_TX_IRQHandler() __attribute__( ( used, externally_visible ) );
	void USART2_RX_IRQHandler() __attribute__( ( used, externally_visible ) );
	void USART2_TX_IRQHandler() __attribute__( ( used, externally_visible ) );

	void I2C0_IRQHandler()      __attribute__( ( used, externally_visible ) );
	void I2C1_IRQHandler()      __attribute__( ( used, externally_visible ) );

	void UART0_RX_IRQHandler()  __attribute__( ( used, externally_visible ) );
	void UART0_TX_IRQHandler()  __attribute__( ( used, externally_visible ) );
	void UART1_RX_IRQHandler()  __attribute__( ( used, externally_visible ) );
	void UART1_TX_IRQHandler()  __attribute__( ( used, externally_visible ) );

	void LEUART0_IRQHandler()   __attribute__( ( used, externally_visible ) );
	void LEUART1_IRQHandler()   __attribute__( ( used, externally_visible ) );
	void LETIMER0_IRQHandler()  __attribute__( ( used, externally_visible ) );

	void PCNT0_IRQHandler()     __attribute__( ( used, externally_visible ) );
	void PCNT1_IRQHandler()     __attribute__( ( used, externally_visible ) );
	void PCNT2_IRQHandler()     __attribute__( ( used, externally_visible ) );

	void ADC0_IRQHandler()      __attribute__( ( used, externally_visible ) );

	void AES_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void ACMP0_IRQHandler()     __attribute__( ( used, externally_visible ) );
	void BURTC_IRQHandler()     __attribute__( ( used, externally_visible ) );
	void CMU_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void DAC0_IRQHandler()      __attribute__( ( used, externally_visible ) );
	void DMA_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void EBI_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void EMU_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void LCD_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void LESENSE_IRQHandler()   __attribute__( ( used, externally_visible ) );
	void MSC_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void RTC_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void USB_IRQHandler()       __attribute__( ( used, externally_visible ) );
	void VCMP_IRQHandler()      __attribute__( ( used, externally_visible ) );
}


class fsm: public driver_interface, public base_system
{
private:
	static void _wrapperIRQ_default();

	friend void GPIO_EVEN_IRQHandler();
	friend void GPIO_ODD_IRQHandler();

	friend void TIMER0_IRQHandler();
	friend void TIMER1_IRQHandler();
	friend void TIMER2_IRQHandler();
	friend void TIMER3_IRQHandler();

	friend void USART0_RX_IRQHandler();
	friend void USART0_TX_IRQHandler();
	friend void USART1_RX_IRQHandler();
	friend void USART1_TX_IRQHandler();
	friend void USART2_RX_IRQHandler();
	friend void USART2_TX_IRQHandler();

	friend void I2C0_IRQHandler();
	friend void I2C1_IRQHandler();

	friend void UART0_RX_IRQHandler();
	friend void UART0_TX_IRQHandler();
	friend void UART1_RX_IRQHandler();
	friend void UART1_TX_IRQHandler();

	friend void LEUART0_IRQHandler();
	friend void LEUART1_IRQHandler();
	friend void LETIMER0_IRQHandler();

	friend void PCNT0_IRQHandler();
	friend void PCNT1_IRQHandler();
	friend void PCNT2_IRQHandler();

	friend void ADC0_IRQHandler();

	friend void AES_IRQHandler();
	friend void ACMP0_IRQHandler();
	friend void BURTC_IRQHandler();
	friend void CMU_IRQHandler();
	friend void DAC0_IRQHandler();
	friend void DMA_IRQHandler();
	friend void EBI_IRQHandler();
	friend void EMU_IRQHandler();
	friend void LCD_IRQHandler();
	friend void LESENSE_IRQHandler();
	friend void MSC_IRQHandler();
	friend void RTC_IRQHandler();
	friend void USB_IRQHandler();
	friend void VCMP_IRQHandler();

	static ISR_HANDLER wrapperGPIO_EVEN_IRQ [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperGPIO_ODD_IRQ  [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperTIMER0_IRQ    [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperTIMER1_IRQ    [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperTIMER2_IRQ    [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperTIMER3_IRQ    [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperUSART0_RX_IRQ [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUSART0_TX_IRQ [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUSART1_RX_IRQ [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUSART1_TX_IRQ [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUSART2_RX_IRQ [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUSART2_TX_IRQ [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperI2C0_IRQ      [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperI2C1_IRQ      [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperUART0_RX_IRQ  [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUART0_TX_IRQ  [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUART1_RX_IRQ  [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUART1_TX_IRQ  [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperLEUART0_IRQ   [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperLEUART1_IRQ   [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperLETIMER0_IRQ  [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperPCNT0_IRQ     [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperPCNT1_IRQ     [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperPCNT2_IRQ     [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperADC0_IRQ      [MAX_NUM_APPS * 16];

	static ISR_HANDLER wrapperAES_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperACMP0_IRQ     [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperBURTC_IRQ     [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperCMU_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperDAC0_IRQ      [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperDMA_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperEBI_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperEMU_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperLCD_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperLESENSE_IRQ   [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperMSC_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperRTC_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperUSB_IRQ       [MAX_NUM_APPS * 16];
	static ISR_HANDLER wrapperVCMP_IRQ      [MAX_NUM_APPS * 16];

	STATUS_BLOCK         myStatusBlock;
	STATUS_BLOCK        *callingStatusBlock;
	static uint32_t      countApplications;

	STATE_FUNCTION stateDefinition [MAX_NUM_STATES];

public:
	fsm();
	~fsm() {}

	void execute();

	static inline void directTransitionTo( uint8_t state ) {
		currentStatusBlock->nextState = state;
		currentStatusBlock->mode = on;
	}

	static inline void prepareSleepTransition( CPU_MODE sleep ) {
		currentStatusBlock->mode = sleep;
	}

	static inline void setFirstState( uint8_t initialState ) {
		currentStatusBlock->initialState  = initialState;
		currentStatusBlock->applicationId = countApplications;
	}

	static inline uint8_t readCurrentState() {
		return currentStatusBlock->nextState;
	}

	inline void registerFsmState( STATE_FUNCTION stateFunction, uint8_t stateNumber ) {
		if(stateNumber < MAX_NUM_STATES)
			stateDefinition[stateNumber] = stateFunction;
		else
			codeError();
	}

	void registerEventHandler( ISR_HANDLER function, uint8_t interruptSource, uint8_t interruptId, ANCHOR_ISR anchorISR );
};

#endif /* FSM_H_ */

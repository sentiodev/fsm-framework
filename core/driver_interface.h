/*
 * driver_interface.h
 */

#ifndef DRIVER_INTERFACE_H_
#define DRIVER_INTERFACE_H_

#include "time_delay.h"

#if PRINT_SERIAL_ENABLE || ISR_MSG_ENABLE || PRINT_STREAM_ENABLE || PRINT_STREAM_SD_ENABLE
#include "print.h"
#endif

#if SENTIO_EM_ENABLE
#include "sentio_em.h"
#endif

#if SENTIO_GW_ENABLE
#include "sentio_gw.h"
#endif

#if SENTIO_CAM_ENABLE
#include "sentio_cam.h"
#endif


#if SENSOR_SHT15_ENABLE
#include "sensor_sht15.h"
#endif

#if SENSOR_VC0706_ENABLE
#include "sensor_vc0706.h"
#endif


#if DEBUG_BOARD_ENABLE
#include "debugboard.h"
#endif
#if CONF_EH_ENABLE
#include "conf_eh.h"
#endif
#if SOLAR_HARVESTER_ENABLE
#include "solarharvester.h"
#endif
#if EMULATOR_BOARD_ENABLE
#endif

#if OIL_IN_WATER_BOARD_ENABLE
#include "oil_in_water_board.h"
#endif
	
#if SMARTLIGHT_ENABLE
#include "smartlight_board.h"
#endif

class driver_interface
{
public:
	static time_delay              delay;


#if PRINT_SERIAL_ENABLE
	static print <print_serial>    cout;
#endif

#if ISR_MSG_ENABLE
	static print <print_stream>    msg;
#endif

#if PRINT_STREAM_ENABLE
	static print <print_stream>    stream;
#endif

#if PRINT_STREAM_SD_ENABLE
	static print <print_stream_sd> sd;
#endif


#if SENTIO_EM_ENABLE
	static sentio_em               sentio;
#endif

#if SENTIO_GW_ENABLE
	static sentio_gw               sentio;
#endif

#if SENTIO_CAM_ENABLE
	static sentio_cam              sentio;
#endif


#if SENSOR_SHT15_ENABLE
	static sensor_sht15            sht;
#endif
#if SENSOR_VC0706_ENABLE
	static sensor_vc0706           vc0706;
#endif


#if DEBUG_BOARD_ENABLE
	static debugboard              dbg;
#endif
#if CONF_EH_ENABLE
	static conf_eh                 confEh;
#endif
#if SOLAR_HARVESTER_ENABLE
	static solarharvester          solarHarvester;
#endif

#if SMARTLIGHT_ENABLE
	static smartlight_board        smartlight;
#endif

#if OIL_IN_WATER_BOARD_ENABLE
	static oil_in_water_board      water;
#endif


#if EMULATOR_BOARD_ENABLE
#endif
	driver_interface() {}
	~driver_interface() {}
	
	void codeError();
};

#endif /* DRIVER_INTERFACE_H_ */

/*
 * fsm.cpp
 *
 *  Created on: May 13, 2011
 *      Author: Matthias Krämer
 */


#if SENTIO_EM_ENABLE
#include "sentio_em.h"
#elif SENTIO_GW_ENABLE
#include "sentio_gw.h"
#elif SENTIO_CAM_ENABLE
#include "sentio_cam.h"
#else
#error No Sensor Platform defined
#endif

#include "fsm.h"

uint32_t fsm::countApplications;

#if GPIO_EVEN_IRQ_ENABLE
ISR_HANDLER fsm::wrapperGPIO_EVEN_IRQ[MAX_NUM_APPS * 16];
void GPIO_EVEN_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = GPIO -> IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperGPIO_EVEN_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	GPIO->IFC = _GPIO_IFC_MASK;
}
#endif

#if GPIO_ODD_IRQ_ENABLE
ISR_HANDLER fsm::wrapperGPIO_ODD_IRQ[MAX_NUM_APPS * 16];
void GPIO_ODD_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = GPIO -> IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperGPIO_ODD_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	GPIO->IFC = _GPIO_IFC_MASK;
}
#endif

#if TIMER0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperTIMER0_IRQ[MAX_NUM_APPS * 16];
void TIMER0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = TIMER0 -> IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperTIMER0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	TIMER0->IFC = _TIMER_IFC_MASK;
}
#endif

#if TIMER1_IRQ_ENABLE
ISR_HANDLER fsm::wrapperTIMER1_IRQ[MAX_NUM_APPS * 16];
void TIMER1_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = TIMER1 -> IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperTIMER1_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	TIMER1->IFC = _TIMER_IFC_MASK;
}
#endif

#if TIMER2_IRQ_ENABLE
ISR_HANDLER fsm::wrapperTIMER2_IRQ[MAX_NUM_APPS * 16];
void TIMER2_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = TIMER2 -> IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperTIMER2_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	TIMER2->IFC = _TIMER_IFC_MASK;
}
#endif

#if TIMER3_IRQ_ENABLE
ISR_HANDLER fsm::wrapperTIMER3_IRQ[MAX_NUM_APPS * 16];
void TIMER3_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = TIMER3->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperTIMER3_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	TIMER3->IFC = _TIMER_IFC_MASK;
}
#endif

#if USART0_RX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSART0_RX_IRQ[MAX_NUM_APPS * 16];
void USART0_RX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USART0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSART0_RX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USART0->IFC = _USART_IFC_MASK;
}
#endif

#if USART0_TX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSART0_TX_IRQ[MAX_NUM_APPS * 16];
void USART0_TX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USART0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSART0_TX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USART0->IFC = _USART_IFC_MASK;
}
#endif

#if USART1_RX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSART1_RX_IRQ[MAX_NUM_APPS * 16];
void USART1_RX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USART1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSART1_RX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USART1->IFC = _USART_IFC_MASK;
}
#endif

#if USART1_TX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSART1_TX_IRQ[MAX_NUM_APPS * 16];
void USART1_TX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USART1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSART1_TX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USART1->IFC = _USART_IFC_MASK;
}
#endif

#if USART2_RX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSART2_RX_IRQ[MAX_NUM_APPS * 16];
void USART2_RX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USART2->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSART2_RX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USART2->IFC = _USART_IFC_MASK;
}
#endif

#if USART2_TX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSART2_TX_IRQ[MAX_NUM_APPS * 16];
void USART2_TX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USART2->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSART2_TX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USART2->IFC = _USART_IFC_MASK;
}
#endif

#if I2C0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperI2C0_IRQ[MAX_NUM_APPS * 16];
void I2C0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = I2C0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperI2C0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	I2C0->IFC = _I2C_IFC_MASK;
}
#endif

#if I2C1_IRQ_ENABLE
ISR_HANDLER fsm::wrapperI2C1_IRQ[MAX_NUM_APPS * 16];
void I2C0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = I2C1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperI2C1_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	I2C1->IFC = _I2C_IFC_MASK;
}
#endif

#if UART0_RX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUART0_RX_IRQ[MAX_NUM_APPS * 16];
void UART0_RX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = UART0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUART0_RX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	UART0->IFC = _UART_IFC_MASK;
}
#endif

#if UART0_TX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUART0_TX_IRQ[MAX_NUM_APPS * 16];
void UART0_TX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = UART0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUART0_TX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	UART0->IFC = _UART_IFC_MASK;
}
#endif

#if UART1_RX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUART1_RX_IRQ[MAX_NUM_APPS * 16];
void UART1_RX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = UART1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUART1_RX_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	UART1->IFC = _UART_IFC_MASK;
}
#endif

#if UART1_TX_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUART1_TX_IRQ[MAX_NUM_APPS * 16];
void UART1_TX_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = UART1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::UART1_TX_IRQHandler[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	UART1->IFC = _UART_IFC_MASK;
}
#endif

#if LEUART0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperLEUART0_IRQ[MAX_NUM_APPS * 16];
void LEUART0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = LEUART0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperLEUART0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	LEUART0->IFC = _LEUART_IFC_MASK;
}
#endif

#if LEUART1_IRQ_ENABLE
ISR_HANDLER fsm::wrapperLEUART1_IRQ[MAX_NUM_APPS * 16];
void LEUART1_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = LEUART1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperLEUART1_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	LEUART1->IFC = _LEUART_IFC_MASK;
}
#endif

#if LETIMER0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperLETIMER0_IRQ[MAX_NUM_APPS * 16];
void LETIMER0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = LETIMER0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperLETIMER0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	LETIMER0->IFC = _LETIMER_IFC_MASK;
}
#endif

#if PCNT0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperPCNT0_IRQ[MAX_NUM_APPS * 16];
void PCNT0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = PCNT0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperPCNT0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	PCNT0->IFC = _PCNT_IFC_MASK;
}
#endif

#if PCNT1_IRQ_ENABLE
ISR_HANDLER fsm::wrapperPCNT1_IRQ[MAX_NUM_APPS * 16];
void PCNT1_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = PCNT1->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperPCNT1_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	PCNT1->IFC = _PCNT_IFC_MASK;
}
#endif

#if PCNT2_IRQ_ENABLE
ISR_HANDLER fsm::wrapperPCNT2_IRQ[MAX_NUM_APPS * 16];
void PCNT2_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = PCNT2->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperPCNT2_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	PCNT2->IFC = _PCNT_IFC_MASK;
}
#endif

#if ADC0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperADC0_IRQ[MAX_NUM_APPS * 16];
void ADC0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = ADC0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperADC0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	ADC0->IFC = _ADC_IFC_MASK;
}
#endif

#if AES_IRQ_ENABLE
ISR_HANDLER fsm::wrapperAES_IRQ[MAX_NUM_APPS * 16];

void AES_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = AES->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperAES_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	AES->IFC = _AES_IFC_MASK;
}
#endif

#if ACMP0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperACMP0_IRQ[MAX_NUM_APPS * 16];
void ACMP0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = ACMP0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperACMP0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	ACMP0->IFC = _ACMP_IFC_MASK;
}
#endif
#if BURTC_ENABLE
ISR_HANDLER fsm::wrapperBURTC_IRQ[MAX_NUM_APPS * 16];
void BURTC_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = BURTC->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperBURTC_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	BURTC->IFC = _BURTC_IFC_MASK;
}
#endif
#if CMU_IRQ_ENABLE
ISR_HANDLER fsm::wrapperCMU_IRQ[MAX_NUM_APPS * 16];
void CMU_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = CMU->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperCMU_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	CMU->IFC = _CMU_IFC_MASK;
}
#endif

#if DAC0_IRQ_ENABLE
ISR_HANDLER fsm::wrapperDAC0_IRQ[MAX_NUM_APPS * 16];
void DAC0_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = DAC0->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperDAC0_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	DAC0->IFC = _DAC_IFC_MASK;
}
#endif

#if DMA_IRQ_ENABLE
ISR_HANDLER fsm::wrapperDMA_IRQ[MAX_NUM_APPS * 16];
void DMA_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = DMA->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperDMA_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	DMA->IFC = _DMA_IFC_MASK;
}
#endif

#if EBI_IRQ_ENABLE
ISR_HANDLER fsm::wrapperEBI_IRQ[MAX_NUM_APPS * 16];
void EBI_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = EBI->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperEBI_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	EBI->IFC = _EBI_IFC_MASK;
}
#endif

#if EMU_IRQ_ENABLE
ISR_HANDLER fsm::wrapperEMU_IRQ[MAX_NUM_APPS * 16];
void EMU_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = EMU->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperEMU_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	EMU->IFC = _EMU_IFC_MASK;
}
#endif

#if LCD_IRQ_ENABLE
ISR_HANDLER fsm::wrapperLCD_IRQ[MAX_NUM_APPS * 16];
void LCD_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = LCD->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperLCD_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	LCD->IFC = _LCD_IFC_MASK;
}
#endif

#if LESENSE_IRQ_ENABLE
ISR_HANDLER fsm::wrapperLESENSE_IRQ[MAX_NUM_APPS * 16];
void LESENSE_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = LESENSE->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperLESENSE_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	LESENSE->IFC = _LESENSE_IFC_MASK;
}
#endif

#if MSC_IRQ_ENABLE
ISR_HANDLER fsm::wrapperMSC_IRQ[MAX_NUM_APPS * 16];
void MSC_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = MSC->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperMSC_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	MSC->IFC = _MSC_IFC_MASK;
}
#endif

#if RTC_IRQ_ENABLE
ISR_HANDLER fsm::wrapperRTC_IRQ[MAX_NUM_APPS * 16];
void RTC_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = RTC->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperRTC_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	RTC->IFC = _RTC_IFC_MASK;
}
#endif

#if USB_IRQ_ENABLE
ISR_HANDLER fsm::wrapperUSB_IRQ[MAX_NUM_APPS * 16];
void RTC_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = USB->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperUSB_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	USB->IFC = _USB_IFC_MASK;
}
#endif

#if VCMP_IRQ_ENABLE
ISR_HANDLER fsm::wrapperVCMP_IRQ[MAX_NUM_APPS * 16];
void VCMP_IRQHandler()
{
	for ( uint32_t count = 0, isrFlag = VCMP->IF; isrFlag; count++, isrFlag >>= 1 )
		if ( isrFlag & 0x00000001 )
			( *fsm::wrapperVCMP_IRQ[ ( ( ( fsm::currentStatusBlock->applicationId ) - 1 ) << 4 ) + count ] )();

	VCMP->IFC = _VCMP_IFC_MASK;
}
#endif

fsm::fsm()
{
	countApplications++;

	currentStatusBlock = &myStatusBlock;

	for ( uint32_t i = 0; i < MAX_NUM_APPS; i++ )
	{

#if BURTC_ENABLE
		fsm::wrapperGPIO_EVEN_IRQ[i]    = _wrapperIRQ_default;
#endif

#if USB_IRQ_ENABLE
		fsm::wrapperUSB_IRQ[i]    = _wrapperIRQ_default;
#endif

#if LESENSE_IRQ_ENABLE
		fsm::wrapperLESENSE_IRQ[i]    = _wrapperIRQ_default;
#endif

#if EBI_IRQ_ENABLE
		fsm::wrapperEBI_IRQ[i]    = _wrapperIRQ_default;
#endif

#if EMU_IRQ_ENABLE
		fsm::wrapperEMU_IRQ[i]    = _wrapperIRQ_default;
#endif

#if GPIO_EVEN_IRQ_ENABLE
		fsm::wrapperGPIO_EVEN_IRQ[i]    = _wrapperIRQ_default;
#endif
#if GPIO_ODD_IRQ_ENABLE
		fsm::wrapperGPIO_ODD_IRQ[i] = _wrapperIRQ_default;
#endif
#if DMA_IRQ_ENABLE
		fsm::wrapperDMA_IRQ[i]          = _wrapperIRQ_default;
#endif
#if TIMER0_IRQ_ENABLE
		fsm::wrapperTIMER0_IRQ[i]       = _wrapperIRQ_default;
#endif
#if USART0_RX_IRQ_ENABLE
		fsm::wrapperUSART0_RX_IRQ[i]    = _wrapperIRQ_default;
#endif
#if USART0_TX_IRQ_ENABLE
		fsm::wrapperUSART0_TX_IRQ[i]    = _wrapperIRQ_default;
#endif
#if ACMP0_IRQ_ENABLE
		fsm::wrapperACMP0_IRQ[i]        = _wrapperIRQ_default;
#endif
#if ADC0_IRQ_ENABLE
		fsm::wrapperADC0_IRQ[i]     = _wrapperIRQ_default;
#endif
#if DAC0_IRQ_ENABLE
		fsm::wrapperDAC0_IRQ[i]     = _wrapperIRQ_default;
#endif
#if I2C0_IRQ_ENABLE
		fsm::wrapperI2C0_IRQ[i]     = _wrapperIRQ_default;
#endif
#if TIMER1_IRQ_ENABLE
		fsm::wrapperTIMER1_IRQ[i]       = _wrapperIRQ_default;
#endif
#if TIMER2_IRQ_ENABLE
		fsm::wrapperTIMER2_IRQ[i]       = _wrapperIRQ_default;
#endif
#if USART1_RX_IRQ_ENABLE
		fsm::wrapperUSART1_RX_IRQ[i]    = _wrapperIRQ_default;
#endif
#if USART1_TX_IRQ_ENABLE
		fsm::wrapperUSART1_TX_IRQ[i]    = _wrapperIRQ_default;
#endif
#if USART2_RX_IRQ_ENABLE
		fsm::wrapperUSART2_RX_IRQ[i]    = _wrapperIRQ_default;
#endif
#if USART2_TX_IRQ_ENABLE
		fsm::wrapperUSART2_TX_IRQ[i]    = _wrapperIRQ_default;
#endif
#if UART0_RX_IRQ_ENABLE
		fsm::wrapperUART0_RX_IRQ[i] = _wrapperIRQ_default;
#endif
#if UART0_TX_IRQ_ENABLE
		fsm::wrapperUART0_TX_IRQ[i] = _wrapperIRQ_default;
#endif
#if LEUART0_IRQ_ENABLE
		fsm::wrapperLEUART0_IRQ[i]      = _wrapperIRQ_default;
#endif
#if LEUART1_IRQ_ENABLE
		fsm::wrapperLEUART1_IRQ[i]      = _wrapperIRQ_default;
#endif
#if LETIMER0_IRQ_ENABLE
		fsm::wrapperLETIMER0_IRQ[i] = _wrapperIRQ_default;
#endif
#if PCNT0_IRQ_ENABLE
		fsm::wrapperPCNT0_IRQ[i]        = _wrapperIRQ_default;
#endif
#if PCNT1_IRQ_ENABLE
		fsm::wrapperPCNT1_IRQ[i]        = _wrapperIRQ_default;
#endif
#if PCNT2_IRQ_ENABLE
		fsm::wrapperPCNT2_IRQ[i]        = _wrapperIRQ_default;
#endif
#if RTC_IRQ_ENABLE
		fsm::wrapperRTC_IRQ[i]          = _wrapperIRQ_default;
#endif
#if CMU_IRQ_ENABLE
		fsm::wrapperCMU_IRQ[i]          = _wrapperIRQ_default;
#endif
#if VCMP_IRQ_ENABLE
		fsm::wrapperVCMP_IRQ[i]     = _wrapperIRQ_default;
#endif
#if LCD_IRQ_ENABLE
		fsm::wrapperLCD_IRQ[i]          = _wrapperIRQ_default;
#endif
#if MSC_IRQ_ENABLE
		fsm::wrapperMSC_IRQ[i]          = _wrapperIRQ_default;
#endif
#if AES_IRQ_ENABLE
		fsm::wrapperAES_IRQ[i]          = _wrapperIRQ_default;
#endif
	}
}

void fsm::registerEventHandler( ISR_HANDLER function, uint8_t interruptSource, uint8_t interruptId, ANCHOR_ISR anchorISR )
{
	callingStatusBlock = currentStatusBlock;
	currentStatusBlock = &myStatusBlock;

// 	// Copy the Pointer to the Interrupt Handler
	if ( function )
	{
		switch ( interruptSource )
		{

#if BURTC_ENABLE
		case BURTC_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperBURTC_IRQ[ i +  interruptId ] = function;
			else 
				wrapperBURTC_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			BURTC->IFC = ~0;
			NVIC_EnableIRQ( BURTC_IRQn );
			break;
#endif
#if USB_IRQ_ENABLE
		case USB_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSB_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSB_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			USB->IFC = ~0;
			NVIC_EnableIRQ( USB_IRQn );
			break;
#endif
#if LESENSE_IRQ_ENABLE
		case LESENSE_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSB_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSB_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			LESENSE->IFC = ~0;
			NVIC_EnableIRQ( LESENSE_IRQn );
			break;
#endif
#if EBI_IRQ_ENABLE
		case EBI_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperEBI_IRQ[ i +  interruptId ] = function;
			else 
				wrapperEBI_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			EBI->IFC = ~0;
			NVIC_EnableIRQ( EBI_IRQn );
			break;
#endif
#if EMU_IRQ_ENABLE
		case EMU_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperEMU_IRQ[ i +  interruptId ] = function;
			else 
				wrapperEMU_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			EMU->IFC = ~0;
			NVIC_EnableIRQ( EMU_IRQn );
			break;
#endif
#if DMA_IRQ_ENABLE
		case DMA_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperDMA_IRQ[ i +  interruptId ] = function;
			else 
				wrapperDMA_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			DMA->IFC = ~0;
			NVIC_EnableIRQ( DMA_IRQn );
			break;
#endif
#if GPIO_EVEN_IRQ_ENABLE
		case GPIO_EVEN_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperGPIO_EVEN_IRQ[ i +  interruptId ] = function;
			else 
				wrapperGPIO_EVEN_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			GPIO->IFC = ~0;
			NVIC_EnableIRQ( GPIO_EVEN_IRQn );
			break;
#endif
#if TIMER0_IRQ_ENABLE
		case TIMER0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperTIMER0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperTIMER0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			TIMER0->IFC = ~0;
			NVIC_EnableIRQ( TIMER0_IRQn );
			break;
#endif
#if USART0_RX_IRQ_ENABLE
		case USART0_RX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSART0_RX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSART0_RX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			USART0->IFC = ~0;
			NVIC_EnableIRQ( USART0_RX_IRQn );
			break;
#endif
#if USART0_TX_IRQ_ENABLE
		case USART0_TX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSART0_TX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSART0_TX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			USART0->IFC = ~0;
			NVIC_EnableIRQ( USART0_TX_IRQn );
			break;
#endif
#if ACMP0_IRQ_ENABLE
		case ACMP0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperACMP0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperACMP0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			ACMP0->IFC = ~0;
			NVIC_EnableIRQ( ACMP0_IRQn );
			break;
#endif
#if ADC0_IRQ_ENABLE
		case ADC0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperADC0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperADC0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			ADC0->IFC = ~0;
			NVIC_EnableIRQ( ADC0_IRQn );
			break;
#endif
#if DAC0_IRQ_ENABLE
		case DAC0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperDAC0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperDAC0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			DAC0->IFC = ~0;
			NVIC_EnableIRQ( DAC0_IRQn );
			break;
#endif
#if I2C0_IRQ_ENABLE
		case I2C0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperI2C0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperI2C0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			I2C0->IFC = ~0;
			NVIC_EnableIRQ( I2C0_IRQn );
			break;
#endif
#if GPIO_ODD_IRQ_ENABLE
		case GPIO_ODD_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperGPIO_ODD_IRQ[ i +  interruptId ] = function;
			else 
				wrapperGPIO_ODD_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

				GPIO->IFC = ~0;
				NVIC_EnableIRQ( GPIO_ODD_IRQn );
			break;
#endif
#if TIMER1_IRQ_ENABLE
		case TIMER1_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperTIMER1_IRQ[ i +  interruptId ] = function;
			else 
				wrapperTIMER1_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			TIMER1->IFC = ~0;
			NVIC_EnableIRQ( TIMER1_IRQn );
			break;
#endif
#if TIMER2_IRQ_ENABLE
		case TIMER2_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperTIMER2_IRQ[ i +  interruptId ] = function;
			else 
				wrapperTIMER2_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			TIMER2->IFC = ~0;
			NVIC_EnableIRQ( TIMER2_IRQn );
			break;
#endif
#if USART1_RX_IRQ_ENABLE
		case USART1_RX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSART1_RX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSART1_RX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			USART1->IFC = ~0;
			NVIC_EnableIRQ( USART1_RX_IRQn );
			break;
#endif
#if USART1_TX_IRQ_ENABLE
		case USART1_TX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSART1_TX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSART1_TX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			USART1->IFC = ~0;
			NVIC_EnableIRQ( USART1_TX_IRQn );
			break;
#endif
#if USART2_RX_IRQ_ENABLE
		case USART2_RX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSART2_RX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSART2_RX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			GPIO->IFC = ~0;
			NVIC_EnableIRQ( USART2_RX_IRQn );
			break;
#endif
#if USART2_TX_IRQ_ENABLE
		case USART2_TX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUSART2_TX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUSART2_TX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			GPIO->IFC = ~0;
			NVIC_EnableIRQ( USART2_TX_IRQn );
			break;
#endif
#if UART0_RX_IRQ_ENABLE
		case UART0_RX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUART0_RX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUART0_RX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			UART0->IFC = ~0;
			NVIC_EnableIRQ( UART0_RX_IRQn );
			break;
#endif
#if UART0_TX_IRQ_ENABLE
		case UART0_TX_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperUART0_TX_IRQ[ i +  interruptId ] = function;
			else 
				wrapperUART0_TX_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			GPIO->IFC = ~0;
			NVIC_EnableIRQ( UART0_TX_IRQn );
			break;
#endif
#if LEUART0_IRQ_ENABLE
		case LEUART0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperLEUART0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperLEUART0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			GPIO->IFC = ~0;
			NVIC_EnableIRQ( LEUART0_IRQn );
			break;
#endif
#if LEUART1_IRQ_ENABLE
		case LEUART1_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperLEUART1_IRQ[ i +  interruptId ] = function;
			else 
				wrapperLEUART1_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			LEUART1->IFC = ~0;
			NVIC_EnableIRQ( LEUART1_IRQn );
			break;
#endif
#if LETIMER0_IRQ_ENABLE
		case LETIMER0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperLETIMER0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperLETIMER0_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			GPIO->IFC = ~0;
			NVIC_EnableIRQ( LETIMER0_IRQn );
			break;
#endif
#if PCNT0_IRQ_ENABLE
		case PCNT0_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperPCNT0_IRQ[ i +  interruptId ] = function;
			else 
				wrapperGPIO_ODD_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			PCNT0->IFC = ~0;
			NVIC_EnableIRQ( PCNT0_IRQn );
			break;
#endif
#if PCNT1_IRQ_ENABLE
		case PCNT1_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperPCNT1_IRQ[ i +  interruptId ] = function;
			else 
				wrapperGPIO_ODD_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			PCNT1->IFC = ~0;
			NVIC_EnableIRQ( PCNT1_IRQn );
			break;
#endif
#if PCNT2_IRQ_ENABLE
		case PCNT2_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperPCNT2_IRQ[ i +  interruptId ] = function;
			else 
				wrapperPCNT2_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			PCNT2->IFC = ~0;
			NVIC_EnableIRQ( PCNT2_IRQn );
			break;
#endif
#if RTC_IRQ_ENABLE
		case RTC_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperRTC_IRQ[ i +  interruptId ] = function;
			else 
				wrapperRTC_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			RTC->IFC = ~0;
			NVIC_EnableIRQ( RTC_IRQn );
			break;
#endif
#if CMU_IRQ_ENABLE
		case CMU_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperCMU_IRQ[ i +  interruptId ] = function;
			else 
				wrapperCMU_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			CMU->IFC = ~0;
			NVIC_EnableIRQ( CMU_IRQn );
			break;
#endif
#if VCMP_IRQ_ENABLE
		case VCMP_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperGPIO_ODD_IRQ[ i +  interruptId ] = function;
			else 
				wrapperGPIO_ODD_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			VCMP->IFC = ~0;
			NVIC_EnableIRQ( VCMP_IRQn );
			break;
#endif
#if MSC_IRQ_ENABLE
		case MSC_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperGPIO_ODD_IRQ[ i +  interruptId ] = function;
			else 
				wrapperGPIO_ODD_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			MSC->IFC = ~0;
			NVIC_EnableIRQ( MSC_IRQn );
			break;
#endif
#if AES_IRQ_ENABLE
		case AES_IRQn:
			if ( anchorISR )
				for ( uint8_t i = 0; i < (MAX_NUM_APPS << 4); i+=16 ) wrapperAES_IRQ[ i +  interruptId ] = function;
			else 
				wrapperAES_IRQ[ ( ( currentStatusBlock->applicationId - 1 ) << 4 ) +  interruptId ] = function;

			AES->IFC = ~0;
			NVIC_EnableIRQ( AES_IRQn );
			break;
#endif
		default:
			codeError();
		}
	}

	currentStatusBlock = callingStatusBlock;
}


void fsm::execute()
{
	callingStatusBlock = currentStatusBlock;
	currentStatusBlock = &myStatusBlock;

	currentStatusBlock->nextState = currentStatusBlock->initialState;

#if CORE_MSG_ENABLE
	cout << endl << "Start Application => Enter Initial State" << endl;
#endif

	// Run the fsm until the execution is stopped by the Application-Code.
	// The State-Function is called within the WHILE-STATEMENT !!!!
	while ( ( currentStatusBlock->mode != on ) || ( *( stateDefinition )[currentStatusBlock->nextState] )() )
	{
		// Which sleep-mode has to be entered?
		switch ( currentStatusBlock->mode )
		{
		case sleep:
			cout.flush(); // Empty the UART Buffer before entering Sleep
			EMU_EnterEM1();
#if ISR_MSG_ENABLE
			cout << "ISR_MSG: " << msg.flush() << endl;
#endif
			break;
		case stop:
			cout.flush(); // Empty the UART Buffer before entering Sleep
			EMU_EnterEM2( true );
#if ISR_MSG_ENABLE
			cout << "ISR_MSG: " << msg.flush() << endl;
#endif
			break;
		case deepStop:
			cout.flush(); // Empty the UART Buffer before entering Sleep
			EMU_EnterEM3( true );
#if ISR_MSG_ENABLE
			cout << "ISR_MSG: " << msg.flush() << endl;
#endif
			break;
		case off:
			cout.flush(); // Empty the UART Buffer before entering Sleep
			EMU_EnterEM4();
#if ISR_MSG_ENABLE
			cout << "ISR_MSG: " << msg.flush() << endl;
#endif
			break;
		case on:
			break;
		default:
			codeError();
			break;
		}

#if CORE_MSG_ENABLE
		cout  << endl << "== Enter State: " << ( uint8_t ) currentStatusBlock->nextState << " ==" << endl;
#endif
	}

	currentStatusBlock->mode = on;

	currentStatusBlock = callingStatusBlock;
}

void fsm::_wrapperIRQ_default()
{
#if CORE_MSG_ENABLE
	cout << "Warning Default IRQ Handler called" << endl;
#endif
}

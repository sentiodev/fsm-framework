/*
 * driver_interface.cpp
 */

#include "driver_interface.h"

time_delay       driver_interface::delay;


#if PRINT_SERIAL_ENABLE
print<print_serial>    driver_interface::cout;
#endif

#if ISR_MSG_ENABLE
#if !(PRINT_SERIAL_ENABLE)
#error ISR_MSG_ENABLE requires to use "include sys/drv/print/print_serial in application makefile"
#endif
print<print_stream>    driver_interface::msg;
#endif

#if PRINT_STREAM_ENABLE
print<print_stream> driver_interface::stream;
#endif

#if PRINT_STREAM_SD_ENABLE
print<print_stream_sd> driver_interface::sd;
#endif


#if SENTIO_EM_ENABLE
sentio_em     driver_interface::sentio;
#endif

#if SENTIO_GW_ENABLE
sentio_gw     driver_interface::sentio;
#endif

#if SENTIO_CAM_ENABLE
sentio_cam     driver_interface::sentio;
#endif


#if SENSOR_SHT15_ENABLE
sensor_sht15 driver_interface::sht;
#endif

#if SENSOR_VC0706_ENABLE
sensor_vc0706 driver_interface::vc0706;
#endif


#if DEBUG_BOARD_ENABLE
debugboard    driver_interface::dbg;
#endif

#if SMARTLIGHT_ENABLE
smartlight_board  driver_interface::smartlight;
#endif

#if SOLAR_HARVESTER_ENABLE
solarharvester   driver_interface::solarHarvester;
#endif

#if OIL_IN_WATER_BOARD_ENABLE
oil_in_water_board     driver_interface::water;
#endif

#if EMULATOR_BOARD
#endif


void driver_interface::codeError()
{
	while(1)
	{
		sentio.tglLed(RED);
		sentio.tglLed(GREEN);
		sentio.tglLed(ORANGE);
		
		for(volatile uint32_t i = 0; i <200000; i++);
	}
}